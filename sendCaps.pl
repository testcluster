#!/usr/bin/perl

use strict;
use warnings;

# Detect the capabilities of this machine and send it back

sub env($) {
    my $var = shift;
    my $val = $ENV{$var};
    if (!defined $val) {
	return "";
    }
    return $val;
}

my $machine = shift;

if (!defined $machine) {
    die("No machine name specified");
}

my $cap;
my $os=env('OS');
my $ostype=env('OSTYPE');
my $hosttype=`uname -m`;
chomp $hosttype;

# No safe way to detect windows bash that I can find. Rely on an
# env var for now.
my $winbash=env('WINDOWSBASH');

if ($os eq "Windows_NT" or $winbash eq "1") {
    $cap = "win32";
    if ($hosttype eq "x86_64") {
	$cap .= ",win64";
    }
} elsif ($ostype =~ m/darwin/) {
    $cap="macos";
} else {
    $cap="linux";

    if (open(F,">test32.c")) {
	print F "int main(int argc, const char *argv[]){return 32;}\n";
	close(F);
        `gcc test32.c -o test32 -m32 > /dev/null 2>&1`;
        `./test32 > /dev/null 2>&1`;
	if (($?>>8) == 32) {
            $cap .= ",w32";
	}
    }
    system("which valgrind");
    if (($?>>8) == 0) {
	$cap .= ",valgrind"
    }
}

my $gcc=`gcc --version 2> /dev/null | head -1`;
if ($gcc =~ m/^gcc.*\s(\d+\.\d+\.\d+)/) {
    $cap.=",gcc=$1";
}

system("which gcov");
if (($?>>8) == 0) {
    $cap.=",coverage";
}

`perl ./heartbeat.pl $machine cap $cap`;
