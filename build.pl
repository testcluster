#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;
use POSIX ":sys_wait_h";

my %allowedProducts = (
    'gs'       => 1,
    'pcl'      => 1,
    'xps'      => 1,
    'ls'       => 1,
    'gpdf'     => 1,
    'gpdl'     => 1,
    'mupdf'    => 1,
    'mujstest' => 1,
    'gsview'   => 1,
    'mupdfmini'=> 1
);

my $lowres  = 0;
my $highres = 0;
my %products;
my $bmpcmp        = 0;
my $cull          = 0;
my $bmpcmphead    = 0;
my $local         = 0;
my $extended      = 0;
my $smoke         = 0;
my $performance   = 0;
my $windows       = 0;
my $valgrind      = 0;
my $coverage      = 0;
my $filename      = "";
my $bmpcmpOptions = "";
my @filters;
my @extras;
my $singlePagePDF = 0;
my $mupdf         = 0;
my $gsview        = 0;
my $mupdfmini     = 0;
my $bmpcmpRef     = "__head__";
my $autoName;
my $ref           = 0;

my $t;

my %testfiles;
my %customTests;
my %customTestFiles;

print STDERR "build.pl: processing args\n";
print "#build.pl args:\n";
while ( $t = shift ) {
    print "#arg='$t'\n";
    print STDERR "arg='" . $t . "'\n";
    if ( $t eq "lowres" ) {
        $lowres = 1;
    } elsif ( $t eq "highres" ) {
        $highres = 1;
    } elsif ( $t eq "singlePagePDF" ) {
        $singlePagePDF = 1;
    } elsif ( $t eq "bmpcmp" ) {
        $bmpcmp   = 1;
        $filename = shift;
        if ($filename =~ m/^auto=(\S+)/) {
            $autoName=$_;
            $filename = "auto/$1/email.txt";
        } else {
            $filename = "users/$filename/email.txt";
        }
        print "#filename='$filename'\n";

        my $opt;
        do {
            $opt = shift;
            if ($opt) {
                if ($opt eq "ref") {
                    $bmpcmpRef = "ref";
                } elsif ($opt eq "extended") {
                    $extended = 1;
                } elsif ($opt eq "smoke") {
                    $smoke = 1;
                } elsif ($opt eq "cull") {
                    $cull = 1;
                } else {
                    $bmpcmpOptions = $bmpcmpOptions." ".$opt;
                }
            }
        } while ($opt);

        # We need to separate the options into 'extras' and options.
        @extras = ( $bmpcmpOptions =~ m/extras=(\S*)/g );

        # Remove the extras from the bmpcmpOptions
        foreach (@extras) {
            $bmpcmpOptions =~ s/extras=$_//;
        }
        print "#bmpcmpOptions=$bmpcmpOptions\n";
    } elsif ( $t eq "bmpcmphead" ) {
        $bmpcmp     = 1;
        $bmpcmphead = 1;
        $filename   = "email.txt";
        print "#filename='$filename'\n";

        my $opt;
        do {
            $opt = shift;
            if ($opt) {
                if ($opt eq "ref") {
                    $bmpcmpRef = "ref";
                } elsif ($opt eq "extended") {
                    $extended = 1;
                } elsif ($opt eq "smoke") {
                    $smoke = 1;
                } elsif ($opt eq "cull") {
                    $cull = 1;
                } else {
                    $bmpcmpOptions = $bmpcmpOptions." ".$opt;
                }
            }
        } while ($opt);

        # We need to separate the options into 'extras' and options.
        @extras = ( $bmpcmpOptions =~ m/extras=(\S*)/g );

        # Remove the extras from the bmpcmpOptions
        foreach (@extras) {
            $bmpcmpOptions =~ s/extras=$_//;
        }
        print "#bmpcmpOptions=$bmpcmpOptions\n";
    } elsif ( $t eq "extended" ) {
        $extended = 1;
    } elsif ( $t eq "smoke" ) {
        $smoke = 1;
    } elsif ( $t eq "local" ) {
        $local = 1;
        $extended = 1;
    } elsif ( $t eq "performance" ) {
        $performance = 1;
        $local       = 1;
    } elsif ( $t eq "windows" ) {
        $windows = 1;
    } elsif ( $t eq "valgrind" ) {
        $valgrind = 1;
    } elsif ( $t eq "coverage" ) {
        $coverage = 1;
    } elsif ( $t =~ m/filter=(\S*)/ ) {
        push @filters, $1;
    } elsif ( $t =~ m/extras=(\S*)/ ) {
        push @extras, $1;
    } else {
        $products{$t} = 1;
        if ( $t eq "mupdf" ) {
            $mupdf = 1;
        }
        if ( $t eq "gsview" ) {
            $gsview = 1;
        }
        if ( $t eq "mupdfmini" ) {
            $mupdfmini = 1;
        }
        die "usage: build.pl [gs] [pcl] [xps] [ls] [gpdf] [gpdl] [mupdf] [mujstest] [gsview] [mupdfmini] - what is $t ?"
          if ( !exists $allowedProducts{$t} );
    }
}

if ( scalar keys %products == 0 ) {
    $products{"gs"}  = 1;
    $products{"pcl"} = 1;
    $products{"xps"} = 1;
}

if (exists $products{"gs"}) {
    $products{"gs_smoke"} = 1;
}

my $bmpcmpStart = 0;
my $bmpcmpCount = 1000;
if ($extended) {
    $bmpcmpCount=100000;
}

if ( $bmpcmpOptions =~ m/-c\s*(\d+)/ ) {
    $bmpcmpCount = $1;
    $bmpcmpOptions =~ s/-c\s*\d+//;
}

if ( $bmpcmpOptions =~ m/-s\s*(\d+)/ ) {
    $bmpcmpStart = $1;
    $bmpcmpOptions =~ s/-s\s*\d+//;
}

if ( $bmpcmpOptions =~ m/filter=(.*)/ ) {
    push @filters, $1;
    $bmpcmpOptions = "";
}

#print STDERR Dumper(\@filters);

my $extra_args = "";
foreach my $tmp (@extras) {
    print "#extras: $tmp\n";
    $extra_args .= " " . $tmp;
}
print "#extras=$extra_args\n";

#print "$extra_args\n";  exit;

my $additionalOptions                   = "";
my $additionalOptions_gs                = "";
my $additionalOptions_pcl               = "";
my $additionalOptions_pdfwrite_step1    = "";
my $additionalOptions_pdfwrite_step2    = "-dPDFSTOPONERROR -dPDFSTOPONWARNING ";
#my $additionalOptions_pdfwrite_step2    = "";
my $additionalOptions_gs_pdfwrite_step1 = "";
my $additionalOptions_gs_pdfwrite_step2 = "";
if ( open( F, "<weekly.cfg" ) ) {
    while (<F>) {
        chomp;
        my @a = split ' ', $_, 2;
        if ( $a[0] eq "runoption" ) {
            $additionalOptions .= ' ' if ( length($additionalOptions) );
            $additionalOptions .= $a[1];
        }
        if ( $a[0] eq "gs_runoption" ) {
            $additionalOptions_gs .= ' ' if ( length($additionalOptions_gs) );
            $additionalOptions_gs .= $a[1];
        }
        if ( $a[0] eq "runoption_pdfwrite_step1" ) {
            $additionalOptions_pdfwrite_step1 .= ' '
              if ( length($additionalOptions_pdfwrite_step1) );
            $additionalOptions_pdfwrite_step1 .= $a[1];
        }
        if ( $a[0] eq "runoption_pdfwrite_step2" ) {
            $additionalOptions_pdfwrite_step2 .= ' '
              if ( length($additionalOptions_pdfwrite_step2) );
            $additionalOptions_pdfwrite_step2 .= $a[1];
        }
        if ( $a[0] eq "gs_runoption_pdfwrite_step1" ) {
            $additionalOptions_gs_pdfwrite_step1 .= ' '
              if ( length($additionalOptions_gs_pdfwrite_step1) );
            $additionalOptions_gs_pdfwrite_step1 .= $a[1];
        }
        if ( $a[0] eq "gs_runoption_pdfwrite_step2" ) {
            $additionalOptions_gs_pdfwrite_step2 .= ' '
              if ( length($additionalOptions_gs_pdfwrite_step2) );
            $additionalOptions_gs_pdfwrite_step2 .= $a[1];
        }
    }
    close(F);
}

my $verbose = 0;

local $| = 1;
my %md5sum;
my %skip;

#print Dumper(\%skip);  exit;

my $baseDirectory = './';

my $gitURLPrivate = '/home/git-private/tests_private.git/';
my $gitURLPublic  = '/home/git/tests.git/';

if ($local) {
    $gitURLPrivate = 'regression@cluster.ghostscript.com:/home/git-private/tests_private.git/';
    $gitURLPublic  = 'regression@cluster.ghostscript.com:/home/git/tests.git/';
}

my $preCommand = "";

my $temp = "__temp__";

#$temp="/tmp/space/temp";
#$temp="/dev/shm/temp";
#$temp="/media/sdd/temp";
#$temp = "./valgrind/out" if ($valgrind);

my $raster         = "$temp/raster";
my $bmpcmpDir      = "$temp/bmpcmp";
my $baselineRaster = "./baselineraster";

my $gsBin = "__bin__/bin/gs";
my $pclBin      = "__bin__/bin/pcl6";
my $xpsBin      = "__bin__/bin/gxps";
my $lsBin       = "__bin__/bin/pspcl6";
my $gpdfBin     = "__bin__/bin/gpdf";
my $gpdlBin     = "__bin__/bin/gpdl";
my $mupdfBin    = "__bin__/bin/pdfdraw";
my $mujstestBin = "__bin__/bin/mujstest";
my $timeBin     = "__bin__/bin/time";
if ($extended || $smoke) {
    $gsBin .= " -I__bin__/lib";
    $gpdlBin .= " -I__bin__/lib";
}

my $flock = $baseDirectory . "gs/bin/flock rsync.lock";

#$flock="";

# mupdf uses the same test files as gs but only filenames ending in pdf (or PDF) and xps

my $publicRepository  = $gitURLPublic;
my $privateRepository = $gitURLPrivate;

my %testSource = (

    # $publicRepository."" => 'gs',
    $publicRepository . "pdf"        => 'gs',
    $publicRepository . "ps"         => 'gs',
    $publicRepository . "eps"        => 'gs',
    $publicRepository . "Ghent_V3.0" => 'gs',
    $publicRepository . "Ghent_V5.0" => 'gs',

    # $privateRepository."" => 'gs',
    $privateRepository . "comparefiles"        => 'gs',
    $privateRepository . "ps/ps3fts"           => 'gs',
    $privateRepository . "ps/ps3cet"           => 'gs',
    $privateRepository . "pdf/PDFIA1.7_SUBSET" => 'gs',
    $privateRepository . "pdf/PDF_1.7_FTS"     => 'gs',
    $privateRepository . "pdf/PDF_2.0_FTS"     => 'gs',
    $privateRepository . "pdf/sumatra"         => 'gs',
    $privateRepository . "pdf/uploads"         => 'gs',
    $privateRepository . "icc"                 => 'gs',
    $privateRepository . "pdf/smoketests"      => 'gs_smoke',
    $privateRepository . "pdf/tifftests"       => 'gs',

    $publicRepository  . "pcl"                 => 'pcl',
    $privateRepository . "customer_tests"      => 'pcl',
    $privateRepository . "pcl/pcl5cfts"        => 'pcl',
    $privateRepository . "pcl/pcl5cats/Subset" => 'pcl',
    $privateRepository . "pcl/pcl5efts"        => 'pcl',
    $privateRepository . "pcl/pcl5ccet"        => 'pcl',
    $privateRepository . "pcl/uploads"         => 'pcl',
    $privateRepository . "xl/pxlfts3.0"        => 'pcl',
    $privateRepository . "xl/pcl6cet"          => 'pcl',
    $privateRepository . "xl/pcl6cet3.0"       => 'pcl',
    $privateRepository . "xl/pxlfts"           => 'pcl',
    $privateRepository . "xl/pxlfts2.0"        => 'pcl',
    $privateRepository . "xl/xlats"            => 'pcl',
    $privateRepository . "xl/uploads"          => 'pcl',

    # $publicRepository."xps" => 'xps',
    $privateRepository . "xps"           => 'xps',
    $privateRepository . "xps/xpsfts-a4" => 'xps',
    $privateRepository . "xps/sumatra"   => 'xps',
    $privateRepository . "xps/uploads"   => 'xps',

    # $privateRepository."/xps/ms/ConformanceViolations/MarkupCompatibility" => 'xps',
    # $privateRepository."/xps/ms/ConformanceViolations/OpenPackagingConventions" => 'xps',
    # $privateRepository."/xps/ms/QualityLogicMinBar" => 'xps',
    # $privateRepository."/xps/ms/HandCrafted" => 'xps',
    # $privateRepository."/xps/ms/MXDW" => 'xps',
    # $privateRepository."/xps/ms/Showcase" => 'xps',
    # $privateRepository."/xps/ms/Office2007" => 'xps',
    # $privateRepository."/xps/ms/WPF" => 'xps',

    # $publicRepository."/language_switch" => 'ls',
    # $gitURLPrivate."/language_switch" => 'ls',

    $privateRepository . "bmp"                               => 'bmp',
    $privateRepository . "bmp/bmptestsuite-0.9/valid"        => 'bmp',
    $privateRepository . "bmp/bmptestsuite-0.9/questionable" => 'bmp',
    $privateRepository . "bmp/bmptestsuite-0.9/corrupt"      => 'bmp',
    $privateRepository . "bmp/bmpsuite-2.4/g"                => 'bmp',
    $privateRepository . "bmp/bmpsuite-2.4/b"                => 'bmp',
    $privateRepository . "bmp/bmpsuite-2.4/q"                => 'bmp',
    $privateRepository . "bmp/bmpsuite-2.4-sebras"           => 'bmp',

    $privateRepository . "epub"  => 'epub',
    $privateRepository . "html"  => 'epub',
    $privateRepository . "xhtml" => 'epub',
    $privateRepository . "tiff"  => 'tif',

    $privateRepository . "j2k"                                 => 'j2k',
    $privateRepository . "j2k/openjpeg"                        => 'j2k',
    $privateRepository . "j2k/openjpeg/input/nonregression"    => 'j2k',
    $privateRepository . "j2k/openjpeg/input/conformance"      => 'j2k',
    $privateRepository . "j2k/openjpeg/baseline/nonregression" => 'j2k',

    $privateRepository . "svg"         => 'svg',
    $privateRepository . "svg/uploads" => 'svg',
    $privateRepository . "svg/w3-svgs" => 'svg',

    $gitURLPrivate . "pdf/forms/v1.2" => 'mujstest',
    $gitURLPrivate . "pdf/forms/v1.3" => 'mujstest',
    $gitURLPrivate . "pdf/forms/v1.4" => 'mujstest',
    $gitURLPrivate . "pdf/forms/v1.5" => 'mujstest',
    $gitURLPrivate . "pdf/forms/v1.6" => 'mujstest',
    $gitURLPrivate . "pdf/forms/v1.7" => 'mujstest'
);

# Override entire set of tests for valgrind tests
if ( 0 && $valgrind ) {
    if ($mupdf) {
        %testSource = ( $gitURLPrivate . "fuzzing/mupdf2" => 'gs' );
    } else {
        %testSource = (
            $gitURLPrivate . "/fuzzing/mupdf2" => 'gs'

              #     $privateRepository."comparefiles" => 'gs',
        );
    }
}

# Override entire set of tests for smoke tests
if ($smoke) {
    %testSource = (
        $publicRepository . "Ghent_V3.0"           => 'gs',
        $privateRepository . "pcl/pcl5efts"        => 'pcl',
        $privateRepository . "xl/pxlfts2.0"        => 'pcl',
        $privateRepository . "xps/ms/Office2007"   => 'xps'
    );
}

if ($coverage) {
    %testSource = (
        # $publicRepository."" => 'gs',
        $publicRepository . "pdf"        => 'gs',
        $publicRepository . "ps"         => 'gs',
        $publicRepository . "eps"        => 'gs',
        #$publicRepository . "Ghent_V3.0" => 'gs',
        $publicRepository . "Ghent_V5.0" => 'gs',

        # $privateRepository."" => 'gs',
        $privateRepository . "comparefiles"        => 'gs',
        $privateRepository . "ps/ps3fts"           => 'gs',
        $privateRepository . "ps/ps3cet"           => 'gs',
        #$privateRepository . "pdf/PDFIA1.7_SUBSET" => 'gs',
        #$privateRepository . "pdf/PDF_1.7_FTS"     => 'gs',
        $privateRepository . "pdf/PDF_2.0_FTS"     => 'gs',
        $privateRepository . "pdf/sumatra"         => 'gs',
        $privateRepository . "pdf/uploads"         => 'gs',
        $privateRepository . "icc"                 => 'gs',
        #$privateRepository . "pdf/smoketests"      => 'gs_smoke',
        $privateRepository . "pdf/tifftests"       => 'gs',
        $privateRepository . "pcl/pcl5efts"        => 'pcl',
        $privateRepository . "pcl/pcl5cfts"        => 'pcl',
        $privateRepository . "xl/pxlfts2.0"        => 'pcl',
        $privateRepository . "xps/ms/Office2007"   => 'xps',
        $privateRepository . "bmp/bmptestsuite-0.9/valid"        => 'bmp',
        $privateRepository . "epub"  => 'epub',
        $privateRepository . "html"  => 'epub',
        $privateRepository . "xhtml" => 'epub',
        $privateRepository . "tiff"  => 'tif',
        $privateRepository . "svg/w3-svgs" => 'svg'
    );
}

# Override entire set of tests for gsview
if ($gsview || $mupdfmini) {
    %testSource = ();
}

my $cmd;
my $s;
my $previousRev;
my $newRev = 99999;

my %tests = (
    'gs' => [
        "pbmraw.72.0",
        "pbmraw.300.0",
        "pbmraw.300.1",
        "pgmraw.72.0",
        "pgmraw.300.0",
        "pgmraw.300.1",
        "pkmraw.72.0",
        "pkmraw.300.0",
        "pkmraw.300.1",
        "ppmraw.72.0",
        "ppmraw.300.0",
        "ppmraw.300.1",
        "pam.72.0",
        "psdcmyk.72.0",
        "psdcmyk.300.1",
        #"psdcmyk16.72.0",
        #"psdcmyk16.300.1",

        # "psdcmykog.72.0",  #local
        # "psdcmykog.300.1", #local
        "cups.300.1",

        # "tiffscaled.600.1", #local
        # "plank.72.0",
        # "plank.300.1",
        "bitrgbtags.300.1",

        "pdf.ppmraw.72.0",
        "pdf.ppmraw.300.0",
        "pdf.pkmraw.300.0",
        "ps.ppmraw.72.0",
        "ps.ppmraw.300.0",
        "ps.pkmraw.300.0",

        # "pxlmono.ppmraw.300.0", #local
        # "pxlmono.ppmraw.300.1", #local
        # "pxlcolor.ppmraw.300.0", #local
        # "pxlcolor.ppmraw.300.1", #local
        "xps.ppmraw.72.0",
        # code below limits this and the other xpswrite output option (immediately below)  to files in the PDF_1.7_FTS and PDFIA1.7_SUBSET directories
        "xps.ppmraw.300.0",

        # "xps.ppmraw.300.1"
    ],

    'pcl' => [
        "pbmraw.75.0",

        # "pbmraw.600.0",
        "pbmraw.600.1",

        # "pgmraw.75.0", #local
        # "pgmraw.600.0",
        # "pgmraw.600.1",
        # "wtsimdi.75.0",
        # "wtsimdi.600.0",
        # "wtsimdi.600.1",
        "ppmraw.75.0",

        # "ppmraw.600.0",
        "ppmraw.600.1",

        # "bitrgb.75.0",
        # "bitrgb.600.0",
        # "bitrgb.600.1",
        # "tiffscaled.600.1", #local
        # "psdcmyk.75.0",
        # "psdcmyk.600.0",
        # "psdcmyk.600.1",
        "bitrgbtags.600.1",

        "pdf.ppmraw.75.0",
        "pdf.ppmraw.600.0",

        # "ps.ppmraw.75.0",
        # "ps.ppmraw.600.0",
        # "pdf.pkmraw.600.0"
    ],

    'xps' => [
        "pbmraw.72.0",
        # "pbmraw.300.0",
        # "pbmraw.300.1",
        # "pgmraw.72.0",
        # "pgmraw.300.0",
        # "pgmraw.300.1",
        # "wtsimdi.72.0",
        # "wtsimdi.300.0",
        # "wtsimdi.300.1",
        "ppmraw.72.0",
        # "ppmraw.300.0",
        # "ppmraw.300.1",
        "bitrgb.72.0",
        # "bitrgb.300.0",
        # "bitrgb.300.1",
        # "psdcmyk.72.0",
        # "psdcmyk.300.0",
        # "psdcmyk.300.1",
        "bitrgbtags.300.1",

        "pdf.ppmraw.72.0",

        # "ps.ppmraw.72.0",  #
        # "pdf.ppmraw.300.0",
        # "pdf.pkmraw.300.0"
    ],

    #'ls' => [
    #    "pbmraw.72.0", #local
    #    "pbmraw.300.0",
    #    "pbmraw.300.1",
    #    "pgmraw.72.0",
    #    "pgmraw.300.0",
    #    "pgmraw.300.1",
    #    "wtsimdi.72.0",
    #    "wtsimdi.300.0",
    #    "wtsimdi.300.1",
    #    "ppmraw.72.0",
    #    "ppmraw.300.0",
    #    "ppmraw.300.1",
    #    "bitrgb.72.0",
    #    "bitrgb.300.0",
    #    "bitrgb.300.1",
    #    "psdcmyk.72.0",
    #    "psdcmyk.300.0",
    #    "psdcmyk.300.1",
    #    "pdf.ppmraw.72.0",
    #    "ps.ppmraw.72.0",
    #    "pdf.ppmraw.300.0",
    #    "pdf.pkmraw.300.0"
    #],

    'gpdf' => [
        "pbmraw.72.0",
        "pbmraw.300.0",
        "pbmraw.300.1",
        "pgmraw.72.0",
        "pgmraw.300.0",
        "pgmraw.300.1",
        "pkmraw.72.0",
        "pkmraw.300.0",
        "pkmraw.300.1",
        "ppmraw.72.0",
        "ppmraw.300.0",
        "ppmraw.300.1",
        "pam.72.0",
        "psdcmyk.72.0",
        "psdcmyk.300.1",
        #"psdcmyk16.72.0",
        #"psdcmyk16.300.1",

        # "psdcmykog.72.0",  #local
        # "psdcmykog.300.1", #local
        "cups.300.1",

        # "tiffscaled.600.1", #local
        # "plank.72.0",
        # "plank.300.1",
        "bitrgbtags.300.1",

        "pdf.ppmraw.72.0",
        "pdf.ppmraw.300.0",
        "pdf.pkmraw.300.0",
        "ps.ppmraw.72.0",
        "ps.ppmraw.300.0",
        "ps.pkmraw.300.0",

        # "pxlmono.ppmraw.300.0", #local
        # "pxlmono.ppmraw.300.1", #local
        # "pxlcolor.ppmraw.300.0", #local
        # "pxlcolor.ppmraw.300.1", #local
        "xps.ppmraw.72.0",
        # code below limits this and the other xpswrite output option (immediately below)  to files in the PDF_1.7_FTS and PDFIA1.7_SUBSET directories
        "xps.ppmraw.300.0",

        # "xps.ppmraw.300.1"
    ],

    'gpdl' => [
        "ppmraw.72.0",
        "ppmraw.75.0",
        "ppmraw.300.0",
        "ppmraw.300.1",
        "ppmraw.600.1",
        "pdf.ppmraw.72.0",
        "pdf.ppmraw.75.0",
        "xps.ppmraw.72.0"
    ],

    'mupdf' => [
        # mupdf only supports pgm and ppmraw output, so don't bother specifying anything else
        "pgmraw.72.0",

        #   "pgmraw.300.0", #200
        #   "pgmraw.300.1", #200
        "ppmraw.72.0",

        #   "ppmraw.300.0", #200
        #   "ppmraw.300.1" #200
    ],

   # mujstest always outputs rgb, at whatever resolution the scripts tell it to.
    'mujstest' => [ "ppmraw.72.0" ]
);

if ($performance) {
    $testSource{ $privateRepository . "performance/ps" }  = 'gs';
    $testSource{ $privateRepository . "performance/pdf" } = 'gs';
    $testSource{ $privateRepository . "performance/pcl" } = 'pcl';
    delete $testSource{ $publicRepository . "Ghent_V3.0" };
    delete $testSource{ $publicRepository . "Ghent_V5.0" };
    delete $testSource{ $privateRepository . "ps/ps3fts" };
    delete $testSource{ $privateRepository . "pdf/sumatra" };
    delete $testSource{ $privateRepository . "pdf/uploads" };
    delete $testSource{ $privateRepository . "xps/sumatra" };
}

push @{ $tests{'gs_smoke'} }, "tiffscaled.600.1";
push @{ $tests{'gs_smoke'} }, "tiffsep.600.1";
push @{ $tests{'gs_smoke'} }, "tiffsep1.600.1";
push @{ $tests{'gs_smoke'} }, "tiffg4.600.1";
push @{ $tests{'gs_smoke'} }, "tiff24nc.600.1";

if ($extended || $smoke) {
    if ( !$performance ) {
        #push @{ $tests{'gs'} },  "psdcmykog.144.0";
        push @{ $tests{'gs'} },  "psdcmykog.600.1";
        push @{ $tests{'gs'} },  "tiffscaled.600.1";
        push @{ $tests{'gs'} },  "pxlmono.ppmraw.300.0";
        push @{ $tests{'gs'} },  "pxlmono.ppmraw.300.1";
        push @{ $tests{'gs'} },  "pxlcolor.ppmraw.300.0";
        push @{ $tests{'gs'} },  "pxlcolor.ppmraw.300.1";
        push @{ $tests{'gs'} },  "pdf.psdcmyk.300.1";
        push @{ $tests{'gs'} },  "ps.psdcmyk.300.1";
        push @{ $tests{'gs'} },  "psdcmyk16.300.1";
        #push @{ $tests{'gs'} },  "pdf.psdcmyk16.300.1";
        #push @{ $tests{'gs'} },  "ps.psdcmyk16.300.1";
        push @{ $tests{'pcl'} }, "tiffscaled.600.1";
        push @{ $tests{'gs'} },  "psdrgb.300.1";
    }
    push @{ $tests{'pcl'} },   "pgmraw.75.0";
    push @{ $tests{'mupdf'} }, "pgmraw.300.0";
    push @{ $tests{'mupdf'} }, "pgmraw.300.1";
    push @{ $tests{'mupdf'} }, "ppmraw.300.0";
    push @{ $tests{'mupdf'} }, "ppmraw.300.1";
} else {
    push @{ $tests{'mupdf'} }, "pgmraw.200.0";
    push @{ $tests{'mupdf'} }, "pgmraw.200.1";
    push @{ $tests{'mupdf'} }, "ppmraw.200.0";
    push @{ $tests{'mupdf'} }, "ppmraw.200.1";

    # code below limits these tests to files in the Ghent_V3.0, comparefiles, and xpsfts-a4 directories
    push @{ $tests{'mupdf'} }, "pbmraw.200.0";
    push @{ $tests{'mupdf'} }, "png.200.0";
    push @{ $tests{'mupdf'} }, "pam.200.0";
    push @{ $tests{'mupdf'} }, "pkmraw.200.0";
}

#print Dumper(\%tests);  exit;

#print Dumper(\%testSource); exit;

# Pull in the custom tests lists.
foreach my $testSource ( sort keys %testSource ) {
    my $sourceSource = $testSource{$testSource};
    if ($sourceSource eq "gs_smoke") {
        $sourceSource = "gs";
    }
    if ( exists $products{ $sourceSource } ) {

        unlink "custom_tests.lst";

	# We need to pull the latest version out of the git checkout,
	# rather than relying on us having the latest version of git
	# checked out. This horrid bit of perl hackery (thanks Marcos)
	# does that.
        #print "# $testSource\n";
        if ( $testSource =~ m|^(.*?/.+?/.+?/.+?)/(.+)| ) {

            #print "# $1 $2\n";
            #print"git archive --format=tar --remote=$1 HEAD $2/custom_tests.lst 2> /dev/null | tar Oxf - >./custom_tests.lst 2> /dev/null\n";
            my $b = `git archive --format=tar --remote=$1 HEAD $2/custom_tests.lst 2> /dev/null | tar Oxf - >./custom_tests.lst 2> /dev/null`;
        }

        if ( open( F, "<./custom_tests.lst" ) ) {
            foreach (<F>) {
                chomp;
                s/\#.+//;
                s/^\s+|\s+$//g;
                s/\s+\t/\t/;
                s/\t\s+/\t/;
                my @a = split "\t";

                # print "## ".scalar(@a)." $_\n";
                if ( scalar(@a) == 2 ) {
                    $customTests{$_} = $testSource{$testSource};
                    $a[0] =~ s|__|/|g;
                    $customTestFiles{ $a[0] } = $_;
                }
            }
            close(F);
        }
        unlink "custom_tests.lst";
    }
}

#print STDERR Dumper(\%customTests);  exit;
#print Dumper(\%testSource);  exit;

sub add_test($$)
{
    my $testfile = shift;
    my $type = shift;

    if (!exists $testfiles{'./'.$testfile}) {
        $testfiles{ './' . $testfile } = $type;
    } elsif (ref $testfiles{'.'.$testfile} eq 'ARRAY') {
        push @{$testfiles{ './'.$testfile}}, $type;
    } else {
        my @arr = ( $testfiles { './'.$testfile} , $type );
        $testfiles{ './' . $testfile } = \@arr;
    }
}

if ( !$bmpcmp ) {

    # For each directory full of source files
    foreach my $testSource ( sort keys %testSource ) {
        # If these source files are appropriate for the current set of products
        # i.e. if there are no products specified (means "all of them").
        #   or if the types of files matches the products that we have
        if (
               scalar keys %products == 0
            || exists $products{ $testSource{$testSource} }
            || (
                (
                       $testSource{$testSource} eq 'gs'
                    || $testSource{$testSource} eq 'xps'
                    || $testSource{$testSource} eq 'bmp'
                    || $testSource{$testSource} eq 'epub'
                    || $testSource{$testSource} eq 'tif'
                    || $testSource{$testSource} eq 'svg'
                    || $testSource{$testSource} eq 'j2k'
                )
                && exists $products{'mupdf'}
            )
            || (
                (
                       $testSource{$testSource} eq 'gs'
                )
                && exists $products{'gpdf'}
            )
            || (
                (
                       $testSource{$testSource} eq 'gs'
                    || $testSource{$testSource} eq 'pcl'
                    || $testSource{$testSource} eq 'xps'
                )
                && exists $products{'gpdl'}
            )
          )
        {
            my @a;

            #print "$testSource\n";
            if (   ( !$local && $testSource =~ m|(/.+?/.+?/.+?)/(.+)| )
                || ( $local && $testSource =~ m|.+/(.+?).git/(.+)| ) )
            {

                #       print "$1 $2\n";
                my $base = $1;
                my $dir  = $2;
                $base = "../" . $base if ($windows);

                #       print "cd $base ; git ls-tree HEAD $dir/\n";
                my $t = `cd $base ; git ls-tree HEAD $dir/`;
                my @t = split "\n", $t;
                foreach (@t) {
                    my @b = split "\t";
                    push @a, $b[1] if ( $b[0] =~ m/ blob / );
                }
            }

            my %nightly_only;
            if ( !$extended ) {
                unlink "nightly_only.lst";
                if ( $testSource =~ m|^(.*?/.+?/.+?/.+?)/(.+)| ) {

                    #         print "$1 $2\n";
                    my $b = `git archive --format=tar --remote=$1 HEAD $2/nightly_only.lst 2> /dev/null | tar Oxf - >./nightly_only.lst 2> /dev/null`;
                }
                if ( open( F, "<nightly_only.lst" ) ) {
                    foreach (<F>) {
                        chomp;
                        $nightly_only{$_} = 1;
                    }
                    close(F);
                    unlink "nightly_only.lst";
                }

                #       print STDERR Dumper(\%nightly_only);
            }

            #   print "$testSource{$testSource}\n";  exit;

            my $dir = "";
            if ( $testSource =~ m|/.+?/.+?/(.+?)\.git/(.+)| ) {
                $dir = $1 . '/' . $2;
            }

            # Now we run over each file in the test directory.
            foreach (@a) {
                chomp;
                s|.+/||;
                if (   exists $nightly_only{$_}
                    || m/\.lst$/
                    || m/\.txt$/
                    || m/\.keep/ )
                {

                    #print STDERR "skipping: $_\n";
                } else {
                    my $testfile = $dir . '/' . $_;
                    if (   $testfile =~ m|/$|
                        || $testfile =~ m/^\./
                           || $testfile =~ m/.disabled$/ )
                    {
                        # Nothing to do here
                    } elsif ( exists $products{'mupdf'} ) {
                        if (   $valgrind
                            || $testfile =~ m/.pdf$/i
                            || $testfile =~ m/.pwd$/i
                            || $testfile =~ m/.xps$/i
                            || $testfile =~ m/.bmp$/i
                            || $testfile =~ m/.epub$/i
                            || $testfile =~ m/.tif$/i
                            || $testfile =~ m/.svg$/i
                            || $testfile =~ m/.j2k$/i
                            || $testfile =~ m/.jp2$/i
                            || $testfile =~ m/.xhtml$/i
                            || $testfile =~ m/.html$/i ) {
                            add_test($testfile, 'mupdf');
                        }
                    } elsif ( exists $products{'mujstest'} ) {
                        if ( $testfile =~ m/.mjs$/i ) {
                            add_test($testfile, 'mujstest');
                        }
                    } else {
                        # FIXME: Need some way to have multiple values for $testfiles{...}
                        if ( exists $products{'gpdf'} ) {
                            if (   $valgrind
                                || $testfile =~ m/.pdf$/i
                                ) {
                                add_test($testfile, 'gpdf');
                            }
                        }
                        if ( exists $products{'gpdl'} ) {
                            add_test($testfile, 'gpdl');
                        }
                        if (exists $products{$testSource{$testSource}}) {
                            add_test($testfile, $testSource{$testSource});
                        }
                    }

                    #          print "$testfile\n"; exit;
                }
            }
        }
    }
}

sub do_build($$$$$$) {
    my $product       = shift;    # gs|pcl|xps|ls|gpdf|gpdl|mupdf|mujstest
    my $inputFilename = shift;
    my $options       = shift;
    my $md5sumOnly    = shift;
    my $bmpcmp        = shift;
    my $customTest    = shift;

    #print "$product $inputFilename $options $md5sumOnly $bmpcmp $customTest\n";

    if ( $product eq 'mupdf' || $product eq 'mujstest' ) {

        # code below limits these tests to files in the Ghent_V3.0, comparefiles, and xpsfts-a4 directories
        # push @{$tests{'mupdf'}},"pbmraw.200.0";
        # push @{$tests{'mupdf'}},"png.200.0";
        # push @{$tests{'mupdf'}},"pam.200.0";
        # push @{$tests{'mupdf'}},"pkmraw.200.0";
        if (   $options eq "pbmraw.200.0"
            || $options eq "png.200.0"
            || $options eq "pam.200.0"
            || $options eq "pkmraw.200.0" )
        {
            if (
                !(
                       $inputFilename =~ m/Ghent_V3.0/
                    || $inputFilename =~ m/Ghent_V5.0/
                    || $inputFilename =~ m/comparefiles/
                    || $inputFilename =~ m/xpsfts-a4/
                )
              )
            {
                return ( "", "", "", "" );
            }
        }
    }
    if ($product eq 'gpdl') {
        # Make sure we only use PS/PDF/XPS resolutions with PS/PDF/XPS,
        # and PCL resolutions with the rest.
        my $bad = 1;
        if ($options eq "ppmraw.72.0" ||
            $options eq "ppmraw.300.0" ||
            $options eq "ppmraw.300.1" ||
            $options eq "pdf.ppmraw.72.0" ||
            $options eq "xps.ppmraw.72.0") {
            if ($inputFilename =~ m/\.PS$/ ||
                $inputFilename =~ m/\.ps$/ ||
                $inputFilename =~ m/\.eps$/ ||
                $inputFilename =~ m/\.pdf$/ ||
                $inputFilename =~ m/\.PDF$/ ||
                $inputFilename =~ m/\.ai$/ ||
                $inputFilename =~ m/\.xpdf$/) {
                $bad = 0;
            }
        }
        if ($options eq "ppmraw.72.0" ||
            $options eq "pdf.ppmraw.72.0") {
            if ($inputFilename =~ m/\.xps$/ ||
                $inputFilename =~ m/\.xps$/) {
                $bad = 0;
            }
        }
        if ($options eq "ppmraw.75.0" ||
            $options eq "ppmraw.600.0" ||
            $options eq "ppmraw.600.1" ||
            $options eq "pdf.ppmraw.75.0") {
            if ($inputFilename =~ m/\.PS$/ ||
                $inputFilename =~ m/\.ps$/ ||
                $inputFilename =~ m/\.eps$/ ||
                $inputFilename =~ m/\.pdf$/ ||
                $inputFilename =~ m/\.PDF$/ ||
                $inputFilename =~ m/\.ai$/ ||
                $inputFilename =~ m/\.xpdf$/ ||
                $inputFilename =~ m/\.xps$/ ||
                $inputFilename =~ m/\.xps$/) {
            } else {
                $bad = 0;
            }
        }
        if ($bad == 1) {
            return ( "", "", "", "" );
        }
    }

    if ($product eq "icc" || $product eq "gs_smoke") {
        $product = "gs";
    }

    if ($valgrind) {
        $preCommand = "nice -n 20 valgrind --track-origins=yes ";
    } elsif ($coverage) {
        $preCommand = "nice -n 20 ";
    } else {
        $preCommand = "nice $timeBin -f \"%U %S %E %P\"";
    }

    my $cmd             = "";
    my $cmd1a           = "";
    my $cmd1b           = "";
    my $cmd1c           = "";
    my $cmd1extras      = "";
    my $cmd1d           = "";
    my $cmd2a           = "";
    my $cmd2b           = "";
    my $cmd2c           = "";
    my $cmd2extras      = "";
    my $cmd2d           = "";
    my $outputFilenames = "";
    my $rmCmd           = "true";

    my @a = split '\.', $options;

    my $filename;
    my $tempname;
    my $logFilename;
    my $md5Filename;
    my $filename2;
    my $customCommand;
    my $outprefix;
    my $pr=$product;

    # $filename2 is the result that goes into the .tab file.
    if ( $customTest eq "" ) {
	# Not a custom test.
        $filename = $inputFilename;
        $filename =~ s|.+/||;

        $tempname = $inputFilename;
        $tempname =~ s|^./||;
        $tempname =~ s|/|__|g;
        if ($a[0] eq 'xps' || $a[0] eq 'pdf' || $a[0] eq 'ps')
        {
            $pr.="_$a[0]";
        }
        $filename2     = "$tempname.$options";
        $outprefix     = "$temp/$filename2..$pr";
        $customCommand = "";
    } else {
	# A custom test.
        my @b = split "\t", $customTest;
        $outprefix     = "$temp/$b[0]..$pr";
        $customCommand = $b[1];
	#print "## $outprefix\t $customCommand\n";
	# This is crap even for the cluster. This extraction of device type
	# from the test file name should be more centralised, or at least
	# more complete.
	if ($b[0] =~ m/\.tiffg4\./) {
	    $a[0] = "tiffg4";
	} elsif ($b[0] =~ m/\.tiffsep\./) {
	    $a[0] = "tiffsep";
	} elsif ($b[0] =~ m/\.tiffsep1\./) {
	    $a[0] = "tiffsep1";
	} elsif ($b[0] =~ m/\.tiff24nc\./) {
	    $a[0] = "tiff24nc";
	} elsif ($b[0] =~ m/\.psdcmyk\./) {
	    $a[0] = "psdcmyk";
	} elsif ($b[0] =~ m/\.psdrgb\./) {
	    $a[0] = "psdrgb";
	} elsif ($b[0] =~ m/\.ppmraw\./) {
	    $a[0] = "ppmraw";
	} elsif ($b[0] =~ m/\.ppmraw\./) {
	    $a[0] = "ppmraw";
	} elsif ($b[0] =~ m/\.pbmraw\./) {
	    $a[0] = "pbmraw";
	} elsif ($b[0] =~ m/\.pgmraw\./) {
	    $a[0] = "pgmraw";
	} else {
            $a[0]          = $b[0];
	}
        $tempname      = "dummy";
        $filename2     = $b[0];
    }
    $logFilename   = "$outprefix.log";
    $md5Filename   = "$outprefix.md5";

    my $outputFilename;
    my $baselineFilename;
    my $rasterFilename;
    my $bmpcmpFilename;

    # $cmd .= " touch $logFilename ; rm -f $logFilename ";

    $cmd .= " true";
    #$cmd .= " ; touch $logFilename ; rm -f $logFilename";
    $cmd .= " ; touch $md5Filename" if ( !$bmpcmp && !$coverage );

    # $a[0] = What sort of job are we running?
    if ( $a[0] eq 'pxlmono' || $a[0] eq 'pxlcolor' ) {
        # 2 stage operation - first to pxlmono or pxlcolor, then to whatever rasterisation device we are using.
        if ( $a[0] eq 'pxlmono' ) {
            $cmd .= " ; /bin/echo \"$product pxlmono\"";
            $outputFilename = "$outprefix.pxlmono";
        } else {
            $cmd .= " ; /bin/echo \"$product pxlcolor\" ";
            $outputFilename = "$outprefix.pxlcolor";
        }

        if ( $product eq 'gs' ) {
            $cmd1a .= "$gsBin";
        } elsif ( $product eq 'pcl' ) {
            $cmd1a .= "$pclBin";
        } elsif ( $product eq 'xps' ) {
            $cmd1a .= "$xpsBin";
        } elsif ( $product eq 'ls' ) {
            $cmd1a .= "$lsBin";
        } elsif ( $product eq 'gpdf' ) {
            $cmd1a .= "$gpdfBin";
        } elsif ( $product eq 'gpdl' ) {
            $cmd1a .= "$gpdlBin";
        } else {
            die "unexpected product: $product";
        }
        $cmd1b .= " -sOutputFile=$outputFilename";
        if ( $a[0] eq 'pxlmono' ) {
            $cmd1c .= " -sDEVICE=pxlmono";
        } else {
            $cmd1c .= " -sDEVICE=pxlcolor";
        }

         #   $cmd1c.=" ".$additionalOptions_pdfwrite_step1;
         #   $cmd1c.=" ".$additionalOptions_gs_pdfwrite_step1 if ($product eq 'gs');
        $cmd1c .= " -r" . $a[2];
        $cmd1c .= " -sPDFPassword=\"`cat $inputFilename.pwd`\""
          if ( exists $testfiles{ $inputFilename . ".pwd" } );
        $cmd1c .= " -Z:";
        if ( $product eq 'gs' || $product eq 'qpdl' || $product eq 'gpdf') {
            $cmd1c .= " -sDEFAULTPAPERSIZE=letter";
        }
        $cmd1c .= " -dNOPAUSE -dBATCH";
        $cmd1c      .= " -dClusterJob" if ( !$local );
        $cmd1c      .= " -dJOBSERVER"  if ( $product eq 'gs' );
        $cmd1extras .= " $extra_args"  if ( length($extra_args) );

        if (($product eq 'gs' || $product eq 'gpdl') &&
            ( $inputFilename =~ m/ps3fts/ || $inputFilename =~ m/ps3cet/ ) ) {
            # -dCETMODE runs gs_cet.ps from gs_init.ps, so it happens before we
            # enter the job loop. We don't really need to run gs_cet.ps manually
            # therefore, but we do so in case we test old versions of the software.
            $cmd1c .= " -dCETMODE %rom%Resource/Init/gs_cet.ps";
        }

        if ( !( $filename =~ m/.pdf$/i || $filename =~ m/.ai$/i ) &&
             ($product eq 'gs' || $product eq 'gpdl_') ) {
            $cmd1d = " - < ";
        }

        $cmd1d .= " $inputFilename";

        if ($bmpcmp) {
            $cmd .= " ; /bin/echo \"$cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d \" ";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d ";
            $cmd .= " ; /bin/echo 'return:' \$? ";
            $cmd1a =~ s|__bin__/|$bmpcmpRef/|;
            $cmd1b =~ s|$temp|$baselineRaster|;
            $cmd .= " ; /bin/echo \"$cmd1a $cmd1b $cmd1c $cmd1d \" ";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1c $cmd1d ";
            $cmd .= " ; /bin/echo 'return:' \$? ";
        } else {
            $cmd .= " ; /bin/echo \"$cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d \" ";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d ; ";
            $cmd .= " /bin/echo 'return:' \$? ";
        }

        $cmd .= " ; /bin/echo '---' ";

        my $inputFilename = $outputFilename;

        $outputFilename   = "$temp/$filename2";
        $baselineFilename = "$baselineRaster/$filename2";
        $rasterFilename   = "$raster/$filename2";
        $bmpcmpFilename   = "$bmpcmpDir/$filename2..$pr";

        $cmd2a .= " $pclBin";
        if (!$bmpcmp && $md5sumOnly) {
	    # FIXME: This is wrong for tiff or psdcmyk output
            $cmd2b .= " -sOutputFile='| md5sum >>$md5Filename'";
        } else {
            if ($local) {
                if ($windows) {
                    $cmd2b .= " -sstdout=%stderr -sOutputFile=-";
                } elsif ($performance) {
                    $cmd2b .= " -sOutputFile=/dev/null";
                } else {
                    $cmd2b .=
                      " -sOutputFile='| lzop -f -o $outputFilename.lzo'";
                }
            } else {
                $cmd2b .= " -sOutputFile='| gzip -1 -n -c >$outputFilename.gz'";
            }
        }

        #$cmd2c.=" -dMaxBitmap=400000000" if ($a[3]==0);
        #$cmd2c.=" -dMaxBitmap=10000"    if ($a[3]==1);

        $cmd2c .= " -sDEVICE=" . $a[1];

        #$cmd2c.=" -dGrayValues=256" if ($a[1] eq 'bitrgb');
        #$cmd2c.=" -dcupsColorSpace=0" if ($a[1] eq 'cups');
        #$cmd2c.=" -dDownScaleFactor=3" if ($a[1] eq 'tiffscaled');
        #$cmd2c.=" ".$additionalOptions_pdfwrite_step2;
        #$cmd2c.=" ".$additionalOptions_gs_pdfwrite_step2 if ($product eq 'gs');
        $cmd2c .= " -r" . $a[2];
        $cmd2c .= " -Z:";

        #$cmd2c.=" -sDEFAULTPAPERSIZE=letter" if ($product eq 'gs');
        $cmd2c .= " -dNOPAUSE -dBATCH -K2000000";    # -Z:
        $cmd2c .= " -dClusterJob" if ( !$local );

        #$cmd2c.=" -dJOBSERVER";
        $cmd2extras = " $extra_args" if ( length($extra_args) );

        $cmd2d = " $inputFilename";

        if ($bmpcmp) {
            $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$outputFilename.gz' $cmd2extras $cmd2c $cmd2d\" ";
            $cmd .= " ; $preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$outputFilename.gz' $cmd2extras $cmd2c $cmd2d ";
            $cmd2a =~ s|/gs/|/$bmpcmpRef/|;
            $cmd2b =~ s|$temp|$baselineRaster|;
            $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$baselineFilename.gz' $cmd2c $cmd2d\" ";
            $cmd .= " ; $preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$baselineFilename.gz' $cmd2c $cmd2d ";
            $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
            #$cmd .= "; nice gzip $bmpcmpFilename.* ";
            $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";
            if ( $bmpcmpFilename =~ m/\.72\./ || $bmpcmpFilename =~ m/\.75\./ )
            {
                $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
            }
            $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
        } else {
            $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
            if ($windows) {
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d | lzop -c >$outputFilename.lzo ; ";
                $cmd .= "/bin/echo 'return:' \$? ";
            } else {
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ; ";
                $cmd .= "/bin/echo 'return:' \$? ";
            }
            if ( $local && !$performance ) {
                $cmd .= " nice lzop -c -d $outputFilename.lzo | nice md5sum >$md5Filename";
            }
        }

        #$cmd.=" ; nice lzop -U -f $inputFilename >>$logFilename 2>&1" if ($local);
        $cmd .= " ; rm $inputFilename " if ($local);
        $outputFilenames .= "$inputFilename ";

    } elsif ( $a[0] eq 'xps' ) {
        # XPSWrite jobs.
        if ( $inputFilename =~ m/PDF_1.7_FTS/
            || ( $extended && $inputFilename =~ m/PDFIA1.7_SUBSET/ ) )
        {

            #print STDERR "$inputFilename\n";

            $cmd .= " ; /bin/echo \"$product xpswrite\" ";
            $outputFilename = "$outprefix.xps";

            if ( $product eq 'gs' ) {
                $cmd1a .= "$gsBin";
            } elsif ( $product eq 'pcl' ) {
                $cmd1a .= "$pclBin";
            } elsif ( $product eq 'xps' ) {
                $cmd1a .= "$xpsBin";
            } elsif ( $product eq 'ls' ) {
                $cmd1a .= "$lsBin";
            } elsif ( $product eq 'gpdf' ) {
                $cmd1a .= "$gpdfBin";
            } elsif ( $product eq 'gpdl' ) {
                $cmd1a .= "$gpdlBin";
            } else {
                die "unexpected product: $product";
            }
            if ($valgrind) {
                $cmd1a =~ s|/gs/|/valgrind/|;
                $outputFilename = "/dev/null";
            }
            if ($coverage) {
                #$cmd1a =~ s|/gs/|/coverage/|;
		$cmd1a = "./runGCov.pl $filename2 ".$cmd1a;
                $outputFilename = "/dev/null";
            }
            $cmd1b .= " -sOutputFile=$outputFilename";
            $cmd1c .= " -sDEVICE=xpswrite";

            #$cmd1c.=" ".$additionalOptions;
            #$cmd1c.=" ".$additionalOptions_pdfwrite_step1;
            #$cmd1c.=" ".$additionalOptions_gs_pdfwrite_step1 if ($product eq 'gs');
            $cmd1c .= " -r" . $a[2];
            $cmd1c .= " -sPDFPassword=\"`cat $inputFilename.pwd`\""
              if ( exists $testfiles{ $inputFilename . ".pwd" } );
            $cmd1c .= " -Z:" if ( !$valgrind || $local );
            if ( $product eq 'gs' || $product eq 'gpdl' ) {
                $cmd1c .= " -sDEFAULTPAPERSIZE=letter";
            }
            $cmd1c .= " -dNOPAUSE -dBATCH";    # -Z:
            $cmd1c .= " -dClusterJob"
              if ( !$local && !$valgrind && !$coverage );
            $cmd1c .= " -dJOBSERVER" if ( $product eq 'gs' );
            $cmd1extras .= " $extra_args" if ( length($extra_args) );

            if (($product eq 'gs' || $product eq 'gpdl') &&
                ($inputFilename =~ m/ps3fts/ || $inputFilename =~ m/ps3cet/ ) ) {
                # -dCETMODE runs gs_cet.ps from gs_init.ps, so it happens before we
                # enter the job loop. We don't really need to run gs_cet.ps manually
                # therefore, but we do so in case we test old versions of the software.
                $cmd1c .= " -dCETMODE %rom%Resource/Init/gs_cet.ps";
            }

	    # FIXME: gpdl_ below?
            if ( !( $filename =~ m/.pdf$/i || $filename =~ m/.ai$/i ) &&
                 ($product eq 'gs' || $product eq 'gpdl_')) {
                $cmd1d = " - < ";
            }

            $cmd1d .= " $inputFilename";

            #   $cmd.=" 2>&1";

            if ($bmpcmp) {
                $cmd .= " ; /bin/echo \"$preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d\" ";
                $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d ";
                $cmd1a =~ s|__bin__/|$bmpcmpRef/|;
                $cmd1b =~ s|$temp|$baselineRaster|;
                $cmd .= " ; /bin/echo \"$preCommand $cmd1a $cmd1b $cmd1c $cmd1d\" ";
                $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1c $cmd1d ";
            } else {
                $cmd .= " ; /bin/echo \"$cmd1a $cmd1b $cmd1c $cmd1d\" ";
                $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1c $cmd1d ;";
                $cmd .= " /bin/echo 'return:' \$? ";
                $cmd .= " ; /bin/echo -n 'md5sum: ' ";
                $cmd .= " ; md5sum $outputFilename ";
                $cmd .= " ; /bin/echo -n 'filesize: ' ";
                $cmd .= " ; stat -c '\%s' $outputFilename ";
            }

            if ( !$valgrind && !$coverage ) {
                $cmd .= " ; /bin/echo '---' ";

                my $inputFilename = $outputFilename;

                $outputFilename   = "$temp/$filename2";
                $baselineFilename = "$baselineRaster/$filename2";
                $rasterFilename   = "$raster/$filename2";
                $bmpcmpFilename   = "$bmpcmpDir/$filename2..$pr";

                $cmd2a .= " $xpsBin";

                if (!$bmpcmp && $md5sumOnly) {
		    # FIXME: Wrong for psd and tiff jobs!
                    $cmd2b .= " -sOutputFile='| md5sum >>$md5Filename'";
                } else {
                    if ($local) {
                        if ($windows) {
                            $cmd2b .= " -sstdout=%stderr -sOutputFile=-";
                        } elsif ($performance) {
                            $cmd2b .= " -sOutputFile=/dev/null";
                        } else {
                            $cmd2b .= " -sOutputFile='| lzop -f -o $outputFilename.lzo'";
                        }
                    } else {
                        $cmd2b .=
                          " -sOutputFile='| gzip -1 -n -c >$outputFilename.gz'";
                    }
                }

                #$cmd2c.=" -dMaxBitmap=400000000" if ($a[3]==0);
                #$cmd2c.=" -dMaxBitmap=10000"    if ($a[3]==1);

                $cmd2c .= " -sDEVICE=" . $a[1];

                #$cmd2c.=" -dGrayValues=256" if ($a[1] eq 'bitrgb');
                #$cmd2c.=" -dcupsColorSpace=0" if ($a[1] eq 'cups');
                #$cmd2c.=" -dDownScaleFactor=3" if ($a[1] eq 'tiffscaled');
                #$cmd2c.=" ".$additionalOptions;
                #$cmd2c.=" ".$additionalOptions_pdfwrite_step2;
                #$cmd2c.=" ".$additionalOptions_gs_pdfwrite_step2 if ($product eq 'gs');
                $cmd2c .= " -r" . $a[2];

                #$cmd2c.=" -sPDFPassword=\"`cat $inputFilename.pwd`\"" if (exists $testfiles{$inputFilename.".pwd"});
                $cmd2c .= " -Z:" if ( !$valgrind );

                #$cmd2c.=" -sDEFAULTPAPERSIZE=letter" if ($product eq 'gs');
                $cmd2c .= " -dNOPAUSE -dBATCH -K2000000";    # -Z:
                $cmd2c .= " -dClusterJob"
                  if ( !$local && !$valgrind && !$coverage );
                $cmd2c .= " -dJOBSERVER";
                $cmd2extras .= " $extra_args" if ( length($extra_args) );

                $cmd2d .= " $inputFilename";

                if ($bmpcmp) {
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ";
                    if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                        # FIXME: This is never hit - we never test xpswrite -> {psd,tiff}
                        my $collate = " ; touch $outputFilename.0000";
                        $collate .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.gz";
                        $collate .= " ; rm $outputFilename.????";
                        $cmd .= $collate;
                    }
                    $cmd2a =~ s|/gs/|/$bmpcmpRef/|;
                    $cmd2b =~ s|$temp|$baselineRaster|;
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\"";
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d";
                    if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                        # FIXME: This is never hit - we never test xpswrite -> {psd,tiff}
                        my $collate = " ; touch $outputFilename.0000";
                        $collate .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.gz";
                        $collate .= " ; rm $outputFilename.????";
                        $collate =~ s/$temp/$baselineRaster/g;
                        $cmd .= $collate;
                    }
                    $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
                    #$cmd .= " ; nice gzip $bmpcmpFilename.* ";
                    $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";
                    if (   $bmpcmpFilename =~ m/\.72\./
                        || $bmpcmpFilename =~ m/\.75\./ )
                    {
                        $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
                    }
                    $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
                } else {
                    $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
                    if ($windows) {
                        $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d | lzop -c >$outputFilename.lzo ; /bin/echo 'return:' \$?";
                    } else {
                        $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ; /bin/echo 'return:' \$?";
                    }
                    if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                        # FIXME: This is never hit - we never test xpswrite -> {psd,tiff}
                        $cmd .= " ; touch $outputFilename.0000";
                        if ($md5sumOnly) {
                            $cmd .= " ; cat $outputFilename.???? | nice md5sum >>$md5Filename";
                        } else {
                            if ($local) {
                                if ( !$performance ) {
                                    $cmd .= " ; cat $outputFilename.???? | nice lzop -c > $outputFilename.lzo";
                                }
                            } else {
                                $cmd .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.lzo";
                            }
                        }
                        $cmd .= " ; rm $outputFilename.????";
                    }
                    if ( $local && !$performance ) {
                        $cmd .= " nice lzop -c -d $outputFilename.lzo | nice md5sum >$md5Filename";
                    }
                }

                #$cmd.=" ; nice lzop -U -f $inputFilename " if ($local);
                $cmd .= " ; rm $inputFilename " if ($local);
                $outputFilenames .= "$inputFilename ";
            }

        } else {
            $cmd = " true ";
        }
    } elsif ( $a[0] eq 'pdf' || $a[0] eq 'ps' ) {
        # pdfwrite and ps2write jobs
        if ( $a[0] eq 'pdf' ) {
            $cmd .= " ; /bin/echo \"$product pdfwrite\" ";
            $outputFilename = "$outprefix.pdf";
            if ($singlePagePDF) {
                $outputFilename = "$outprefix.pdf.%03d";
            }
        } else {
            $cmd .= " ; /bin/echo \"$product ps2write\" ";
            $outputFilename = "$outprefix.ps";
        }

        if ( $product eq 'gs' ) {
            $cmd1a .= "$gsBin";
        } elsif ( $product eq 'pcl' ) {
            $cmd1a .= "$pclBin";
        } elsif ( $product eq 'xps' ) {
            $cmd1a .= "$xpsBin";
        } elsif ( $product eq 'ls' ) {
            $cmd1a .= "$lsBin";
        } elsif ( $product eq 'gpdf' ) {
            $cmd1a .= "$gpdfBin";
        } elsif ( $product eq 'gpdl' ) {
            $cmd1a .= "$gpdlBin";
        } else {
            die "unexpected product: $product";
        }
        if ($valgrind) {
            $cmd1a =~ s|__bin__/|valgrind/|;
            $outputFilename = "/dev/null";
        }
        if ($coverage) {
            #$cmd1a =~ s|/gs/|/coverage/|;
            $cmd1a = "./runGCov.pl $filename2 ".$cmd1a;
            $outputFilename = "/dev/null";
        }
        $cmd1b .= " -sOutputFile=$outputFilename";
        if ( $a[0] eq 'pdf' ) {
            $cmd1c .= " -sDEVICE=pdfwrite";
        } else {
            $cmd1c .= " -sDEVICE=ps2write";
        }
        $cmd1c .= " " . $additionalOptions;
        $cmd1c .= " " . $additionalOptions_pdfwrite_step1;
        if ( $product eq 'gs' || $product eq 'gpdf' || $product eq 'gpdl') {
            $cmd1c .= " " . $additionalOptions_gs_pdfwrite_step1
	}
        $cmd1c .= " -r" . $a[2];
        $cmd1c .= " -sPDFPassword=\"`cat $inputFilename.pwd`\""
          if ( exists $testfiles{ $inputFilename . ".pwd" } );
        $cmd1c .= " -Z:" if ( !$valgrind || $local );
        if ( $product eq 'gs' || $product eq 'gpdl' || $product eq 'gpdf' ) {
            $cmd1c .= " -sDEFAULTPAPERSIZE=letter";
        }
        $cmd1c .= " -dNOPAUSE -dBATCH";    # -Z:
        $cmd1c .= " -dClusterJob" if ( !$local && !$valgrind && !$coverage );
        $cmd1c .= " -dJOBSERVER" if ( $product eq 'gs' );
        $cmd1extras .= " $extra_args" if ( length($extra_args) );

        if (($product eq 'gs' || $product eq 'gpdl') &&
            ( $inputFilename =~ m/ps3fts/ || $inputFilename =~ m/ps3cet/ ) ) {
            # -dCETMODE runs gs_cet.ps from gs_init.ps, so it happens before we
            # enter the job loop. We don't really need to run gs_cet.ps manually
            # therefore, but we do so in case we test old versions of the software.
            $cmd1c .= " -dCETMODE %rom%Resource/Init/gs_cet.ps";
        }

	# FIXME: gpdl_ below?
        if ( !( $filename =~ m/.pdf$/i || $filename =~ m/.ai$/i ) &&
             ($product eq 'gs' || $product eq 'gpdl_') && !$valgrind ) {
            $cmd1d = " - < ";
        }

        $cmd1d .= " $inputFilename";

        if ($bmpcmp) {
            $cmd .= " ; /bin/echo \"$preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d\"";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d";
            $cmd1a =~ s|__bin__/|$bmpcmpRef/|;
            $cmd1b =~ s|$temp|$baselineRaster|;
            $cmd .= " ; /bin/echo \"$preCommand $cmd1a $cmd1b $cmd1c $cmd1d\" ";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1c $cmd1d ";
        } else {
            $cmd .= " ; /bin/echo \"$cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d \" ";
            $cmd .= " ; $preCommand $cmd1a $cmd1b $cmd1extras $cmd1c $cmd1d ; /bin/echo 'return:' \$? ";
            $cmd .= " ; /bin/echo -n 'md5sum: ' ";
            $cmd .= " ; md5sum $outputFilename ";
            $cmd .= " ; /bin/echo -n 'filesize: ' ";
            $cmd .= " ; stat -c '\%s' $outputFilename ";
        }
        if ( !$valgrind && !$coverage ) {
            $cmd .= " ; /bin/echo '---' ";

            my $inputFilename = $outputFilename;
            if ($singlePagePDF) {
                $inputFilename =~ s/\.\%03d/\.\*/;
            }

            $outputFilename   = "$temp/$filename2";
            $baselineFilename = "$baselineRaster/$filename2";
            $rasterFilename   = "$raster/$filename2";
            $bmpcmpFilename   = "$bmpcmpDir/$filename2..$pr";

	    if ( $product eq 'gpdl') {
                $cmd2a .= " $gpdlBin";
	    } elsif ( $product eq 'gpdf' && $a[0] eq 'pdf' ) {
                $cmd2a .= " $gpdfBin";
	    } else {
                $cmd2a .= " $gsBin";
	    }
	    # FIXME: I believe that we do not currently pdfwrite -> {psd,tiff}
            if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                $cmd2b .= " -sOutputFile=$outputFilename.%04d";
            } elsif ($a[1] eq 'tiffsep' || $a[1] eq 'tiffsep1' || $a[1] eq 'tiffg4' || $a[1] eq 'tiffg4' || $a[1] eq 'tiff24nc') {
                $cmd2b .= " -sOutputFile=$outputFilename.%04d.tif -dTIFFDateTime=false";
            } elsif (!$bmpcmp && $md5sumOnly) {
                $cmd2b .= " -sOutputFile='| md5sum >>$md5Filename'";
            } else {
                if ($local) {
                    if ($windows) {
                        $cmd2b .= " -sstdout=%stderr -sOutputFile=-";
                    } elsif ($performance) {
                        $cmd2b .= " -sOutputFile=/dev/null";
                    } else {
                        $cmd2b .=
                          " -sOutputFile='| lzop -f -o $outputFilename.lzo'";
                    }
                } else {
                    $cmd2b .=
                      " -sOutputFile='| gzip -1 -n -c >$outputFilename.gz'";
                }
            }

            $cmd2c .= " -dMaxBitmap=400000000" if ( $a[3] == 0 );
            $cmd2c .= " -dMaxBitmap=10000"     if ( $a[3] == 1 );

            $cmd2c .= " -sDEVICE=" . $a[1];
            $cmd2c .= " -dGrayValues=256" if ( $a[0] eq 'bitrgb' );
            $cmd2c .= " -dcupsColorSpace=0" if ( $a[0] eq 'cups' );

            # $cmd2c.=" -dDownScaleFactor=3" if ($a[1] eq 'tiffscaled');
            $cmd2c .= " " . $additionalOptions;
            $cmd2c .= " " . $additionalOptions_pdfwrite_step2;
            $cmd2c .= " " . $additionalOptions_gs_pdfwrite_step2;
            $cmd2c .= " -r" . $a[2];
            if ( exists $testfiles{ $inputFilename . ".pwd" } )
            {
                $cmd2c .= " -sPDFPassword=\"`cat $inputFilename.pwd`\"";
            }
            $cmd2c .= " -Z:" if ( !$valgrind );
            if ( $product eq 'gs' || $product eq 'gpdl' || $product eq 'gpdf' ) {
                $cmd2c .= " -sDEFAULTPAPERSIZE=letter";
            }
            $cmd2c .= " -dNOPAUSE -dBATCH -K2000000";    # -Z:
            if ( !$local && !$valgrind && !$coverage )
            {
                $cmd2c .= " -dClusterJob";
            }
            $cmd2c .= " -dJOBSERVER";
            $cmd2extras .= " $extra_args" if ( length($extra_args) );

            $cmd2d .= " $inputFilename";

            if ($bmpcmp) {
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\"";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d";
                if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                    my $collate = " ; touch $outputFilename.0000";
                    $collate .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.gz";
                    $collate .= " ; rm $outputFilename.????";
                    $cmd .= $collate;
                } elsif ($a[1] eq 'tiffsep' || $a[1] eq 'tiffsep1' || $a[1] eq 'tiffg4' || $a[1] eq 'tiffg4' || $a[1] eq 'tiff24nc') {
                    my $collate = " ; touch $outputFilename.0000.tif";
                    $collate .= " ; cat $outputFilename.*.tif | nice gzip -1 -n -c > $outputFilename.gz";
                    $collate .= " ; rm $outputFilename.*.tif";
                    $cmd .= $collate;
                }
                $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                $cmd2b =~ s|$temp|$baselineRaster|;
                $cmd2d =~ s|$temp|$baselineRaster|;
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ";
                if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                    my $collate = " ; touch $outputFilename.0000";
                    $collate .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.gz ";
                    $collate .= " ; rm $outputFilename.????";
                    $collate =~ s/$temp/$baselineRaster/g;
                    $cmd .= $collate;
                } elsif ($a[1] eq 'tiffsep' || $a[1] eq 'tiffsep1' || $a[1] eq 'tiffg4' || $a[1] eq 'tiffg4' || $a[1] eq 'tiff24nc') {
                    my $collate = " ; touch $outputFilename.0000.tif";
                    $collate .= " ; cat $outputFilename.*.tif | nice gzip -1 -n -c > $outputFilename.gz ";
                    $collate .= " ; rm $outputFilename.*.tif";
                    $collate =~ s/$temp/$baselineRaster/g;
                    $cmd .= $collate;
                }
                $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
                #$cmd .= "; nice gzip $bmpcmpFilename.* ";
                $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";
                if (   $bmpcmpFilename =~ m/\.72\./
                    || $bmpcmpFilename =~ m/\.75\./ )
                {
                    $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
                }
                $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
            } else {
                $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
                if ($windows) {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d | lzop -c >$outputFilename.lzo ; /bin/echo 'return:' \$? ";
                } else {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ; /bin/echo 'return:' \$? ";
                }

                if ($a[1] eq 'psdcmyk' || $a[1] eq 'psdcmyk16' || $a[1] eq 'psdcmykog' || $a[1] eq 'psdrgb') {
                    $cmd .= " ; touch $outputFilename.0000";
                    if ($md5sumOnly) {
                        $cmd .= " ; cat $outputFilename.???? | nice md5sum >>$md5Filename";
                    } else {
                        if ($local) {
                            if ( !$performance ) {
                                $cmd .= " ; cat $outputFilename.???? | nice lzop -c > $outputFilename.lzo";
                            }
                        } else {
                            $cmd .= " ; cat $outputFilename.???? | nice gzip -1 -n -c > $outputFilename.lzo ";
                        }
                    }
                    $cmd .= " ; rm $outputFilename.????";
                } elsif ($a[1] eq 'tiffsep' || $a[1] eq 'tiffsep1' || $a[1] eq 'tiffg4' || $a[1] eq 'tiffg4' || $a[1] eq 'tiff24nc') {
                    $cmd .= " ; touch $outputFilename.0000.tif";
                    if ($md5sumOnly) {
                        $cmd .= " ; cat $outputFilename.*.tif | nice md5sum >>$md5Filename";
                    } else {
                        if ($local) {
                            if ( !$performance ) {
                                $cmd .= " ; cat $outputFilename.*.tif | nice lzop -c > $outputFilename.lzo";
                            }
                        } else {
                            $cmd .= " ; cat $outputFilename.*.tif | nice gzip -1 -n -c > $outputFilename.lzo ";
                        }
                    }
                    $cmd .= " ; rm $outputFilename.*.tif";
                }

                if ( $local && !$performance ) {
                    $cmd .= " nice lzop -c -d $outputFilename.lzo | nice md5sum >$md5Filename";
                }
            }

            #$cmd.=" ; nice lzop -U -f $inputFilename" if ($local);
            $cmd .= " ; rm $inputFilename " if ($local);
            $outputFilenames .= "$inputFilename ";
        }

    } else {
        # non pdfwrite/ps2write/xpswrite/pxlmono/pxlcolor test
        $cmd .= " ; /bin/echo \"$product\" ";

        $outputFilename   = "$outprefix";
        $baselineFilename = "$baselineRaster/$filename2";
        $rasterFilename   = "$raster/$filename2";
        $bmpcmpFilename   = "$bmpcmpDir/$filename2..$pr";

        if ( $product eq 'gs' ) {
            $cmd2a .= " $gsBin";
        } elsif ( $product eq 'pcl' ) {
            $cmd2a .= " $pclBin";
        } elsif ( $product eq 'xps' ) {
            $cmd2a .= " $xpsBin";
        } elsif ( $product eq 'ls' ) {
            $cmd2a .= " $lsBin";
        } elsif ( $product eq 'gpdf' ) {
            $cmd2a .= " $gpdfBin";
        } elsif ( $product eq 'gpdl' ) {
            $cmd2a .= " $gpdlBin";
        } elsif ( $product eq 'mupdf' ) {
            $cmd2a .= " $mupdfBin";
        } elsif ( $product eq 'mujstest' ) {
            $cmd2a .= " $mujstestBin";
        } else {
            die "unexpected product: $product";
        }

        if ($valgrind) {
            $cmd2a =~ s|/gs/|/valgrind/|;
        }
        if ($coverage) {
            #$cmd2a =~ s|/gs/|/coverage/|;
            $cmd2a = "./runGCov.pl $filename2 ".$cmd2a;
        }
        if ( $product eq 'mupdf' ) {
            $cmd2b .= " -r$a[1]";
            $cmd2b .= " -p \"`cat $inputFilename.pwd`\""
              if ( exists $testfiles{ $inputFilename . ".pwd" } );
            $cmd2b .= " -D" if ( $a[2] == 0 );
            my $fmt;
            $fmt = $1 if ( $a[0] =~ m/(...)raw/ );
            $fmt = "png"         if ( $a[0] eq "png" );
            $fmt = "pam -c cmyk" if ( $a[0] eq "pam" );
            $cmd2b .= " -F $fmt";
            $cmd2b .= " -smt" if ($local);
            $cmd2b .= " -stm"
              if ( !$local )
              ; # reverse order so we can differentiate cluster jobs from local jobs

            if ( !$bmpcmp || $local ) {
                $cmd2b .= " -o -";
            } else {
                $cmd2b .= " -o $outputFilename.%04d";
            }
            $cmd2extras = " $extra_args" if ( length($extra_args) );
            $cmd2c = " $inputFilename ";
            if ($bmpcmp) {
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c ";
                $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$outputFilename.gz ; rm $outputFilename.????";
                $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c ";
                $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$baselineFilename.gz ; rm $outputFilename.????";
                $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
                #$cmd .= " ; nice gzip $bmpcmpFilename.* ";
                $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";

                #$cmd.=" ; touch $bmpcmpFilename.00000.meta";
                #$cmd.=" ; bash -c \"for (( c=1; c<=5; c++ )); do scp -q -o ConnectTimeout=30 -i ~/.ssh/cluster_key $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'scp retry \$c' ; done\"";
                if (   $bmpcmpFilename =~ m/\.72\./
                    || $bmpcmpFilename =~ m/\.75\./ )
                {
                    $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
                }
                $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
            } else {
                $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                if ($local) {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c | lzop -c >$outputFilename.lzo ; /bin/echo 'return:' \$?";
                    $cmd .= " ; nice lzop -c -d $outputFilename.lzo | nice md5sum >$md5Filename";
                } else {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c | md5sum >$md5Filename ; /bin/echo 'return:' \$?";

                    #$cmd.=" ; $preCommand $cmd2a $cmd2b $cmd2c ; /bin/echo 'return:' \$?";
                    #$cmd.=" ; nice cat $outputFilename.???? | nice md5sum >$md5Filename";
                    #$cmd.=" ; rm $outputFilename.????";
                }
            }
        } elsif ( $product eq 'mujstest' ) {

            # FIXME:
            $cmd2b .= " -o $outputFilename.%04d";
            $cmd2extras = " $extra_args" if ( length($extra_args) );
            $cmd2c = " $inputFilename ";
            if ($bmpcmp) {
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c ";
                $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$outputFilename.gz ; rm $outputFilename.????";
                $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                $cmd .= " ; /bin/echo \"$preCommand $cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c ";
                $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$baselineFilename.gz ; rm $outputFilename.????";
                $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
                #$cmd .= " ; nice gzip $bmpcmpFilename.* ";
                $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";
                if (   $bmpcmpFilename =~ m/\.72\./
                    || $bmpcmpFilename =~ m/\.75\./ )
                {
                    $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
                }
                $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
            } else {
                $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c\" ";
                $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c; /bin/echo 'return:' \$? ";
                $cmd .= " ; nice cat $outputFilename.???? | nice md5sum >$md5Filename ; rm $outputFilename.????";
            }
        } else {    # non-mupdf/mujstest test

            if ($valgrind) {
                $cmd2b .= " -sOutputFile=/dev/null";
            } elsif ($coverage) {
                $cmd2b .= " -sOutputFile=/dev/null";
            } elsif ($performance) {
                $cmd2b .= " -sOutputFile=/dev/null";
            } elsif ( $a[0] eq 'tiffscaled' ) {
                $cmd2b .= " -sOutputFile=$outputFilename.tif -dTIFFDateTime=false";
            } elsif ( $a[0] eq 'psdcmyk' || $a[0] eq 'psdcmyk16' || $a[0] eq 'psdcmykog' || $a[0] eq 'psdrgb' ) {
                $cmd2b .= " -sOutputFile=$outputFilename.%04d";
            } elsif ( $a[0] eq 'tiffsep' || $a[0] eq 'tiffsep1' || $a[0] eq 'tiffg4' || $a[0] eq 'tiffg4' || $a[0] eq 'tiff24nc') {
                $cmd2b .= " -sOutputFile=$outputFilename.%04d.tif -dTIFFDateTime=false";
            } elsif ($md5sumOnly) {
                $cmd2b .= " -sOutputFile='| md5sum >>$md5Filename'";
            } else {
                if ($local) {
                    if ($windows) {
                        $cmd2b .= " -sstdout=%stderr -sOutputFile=-";
                    } else {
                        $cmd2b .=
                          " -sOutputFile='| lzop -f -o $outputFilename.lzo'";
                    }
                } else {
                    $cmd2b .=
                      " -sOutputFile='| gzip -1 -n -c >$outputFilename.gz'";
                }
            }

            if ( $customTest ne "" ) {
                $cmd2c .= " -dNOPAUSE -dBATCH";
                $cmd2c .= " " . $additionalOptions;
                $cmd2c .= " " . $additionalOptions_gs if ( $product eq 'gs' || $product eq 'gpdl' || $product eq 'gpdf' );
                $cmd2c .= " -dClusterJob" if ( !$local );
                $cmd2c .= " $customCommand";
            } else {

                $cmd2c .= " -dMaxBitmap=400000000" if ( $a[2] == 0 );
                $cmd2c .= " -dMaxBitmap=10000"     if ( $a[2] == 1 );

                $cmd2c .= " -sDEVICE=" . $a[0];
                $cmd2c .= " -dGrayValues=256" if ( $a[0] eq 'bitrgb' );
                $cmd2c .= " -dcupsColorSpace=0" if ( $a[0] eq 'cups' );
                $cmd2c .= " -dDownScaleFactor=3 -dTIFFDateTime=false" if ( $a[0] eq 'tiffscaled' );
                $cmd2c .= " " . $additionalOptions;
                $cmd2c .= " " . $additionalOptions_gs if ( $product eq 'gs' || $product eq 'gpdl' || $product eq 'gpdf' );
                $cmd2c .= " -r" . $a[1];
                $cmd2c .= " -sPDFPassword=\"`cat $inputFilename.pwd`\""
                  if ( exists $testfiles{ $inputFilename . ".pwd" } );
                $cmd2c .= " -Z:" if ( !$valgrind );
                if ( $product eq 'gs' || $product eq 'gpdl' || $product eq 'gpdf' ) {
                    $cmd2c .= " -sDEFAULTPAPERSIZE=letter";
                }
                $cmd2c .= " -dNOPAUSE -dBATCH -K2000000";    # -Z:
                $cmd2c .= " -dClusterJob"
                  if ( !$local && !$valgrind && !$coverage );
                $cmd2c .= " -dJOBSERVER" if ( $product eq 'gs' );
                $cmd2extras .= " $extra_args" if ( length($extra_args) );

                if (($product eq 'gs' || $product eq 'gpdl') &&
                    ($inputFilename =~ m/ps3fts/ || $inputFilename =~ m/ps3cet/ )) {
                    # -dCETMODE runs gs_cet.ps from gs_init.ps, so it happens before we
                    # enter the job loop. We don't really need to run gs_cet.ps manually
                    # therefore, but we do so in case we test old versions of the software.
                    $cmd2c .= " -dCETMODE %rom%Resource/Init/gs_cet.ps";
                }

		# FIXME: gpdl_ below?
                if ( !( $filename =~ m/.pdf$/i || $filename =~ m/.ai$/i ) &&
                     ($product eq 'gs' || $product eq 'gpdl_') && !$valgrind ) {
                    $cmd2d .= " - < ";
                }

                $cmd2d .= " $inputFilename ";
            }

            if ($bmpcmp) {
                if ( $a[0] eq 'psdcmyk' || $a[0] eq 'psdcmyk16' || $a[0] eq 'psdcmykog' || $a[0] eq 'psdrgb' ) {
                    $cmd .= " ; touch $outputFilename.0000 ; rm $outputFilename.????";
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$outputFilename.%04d $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$outputFilename.%04d $cmd2extras $cmd2c $cmd2d ";
                    $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$outputFilename.gz";
                    $cmd .= " ; touch $outputFilename.0000 ; rm $outputFilename.????";
                    $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$outputFilename.%04d $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$outputFilename.%04d $cmd2extras $cmd2c $cmd2d ";
                    $cmd .= " ; cat $outputFilename.???? | gzip -1 -n -c >$baselineFilename.gz";
                    $cmd .= " ; touch $outputFilename.0000 ; rm $outputFilename.????";
                } elsif ( $a[0] eq 'tiffsep' || $a[0] eq 'tiffsep1' || $a[0] eq 'tiffg4' || $a[0] eq 'tiffg4' || $a[0] eq 'tiff24nc' ) {
                    $cmd .= " ; touch $outputFilename.0000.tif ; rm $outputFilename.*.tif";
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$outputFilename.%04d.tuf $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$outputFilename.%04d.tuf $cmd2extras $cmd2c $cmd2d ";
                    $cmd .= " ; cat $outputFilename.*.tif | gzip -1 -n -c >$outputFilename.gz";
                    $cmd .= " ; touch $outputFilename.0000.tif ; rm $outputFilename.*.tif";
                    $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$outputFilename.%04d.tif $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$outputFilename.%04d.tif $cmd2extras $cmd2c $cmd2d ";
                    $cmd .= " ; cat $outputFilename.*.tif | gzip -1 -n -c >$baselineFilename.gz";
                    $cmd .= " ; touch $outputFilename.0000.tif ; rm $outputFilename.*.tif";
                } elsif ( $a[0] eq 'tiffscaled' ) {
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$outputFilename $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$outputFilename $cmd2extras $cmd2c $cmd2d ";
                    $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                    $cmd .= " ; /bin/echo \"$preCommand $cmd2a -sOutputFile=$baselineFilename $cmd2extras $cmd2c $cmd2d\" ";
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile=$baselineFilename $cmd2extras $cmd2c $cmd2d ";
                    $cmd .= " ; convert $outputFilename pgm:- | gzip -1 -n -c > $outputFilename.gz ; touch $outputFilename ; rm $outputFilename";
                    $cmd .= " ; convert $baselineFilename pgm:- | gzip -1 -n -c > $baselineFilename.gz ; touch $baselineFilename ; rm $baselineFilename";
                } else {
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$outputFilename.gz' $cmd2extras $cmd2c $cmd2d ";

                    #$cmd.=" ; $preCommand $cmd2a -dNOTRANSPARENCY -sOutputFile='| gzip -1 -n >$outputFilename.gz' $cmd2c ";   #mhw
                    $cmd2a =~ s|__bin__/|$bmpcmpRef/|;
                    $cmd .= " ; $preCommand $cmd2a -sOutputFile='| gzip -1 -n -c >$baselineFilename.gz' $cmd2extras $cmd2c $cmd2d ";
                }
                $cmd .= " ; bash -c \"nice ./bmpcmp $bmpcmpOptions <(gunzip -c $outputFilename.gz) <(gunzip -c $baselineFilename.gz) $bmpcmpFilename 0 100 2>&1 | tee $bmpcmpFilename.out\"";
                #$cmd .= " ; nice gzip $bmpcmpFilename.* ";
                $cmd .= " ; ./checkSize.pl $bmpcmpFilename.*";
                if (   $bmpcmpFilename =~ m/\.72\./
                    || $bmpcmpFilename =~ m/\.75\./ )
                {
                    $cmd .= " ; cp $temp/$filename2.gz $bmpcmpDir/$filename2.new.gz ; cp $baselineRaster/$filename2.gz $bmpcmpDir/$filename2.baseline.gz";
                }
                $cmd .= " ; bash -c \"for (( c=1; c<=5; c++ )); do $flock rsync -a -e 'ssh -l regression -i \$HOME/.ssh/cluster_key' $bmpcmpFilename.* regression\@cluster.ghostscript.com:/home/regression/cluster/bmpcmp/. >/dev/null 2>&1 ; t=\\\$?; if [ \\\$t == 0 ]; then break; fi; /bin/echo 'rsync retry: ' \\\$c ; sleep 5 ; done\"";
            } else {
                $cmd .= " ; /bin/echo \"$cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d\" ";
                if (   $windows
                    && $a[0] ne 'tiffscaled'
                    && $a[0] ne 'psdcmyk'
                    && $a[0] ne 'psdcmyk16'
                    && $a[0] ne 'psdcmykog'
                    && $a[0] ne 'psdrgb')
                {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d | lzop -c >$outputFilename.lzo ; /bin/echo 'return:' \$? ";
                } else {
                    $cmd .= " ; $preCommand $cmd2a $cmd2b $cmd2extras $cmd2c $cmd2d ; /bin/echo 'return:' \$?";
                }
                if ( $a[0] eq 'tiffscaled' ) {

                    #$cmd.=" ; nice convert $outputFilename.tif pbm:$outputFilename ";
                    if (0) {
                        if ($windows) {

                           # for reasons that I don't understand the filenames passed to alchemy cannot begin with ./
                            my $file1 = $outputFilename;
                            $file1 =~ s|^\./||;
                            $cmd .= " ; nice /usr/local/bin/alchemy $file1.tif $file1.page. -U -Q -k ";
                        } else {
                            $cmd .= " ; nice /usr/local/bin/alchemy $outputFilename.tif $outputFilename.page. -U -Q -k ";
                        }
                        $cmd .= " ; cat $outputFilename.page.* > $outputFilename";
                        $cmd .= " ; touch $outputFilename.page.000 ; rm $outputFilename.page.*";
                    } else {
                        # Use convert rather than alchemy
                        $cmd .= " ; convert $outputFilename.tif pbm:$outputFilename ";
                    }
                    $cmd .= " ; touch $outputFilename.tif ; rm $outputFilename.tif ";
                    if ($md5sumOnly) {
                        $cmd .= " ; cat $outputFilename | nice md5sum >>$md5Filename";
                    } else {
                        if ($local) {
                            if ($windows) {
                                $cmd .= " ; cat $outputFilename | lzop -c >$outputFilename.lzo ; rm -f $outputFilename";
                            } else {
                                $cmd .= " ; nice lzop -f -U $outputFilename ";
                            }
                        } else {
                            $cmd .= " ; nice gzip -f $outputFilename ";
                        }
                    }
                }

                #./gs/bin/gs -I./gs/lib  -sOutputFile=./temp/tests_private__comparefiles__BFAZCFPB.PDF.psdcmykog.72.0.%04d -dMaxBitmap=400000000 -sDEVICE=psdcmykog   -r72 -Z: -sDEFAULTPAPERSIZE=letter -dNOPAUSE -dBATCH -K1000000 -dJOBSERVER ./tests_private/comparefiles/BFAZCFPB.PDF
                #cat ./temp/tests_private__comparefiles__BFAZCFPB.PDF.psdcmykog.72.0.???? | lzop -f -o ./temp/tests_private__comparefiles__BFAZCFPB.PDF.psdcmykog.72.0.lzo
                #touch ./temp/tests_private__comparefiles__BFAZCFPB.PDF.psdcmykog.72.0.0000
                #rm -f ./temp/tests_private__comparefiles__BFAZCFPB.PDF.psdcmykog.72.0.????

                if ( $a[0] eq 'psdcmyk' || $a[0] eq 'psdcmyk16' || $a[0] eq 'psdcmykog' || $a[0] eq 'psdrgb' ) {
                    $cmd .= " ; touch $outputFilename.0000";
                    if ($md5sumOnly) {
                        $cmd .= " ; cat $outputFilename.???? | nice md5sum >>$md5Filename";
                    } else {
                        if ($local) {
                            if ( !$performance ) {
                                $cmd .= " ; cat $outputFilename.???? | nice lzop -c > $outputFilename.lzo";
                            }
                        } else {
                            $cmd .= " ; cat $outputFilename.???? | nice gzip -f -c > $outputFilename.lzo";
                        }
                    }
                    $cmd .= " ; rm $outputFilename.????";
                } elsif ( $a[0] eq 'tiffsep' || $a[0] eq 'tiffsep1' || $a[0] eq 'tiffg4' || $a[0] eq 'tiffg4' || $a[0] eq 'tiff24nc' ) {
                    $cmd .= " ; touch $outputFilename.0000.tif";
                    if ($md5sumOnly) {
                        $cmd .= " ; cat $outputFilename.*.tif | nice md5sum >>$md5Filename";
                    } else {
                        if ($local) {
                            if ( !$performance ) {
                                $cmd .= " ; cat $outputFilename.*.tif | nice lzop -c > $outputFilename.lzo";
                            }
                        } else {
                            $cmd .= " ; cat $outputFilename.*.tif | nice gzip -f -c > $outputFilename.lzo";
                        }
                    }
                    $cmd .= " ; rm $outputFilename.*.tif";
                }

                if ( $local && !$performance ) {
                    $cmd .= " nice lzop -c -d $outputFilename.lzo | nice md5sum >$md5Filename";
                }

            }

        }
    }
    $cmd .= " ; /bin/date " if ($local);

    return ( $cmd, $outputFilenames, $filename2, $outprefix );
}

sub build($$$$$$$) {
    my $product_ref   = shift;    # gs|pcl|xps|ls|gpdf|gpdl|mupdf|mujstest
    my $inputFilename = shift;
    my $options       = shift;
    my $md5sumOnly    = shift;
    my $bmpcmp        = shift;
    my $customTest    = shift;
    my $storeFn       = shift;

    if (ref($product_ref) eq 'ARRAY') {
        my @product = @{$product_ref};
        foreach my $p (@product) {
            $storeFn->(do_build($p, $inputFilename, $options, $md5sumOnly, $bmpcmp, $customTest));
        }
    } else {
        $storeFn->(do_build($product_ref, $inputFilename, $options, $md5sumOnly, $bmpcmp, $customTest));
    }
}

my %slowFiles;
if ( open( F, "<slowfiles.lst" ) ) {
    while (<F>) {
        chomp;
        $slowFiles{$_} = 1;
    }
    close(F);
}

my %quickFiles;
if ( open( F, "<quickfiles.lst" ) ) {
    while (<F>) {
        chomp;
        $quickFiles{$_} = 1
          if ( !exists $slowFiles{$_} )
          ;    # if a file is listed in both, assume it is no longer quick
    }
    close(F);
}

my @quickCommands;
my @quickOutputFilenames;
#my @quickFilenames;
my @commands;
my @outputFilenames;
#my @filenames;
my @slowCommands;
my @slowOutputFilenames;
#my @slowFilenames;

sub build_if_matched($$$)
{
    my $test = shift;
    my $testfile = shift;
    my $product = shift;
    my $matched = 0;
    if ( $lowres == 1 && ( $test =~ m/\.72\./ || $test =~ m/\.75\./ ) )
    {
        $matched = 1;
    } elsif ( $highres == 1
        && ( $test =~ m/\.300\./ || $test =~ m/\.600\./ ) )
    {
        $matched = 1;
    } elsif ( $lowres == 0 && $highres == 0 ) {
        $matched = 1;
    }
    if ( $testfile =~ m/\.pwd$/ ) {
        $matched = 0;
    }

    if ( $matched && @filters != 0 ) {
        # Test the filters
        my $t = $testfile . "." . $test;
        $matched = 0;
        foreach my $filter (@filters) {
            my @terms = split ',', $filter;
            my $localmatch = 1;
            foreach my $term (@terms) {
                if ( $t =~ m/$term/ ) {
                } else {
                    $localmatch = 0;
                }
            }
            if ($localmatch) {
                $matched = 1;
            }
        }
    }

    sub store_build($$$$)
    {
        my $cmd = shift;
        my $outputFilenames = shift;
        my $filename = shift;
        my $outprefix = shift;
        if ( length($cmd) > 200 ) {
            if ( exists $quickFiles{$filename} ) {
                push @quickCommands,        [$cmd, $filename, $outprefix];
                push @quickOutputFilenames, $outputFilenames;
                #push @quickFilenames,       $filename;
            } elsif ( exists $slowFiles{$filename} ) {
                push @slowCommands,        [$cmd, $filename, $outprefix];
                push @slowOutputFilenames, $outputFilenames;
                #push @slowFilenames,       $filename;
            } else {
                push @commands,        [$cmd, $filename, $outprefix];
                push @outputFilenames, $outputFilenames;
                #push @filenames,       $filename;
            }
        }
    }

    if ($matched) {
        my $t=$testfile.".".$test;
        build( $product, $testfile, $test, !$local, 0, "", \&store_build );
    }
}

sub my_store($$$$)
{
    my $cmd = shift;
    my $outputFilenames = shift;
    my $filename = shift;
    my $outprefix = shift;
    push @commands,        [$cmd, $filename, $outprefix];
    push @outputFilenames, $outputFilenames;
    #push @filenames,       $filename;
}

if ($bmpcmp) {
    open( F, "$filename" ) || die "file $filename not found";
    my $done  = 0;
    my $start = 0;
    while (<F>) {
        chomp;

        #print "$start $done $_\n";
        my $cmd             = "";
        my $outputFilenames = "";
        my $filename        = "";
        s/^ +//;
        my @a = split ' ';
        if (m/ran \d+ tests in \d+ seconds/i) {
            $start = 1;
        } elsif (m/Differences in \d+ \(\+\d+ swallowed\) of \d+/) {
            $start = 1;
        }

        last if (m/previous clusterpush/);

        # if (m/The following \d+ regression file(s) had differences but matched at least once in the previous \d+ runs/i && $bmpcmphead != 0)
        if (m/matched at least once/i) {
            $done = 0;
        }
        if (
            $start
            && (   m/previous clusterpush/i
                || m/are producing errors/i
                || m/started timing out/i )
          )
        {
            $done = 1;
        }

        #print Dumper(\@a);

        if ( $start && !$done && scalar(@a) >= 4 ) {   # horrible, horrible hack

            #print Dumper(\@a);
            #($cmd,$outputFilenames,$filename)=build($testfiles{$testfile},$testfile,$test,!$local,0,"");
            #($cmd,$outputFilenames,$filename)=build($customTests{$customTest},"","",!$local,0,$customTest);
            #($cmd,$outputFilenames,$filename)=build($c,$a,$b,!$local,1,"");
            if ( exists $customTestFiles{ $a[0] } ) {
                build( $customTests{ $customTestFiles{ $a[0] } },
                       "", "", !$local, 1, $customTestFiles{ $a[0] },
                       \&my_store );
            } elsif (
                m/^(.+)\.(pdf\.p.mraw\.\d+\.[01]) (\S+) pdfwrite /
                || m/^(.+)\.(ps\.p.mraw\.\d+\.[01]) (\S+) ps2write /
                || m/^(.+)\.(xps\.p.mraw\.\d+\.[01]) (\S+) xpswrite /
                || m/^(.+)\.(pdf\.psdcmyk\.\d+\.[01]) (\S+) pdfwrite /
                || m/^(.+)\.(ps\.psdcmyk\.\d+\.[01]) (\S+) ps2write /
                || m/^(.+)\.(xps\.psdcmyk\.\d+\.[01]) (\S+) xpswrite /
                || m/^(.+)\.(pdf\.psdcmyk16\.\d+\.[01]) (\S+) pdfwrite /
                || m/^(.+)\.(ps\.psdcmyk16\.\d+\.[01]) (\S+) ps2write /
                || m/^(.+)\.(xps\.psdcmyk16\.\d+\.[01]) (\S+) xpswrite /
                || m/^(.+)\.(p.mraw\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(cups\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(pam\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(psdcmykog\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(psdrgb\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(bitrgbtags\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(tiffscaled\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(tiffsep\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(tiffsep1\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(tiffg4\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(tiff24nc\.\d+\.[01]) (\S+)/
                ||

                #m/^(.+)\.(png\.\d+\.[01]) (\S+)/ ||  # bmpcmp doesn't work reading gz compressed png files, i.e. bmpcmp <(gunzip -c file1.gz) <(gunzip -c file2.gz)
                m/^(.+)\.(psdcmyk\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(psdcmyk16\.\d+\.[01]) (\S+)/
                || m/^(.+)\.(plank\.\d+\.[01]) (\S+)/
              )
            {
                my $a       = $1;
                my $b       = $2;
                my $c       = $3;
                my $matched = 1;

                if ( scalar @filters != 0 ) {    # Test the filters
                    my $t = $a . "." . $b;
                    $matched = 0;
                    foreach my $filter (@filters) {
                        my @terms = split ',', $filter;
                        my $localmatch = 1;
                        foreach my $term (@terms) {
                            if ( $t =~ m/$term/ ) {
                            } else {
                                $localmatch = 0;
                            }
                        }
                        if ($localmatch) {
                            $matched = 1;
                        }
                    }
                }

                if ( $matched ) {
                    # build relies on being able to check for a .pwd file
                    # in the testfiles hash.
                    my $filehack = $a;
                    $filehack =~ s/^tests_private\//tests_private.git\//;
                    $filehack =~ s/^tests\//tests.git\//;
                    if (-e $filehack.'.pwd') {
                        $testfiles{$a.'.pwd'} = "bmpcmp";
                    }

                    build( $c, $a, $b, !$local, 1, "", \&my_store );
                }
            }
            else {
                #print "Ignoring $_\n";
            }
        }
    }
    close(F);
} else {
    foreach my $testfile ( sort keys %testfiles ) {
        if (ref $testfiles{$testfile} eq 'ARRAY') {
            my @arr = @{$testfiles{$testfile}};
            foreach my $tf (@arr) {
                foreach my $test ( @{ $tests{ $tf } } ) {
                    build_if_matched($test, $testfile, $tf);
                }
            }
        } else {
            foreach my $test ( @{ $tests{ $testfiles{$testfile} } } ) {
                build_if_matched($test, $testfile, $testfiles{$testfile});
            }
        }
    }

    sub store_filtered($$$$)
    {
        my $cmd             = shift;
        my $outputFilenames = shift;
        my $filename        = shift;
        my $outprefix       = shift;
        my $matched         = 1;
        if ( scalar @filters != 0 ) {
            # Test the filters
            $matched = 0;
            foreach my $filter (@filters) {
                my @terms = split ',', $filter;
                my $localmatch = 1;
                foreach my $term (@terms) {
                    if ( $filename =~ m/$term/ ) {
                    } else {
                        $localmatch = 0;
                    }
                }
                if ($localmatch) {
                    $matched = 1;
                }
            }
        }
        if ($matched) {
            push @commands,        [$cmd, $filename, $outprefix];
            push @outputFilenames, $outputFilenames;
            #push @filenames,       $filename;
        }
    }

    foreach my $customTest ( sort keys %customTests ) {
        build( $customTests{$customTest}, "", "", !$local, 0, $customTest, \&store_filtered );
    }

}

sub splitCommand($) {
    my $acmd = $_[0][0];
    my $a    = $_[0][1];
    if ($acmd =~ m/sDEVICE=pdfwrite/ &&
        $a =~ m/^(.+)\.(pdf\.p.mraw)\.(\d+)\.([01])/) {
        return ($1,$2,$3,$4);
    } elsif ($acmd =~ m/sDEVICE=ps2write/ &&
             $a =~ m/^(.+)\.(ps\.p.mraw)\.(\d+)\.([01])/) {
        return ($1,$2,$3,$4);
    } elsif ($acmd =~ m/sDEVICE=xpswrite/ &&
             $a =~ m/^(.+)\.(xps\.p.mraw)\.(\d+)\.([01])/) {
        return ($1,$2,$3,$4);
    } elsif (   $a =~ m/^(.+)\.(p.mraw)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(cups)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(pam)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(psdcmykog)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(psdrgb)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(bitrgbtags)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(tiffscaled)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(tiffsep)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(tiffsep1)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(tiffg4)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(tiff24nc)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(psdcmyk)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(psdcmyk16)\.(\d+)\.([01])/
        || $a =~ m/^(.+)\.(plank)\.(\d+)\.([01])/) {
        return ($1,$2,$3,$4);
    }
    return undef;
}

sub bmpcmpOrder($$) {
    my $a = $_[0][1];
    my $b = $_[1][1];
    my $acmd = $_[0][0];
    my $bcmd = $_[1][0];
    my $afile;
    my $adev;
    my $ares;
    my $aband;
    my $bfile;
    my $bdev;
    my $bres;
    my $bband;
    ($afile, $adev, $ares, $aband) = splitCommand($_[0]);
    ($bfile, $bdev, $bres, $bband) = splitCommand($_[1]);
    if (!defined $afile || !defined $bfile || $afile ne $bfile) {
        return $a cmp $b;
    }
    # pdfwrite differences override all others.
    if ($adev eq "pdf.ppmraw") {
        return -1;
    }
    if ($bdev eq "pdf.ppmraw") {
        return 1;
    }
    # then ps2write
    if ($adev eq "ps.ppmraw") {
        return -1;
    }
    if ($bdev eq "ps.ppmraw") {
        return 1;
    }
    # then xpswrite
    if ($adev eq "xps.ppmraw") {
        return -1;
    }
    if ($bdev eq "xps.ppmraw") {
        return 1;
    }
    # then psdcmykog
    if ($adev eq "psdcmykog") {
        return -1;
    }
    if ($bdev eq "psdcmykog") {
        return 1;
    }
    # then psdcmyk16
    if ($adev eq "psdcmyk16") {
        return -1;
    }
    if ($bdev eq "psdcmyk16") {
        return 1;
    }
    # then psdcmyk
    if ($adev eq "psdcmyk") {
        return -1;
    }
    if ($bdev eq "psdcmyk") {
        return 1;
    }
    # then psdcmykog
    if ($adev eq "psdrgb") {
        return -1;
    }
    if ($bdev eq "psdrgb") {
        return 1;
    }
    # then ppmraw
    if ($adev eq "ppmraw") {
        return -1;
    }
    if ($bdev eq "ppmraw") {
        return 1;
    }
    # then pgmraw
    if ($adev eq "pgmraw") {
        return -1;
    }
    if ($bdev eq "pgmraw") {
        return 1;
    }
    # then bitrgbtags
    if ($adev eq "bitrgbtabs") {
        return -1;
    }
    if ($bdev eq "bitrgbtags") {
        return 1;
    }
    # then pkmraw
    if ($adev eq "pkmraw") {
        return -1;
    }
    if ($bdev eq "pkmraw") {
        return 1;
    }
    # then pam
    if ($adev eq "pam") {
        return -1;
    }
    if ($bdev eq "pam") {
        return 1;
    }
    # then plank
    if ($adev eq "plank") {
        return -1;
    }
    if ($bdev eq "plank") {
        return 1;
    }
    # then pbmraw
    if ($adev eq "pbmraw") {
        return -1;
    }
    if ($bdev eq "pbmraw") {
        return 1;
    }
    # then cups
    if ($adev eq "cups") {
        return -1;
    }
    if ($bdev eq "cups") {
        return 1;
    }
    # then tiffscaled
    if ($adev eq "tiffscaled") {
        return -1;
    }
    if ($bdev eq "tiffscaled") {
        return 1;
    }
    # then tiffsep
    if ($adev eq "tiffsep") {
        return -1;
    }
    if ($bdev eq "tiffsep") {
        return 1;
    }
    if ($aband > $bband) {
        return -1;
    } else {
        return 1;
    }
    if ($ares > $bres) {
        return -1;
    } else {
        return 1;
    }
    return $a cmp $b;
}

sub bmpcmpMasks($$$$$$)
{
    my $adev = shift;
    my $ares = shift;
    my $aband = shift;
    my $bdev = shift;
    my $bres = shift;
    my $bband = shift;

    if ($adev eq $bdev) {
        if ($aband > $bband) {
            return 1;
        }
        if ($ares > $bres) {
            return 1;
        }
    } else {
        if ($adev eq "pdf.ppmraw") {
            if ($bdev eq "ps.ppmraw") {
                return 1;
            }
        } elsif ($adev eq "ppmraw") {
            if ($bdev eq "pgmraw" ||
                $bdev eq "pkmraw" ||
                $bdev eq "pam" ||
                $bdev eq "plank" ||
                $bdev eq "pbmraw" ||
                $bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "pgmraw") {
            if ($bdev eq "pkmraw" ||
                $bdev eq "pam" ||
                $bdev eq "plank" ||
                $bdev eq "pbmraw" ||
                $bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "pkmraw") {
            if ($bdev eq "pam" ||
                $bdev eq "plank" ||
                $bdev eq "pbmraw" ||
                $bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "pam") {
            if ($bdev eq "plank" ||
                $bdev eq "pbmraw" ||
                $bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "plank") {
            if ($bdev eq "pbmraw" ||
                $bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "pbmraw") {
            if ($bdev eq "cups" ||
                $bdev eq "tiffscaled" ||
                $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "cups") {
            if ($bdev eq "tiffscaled" ||
	        $bdev eq "tiffsep") {
                return 1;
            }
        } elsif ($adev eq "psdcmykog") {
            if ($bdev eq "psdcmyk" ||
                $bdev eq "psdcmyk16") {
                return 1;
            }
        } elsif ($adev eq "psdcmyk16") {
            if ($bdev eq "psdcmyk") {
                return 1;
            }
        }
    }
    return 0;
}


# Cull the tests for bmpcmp
if ($bmpcmp) {
    print "#Sorting ".scalar(@commands)."\n";
    # First, sort them, cleverly
    my @sortedCommands = sort bmpcmpOrder @commands;
    @commands = @sortedCommands;
}
if ($bmpcmp && $cull) {
    # Then cull them.
    my $n = scalar(@commands);
    my $removed = 0;
    my $i;
    for ($i = 0; $i+$removed < $n; $i++) {
        my $afile;
        my $adev;
        my $ares;
        my $aband;
        $commands[$i] = $commands[$i+$removed];
        ($afile, $adev, $ares, $aband) = splitCommand($commands[$i]);
        for (my $j = $i+$removed+1; $j < $n; $j++) {
            my $bfile;
            my $bdev;
            my $bres;
            my $bband;
            ($bfile, $bdev, $bres, $bband) = splitCommand($commands[$j]);
            if ($afile ne $bfile) {
                last;
            }
            if (bmpcmpMasks($adev,$ares,$aband,$bdev,$bres,$bband)) {
                $removed++;
            }
        }
    }
    splice(@commands, $i);
    print "#After culling we have ".scalar(@commands)."\n";
}


# Restrict the tests for bmpcmp
if ($bmpcmp) {
    #splice( @filenames, 0, $bmpcmpStart );
    #splice( @filenames, $bmpcmpCount );
    splice( @commands, 0, $bmpcmpStart );
    splice( @commands, $bmpcmpCount );
}


if (   !$coverage
    && !$bmpcmp
    && !$local
    && ( scalar keys %products == 0 || exists $products{'gs'} ) )
{
    print "build\tbuild\tcd __gsSource__ ; make clean >/dev/null 2>&1 ; make CLUSTER=1 -j 1 >__temp__/build.log 2>&1\n";
}
if ( !$coverage && !$bmpcmp && !$local &&
     (exists $products{'mupdf'} || exists $products{'mujstest'}) ) {
    print "build\tbuild\tcd __mupdfSource__ ; make clean >/dev/null 2>&1 ; make CLUSTER=1 -j 1 >__temp__/build.log 2>&1\n";
}
if ( !$coverage && !$bmpcmp && !$local &&
     exists $products{'gsview'} ) {
    print "build\tbuild\tcd __gsviewSource__ ; make clean >/dev/null 2>&1 ; make CLUSTER=1 -j 1 >__temp__/build.log 2>&1\n";
}
if ( !$coverage && !$bmpcmp && !$local &&
     exists $products{'mupdfmini'} ) {
    print "build\tbuild\tcd __mupdfminiSource__ ; ./gradlew clean >/dev/null 2>&1 ; ./gradlew assembleRelease >__temp__/build.log 2>&1\n";
}

while ( scalar(@slowCommands) || scalar(@commands) ) {

    #printf "%d %d\n",scalar(@slowCommands),scalar(@commands);
    my $n = 0;
    my $filename;
    my $command;
    my $prefix;
    if ( scalar @slowCommands ) {
        $n = rand( scalar @slowCommands ) if ( !$bmpcmp );
        $command = $slowCommands[$n][0];
        $filename = $slowCommands[$n][1];
        $prefix = $slowCommands[$n][2];
        splice( @slowCommands, $n, 1 );
        print "$filename\t$prefix\t$command\n";
    }
    for ( my $i = 0 ; $i < 9 && scalar(@commands) ; $i++ ) {
        $n = rand( scalar @commands ) if ( !$bmpcmp );
        $command = $commands[$n][0];
        $filename = $commands[$n][1];
        $prefix = $commands[$n][2];
        splice( @commands, $n, 1 );
        print "$filename\t$prefix\t$command\n";
    }
}

while ( scalar(@quickCommands) ) {
    my $n = 0;
    $n = rand( scalar @quickCommands ) if ( !$bmpcmp );
    my $command = $quickCommands[$n][0];
    my $filename = $quickCommands[$n][1];
    my $prefix = $quickCommands[$n][2];
    splice( @quickCommands, $n, 1 );
    print "$filename\t$prefix\t$command\n";
}
