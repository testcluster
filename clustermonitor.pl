#!/usr/bin/perl

use strict;
use warnings;

use Time::localtime;
use File::stat;

use Data::Dumper;


my %status;

my $b="";

my $lastCols=0;
my $lastRows=0;

my @lastMachines;

my %order;
my $count=0;

if (open(F,"</home/regression/c.order")) {
  while(<F>) {
    chomp;
    $order{$_}=$count++;
  }
  close(F);
}


while(1) {

  my @machines = </home/regression/cluster/*.status>;

  for (my $i=0;  $i<scalar(@machines);  $i++) {
    $machines[$i]=~s/.status//;
    $machines[$i]=~s|.*cluster/||;
    if ($machines[$i]=~m/^aws/ && stat("/home/regression/cluster/$machines[$i].up") && time-(stat("/home/regression/cluster/$machines[$i].up")->mtime)>500) {
      splice(@machines,$i,1);
      $i--;
    } else {
    if (scalar(@lastMachines)<$i || $lastMachines[$i] ne $machines[$i]) {
      print chr(0x1b)."[1;1H";  # clear screen
      print chr(0x1b)."[J";
      undef %status;  # force refresh
      $b="";
    }
    }
  }
  @lastMachines=@machines;

#print "@machines\n";
  my ($rows,$cols) = split(/ /,`/bin/stty size`);
  chomp $cols;
  if ($rows != $lastRows || $cols != $lastCols) {
    $lastRows=$rows;
    $lastCols=$cols;
    print chr(0x1b)."[1;1H";  # clear screen
    print chr(0x1b)."[J";
    undef %status;   # force refresh
    $b="";
  }

  my $status=`cat /home/regression/cluster/status`;
  chomp $status;

  if (!exists $status{'main'}{"status"} || $status{'main'}{"status"} ne $status) {
    print chr(0x1b)."[".(1).";1H";
    $status=substr($status,0,$cols-1);
    printf "%s".chr(0x1b)."[K\n",$status;
#   print chr(0x1b)."[".(scalar(@machines)+2).";1H";
    print chr(0x1b)."[".($count+2).";1H";
    $status{'main'}{"status"}=$status;
  }

  for (my $i=0;  $i<scalar(@machines);  $i++) {
    my $machine=$machines[$i];

    my $s1=`cat /home/regression/cluster/$machine.contrib`;
    chomp $s1;
    my $s0=`cat /home/regression/cluster/$machine.status`;
    chomp $s0;
    my $down="";
    my $downTime=0;
    if (stat("/home/regression/cluster/$machine.up")) {
      $downTime=time-(stat("/home/regression/cluster/$machine.up")->mtime);
    }
    my $duration=sprintf "%d minutes",int($downTime/60);
    $duration=sprintf "%d hours",int($downTime/3600) if ($downTime>7200);
    $duration=sprintf "%d days",int($downTime/(3600*24)) if ($downTime>7200*24);
    $down="--DOWN ($duration)--" if (!stat("/home/regression/cluster/$machine.up") || $downTime>500);
    if (!exists $status{$machine}{"status"} || $status{$machine}{"status"} ne $s0 || $status{$machine}{"down"} ne $down) {
      $status{$machine}{"status"}=$s0;
      $status{$machine}{"down"}=$down;
#     print chr(0x1b)."[".($i+2).";1H";
      print chr(0x1b)."[".($order{$machine}+2).";1H";
      $s0=substr($s0,0,$cols-19-length($down));
      printf "%-10s %5.2f %s %s".chr(0x1b)."[K\n",$machine,$s1,$down,$s0;
#     print chr(0x1b)."[".(scalar(@machines)+2).";1H";
      print chr(0x1b)."[".($count+2).";1H";
    }
    my $a=`cat /home/regression/cluster/queue.lst`;
    if ($a ne $b) {
#     print chr(0x1b)."[".(scalar(@machines)+3).";1H";
      print chr(0x1b)."[".($count+3).";1H";
      print "Current and pending jobs:\n";
      print chr(0x1b)."[J";
#    print $a;
      my @a=split '\n',$a;
#     for (my $i=0;  $i<scalar(@a) && $i<$rows-(scalar(@machines)+3+1);  $i++) {
      for (my $i=0;  $i<scalar(@a) && $i<$rows-($count+3+1);  $i++) {
        print "$a[$i]\n";
      }
      $b=$a;
      undef %status;   # force refresh
    }
  }

  sleep 5;
}

