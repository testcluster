#!/usr/bin/perl

# Generate a table showing the history of files that have changed between
# given revisions. This script must be run on the cluster master machine
# (current casper).
#
#   --revision=<revision>
#   -r=<revision>
#        Final revision. (Initial revision is taken to be the one before)
#   --revision=<revision>:<revision>
#   -r=<revision>:<revision>
#        Initial/Final revision.
#   If -r is not specified -r is taken to be the most recent revision
#
#   --numrevs=<num>
#   -n=<num>
#        NumRevs. Maximum number of revisions to include.
#   If unspecified taken to be 20.
#
#   --to=<revision>
#   -t=<revision>
#        To revision. Latest revision to include in the table.
#   If omitted taken to be the most recent revision in this branch
#
#   --from=<revision>
#   -f=<revision>
#        From revision. Earliest revision to include in the table.
#   If omitted taken to be NumRevs before that given by -t.
#
#   -p=ghostpdl|mupdf
#   If omitted, assumed to be ghostpdl
#
# The table is generated from 'From' revision to 'To' revision,
# to a maximum of 'NumRevisions'. Lines are only included in the output
# table if the results differ between 'Initial' and 'Final' revisions.
#
# Probably best to explain with reference to some examples.
#
# 1) To show all the files that revision 1234 caused to change:
#   showtestchanges.pl -r=1234
#
# 2) To show all the files that changed between revision 1234 and 1240:
#   showtestchanges.pl -r1234:1240
#
# 3) To show all the files that changes on the icc branch between
# revision 1234 and 1240 with some specific context:
#   showtestchanges.pl -r1234:1240 -f=1200 -t=1260 -b=icc_work

use strict;
use warnings;
use POSIX;
use Getopt::Long;

setpriority(0,0,10);

# Subroutines
sub machinekey
{
  my (@machinelist) = @{$_[0]};
  my (@entries) = @{$_[1]};
  my $temp;
  my $key = 0;
  foreach $temp (@entries)
  {
    my $i = 0;
    my $temp2;
    foreach  $temp2 (@machinelist)
    {
      if ($temp2 eq $temp)
      {
        $key |= (1<<$i);
      }
      $i++;
    }
  }
  return $key;
}

# Main starts here

# Set defaults
my $numrevs = 20; # How many revisions of history to display?
my $final;
my $initial;
my $from;
my $to;
my $branch;
my $project;
my $maxlines = 1000;

# Set to 1 to take input from 'testfile' in PWD rather than to generate from
# scratch.
my $test = 0;

# You won't need to change this.
my $clusteroot = '/home/regression/cluster';

# Let's parse some command line parameters
GetOptions("r|revision=s" => \$final,
  "n|numrevs=i"  => \$numrevs,
  "f|from=i"     => \$from,
  "t|to=i"       => \$to,
  "p|project=s"  => \$project);

print STDERR "comparerevs.pl:<br>\n";
if (defined($final)) {
  print STDERR "  final   = $final<br>\n";
}
if (defined($from)) {
  print STDERR "  from    = $from<br>\n";
}
if (defined($to)) {
  print STDERR "  to      = $to<br>\n";
}
if (defined($project)) {
  print STDERR "  project = $project<br>\n";
}

# Interpret the options

# Get the possible list of versions for this branch, sorted by time
my $archivedir = "$clusteroot/archive";

if ($project eq "mupdf") {
  $archivedir = "$clusteroot/mupdf-archive";
}

my @versions = <$archivedir/*.tab>;
@versions = map { m/(.*\/.+\.tab)/ } @versions;
my @sorted = map {$_->[0]} sort {$b->[1]<=>$a->[1]} map {[$_, -M]} @versions;
my @rawsorted = map { m/.*\/(.+)\.tab/ } @sorted;

if (!defined($final)) {
# None specified
  $final   = $rawsorted[-1];
  $initial = $rawsorted[-2];
  $final   =~ s/.*\/(.+)\.tab/$1/;
  $initial =~ s/.*\/(.+)\.tab/$1/;
} elsif ($final =~ m/(.+):(.+)/) {
# Initial:Final
  $initial = $1;
  $final   = $2;
} elsif ($final =~ m/(.+)/) {
# Final
  $final = $1;
  ($initial) = grep { $rawsorted[$_] eq $final } 0..$#rawsorted;
  $initial = $rawsorted[$initial-1];
}

my $finalindex;
my $initialindex;
($finalindex) = grep { $rawsorted[$_] eq $final } 0..$#rawsorted;
($initialindex) = grep { $rawsorted[$_] eq $initial } 0..$#rawsorted;

# Make $to and $from be the index rather than the revision
if (defined($to)) {
  ($to) = grep { $rawsorted[$_] eq $to } 0..$#rawsorted;
}
if (!defined($from)) {
  $from = 0;
} else {
  ($from) = grep { $rawsorted[$_] eq $from } 0..$#rawsorted;
  if (defined($to)) {
    $numrevs = $to-$from;
  }
}
if (!defined($to)) {
  $to = $#rawsorted;
}

# Constrain to $numrevs
#print STDERR "Initial:Final = $initial:$final<BR>\n";
#print STDERR "InitialIndex:FinalIndex = $initialindex:$finalindex<BR>\n";
#print STDERR "From $from To $to\n<BR>";
my $reduceby = $to-$from-$numrevs;
my $d;
if ($reduceby > 0) {
    $d = $to - $finalindex;
    if ($d > 0) {
        if ($d > $reduceby) {
            $d = $reduceby;
        }
        $to = $to - $d;
#print STDERR "Reducing top end to $to\n<BR>";
        $reduceby -= $d;
    }
}
print STDERR "From $from To $to<br>\n";
if ($reduceby > 0) {
    $d = $initialindex-$from;
    if ($d > 0) {
        if ($d > $reduceby) {
            $d = $reduceby;
        }
        $from = $from + $d;
#print STDERR "Reducing bottom end to $from\n<BR>";
        $reduceby -= $d;
    }
}

print STDERR "Initial:Final = $initial:$final<br>\n";
print STDERR "From $from To $to<br>\n";
print STDERR "$rawsorted[$from] to $rawsorted[$to]<br>\n";

@rawsorted = splice(@rawsorted,$from,$to-$from+1);
@sorted    = splice(@sorted,   $from,$to-$from+1);
@versions  = @rawsorted;


#print STDERR "revision range $initial $final:\n";
#foreach my $v (@rawsorted) {
#    print STDERR "$v\n";
#}

my $mainfile = tmpnam();

if ($test != 0)
{
  $mainfile = "testfile";
} else {
  print STDERR "Processing $sorted[0]<br>\n";
  system("cut -f1,2,7,10 ".shift(@sorted)." | sort -k1b,1 > $mainfile");
#print STDERR "$mainfile\n";

  foreach my $version (@sorted) {
    print STDERR "Processing $version<br>\n";
    my $tempfile1 = tmpnam();
    my $tempfile2 = tmpnam();
    system("cut -f1,2,7,10 $version | sort -k1b,1 > $tempfile1");
    #system("/home/robin/bin/fastjoin $mainfile $tempfile1 > $tempfile2");
    system("join --nocheck-order -j1 $mainfile $tempfile1 | head $maxlines > $tempfile2");
#print STDERR "$tempfile1\n";
    unlink($tempfile1);
    unlink($mainfile);
    $mainfile = $tempfile2;
  }
}
#print STDERR "<br>$mainfile\n<br>";
#my $file;
#if (open($file, "<$mainfile")) {
#    while (<$file>)
#    {
#        print STDERR "$_<br>";
#    }
#    close $file;
#}

print "<HTML><HEAD><TITLE>Test results by revision</TITLE>\n";
print "<SCRIPT>\n";
print "var visible = 0;\n";
print "var undefined;\n";
print "function toggleBorders(i) {\n";
print "  visible = visible^(1<<i);\n";
print "  updateBorders();\n";
print "}\n";
print "function updateBorders() {\n";
print "  var i = 0;\n";
print "  var cell;\n";
print "  for (i=0; i < maxcell; i++) {\n";
print "    cell = 'cell'+i;\n";
print "    var obj = document.getElementById(cell);\n";
print "    if (obj == undefined) continue;\n";
print "    var key = obj.getAttribute(\"key\");\n";
print "    if (key & visible) obj.style.border = \"solid 1px #00F\"; else obj.style.border = \"solid 0px\";\n";
print "  }\n";
print "}\n";
print "</SCRIPT>";
print "</HEAD><BODY>\n";
print "<H1>Files that changed between revisions $initial and $final.</H1>\n";
print "<TABLE>";

my $headers = 0;
my $errors_new  = 0;
my $errors_old  = 0;
my $hashchanges = 0;
my $cellseq = 0;
my @machinelist = ();

open(FH,"<$mainfile");
while (<FH>)
{
#    print STDERR "$_<br>\n";
  my @fields = split;
  my $line = "<tr align=center><td align=right>";
  my $file;
  my $dev;
  my $res;
  my $band;

#  foreach my $xxx (@fields)
#  {
#      print STDERR "$xxx ";
#  }
#  print STDERR "<br>\n";
  $_ = shift(@fields);
  ($file,$dev, $res, $band) = m/(.*)\.(.*)\.(\d*)\.(\d)/;
  if (!defined($band))
  {
    next;
  }

  $line .= "$file<td>$dev<td>$res<td>$band";
#  print STDERR "$line<BR>\n";

  my $dull = 1;

  my $oldhash  = undef;
  my %hashes   = ();
  my $change   = 0;
  my $acc      = "";
  my $acccols  = 0;
  my $accend   = "";
  my @accmachines = ();
  my $olderr   = undef;
  my $fieldnum = $from;
  my $initial_err;
  my $initial_hash;
  my $final_err;
  my $final_hash;

  while (@fields)
  {
    my $err     = shift(@fields);
    my $hash    = shift(@fields);
    my $machine = shift(@fields);

    if (grep {$_ eq $machine} @machinelist) {
# Already in
    } else {
      push @machinelist, ($machine);
    }

# Ignore hash changes on timeouts
    if ($err == 3 || $err == 4) {
      $hash = 0;
    }
    if ($fieldnum == $initialindex) {
      $initial_err  = $err;
      $initial_hash = $hash;
    } elsif ($fieldnum == $finalindex) {
      $final_err  = $err;
      $final_hash = $hash;
    }

    if ($err != 3 && $err != 4) {
# No error, or an error that wasn't a timeout
# If the hash values match, and either no previous error value
# or a matching one, then just extend the run
      if (defined($oldhash) && $oldhash eq $hash && (!defined($olderr) || $olderr eq $err)) {
        $acccols++;
      } else {
        if ($acccols != 0) {
          $line .= $acc;
          if ($acccols != 1) {
            $line .= " colspan=$acccols";
          }
          if (scalar(@accmachines) != 0) {
            @accmachines = sort @accmachines;
            $line .= " title=\"".join(",",@accmachines)."\"";
            $line .= " key=".machinekey(\@machinelist, \@accmachines);
            @accmachines = ();
          }
          $line .= $accend;
        }
        $acccols = 1;
        if (!defined($hashes{$hash})) {
          $hashes{$hash} = $change++;
        }
        if ($hashes{$hash} == 0) {
          $acc = "<td bgcolor=#80ff80"; # Green
        } else {
          $acc = "<td bgcolor=#ff8040"; # Orange
          $dull = 0;
        }
        $acc .=" id=cell$cellseq";
        $cellseq++;
        $accend = ">".$hashes{$hash};
        if ($err) {
          $accend .= "<span style=\"background-color:#ff4040\">($err)</span>";
        }
      }
      $oldhash = $hash;
      $olderr = undef;
    } else {
      if (defined($olderr) && $olderr == $err) {
        $acccols++;
      } else {
        if ($acccols != 0) {
          $line .= $acc;
          if ($acccols != 1) {
            $line .= " colspan=$acccols";
          }
          if (scalar(@accmachines) != 0) {
            @accmachines = sort @accmachines;
            $line .= " title=\"".join(",",@accmachines)."\"";
            $line .= " key=".machinekey(\@machinelist, \@accmachines);
            @accmachines = ();
          }
          $line .= $accend;
        }
        $acc = "<td bgcolor=#ff4040"; #Red
        $acc .=" id=cell$cellseq";
        $cellseq++;
        $acccols = 1;
        $accend = ">($err)";
        $dull = 0;
      }
      $olderr = $err;
    }
    if (grep {$_ eq $machine} @accmachines) {
# Already in
    } else {
      push @accmachines, ($machine);
    }
    $fieldnum++;
  }
  if ($acccols != 0) {
    $line .= $acc;
    if ($acccols != 1) {
      $line .= " colspan=$acccols";
    }
    if (scalar(@accmachines) != 0) {
      @accmachines = sort @accmachines;
      $line .= " title=\"".join(",",@accmachines)."\"";
      $line .= " key=".machinekey(\@machinelist, \@accmachines);
      @accmachines = ();
    }
    $line .= $accend;
  }
  $line .= "\n";
  if (($initial_err == 0) && ($final_err != 0)) {
    $errors_new++;
  } elsif (($initial_err != 0) && ($final_err == 0)) {
    $errors_old++;
  } elsif ($initial_hash ne $final_hash) {
    $hashchanges++;
  }
  if (($initial_err == $final_err) && ($initial_hash eq $final_hash)) {
#        print STDERR "Dull\n";
    $dull = 1;
  }
  if (!$dull) {
#        print STDERR "I:$initial_err $initial_hash\n";
#        print STDERR "F:$final_err $final_hash\n";
    if ($headers-- == 0)
    {
      print "<TR bgcolor=#8080ff><TH>File<TH>Dev<TH>Res<TH>Band";
      $fieldnum = $from;
      foreach my $version (@versions)
      {
        print "<TH";
        if (($fieldnum == $initialindex) || ($fieldnum == $finalindex)) {
          print " bgcolor=#ff8080";
        }
        open(EMAIL, "$archivedir/$version/email.txt");
        my $git1line = <EMAIL>;
        close(EMAIL);
        print "><a href=\"http://git.ghostscript.com/?p=ghostpdl.git;a=commit;h=$version\" title=\"$git1line $version\">".substr($version,0,7)."</a>";
        $fieldnum++;
      }
      print "\n";
      $headers += 50;
    }

    print $line;
  }
}
close(FH);
if ($test == 0) {
  unlink($mainfile);
#print STDERR "$mainfile\n";
}

print "</TABLE>\n";
print "<H2>Machines used (Click to highlight):</H2>";
print "<TABLE><TR>";
my $temp;
my $i = 0;
foreach $temp (@machinelist)
{
  print "<TD id=\"cell$cellseq\" key=".(1<<$i)."><A href=\"javascript:toggleBorders($i)\">$temp</A></TD>";
  $i++;
  $cellseq++;
}
print "</TR></TABLE>";
print "<SCRIPT>var maxcell=$cellseq;</SCRIPT>\n";
print "<P>$errors_new new errors.</P>\n";
print "<P>$errors_old fixed errors.</P>\n";
print "<P>$hashchanges changed images.</P>\n";
print "</BODY>";
