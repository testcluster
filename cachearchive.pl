#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my $previousValues=25;

my $mupdf=shift;

my %current;
my %archiveCache;

my $revs;
my $repository="ghostpdl";
my $archive="archive";
if ($mupdf && $mupdf eq "mupdf") {
  $repository="mupdf";
  $archive="mupdf-archive";
  $revs=`cd $repository ; git rev-list master`;
} elsif ($mupdf && $mupdf eq "mujstest") {
  $repository="mupdf";
  $archive="mujstest-archive";
  $revs=`cd $repository ; git rev-list master`;
} else {
  $revs=`cd $repository ; git rev-list master`;
}

my @revs=split '\n',$revs;

sub myCmp($$) {
  my $a=shift;
  my $b=shift;
  $a=~m/(\d+)/;
  my $a1=$1;
  $b=~m/(\d+)/;
  my $b1=$1;
  return($b1 cmp $a1);
}

my $count=$previousValues;
foreach my $i (@revs) {
  chomp $i;
# print STDERR "$i\n";
  if ($count>0) {
#   print STDERR "reading $archive/$i.tab\n";
    if (open(F,"<$archive/$i.tab")) {
      while(<F>) {
        chomp;
        s|__|/|g;
#print STDERR "$i $_\n";
        my @a=split '\t';
        my $r=$i;
        $a[6]=$a[1] if ($a[1] ne "0");  # if there is an error code store the error code instead of the md5sum
        my $key=$a[0].' '.$a[6];
        if ($count==$previousValues) {
          $current{$key}=1;
        } else {
          if (!exists $current{$key} && !exists $archiveCache{$key}) {
            $archiveCache{$key}=$r."\t".$a[8]."\t".$a[9]."\t".($previousValues-$count+1);
          }
        }
      }
      close(F);
      $count--;
    }
  }
}

#print Dumper(\%archiveCache);

print "$previousValues\n";
foreach my $i (sort keys %archiveCache) {
  print "$i | $archiveCache{$i}\n";
}

