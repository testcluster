#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;
use POSIX ":sys_wait_h";

my $file="";
my $error=0;
my $md5sum=0;
my $divider=0;
my $file_prod="";

my $local=0;
my $emptyMd5sumPDF="d41d8cd98f00b204e9800998ecf8427e";
my $emptyMd5sumXPS="d13b1d9bad19f53765eb1486b71ce427";

my $t1;
my $t2;
my $mem1=0;
my $mem2=0;

my %results;
my %bmpcmpErrors;

my $bmpcmp=0;
my $input=shift;
if ($input && $input eq "bmpcmp") {
  $bmpcmp=1;
  $input=shift;
}
my $output=shift;
my $machine=shift;
my $input2=shift;
my $rev=shift;

# from compare.pl:
# 0: "none",
# 1: "Error_reading_input_file",
# 2: "Error_reading_Ghostscript_produced_PDF/PS/XPS_file",
# 3: "Timeout_reading_input_file",
# 4: "Timeout_reading_Ghostscript_produced_PDF/PS/XPS_File",
# 5: "Input_file_missing",
# 6: "Ghostscript_generated_PDF/PS/XPS_file_missing",
# 7: "Seg_Fault_during_pdfwrite/ps2write/xpswrite",
# 8: "Seg_Fault",
# 9: "Seg_Fault_reading_Ghostscript_produced_PDF/PS/XPS_File",
# 10: "Internal_error"

$rev=0 if (!$rev);

($machine) || die "usage: readlog.pl input.log output machine [input.out] [rev]";

$local=1 if ($machine eq "local");

my %correctedMd5Sum;

my $uname=`uname`;
chomp $uname;
if(0) {
# RJW: Disabled now.
    # occasionally ghostscript on windows prepends a warning to the start
    # of the stdout stream: "Can't post message to GUI thread".  needles to
    # say this messes up the m5sum, so we look through all of the output
    # files and if found remove the extra junk and recalculate the md5sum.
    # luckily the string is still grepable in the .lzo file, so we don't need
    # to uncommpress/recompress files needlessly.

    if ($uname=~m/CYGWIN/) {
	my @a=<temp/*.lzo>;
	foreach my $f (@a) {
	    #   `grep -q --text C.a.n...t...p.o.s....m.e.s.s $f`;
	    # this is faster but not by as much as you'd expect
	    `head --bytes=128 $f | grep -q --text C.a.n...t...p.o.s....m.e.s.s`;
	    if ($?==0) {
		print "readlog: lzop -d -c $f | tail -c +67 >temp/file.tmp\n";
		`lzop -d -c $f | tail -c +67 >temp/file.tmp`;
		my $md5=`cat temp/file.tmp | md5sum`;
		chomp $md5;
		$md5=~s/ .*//;
		print "readlog: md5=$md5\n";
		print "readlog: mv $f $f.old\n";
		`mv $f $f.old`;
		print "readlog: cat temp/file.tmp | lzop >$f\n\n";
		`cat temp/file.tmp | lzop >$f`;
		unlink("temp/file.tmp");
		$f=~s|^temp/||;
		$f=~s|.lzo$||;
		$correctedMd5Sum{$f}=$md5;
	    }
	}
	print Dumper(\%correctedMd5Sum);
    }
}


open (F,"<$input") || die "file $input not found";
my $a=<F>;
close(F);
if (!$a) {
  `touch $output`;
  exit 0;
}
chomp $a;
#print "$a\n";
if ($a=~m/.+\t.+\t.+\t.+\t.+\t.+\t.+\t.+\t.+/) {
#print "match\n";
  `cp $input $output`;
  exit 0;
}


open (F,"<$input") || die "file $input not found";

while(<F>) {

  chomp;

  if (m/^compileFail/ || m/^md5sumFail/ || m/^timeoutFail/ || m/^nodeFail/) {
    close(F);
    print "$_\n";
    exit;
  } elsif (m/===gs_build===/) {
    open (F2,">gs_build.log");
    while(<F>) {
      chomp;
      close(F2) if (m/^===(.+)===$/);
      last if (m/^===(.+)===$/);
      print F2 "$_\n";
    }
  } elsif (m/===mupdf_build===/) {
    open (F2,">mupdf_build.log");
    while(<F>) {
      chomp;
      close(F2) if (m/^===(.+)===$/);
      last if (m/^===(.+)===$/);
      print F2 "$_\n";
    }
  } elsif (m/===mujstest_build===/) {
    open (F2,">mujstest_build.log");
    while(<F>) {
      chomp;
      close(F2) if (m/^===(.+)===$/);
      last if (m/^===(.+)===$/);
      print F2 "$_\n";
    }
  }
  if ($_) {

    if ((m/===(.+).log===/ || m/===(.+)===/) && !m/=====/) {
      $file=$1;
      $error=0;
      $divider=0;
      $md5sum=0;
      $t1=0;
      $t2=0;
      $mem1=0;
      $mem2=0;
      my $t=<F>;
      chomp $t;
      if ($file eq "gs_build") {
	  $file_prod = "gs_build";
      } else {
          $file_prod = "$file:$t";
      }
      $results{$file_prod}{"error"}=-1;
      $results{$file_prod}{"md5"}=$md5sum;
      $results{$file_prod}{"time1"}=0;
      $results{$file_prod}{"time2"}=0;
      $results{$file_prod}{"mem1"}=0;
      $results{$file_prod}{"mem2"}=0;
      $results{$file_prod}{"size"}=0;
      $results{$file_prod}{"inter_md5"}=0;
      $results{$file_prod}{"product"}=$t;
      $bmpcmpErrors{$file_prod}="okay";
    }
    if (m/^---$/) {
      $divider=1;
    }

    #if (m/Unrecoverable error, exit code/ || m/Command exited with non-zero status/ || m/Warning interpreter exited with error code/) {
    if (m/Unrecoverable error, exit code/ || m/Command exited with non-zero status/) {
      $error=1 if ($divider==0 && $error==0);
      $error=2 if ($divider==1 && $error==0);
      $results{$file_prod}{"error"}=$error;
    }
    if (m/return: (\d+)/) {
      if ($1 != 0) {
        $error=1 if ($divider==0 && $error==0);
        $error=2 if ($divider==1 && $error==0);
        $results{$file_prod}{"error"}=$error;
      }
    }
    if (m/Segmentation fault/ || m/Backtrace:/ || m/Command terminated by signal/) {
      $error=8 if ($divider==0 && $error==0);
      $error=9 if ($divider==1 && $error==0);
      $results{$file_prod}{"error"}=$error;
    }
    if (m/killed: timeout/) {
      $error=3 if ($divider==0); # && $error==0);
      $error=4 if ($divider==1); # && $error==0);
      $results{$file_prod}{"error"}=$error;
    }
    if (m/Unable to open/) {
      $error=5 if ($divider==0 && $error==0);
      $error=6 if ($divider==1 && $error==0);
      $results{$file_prod}{"error"}=$error;
    }
    if (m/bmpcmp: (.+)/) {
      $bmpcmpErrors{$file_prod}=$1;
    }
    if (m/(\d+\.\d+) (\d+\.\d+) (\d+:\d\d\.\d\d) (\d+)%/) {
      if ($divider==0) {
        $t1=$1;
      } else {
        $t2=$1;
      }
#   print "time=$t1 $t2\n";
    }
#% Final time = 0.97, memory allocated = 13815462, used = 6122034, max_used = 45428478
    if (m/^filesize: (\d+)/) {
      $results{$file_prod}{"size"}=$1;
    }
    if (m/^md5sum: ([0-9a-f]{32})/) {
      $results{$file_prod}{"inter_md5"}=$1;
    }
    if (m/Final time .+ max_used = (\d+)/) {
      if ($divider==0) {
        $mem1=$1 if ($1>$mem1);
      } else {
        $mem2=$1 if ($1>$mem2);
      }
    }
    if (m/Peak memory use = (\d+)/) {
      $mem1=$1;
    }
    if ($file_prod) {
      $results{$file_prod}{"time1"}=$t1;
      $results{$file_prod}{"time2"}=$t2;
      $results{$file_prod}{"mem1"}=int($mem1/(1024*1024)*100+0.5)/100;
      $results{$file_prod}{"mem2"}=int($mem2/(1024*1024)*100+0.5)/100;
    }
    if (m/^([0-9a-f]{32})$/ || m/^([0-9a-f]{32}) \*-/ || m/^([0-9a-f]{32})  -/ || m/^([0-9a-f]{32})  \.\/temp/ || m/^MD5 .+ = ([0-9a-f]{32})$/) {
      $md5sum=$1;
      if (($md5sum eq $emptyMd5sumPDF || $md5sum eq $emptyMd5sumXPS) && $error==0) {
        if ($local) {
          if (-e "./temp/$file.lzo") {
            $md5sum=`nice lzop -c -d ./temp/$file.lzo | nice md5sum`;
            chomp $md5sum;
            $md5sum=~s/(.{32}).*/$1/;
            $error=10 if ($md5sum eq $emptyMd5sumPDF || $md5sum eq $emptyMd5sumXPS);
          } else {
            $error=10;
          }
        } else {
          $error=1;  # treat empty output files without an error code as an input file read error
        }
      }
#   print "md5sum=$md5sum\n";
      $results{$file_prod}{"error"}=$error;
      $results{$file_prod}{"md5"}=$md5sum;
      $results{$file_prod}{"md5"}=$correctedMd5Sum{$file_prod} if (exists $correctedMd5Sum{$file_prod});
    }
  }
}

close(F);

if (0) {
    # Disabled by RJW as a) I can't see how this can ever be triggered (nothing produces "Segmentation fault" lines in the logs AFAICT)
    # and b) this doesn't allow for products being added to form $file_prod, rather than $file.
    if ($input2) {
	open (F,"<$input2") || die "file $input2 not found";
	while(<F>) {
	    if (m|Segmentation fault .+ ./temp/(\S+).log 2|) {
		my $file=$1;
		my $pdfwrite=0;
		$pdfwrite=1 if (m/pdfwrite/);
		$pdfwrite=1 if (m/ps2write/);
		$pdfwrite=1 if (m/xpswrite/);
		#     print "$pdfwrite $file\n";
		if (exists $results{$file}{"error"}) {
		    $results{$file}{"error"}=7 if ($pdfwrite==1);
		    $results{$file}{"error"}=8 if ($pdfwrite==0 && ($results{$file}{"error"} % 2 == 0 || $results{$file}{"error"} == -1));
		} else {
		    #       die "$file not found in ressults";
		}
	    }
	}
	close(F);
    }
}

delete $results{"gs_build"} if (exists $results{"gs_build"});

open(F,">$output") || die "file $output can't be written to";
if ($bmpcmp) {
  foreach (sort keys %bmpcmpErrors) {
    (my $f, my $p) = $_ =~ m/(.+):(.+)/;
    print F "$f $bmpcmpErrors{$_}\n";
  }
} else {
  foreach (sort keys %results) {
    # $results{$_}{"md5"}=0 if ($results{$_}{"error"}!=0);
    (my $f, my $p) = $_ =~ m/(.+):(.+)/;
    print F "$f\t$results{$_}{'error'}\t$results{$_}{'time1'}\t$results{$_}{'time2'}\t$results{$_}{'mem1'}\t$results{$_}{'mem2'}\t$results{$_}{'md5'}\t$rev\t$results{$_}{'product'}\t$machine\t$results{$_}{'size'}\t$results{$_}{'inter_md5'}\n";
  }
}
close(F);

if (0) {
  foreach (sort keys %results) {
    print "$_\n" if ($results{$_}{'error'} != 0);
  }
}

