#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my $timeoutTime=300; # number of seconds that files which timeout

my $file1=shift;
my $file2=shift;
my $products=shift || die "usage: totalTime file1.tab file2.tab \"products\"";

`./filterTab.pl $file1 jobs >$file1.tmp`;
$file1=$file1.".tmp";
`./filterTab.pl $file2 jobs >$file2.tmp`;
$file2=$file2.".tmp";


my %error;
my %time1;
my %time2;

my %productBits=(
  gs  =>  1,
  pcl =>  2,
  xps =>  4,
  svg =>  8,
  ls  => 16,
  mupdf => 32,
  mujstest => 64
);

my $productBits;
{
  my @a=split ' ',$products;
  foreach my $i (@a) {
    next if (!exists $productBits{$i});
    $productBits+=$productBits{$i};
  }
}


sub printTime($) {
  my $t=shift;
  my $sign=" ";
  if ($t<0) {
    $sign='-';
    $t*=-1;
  }
  my $t1=int($t/3600);
  my $t2=int(($t-$t1*3600)/60);
  my $t3=int($t-$t1*3600-$t2*60);
  my $s=sprintf "%02d:%02d:%02d",$t1,$t2,$t3;
  return($sign.$s);
}

my @time1=0;
my @time2=0;
my @timeout1;
my @timeout2;
$timeout1[0]=0;
$timeout2[0]=0;
$timeout1[1]=0;
$timeout2[1]=0;
my @s1;
my @s2;
my @timeoutS1;
my @timeoutS2;

sub readTime($$) {
  my $file=shift;
  my $i=shift;
  open(F,"<$file") || print "file $file not found";
  while(<F>) {
    chomp;
    my @a=split '\t';
    my $a=$a[8];
    $a=$1 if($a[8] =~ m/^(.+) /);
    $a=$productBits{$a};
    next if (!$a);
    next if (!($a & $productBits));
#   next if (exists $time1{$a[0]} && $time1{$a[0]}>1000);
    if ($i==0) {
      $error{$a[0]}=$a[1];
      $time1{$a[0]}=$a[2];
      $time2{$a[0]}=$a[3];
    } else {
      if (exists $error{$a[0]}) {
        if ($a[1]==0 && $error{$a[0]}==0) {
          $time1[0]+=$time1{$a[0]};
          $time2[0]+=$time2{$a[0]};
          $s1[0]{$a[8]}+=$time1{$a[0]};
          $s2[0]{$a[8]}+=$time2{$a[0]};
          $time1[1]+=$a[2];
          $time2[1]+=$a[3];
          $s1[1]{$a[8]}+=$a[2];
          $s2[1]{$a[8]}+=$a[3];
        }
        $timeout1[0]++ if ($error{$a[0]}==3);
        $timeout2[0]++ if ($error{$a[0]}==4);
        $timeout1[1]++ if ($a[1]        ==3);
        $timeout2[1]++ if ($a[1]        ==4);
        $timeoutS1[0]{$a[8]}++ if ($error{$a[0]}==3);
        $timeoutS2[0]{$a[8]}++ if ($error{$a[0]}==4);
        $timeoutS1[1]{$a[8]}++ if ($a[1]        ==3);
        $timeoutS2[1]{$a[8]}++ if ($a[1]        ==4);
      }
    }
  }
  close(F);
}


my $compare=0;
readTime($file1,0);
$compare=1;
readTime($file2,1);

#print Dumper(\@timeoutS1);

print "\n";
print "| cpu time         current";
if ($compare) {
    print "                          previous                         difference";
}
print "\n";
printf "| %-15s ","total";
printf "".(printTime($time1[0]+$time2[0]))." (%3d)                ",$timeout1[0]+$timeout2[0];
if (exists $time1[1] && exists $time2[1] && exists $timeout1[1] && exists $timeout2[1]) {
  printf " ".(printTime($time1[1]+$time2[1]))." (%3d)                ",$timeout1[1]+$timeout2[1] if ($compare);
  if (exists $time1[0] && exists $time2[0] && exists $timeout1[0] && exists $timeout2[0] && $compare) {
    printf "  ".(printTime(($time1[0]+$time2[0])-($time1[1]+$time2[1])))." (%3d)",($timeout1[0]+$timeout2[0])-($timeout1[1]+$timeout2[1]);
  }
}
print "\n|\n";

foreach my $i (sort keys %{$s1[0]}) {
    if (exists $s1[1]{$i}) {
        printf "| %-15s ",$i;
        #print "$s1[0]{$i} $s2[0]{$i}\n";
        $timeoutS1[0]{$i}=0 if (!exists $timeoutS1[0]{$i});
        $timeoutS2[0]{$i}=0 if (!exists $timeoutS2[0]{$i});
        $timeoutS1[1]{$i}=0 if (!exists $timeoutS1[1]{$i});
        $timeoutS2[1]{$i}=0 if (!exists $timeoutS2[1]{$i});

        printf "".(printTime($s1[0]{$i}))." (%3d)",$timeoutS1[0]{$i};
        printf " ".(printTime($s2[0]{$i}))." (%3d)",$timeoutS2[0]{$i} if ($s2[0]{$i}>0);
        print  "                "                if ($s2[0]{$i}==0);
        printf " ".(printTime($s1[1]{$i}))." (%3d)",$timeoutS1[1]{$i} if ($compare);
        printf " ".(printTime($s2[1]{$i}))." (%3d)",$timeoutS2[1]{$i} if ($compare && $s2[1]{$i}>0);
        print  "                "                if ($compare && $s2[1]{$i}==0);
        printf "  ".(printTime($s1[0]{$i}-$s1[1]{$i}))." (%3d)",($timeoutS1[0]{$i}-$timeoutS1[1]{$i}) if ($compare);
        printf " ".(printTime($s2[0]{$i}-$s2[1]{$i}))." (%3d)",($timeoutS2[0]{$i}-$timeoutS2[1]{$i}) if ($compare && $s2[0]{$i}>0 && $s2[1]{$i}>0);
        print "\n";
    }
}

if (0){
printf "%-15s ","total";
print "".(printTime($time1[0]-$time1[1]))." ".(printTime($time2[0]-$time1[1]))."\n";
print "\n";

foreach my $i (sort keys %{$s1[0]}) {
printf "%-15s ",$i;
#print "$s1[0]{$i} $s2[0]{$i}\n";

print "".(printTime($s1[0]{$i}-$s1[1]{$i}));
print " ".(printTime($s2[0]{$i}-$s2[1]{$i})) if ($s2[0]{$i}>0);
print "\n";
}

}

unlink($file1);
unlink($file2);
