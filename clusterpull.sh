#!/bin/bash

machine=REPLACE_WITH_YOUR_MACHINE_NAME

# check to see if the previous clusterpull process locked up
find . -maxdepth 1 -name run.pid -mmin +120 -type f -print | xargs /bin/rm -f
find . -maxdepth 1 -name clusterpull.pid -mmin +150 -type f -print | xargs /bin/rm -f

if [ ! -e run.pid ]; then
  find . -maxdepth 1 -name clusterpull.pid -mmin +2 -type f -print | xargs /bin/rm -f
fi

# check to see if script is already running and hasn't crashed
if [ -e clusterpull.pid ]; then
  p=`cat clusterpull.pid`;
  a=`ps -p $p | grep clusterpull | wc -l`
  if [ $a == 0 ]; then
    touch clusterpull.pid  ; rm clusterpull.pid
    touch run.pid          ; rm run.pid
  else
    exit
  fi
fi

# there is no clusterpull process currently running, so we keep running
echo $$ >clusterpull.pid

while true; do

  # check to see if something unexpected happened and we are no
  #longer the clusterpull process that is supposed to be running
  if [ ! -e clusterpull.pid ]; then
    exit
  fi

  p=`cat clusterpull.pid`
  if [ $p != $$ ]; then
    exit
  fi
  touch clusterpull.pid

  if [ ! -e run.pid ]; then
    rm -f job.start
    scp -o StrictHostKeyChecking=no -o BatchMode=yes -i ~/.ssh/cluster_key -q regression@cluster.ghostscript.com:/home/regression/cluster/job.start . >&/dev/null
    if [ -e job.start ]; then
      echo -n "<start>"
      scp -o StrictHostKeyChecking=no -o BatchMode=yes -i ~/.ssh/cluster_key -q regression@cluster.ghostscript.com:/home/regression/cluster/run.pl . >&/dev/null
      chmod +x run.pl
      perl run.pl $machine >&$machine.out
      rm job.start
      echo -n "<end>"
    else
      echo -n "."
      ssh -o StrictHostKeyChecking=no -o BatchMode=yes -i ~/.ssh/cluster_key regression@cluster.ghostscript.com touch /home/regression/cluster/$machine.up
    fi
  fi

  sleep 10
done
