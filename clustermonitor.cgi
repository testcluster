#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

use CGI;

use Time::localtime;
use File::stat;

my $clusteroot = '/home/regression/cluster';

my $watchdog=0;
my %status;
my @machines;

sub http_send {
  my $msg = shift;
  
  #print "HTTP/1.0 200 OK\r\n";
  print "Date: ", scalar gmtime, "\r\n";
  print "Content-Length: ", length $msg, "\r\n";
  #print "Content-Type: text/plain\r\n";
  #print "Content-Type: text/html\r\n";
  print "Content-Type: application/json\r\n";
  print "Cache-Control: no-cache, no-store, must-revalidate\r\n";
  print "\r\n";
  print $msg;
}

sub get_machines {
  #my @machines=("i7","i7a","macpro","peeves","test");
  @machines = <$clusteroot/*.status>;
  #print "@machines\n";

  for (my $i=0;  $i<scalar(@machines);  $i++) {
    $machines[$i]=~s/.status//;
    $machines[$i]=~s|.*cluster/||;
  }
  #print "@machines\n";
}

sub get_status {
  my %status;

  my $status=`cat $clusteroot/status`;
  chomp $status;

  get_machines();

  if (!exists $status{'main'}{"status"} || $status{'main'}{"status"} ne $status) {
    $status{'main'}{"status"}=$status;
  }

  foreach my $machine (@machines) {
    my $s0=`cat $clusteroot/$machine.status`;
    chomp $s0;

    my $down="";
    my $downTime=0;
    if (!stat("$clusteroot/$machine.up")) {
        $down="????";
    } else {
        $downTime=(time-stat("$clusteroot/$machine.up")->ctime);
        if ($downTime>300) {
            $down="--DOWN--";
        } else {
            $down = "$downTime secs"; 
        }
    }

    my $disabled="";
    if (-e "$clusteroot/$machine.down") {
        $disabled = "1";
    }

    my $active=0;
    if (-e "$clusteroot/$machine.active") {
        $active = "1";
    }

    if ($down eq "--DOWN--" && $machine =~ /^aws/ ) {
    } elsif (!exists $status{$machine}{"status"} || 
         $status{$machine}{"status"} ne $s0 ||
         $status{$machine}{"down"} ne $down ||
         $status{$machine}{"disabled"} ne $disabled ||
         $status{$machine}{"active"} ne $active) {
      $status{$machine}{"status"}=$s0;
      $status{$machine}{"down"}=$down;
      $status{$machine}{"disabled"}=$disabled;
      $status{$machine}{"active"}=$active;
    }
  }

  my @jobs = `cat $clusteroot/queue.lst` ;
  for (my $i = 0; $i < scalar(@jobs); $i++) {
    $status{'pending'}{$i} = $jobs[$i];
    chomp $status{'pending'}{$i};
  }

  return %status;
}

sub get_users {
  my @users;

  open TABLE, "<$clusteroot/emails.tab";
  my @lines = <TABLE>;
  close TABLE;

  foreach my $line (@lines) {
    my @tokens = split /\s+/, $line;
    chomp $tokens[0];
    push @users, $tokens[0];
  }

  @users = sort @users;

  return @users;
}

sub get_nightlies {
  my @nightlies = ();

  foreach my $a (<$clusteroot/auto/nightly/*/jobdef.txt>) {
    $a =~ m/nightly\/(.+)\/jobdef.txt/;
    push @nightlies, $1;
  }

  return @nightlies;
}

sub get_weeklies {
  my @weeklies = ();

  foreach my $a (<$clusteroot/auto/weekly/*/jobdef.txt>) {
      $a =~ m/weekly\/(.+)\/jobdef.txt/;
      push @weeklies, $1;
  }

  return @weeklies;
}

sub get_usertimes {
  my @usertimes;
  my @users = <$clusteroot/users/*>;

  foreach my $user (@users) {
    $user =~ s/$clusteroot\/users\/(.*)/$1/;
    my $time  = "";
    my $time2 = "";
    my $time3 = "";
    if (-e "$clusteroot/users/$user/starttime") {
       $time = ctime((stat("$clusteroot/users/$user/starttime"))->mtime);
    }
    if (-e "$clusteroot/users/$user/stoptime") {
       $time2 = ctime((stat("$clusteroot/users/$user/stoptime"))->mtime);
    }
    if (-e "$clusteroot/../public_html/$user/index.html") {
       $time3 = ctime((stat("$clusteroot/../public_html/$user/index.html"))->mtime);
    }
    push @usertimes, [$user,$time,$time2,$time3];
  }

  return @usertimes;
}

sub get_nightlytimes {
  my @nightlytimes;
  my @ns = <$clusteroot/auto/nightly/*>;

  foreach my $n (@ns) {
    $n =~ s/$clusteroot\/auto\/nightly\/(.*)/$1/;
    my $time  = "";
    my $time2 = "";
    my $time3 = "";
    if (-e "$clusteroot/auto/nightly/$n/starttime") {
       $time = ctime((stat("$clusteroot/auto/nightly/$n/starttime"))->mtime);
    }
    if (-e "$clusteroot/auto/nightly/$n/stoptime") {
       $time2 = ctime((stat("$clusteroot/auto/nightly/$n/stoptime"))->mtime);
    }
    push @nightlytimes, [$n,$time,$time2];
  }

  return @nightlytimes;
}

sub get_weeklytimes {
  my @weeklytimes;
  my @ns = <$clusteroot/auto/weekly/*>;

  foreach my $n (@ns) {
    $n =~ s/$clusteroot\/auto\/weekly\/(.*)/$1/;
    my $time  = "";
    my $time2 = "";
    my $time3 = "";
    if (-e "$clusteroot/auto/weekly/$n/starttime") {
       $time = ctime((stat("$clusteroot/auto/weekly/$n/starttime"))->mtime);
    }
    if (-e "$clusteroot/auto/weekly/$n/stoptime") {
       $time2 = ctime((stat("$clusteroot/auto/weekly/$n/stoptime"))->mtime);
    }
    push @weeklytimes, [$n,$time,$time2];
  }

  return @weeklytimes;
}

sub get_reports {
  my @reports;
  my @emails_gs = <$clusteroot/archive/*/email.txt>;
  my @emails_mu = <$clusteroot/mupdf-archive/*/email.txt>;
  my @emails_mj = <$clusteroot/mujstest-archive/*/email.txt>;
  my @emails = (@emails_gs, @emails_mu, @emails_mj);
  my @sorted = map {$_->[0]} sort {$b->[1]<=>$a->[1]} map {[$_, -M]} @emails;

  # Take the last 20
  @sorted = (20 > @sorted) ? @sorted : @sorted[-20..-1];

  foreach my $elt (@sorted) {
    my $project = $elt;
    $project =~ s/.*\/(.*)archive\/.*\/email.txt/$1/;
    my $email = $elt;
    $email =~ s/.*\/(.*)\/email.txt/$1/;
    my $ok = 0;
    my $stat;
    my $place;
    if ($project eq "") {
      $stat = stat("$clusteroot/archive/$email");
      $place = "$clusteroot/archive";
      $project="ghostpdl";
      if (-e "$place/$email/gpdf") {
          $project="gpdf";
      }
    }
    if ($project eq "mupdf-") {
      $stat = stat("$clusteroot/mupdf-archive/$email");
      $place = "$clusteroot/mupdf-archive";
      $project = "mupdf";
    }
    if ($project eq "mujstest-") {
      $stat = stat("$clusteroot/mujstest-archive/$email");
      $place = "$clusteroot/mujstest-archive";
      $project = "mujstest";
    }
    if ($project eq "gpdf-") {
      $stat = stat("$clusteroot/archive/$email");
      $place = "$clusteroot/archive";
      $project = "gpdf";
    }
    my $time = ctime($stat->mtime);
    if (-e "$place/$email.tab") {
        $ok = 1;
    }
    open(EMAIL, "$place/$email/email.txt");
    my $git1line = <EMAIL>;
    my $para = "";
    my $line;
    my $count = 20;
    chomp($git1line);
    $git1line = substr($git1line, 0, 80);
    while ($line = <EMAIL>)
    {
      if ($line =~ /Changed files:/) {
        last;
      }
      if ($count-- == 0) {
          last;
      }
      chomp($line);
      $para .= $line." ";
    }
    close(EMAIL);

    # Some quoting hackery.
    # First get rid of tabs/returns/newlines (Firefox barfs)
    $git1line =~ s/([\n\r\t])/ /g;
    $para     =~ s/([\n\r\t])/ /g;

    # Replace \ with \\
    $git1line =~ s/([\\])/\\$1/g;
    $para     =~ s/([\\])/\\$1/g;

    # Now get rid of quotes
    $git1line =~ s/([\"])/\\$1/g;
    $para     =~ s/([\"])/\\$1/g;

    push @reports, "[\"$project $email\",$ok,\"$time\",\"$git1line\",\"$para\"]";
  }

  return @reports;
}

sub get_pendings
{
  my @pendings;

  if (!defined $status{'pending'}) {
      return @pendings;
  }

  my %jobhash = %{$status{'pending'}};
  my @jobindex = (sort {$a <=> $b} keys %jobhash);
  foreach my $i (@jobindex) {
    my $jobname = $jobhash{$i};
    my $hash = "";
    my $gitline = "";
    my $gitpara = "";
    my $gitname = "";
    ($hash) = ($jobname =~ /^git (.*)$/, "");
    if ($hash && $hash ne "") {
      $gitname = "ghostpdl";
    } else {
      ($hash) = ($jobname =~ /^mupdf (.*)$/);
      if ($hash && $hash ne "") {
        $gitname = "mupdf";
      } else {
        ($hash) = ($jobname =~ /^mujstest (.*)$/);
        if ($hash && $hash ne "") {
          $gitname = "mujstest";
        } else {
          ($hash) = ($jobname =~ /^gpdf (.*)$/);
          if ($hash && $hash ne "") {
            $gitname = "gpdf";
          }
        }
      }
    }
    if ($hash && $hash ne "") {
      if (open(F,"<$clusteroot/queued/$hash/git1line")) {
        $gitline = <F>;
        close(F);
      }
      if (open(F,"<$clusteroot/queued/$hash/gitcommit")) {
        while (<F>) {
          chomp;
          $gitpara .= $_;
        }
        close(F);
      }
      # Some quoting hackery.
      # First get rid of tabs/returns/newlines (Firefox barfs)
      $gitline =~ s/([\n\r\t])/ /g;
      $gitpara =~ s/([\n\r\t])/ /g;

      # Replace \ with \\
      $gitline =~ s/([\\])/\\$1/g;
      $gitpara =~ s/([\\])/\\$1/g;

      # Now get rid of quotes
      $gitline =~ s/([\"])/\\$1/g;
      $gitpara =~ s/([\"])/\\$1/g;
      $jobname = $gitname." ".$gitline;
    }
    if (!defined $hash) {
        $hash="";
    }
    $jobname  = "{ \"name\" : \"$jobname\",\n";
    $jobname .= "  \"hash\" : \"$hash\",\n";
    $jobname .= "  \"para\" : \"$gitpara\"\n";
    $jobname .= "}";
    push @pendings, $jobname;
  }
  return @pendings;
}

sub html_print
{
    my $line = shift;
    my $user = shift;
    my $tableize = shift;
    my $intable = shift;
    my $project = shift;
    my $pre;
    my $post;
    my $match;
    my $rewritten = "";
    my $real;
    my $tdev;
    my $tres;
    my $tband;
    my $test0;
    my $test1;
    my $machine0;
    my $machine1;

    $line =~ s/\&/&amp;/g;
    $line =~ s/\"/&quot;/g;
    $line =~ s/\</&lt;/g;
    $line =~ s/\>/&gt;/g;
    $line =~ s/ +/ /g;

    # Try to match the first set of test lines
    # tests_private/...dev.res.band product machine1 machine2 hash error
    my @line_comps = split(' ', $line);
    my $test_name = shift(@line_comps);

    $match = "";
    if (($pre, $match, $post) = $test_name =~ m/(.*)(tests_private\/\S+)(.*)/)
    {
    }
    elsif (($pre, $match, $post) = $line =~ m/(.*)(tests\/\S+)(.*)/)
    {
    }

    $post = "";
    if ($match ne "")
    {
        ($real,$tdev,$tres,$tband) = $match =~ m/(.*)\.(.*)\.(.*)\.([01])/;

        if ($tband ne "") {
            if ($tableize != 0) {
                $rewritten = "$pre<a href=\"https://ghostscript.com/regression/cgi-bin/gitfetch.cgi?file=$real\">$real</a><td>$tdev<td>$tres<td>$tband<td>\n";
            } else {
                $rewritten = "$pre<a href=\"https://ghostscript.com/regression/cgi-bin/gitfetch.cgi?file=$real\">$real</a>.$tdev.$tres.$tband\n";
            }
        }
        $test0 = shift(@line_comps);
        if (($test0 eq "gs") or ($test0 eq "xps") or ($test0 eq "pcl") or ($test0 eq "mujstest") or ($test0 eq "mupdf") or ($test0 eq "gpdf") or ($test0 eq "gpdl"))
        {
            $test1 = shift(@line_comps);
        }
        else
        {
            $test1 = $test0;
            $test0 = "";
        }
        if (($test1 eq "pdfwrite") or ($test1 eq "pswrite") or ($test1 eq "ps2write") or ($test1 eq "xpswrite") or ($test1 eq "pxlcolor" or ($test1 eq "pxlmono")))
        {
            $machine0 = shift(@line_comps);
        } else {
            $machine0 = $test1;
            $test1 = "";
        }
        $machine1 = shift(@line_comps);
        $machine1 =~ s/\s+$//;
        if ($tableize != 0) {
            $post = join('<td>', $test0, $test1, $machine0, $machine1, @line_comps);
        } else {
            $post = join(' ', $test0, $test1, $machine0, $machine1, @line_comps);
        }
        if (not grep { "$_" eq "$machine1" } @machines) {
            $machine1 = $machine0;
        }
    }

    if ($rewritten ne "") {
        if ($tableize != 0) {
            if ($intable != 1) {
                $intable = 0;
                print "</table>";
            }
            if ($intable == 0) {
                $intable = 1;
                print "<table>";
            }
            print "<tr><td>";
        }
        print "$rewritten$post";
        if ($tableize != 0) {
            $test1 =~ s/^\<td\>//;
            print "<td><a href=\"https://ghostscript.com/regression/cgi-bin/clustermonitor.cgi?file=$real&dev=$tdev&res=$tres&band=$tband&test0=$test0&test1=$test1&report=$user&machine=$machine1&log=log&project=$project\">log</a>\n";
        }
        return $intable;
    }

    # FIXME: Try to match CPU time table
    # FIXME: Try to match 'memory increase' table.

    if ($tableize != 0) {
        if ($line =~ m/^\s*(\d+\.\d+)%\t\s*(\d+\.\d+)\t\s*(\d+\.\d+)\t(\S+)\t(\S+)\t(\S+)$/)
        {
            if ($intable != 2) {
                $intable = 0;
                print "</table>";
            }
            if ($intable == 0) {
                $intable = 2;
                print "<table>";
            }
            print "<tr><td>$1%<td>$2<td>$3<td>$4<td>$5<td>$6</tr>\n";
            return $intable;
        }
    }

    if ($tableize != 0) {
        if ($intable != 0) {
            $intable = 0;
            print "</table>";
        }
    }
    print $line;
    return $intable;
}

sub htmlize
{
    my $file = shift;
    my $user = shift;
    my $tableize = shift;
    my $head = shift;
    my $project = shift;
    my $intable = 0;

    get_machines();

    print CGI::header('text/html');
    print "<HTML><HEAD><meta http-equiv=\"cache-control\" content=\"no-cache, no-store, must-revalidate\"><TITLE>$head</TITLE></HEAD><BODY><pre>\n";
    if (eof($file)) {
      print "<p>Empty or non-existent file.</p>";
    } else {
      $intable = html_print($head, $user, $tableize, $intable, $project);
      while (<$file>) {
        $intable = html_print($_, $user, $tableize, $intable, $project);
      }
    }
    print "</pre></BODY></HTML>";
    close $file;
}

sub grep_log_for_test
{
    my $f = shift;
    my $file = shift;
    my $dev = shift;
    my $res = shift;
    my $band = shift;
    my $test0 = shift;
    my $test1 = shift;
    my $project = shift;

    $file =~ s/\//__/g;

    my $grep0 = "$file.$dev.$res.$band";
    my $grep1 = "$test0";
    my $intable = 0;

    if (defined($test1) and ($test1 ne "")) {
        $grep1 = $grep1." ".$test1;
    }

    print CGI::header('text/html');
    print "<HTML><HEAD><TITLE>$grep0 $grep1</TITLE></HEAD><BODY><pre>\n";

    #print "looking for: $grep0 then $grep1";
    my $state = 0;
    my $regex0 = qr/===$grep0===\s*/;
    my $regex1 = qr/$grep1\s*/;
    my $regex2 = qr/===.*===\s*/;
    my $held;

    while (<$f>) {
        #$intable = html_print($_, "", 0, $intable, $project);
        if ($state == 2) {
            if ($_ =~ $regex2) {
                $state = 0;
            } else {
                $intable = html_print($_, "", 0, $intable, $project);
            }
        }
        if ($state == 0) {
            if ($_ =~ $regex0) {
                $held = $_;
                $state = 1;
            }
        } elsif ($state == 1) {
            if ($_ =~ $regex1) {
                $state = 2;
            } else {
                $state = "";
            }
        }
    }
    print "</pre></BODY></HTML>";
    close $f;
}

# if we've been asked for a specific result, just return it
my $file = CGI::param('file');
my $dev = CGI::param('dev');
my $res = CGI::param('res');
my $band = CGI::param('band');
my $test0 = CGI::param('test0');
my $test1 = CGI::param('test1');
my $report = CGI::param('report');
my $project = CGI::param('project');
my $log     = CGI::param('log');
my $machine = CGI::param('machine');
if (!defined($project)) {
  $project="ghostpdl";
}
my @users = get_users;
my $f;
my $place = "$clusteroot/archive";
if ("$project" eq "mupdf") {
  $place = "$clusteroot/mupdf-archive";
}    
if ("$project" eq "mujstest") {
  $place = "$clusteroot/mujstest-archive";
}
if ("$project" eq "gpdf") {
  $place = "$clusteroot/archive";
}

sub display_report($$$$) {
  my $file=shift;
  my $location=shift;
  my $title=shift;
  my $project=shift;
  my $f;
  open $f, "<$location" or open($f, "-|", "zcat $location.gz");

  if (defined $file) {
    grep_log_for_test($f, $file, $dev, $res, $band, $test0, $test1);
  } else {
    htmlize($f, "", 0, "$title Machine: $machine Log:$log\n\n", $project);
  }
  exit;
}

if (defined $report) {
  if (!defined $log) {
      $log = "log";
  }
  if (defined $machine) {
    if ($report =~ m/auto/) {
      display_report($file, "$place/../$report/$machine.$log", "Local cluster auto test for $report", $project);
    } elsif (grep { "$_" eq "$report" } @users) {

      display_report($file, "$clusteroot/users/$report/$machine.$log", "Local cluster test for:", $project);
    } elsif ($report =~ /^(.*)$/) {
      display_report($file, "$place/$report/$machine.$log", "Revision: $report", $project);
    }
  }

  if ($report =~ m/auto/) {
    open $f, "<$clusteroot/$report/email.txt";
    htmlize($f, $report, 1, "report $report clusterpush.\n\n", $project);
    exit;
  } elsif (grep { "$_" eq "$report" } @users) {
    open $f, "<$clusteroot/users/$report/email.txt";
    htmlize($f, $report, 1, "report $report clusterpush.\n\n", $project);
    exit;
  } elsif ($report =~ /^(.{40})$/) {
    open $f, "<$place/$report/email.txt";
    htmlize($f, $report, 1, "report $report.\n\n", $project);
    exit;
  }
}

# otherwise, send the dashboard json
%status    = get_status;
my @reports   = get_reports;
my @usertimes = get_usertimes;
my @pending   = get_pendings;
my @nightlytimes = get_nightlytimes;
my @weeklytimes  = get_weeklytimes;

# FIXME: need to escape the strings we encode as json
my $json = "";
$json .= "{\n";
$json .= "  \"status\" : \"$status{'main'}{'status'}\",\n";
$json .= "  \"nodes\"  : [\n";
foreach my $machine (sort keys %status) {
  next if $machine eq 'main';
  next if $machine eq 'pending';
  $json .= "   {\n";
  $json .= "    \"name\"     : \"$machine\",\n";
  $json .= "    \"status\"   : \"".$status{$machine}{'status'}."\",\n";
  $json .= "    \"down\"     : \"".$status{$machine}{'down'}."\",\n";
  $json .= "    \"active\"   : \"".$status{$machine}{'active'}."\",\n";
  $json .= "    \"disabled\" : \"".$status{$machine}{'disabled'}."\"\n";
  $json .= "   },\n";
}
$json .= "  ],\n";
$json .= "  \"pending\" : [\n";
foreach my $job (@pending) {
  $json .= "  $job,\n";
}
$json .= "  ],\n";
$json .= "  \"reports\" : [\n";
foreach my $report (@reports) {
  $json .= "  $report,\n";
}
$json .= "  ],\n";
$json .= "  \"users\" : [\n";
foreach my $usertime (@usertimes) {
  my @ut = @{$usertime};
  $json .= "  [\"".$ut[0]."\",\"".$ut[1]."\",\"".$ut[2]."\",\"".$ut[3]."\"],\n";
}
$json .= "  ],\n";
$json .= "  \"nightlies\" : [\n";
foreach my $usertime (@nightlytimes) {
  my @ut = @{$usertime};
  $json .= "  [\"".$ut[0]."\",\"".$ut[1]."\",\"".$ut[2]."\"],\n";
}
$json .= "  ],\n";
$json .= "  \"weeklies\" : [\n";
foreach my $usertime (@weeklytimes) {
  my @ut = @{$usertime};
  $json .= "  [\"".$ut[0]."\",\"".$ut[1]."\",\"".$ut[2]."\"],\n";
}
$json .= "  ]\n";
$json .= "}\n";

# Now, to keep stupid internet explorer happy, we need to remove stray commas
# from the end of things.
$json =~ s/,(\s*\})/$1/g;
$json =~ s/,(\s*\])/$1/g;

print "Expires: ".CGI::expires('+15s')."\n";
print CGI::header('application/json');
print $json;
#http_send $json;
