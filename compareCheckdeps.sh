#!/bin/tcsh

set a=`diff gs_checkdeps.old gs_checkdeps.log | grep '>' | cut -f 2- -d ' ' | head`
if ( "$a" == "" ) then
  echo "None"
else
  diff gs_checkdeps.old gs_checkdeps.log | grep '>' | cut -f 2- -d ' '
endif
