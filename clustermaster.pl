#!/usr/bin/perl

use strict;
use warnings;

# stop stop stop

#use Time::localtime;
use File::stat;
use Fcntl qw(:flock);
use POSIX qw(strftime);

use Data::Dumper;

use Time::HiRes qw(gettimeofday tv_interval);

# Detect what priority level we are by what name we are being run as.
my $pri = 0;
my $prisuf= "";
if ($0 =~ m/clustermaster(\d).pl/) {
    $pri = $1;
    $prisuf = ".$1";
}

if ($pri == 1) {
#    exit(1);
}

# Configuration settings

# Verbosity setting
my $verbose=1;

# Should we collect gccWarnings?
my $gccWarnings=1;

# What phrase to look for to decide whether we should skip testing.
my $skip="CLUSTER_UNTESTED_DISABLED";

# how many seconds after the last touch before a machine is considered down
# was 400. Then was 180. Now 300.
my $maxTouchTime=300;

# how many seconds after the last transfer before a machine is considered down
# was 800
my $maxTransferTime=600;


# Machines to exclude if we are doing a w32 build
# Normally this list comes from .caps files,
# but macpro thinks it can, but it can't!
my %w32Exclude=(
    'macpro'=>1
);
# don't send jobs to the machines listed below if the number of jobs
# available is less than this (set to 0 to disable)
# if this number is >1 it's treated as a value, if >0 & <1 as a proportion
my $slowMachineMaximumJobs=0.10;

# OfficeMachines
my %officeMachines =
(
    'miles'=>1,
    'i7'=>1,
    'w7'=>1,
    'x6'=>1,
    'feet'=>1,
    'parsecs'=>1,
    'fathoms'=>1,
    'hubbles'=>1,
    'kilometers'=>1,
    'smoots'=>1,
    'meters'=>1,
    'points'=>1,
    'picas'=>1,
);

#my %slowConnectionMachines=('feet'=>1,'henrysx6'=>1,'yards'=>1,'parsecs'=>1,'hubbles'=>1);
my %slowConnectionMachines=();
my %mupdfExclude=('macpro'=>1);

my $runningSemaphore="./clustermaster$prisuf.pid";
my $queue="./queue$prisuf.lst";
my $lock="./queue$prisuf.lck";
my $usersDir="users";
my $jobs="./jobs$prisuf";

#gs/base, gs/Resource: all languages
#gs/psi: ps, pdf
#psi: language switch
#pl, main, common: pcl, pxl, xps, svg, language switch
#pcl, pxl, urwfonts: pcl, pxl
#xps: xps
#svg: svg

# ls : 10000
# svg: 01000
# xps: 00100
# pcl: 00010
# ps : 00001

my %tests=(
  'gs'  =>  1,
  'pcl' =>  2,
  'xps' =>  4,
  'svg' =>  8,
# 'ls'  => 16
  'gpdf'=>  32,
  'gpdl'=>  15+64
  );

my $allTests=15+64;

# ls is tested if any test is called for

my %rules=(
  'xps' => 4+64,
  'svg' => 8,
  'pcl' => 2+64,
  'pcl6' => 2+64,
  'pdf' => 32+64,
  'pxl' => 2+64,
  'gpdl' => 64,
  'gspdl' => 64,
# 'language_switch' => 16,
  'language_switch' => 0,
  'urwfonts' => 2+64,
  'pl' => 14+64,
  'main' => 2+64,
  'common' => 14+64,
  'doc' => 0,
  'tools' => 0,
  'win32' => 0,
  'gs/psi' => 15+64,  # test pdfwrite with pcl/xps
  'gs/base' => 15+64,
  'gs/Resource' => 15+64,
  'gs/doc' => 0,
  'gs/man' => 0,
  'gs/toolbin' => 0,
# 'gs/toolbin' => 15,  # used to force a run of all test files
  'gs/examples' => 0,
  'gs/lib' => 0,
  'gs/freetype' => 15+64,
  'gs/devices' => 15+64,
  'gs/openjpeg' => 15+64,
  'gs/lcms2' => 15+64,
  'gs/contrib' => 0,
  'gs/jasper' => 15+64,
  'gs/cups' => 15+64,
  'psi' => 15+64,  # test pdfwrite with pcl/xps
  'base' => 15+64,
  'Resource' => 15+64,
  'man' => 0,
  'toolbin' => 0,
# 'toolbin' => 15,  # used to force a run of all test files
  'examples' => 0,
  'lib' => 15+64,
  'expat' => 15+64,
  'freetype' => 15+64,
  'devices' => 15+64,
  'openjpeg' => 15+64,
  'lcms2' => 15+64,
  'lcms2mt' => 15+64,
  'contrib' => 0,
  'jasper' => 15+64,
  'iccprofiles' => 15+64,
  'cups' => 15+64,
  'ijs' => 15+64,
  'jbig2dec' => 15+64,
  'jpeg' => 15+64,
  'jpegxr' => 15+64,
  'tiff' => 15+64,
  'arch' => 15+64,
  'libpng' => 15+64,
  'old-stuff' => 0,
  'winrt' => 0,
  'xcode' => 0,
  'zlib' => 15+64
  );

# Global variables

# We build a job definition in the job hash. Each field is defined
# as follows:
#    regression    The definition of this job as found in the queue
#                  file.
#    rev           The git revision (if there is one)
#    ref           The reference git revision (if there is one)
#    type          The type of the job this is (e.g. "ghostpdl", "mupdf",
#                  "mujstest")
#    product       The product(s) to build (e.g. "gs xps pcl", "mupdf",
#                  "mujstest")
#    dir           The directory in which we will work ("ghostpdl", "mupdf")
#    footer        Footer text for the report.
#    changes       List of changed files for a job
#    nonredundant  exists if we should only test nonredundant jobs
#    pdfwrite      exists if we should only test pdfwrite/ps2write/xpswrite jobs
#    nopdfwrite    exists if we should NOT test pdfwrite/ps2write/xpswrite jobs
#    vector        exists if we should only test pdfwrite/ps2write/xpswrite/pxlcolor/pxlmono jobs
#    novector      exists if we should NOT test pdfwrite/ps2write/xpswrite/pxlcolor/pxlmono jobs
#    relaxTimeout  exists if we should relax the timeout
#    userRegression   exists if this a userRegression run, and is set to the
#                     string that defines the run
#    userMupdfRegression     exists if this is a userMupdfRegression run
#    userMujstestRegression  exists if this is a userMujstestRegression run
#    options       Any options set for this run
#    bmpcmp        exists if this is a bmpcmp run
#    bmpcmphead    exists if this is a bmpcmphead run
#    w32           exists if this is a 32bit wordsize run.
my %job;
my %emails;
my %machine;
my %machinesBuildOnly;
my %machineCaps;
my %lastTransferTime;
my @jobs;

# Output a timed string to the debug log.
sub mydbg($) {
    my $s=shift;
    chomp $s;
    my $pid=sprintf("%5d",$$);
    my $d=localtime;
    print "$s\n";
    print DBG "$pid $d: $s\n";
}

sub mydbgv($) {
    my $s=shift;
    if ($verbose) {
        mydbg($s);
    }
}

sub putToFile($$) {
    my $s = shift;
    my $file = shift;
    if (open(F,">$file")) {
        print F "$s";
        close(F);
    } else {
        mydbg("putToFile: Failed to open $file\n");
    }
}

sub appendToFile($$) {
    my $s = shift;
    my $file = shift;
    if (open(F,">>$file")) {
        print F "$s";
        close(F);
    } else {
        mydbg("appendToFile: Failed to open $file\n");
    }
}

sub logRun($) {
    my $cmd = shift;
    my $res = `$cmd`;
    mydbg("$cmd\n");
    if ($res ne "") {
        mydbg("$res\n");
    }
}

# The cluster status is stored in a file called 'status'.
# This function updates it.
sub updateStatus($) {
    my $s=shift;
    putToFile($s,"status$prisuf");
}

# The status message for any given node m is stored
# in m.status. This function updates it.
sub updateNodeStatus($$) {
    my $m=shift;  # machine
    my $s=shift;  # message
    putToFile($s,"$m$prisuf.status");
}

sub openDebugOutput() {
    # Rotate the debug log if it gets too big.
    my $filesize = -s "clustermaster$prisuf.dbg";
    if ($filesize>100000000) {
        `mv clustermaster$prisuf.dbg clustermaster$prisuf.dbg.old`;
    }

    # Open debug output
    open (DBG,">>clustermaster$prisuf.dbg");

    # Set DBG to autoflush
    my $ofh = select DBG;
    $| = 1;
    select $ofh;

    mydbg("Started");
}

sub exitNeatly {
    mydbg("exitNeatly");
    unlink $runningSemaphore;
    unlink "job$prisuf.start";
    close DBG;
    exit;
}

# Remove the top entry from the jobs queue (with locking)
sub removeTopEntryFromQueue {
    mydbg "removing top element from queue\n";
    open(LOCK,">$lock") || die "can't write to $lock";
    flock(LOCK,LOCK_EX);
    if (open(F,"<$queue")) {
        # Read the queue in
        my @a;
        while(<F>) {
            chomp;
            push @a,$_;
        }
        close(F);
        # Write the queue out again, user jobs first, omitting
        # entry 0 (hence dropping the top job).
        open(F,">$queue");
        for (my $i=1;  $i<scalar(@a);  $i++) {
            print F "$a[$i]\n" if ($a[$i] =~ m/^user/);
        }
        for (my $i=1;  $i<scalar(@a);  $i++) {
            print F "$a[$i]\n" if (!($a[$i] =~ m/^user/));;
        }
        close(F);
    }
    close(LOCK);
}

# Routine to abort a cluster job in progress.
sub markAllNodesInactive() {
    my $startTime=time;
    foreach my $m (keys %machine) {
        unlink("$m$prisuf.active");
    }
    # Once aborted, all machines are now idle.
    foreach my $machine (keys %machine) {
        updateNodeStatus($machine,"idle");
    }
}

# Function to shutdown and exit for a given reason
sub shutdownNeatly($$$) {
    my $abortReason = shift;
    my $sleepTime = shift;
    my $exitLabel = shift;
    markAllNodesInactive();
    sleep $sleepTime;
    updateStatus($abortReason);
    mydbg($exitLabel);
    exitNeatly();
}

# The cluster is asked to abort a job by an 'abort.job' file being
# created. This function checks for that existing and acts upon it.
sub checkForAbortCommand {
    # If the file does not exist, nothing to do.
    if (!-e "abort$prisuf.job") {
        return 0;
    }

    mydbg "abort$prisuf.job found\n";
    # Cancel any outstanding alarm requests.
    alarm 0;
    # Remove the file
    unlink "abort$prisuf.job";
    $job{abort} = 1;

    return 1;
}

# the concept with checkPID is that if the semaphore file is missing or doesn't
# contain our PID something bad has happened and we just just exit
sub checkPID {
    if (open(F,"<$runningSemaphore")) {
        my $t=<F>;
        close(F);
        $t=0 if (!$t);
        chomp $t;
        if ($t == $$) {
            return(0);
        }
        mydbg "terminating: $runningSemaphore mismatch $t $$\n";
    } else {
        mydbg "terminating: $runningSemaphore missing\n";
    }
    alarm 0;

    # FIXME?
    # putToFile("$$\n", $runningSemaphore);
    shutdownNeatly("checkPID() failed", 0, "checkPID");
}

# Remove a given machine for the rest of this run
sub dropMachine($)
{
    my $m = shift;

    if (exists $lastTransferTime{$m}) {
        delete $lastTransferTime{$m};
    }
    if (exists $machine{$m}) {
        delete $machine{$m};
    }
    `touch $m$prisuf.abort`;
    unlink "$m$prisuf.start";
    unlink "$m$prisuf.active";
    unlink "$m$prisuf.done";
    unlink "$m$prisuf.up";
}

# Safe function for 'making an empty directory'
sub mkdirEmpty($)
{
    my $dir=shift;
    # Protect ourselves
    if (!defined($dir) || $dir eq "" || $dir eq "." || $dir eq ".." || $dir eq "/") {
        mydbg("Invalid call to mkdirEmpty: '$dir'!\n");
        return;
    }
    `touch $dir ; rm -fr $dir`;
    `mkdir $dir`;
}
     
sub enqueueJobWithOptions($$)
{
    my $s=shift;
    my $options=shift;
    my $queued=0;

    if ($options ne "") {
        $options = " $options";
    }

    my $t=`grep "$s" $queue`;
    chomp $t;
    #mydbg "  grep '$s' returned: $t\n";
    if (length($t)==0) {
        mydbg "  grep '$s' returns no match, adding to queue\n";
        appendToFile("$s$options\n", $queue);
        $queued = 1;
    }
    return $queued;
}

sub enqueueJob($)
{
    my $s=shift;
    return enqueueJobWithOptions($s, "");
}

# Check to see if a given revision/job is already in the queue. If not
# enqueue it.
sub enqueueRev($$$) {
    my $rev = shift;
    my $dir = shift;
    my $namesref = shift;
    my @names = @{$namesref};
    my $queued = 0;

    chomp $rev;
    open(LOCK,">$lock") || die "can't write to $lock";
    flock(LOCK,LOCK_EX);

    # Queue all the jobs required.
    foreach my $name (@names) {
        $queued += enqueueJob("$name $rev");
    }

    # If we queued any jobs, ensure the records of the job names
    # are up to date.
    if ($queued != 0) {
        mkdirEmpty("queued/$rev");
        my $oneline = `cd $dir ; git --no-pager log $rev -1 --oneline`;
        chomp $oneline;
        putToFile("$oneline\n", "queued/$rev/git1line");
        my $gitcommit = `cd $dir ; git --no-pager log $rev -1`;
        chomp $gitcommit;
        putToFile("$gitcommit\n","queued/$rev/gitcommit");
    }
    close(LOCK);
}

sub gitLockDie() {
    mydbg("Git lock failed");
    die "Git lock failed";    
}

sub enqueueRevs($$$$) {
    my $allRevs = shift;
    my $branch = shift;
    my $dir = shift;
    my $namesref = shift;
    my @allRevs=split '\n',$allRevs;
    my $headRev="";
    my @newRevs;
    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    `cd $dir ; git checkout -q $branch`;
    foreach my $rev (@allRevs) {
        if (($headRev eq "") || ($headRev eq $rev)) {
            push @newRevs, $rev;
            $headRev =`cd $dir; git rev-list $rev~1 -1`;
            chomp $headRev;
        }
    }
    `cd $dir ; git checkout -q master`;
    @newRevs = reverse @newRevs;

    foreach my $rev (@newRevs) {
        enqueueRev($rev, $dir, $namesref);
    }
    close(GITLOCK);
}

# Given a new origin/master, and a current master, how do we generate the
# list of intermediate commits that need testing?
#
# Consider the following example. We are on A, and when the script is called
# we find that 3 commits have been done since:
#
# D merge 'newbranch' into 'master' = origin/master
# |\
# C |  commit on 'master'
# | B  commit on 'newbranch'
# |/
# A master
#
# A key observation is that when you merge B into A, A is always the first
# parent of the commit. This means that we can isolate the commits we need
# to test by starting at origin/master and stepping back using ~1 repeatedly.
sub enqueueGhostPDLJobs {
    if (-e "ghostpdl/.git/refs/remotes/origin/master.lock") {
        return;
    }

    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    #`cd ghostpdl ; git checkout -q master`;
    my $allRevs=`cd ghostpdl ; git fetch -q >/dev/null 2>&1 ; git rev-list master..origin/master`;
    my @gsjobs = ( "git" );
    close(GITLOCK);
    if ($allRevs =~ m/fatal/ || $allRevs =~ m/error/) {
        mydbg($allRevs);
    } else {
        enqueueRevs($allRevs, "master", "ghostpdl", \@gsjobs);
    }

    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    #`cd ghostpdl ; git checkout -q pdfi`;
    $allRevs=`cd ghostpdl ; git fetch -q >/dev/null 2>&1 ; git rev-list pdfi..origin/pdfi`;
    close(GITLOCK);
    my @gpdfjobs = ( "gpdf" );
    if ($allRevs =~ m/fatal/ || $allRevs =~ m/error/) {
        mydbg($allRevs);
    } else {
        enqueueRevs($allRevs, "pdfi", "ghostpdl", \@gpdfjobs);
    }
    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    `cd ghostpdl ; git checkout -q master`;
    close(GITLOCK);
}

sub enqueueMuPDFJobs {
    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    my $allRevs=`cd mupdf ; git fetch -q origin master:incoming_master >/dev/null 2>&1 ; git rev-list master..incoming_master`;
    close(GITLOCK);
    #my @mupdfjobs = ( "mupdf", "mujstest" );
    my @mupdfjobs = ( "mupdf" );
    enqueueRevs($allRevs, "master", "mupdf", \@mupdfjobs);
}

# check for mupdf commits on the forms branch
sub enqueueMuPDFFormsJobs {
    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    my $allRevs=`cd mupdf ; git fetch -q origin forms:incoming_forms >/dev/null 2>&1 ; git rev-list forms..incoming_forms`;
    close(GITLOCK);
    my @mupdfjobs = ( "mupdf", "mujstest" );
    enqueueRevs($allRevs, "master", "mupdf", \@mupdfjobs);
}

# Remove leading/trailing/double spaces.
sub cleanSpaces($) {
    my $product = shift;
    while($product =~ s/  / /g) {
    }
    $product =~ s/^ +//;
    $product =~ s/ +$//;
    return $product;
}

sub addUserJobToQueue($$$$$) {
    my $user=shift;
    my $product=shift;
    my $options=shift;
    my $filters=shift;
    my $extras=shift;

    $product = cleanSpaces($product);

    # Is this job queued already?
    my $queued = enqueueJobWithOptions("user $user $product $filters $extras", "$options");
    if ($queued != 0) {
        my $srcDir = "$usersDir/$user/ghostpdl";

        # Sanitize the users copy of the ghostscript source.
        # Ensure that the .sh files in the users source tree are
        # unix files and that everything is readable/executable.
        logRun("find $srcDir -name \\*.sh | xargs \$HOME/bin/flip -u");
        logRun("find $srcDir -name instcopy | xargs \$HOME/bin/flip -u");
        logRun("chmod -R +xr $srcDir");
    }
}

# Function to chomp a string, coping with the string not being there.
sub chump($) {
    my $val = shift;
    if ($val) {
        chomp $val;
    } else {
        $val = "";
    }
    return $val;
}

sub enqueueUserJobs {
    opendir(DIR, $usersDir) || die "can't opendir $usersDir: $!";
    foreach my $user (readdir(DIR)) {
        my $product="";
        my $options="";
        my $filters="";
        my $extras="";
        my $s="";
        if (open(F,"<$usersDir/$user/gs.run")) {
            close(F);
            unlink "$usersDir/$user/gs.run";
            $product="gs pcl xps gpdl";
        } elsif (open(F,"<$usersDir/$user/ghostscript.run")) {
            close(F);
            unlink "$usersDir/$user/ghostscript.run";
            $product="gs pcl xps gpdl";
        } elsif (open(F,"<$usersDir/$user/ghostpdl/cluster_command.run")) {
            $product=chump(<F>);
            $options=chump(<F>);
            $filters=chump(<F>);
            $extras=chump(<F>);
            close(F);
            unlink "$usersDir/$user/ghostpdl/cluster_command.run";
        }
        # Old case, never used now.
        elsif (open(F,"<$usersDir/$user/ghostpdl/gs/cluster_command.run")) {
            $product=chump(<F>);
            $options=chump(<F>);
            $filters=chump(<F>);
            $extras=chump(<F>);
            close(F);
            unlink "$usersDir/$user/ghostpdl/gs/cluster_command.run";
        } elsif (open(F,"<$usersDir/$user/mupdf/cluster_command.run")) {
            $product=chump(<F>);
            $options=chump(<F>);
            $filters=chump(<F>);
            $extras=chump(<F>);
            close(F);
            unlink "$usersDir/$user/mupdf/cluster_command.run";
        }

        if ($product) {
            $product =~ s/ +$//;
            $product =~ s/^ +//;
            $product =~ m/(.+?) (.+)/;
            $user=$1;
            $product=$2;
            mydbg "  user $user product $product $options\n";
            open(LOCK,">$lock") || die "can't write to $lock";
            flock(LOCK,LOCK_EX);
            if ($product =~  m/abort$/) {
                mydbg "  abort for user $user\n";
                if (open(F,"<$queue")) {
                    my $first=1;
                    my @a;
                    while(<F>) {
                        chomp;
                        if (! m/^user $user/) {
                            push @a,$_
                        } else {
                            if ($first) {
                                logRun("touch abort$prisuf.job");
                            }
                        }
                        $first=0;
                    }
                    close(F);
                    open(F,">$queue");
                    for (my $i=0;  $i<scalar(@a);  $i++) {
                        print F "$a[$i]\n";
                    }
                    close(F);
                }
            } else {
                if ($product =~ m/bmpcmphead/) {
                    $product =~ s/bmpcmphead//;
                    if (length($product)) {
                        addUserJobToQueue($user,$product,"",$filters,$extras);
                    }
                    addUserJobToQueue($user,'bmpcmphead',$options,$filters,$extras);
                } elsif ($product =~ m/bmpcmp/) {
                    my $queueString="bmpcmp";
                    $product =~ s/bmpcmp//;
                    if ($product =~ m/win32/) {
                        $product =~ s/win32//;
                        $product = cleanSpaces($product);
                        $queueString .= " win32";
                    }
                    if ($product =~ m/office/) {
                        $product =~ s/office//;
                        $product = cleanSpaces($product);
                        $queueString .= " office";
                    }
                    if (length($product)) {
                        addUserJobToQueue($user,$product,"",$filters,$extras);
                    }
                    addUserJobToQueue($user,$queueString,$options,$filters,$extras);
                } elsif ($product =~ m/bmpcmp32/) {
                    $product =~ s/bmpcmp32//;
                    if (length($product)) {
                        addUserJobToQueue($user,$product,"",$filters,$extras);
                    }
                    addUserJobToQueue($user,'bmpcmp32',$options,$filters,$extras);
                } else {
                    addUserJobToQueue($user,$product,$options,$filters,$extras);
                }
            }
            close(LOCK);
        }
    }
    closedir DIR;
}

# To kill an old clustermaster, we write our PID to the semaphore file,
# wait 5 minutes, delete the semaphore file, and then exit. This means
# no other clustermasters will start up during that time (as they think
# we are current), and it gives any old clustermaster that might still
# be hanging around time to realise that it should die.
sub killOldClusterMaster($) {
    my $reason = shift;
    mydbg("$reason - killing clustermaster to heal - wait 5 mins");
    updateStatus($reason);
    putToFile("$$\n","$runningSemaphore");
    sleep 300;
    mydbg("Exiting after delay because: $reason");
    exitNeatly();
}

sub checkForOtherClusterMasters {

    # If the semaphore file doesn't exist, then we are safe to assume
    # that we're the only one.
    if (open(F,"<$runningSemaphore")) {
        # Read the PID of the supposedly existing clustermaster.
        my $pid=<F>;
        close(F);

        # If it's 0 (or not a number), then something has gone wrong. Clean
        # up by killing any old clustermaster.
        if ($pid==0) {
            killOldClusterMaster("Invalid value read from PID file");
        }

        # Check the time of the semaphore. Clustermasters start up, touch the
        # semaphore, run one job, delete the semaphore and then die. Jobs take
        # up to 40 mins or so, so if a sempahore exists, but is more than 2
        # hours old, something is very ill. Kill it and start again.
        my $fileTime = stat($runningSemaphore)->mtime;
        my $t=time;
        my $maxClusterTime = 3600*2; # User or git jobs should never take more than 2 hours.
        if ($pri == 2) {
            $maxClusterTime = 3600*12; # Auto jobs (such as coverage) can take longer; 12 hours.
        }
        if ($t-$fileTime>$maxClusterTime) {
            killOldClusterMaster("Old Clustermaster taking WAY too long");
        }

        # So, we have a clustermaster running as $pid. Really?
        chomp $pid;
        my $running=`ps -p $pid`;
        if ($running =~ m/clustermaster/) {
            # Yeah, fair cop. No need for us then, just let the old cluster
            # master do its job.
            close DBG;
            exit;
        } else {
            killOldClusterMaster("Process $pid no longer running (our pid: $$)");
        }
    }

    # Take the controls!
    putToFile("$$\n","$runningSemaphore");
}

sub getEmails {
    my %emails;
    if (open(F,"<emails.tab")) {
        while(<F>) {
            chomp;
            my @a=split '\t';
            $emails{$a[0]}=$a[1];
        }
        close(F);
    }
    return %emails;
}

sub peekAtTopOfQueue {
    my $regression;
    open(LOCK,">$lock") || die "can't write to $lock";
    flock(LOCK,LOCK_EX);
    if (open(F,"<$queue")) {
       $regression=<F>;
       close(F);
    }
    close(LOCK);
    return $regression;
}

# Create a generic regression job. Args are:
#    regression
#    rev
#    type
#    dir
# Returns the hash for the job definition
sub startGenericRegression($$$$) {
    my %job = (
        regression => shift,
        rev => shift,
        type => shift,
        dir => shift
    );
    my $regression = $job{regression};
    my $rev = $job{rev};
    my $type = $job{type};
    my $dir = $job{dir};
    print DBG "\n";
    mydbg "found $type regression in queue: $regression";

    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    `cd $dir ; git log -1 $rev`;
    close(GITLOCK);
    my $status=$?;
    if ($status!=0) {
        mydbg "commit is missing from repository: status=$status";
        `touch queued/$rev ; rm -fr queued/$rev`;
        return undef;
    }

    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    my $footer=`cd $dir ; git --no-pager log $rev -1 --oneline`;

    $footer.="------------------------------------------------------------------------\n";
    $footer.=`cd $dir ; git --no-pager log $rev -1`;

    $footer.="\nChanged files:\n";
    my $changes = `cd $dir ; git diff --name-only $rev~1 $rev`;
    $footer .= $changes;
    close(GITLOCK);

    $job{footer} = $footer;
    $job{changes} = $changes;

    # Disable the ability to skip tests now.
    #my $logMessage.=`cd $dir ; git log $rev -1`;
    #if ($logMessage =~ m/$skip/) {
    #    mydbg "$skip found in $rev, skipping";
    #    `touch queued/$rev ; rm -fr queued/$rev`;
    #    return 0;
    #}
    $job{product}=$type;
    return %job;
}

sub startGhostPDLRegression($$$) {
    my $regression = shift;
    my $revision = shift;
    my $branch = shift;

    my $type = $branch;
    if ($branch eq "master") {
        $type = "git";
    }
    
    my %job = startGenericRegression($regression, $revision, $type, "ghostpdl");
    my $changes = $job{changes};
    my $rev = $job{rev};

    if (!defined($changes)) {
        my %nojob = { skip => 1 };
        return %nojob;
    }
    my @a=split '\n',$changes;
    my $set=0;
    for (my $i=0;  $i<scalar(@a);  $i++) {
        my $s=$a[$i];
        chomp $s;
        mydbg($s);

        if ($s =~ m|/|) {
            my $t=$s;
            if ($t =~ m|gs/(.+?)/|) {
                $t="gs/$1";
            } else {
                $t=$1 if ($t =~ m|(.+?)/|);
            }
            if (exists $rules{$t}) {
                mydbg "$s: $rules{$t}\n";
                $set|=$rules{$t};
                if ($rules{$t}) {
                    # put this back when gpdl goes to master.
                    #$set |= 64; # gpdl
                }
            } else {
                mydbg "$s ($t): missing, testing all\n";
                $set|=$allTests;
            }
        } else {
            # Something changed, but not in a directory, presumably
            # it's configure.ac or some other root level file. Safest
            # to test everything
            $set|=$allTests;
        }
    }

    mydbg "gs regression changed flags detected as $set\n";

    # $set=$allTests;  # force it to run all tests

    my $product="";
    if ($branch eq "gpdf") {
        $product = "gpdf";
        $job{gpdfRegression} = 1;
    } elsif ($branch eq "master") {
      foreach my $i (sort keys %tests) {
          if ($set & $tests{$i}) {
              $product .= "$i " 
          }
      }
    }

    $product =~ s/svg//;  # disable svg tests
    $product =~ s/ls//;  # disable language switch tests
    $product = cleanSpaces($product);

    mydbg "products: $product\n";

    $job{title}="Regression $rev ($product)";
    $job{product}=$product;
    $job{footer}.="\nProducts tested: $product\n\n";
    $job{normalRegression} = 1;
    $job{trigger}="git\t$rev\t$product";

    if (length($product)) {
    } else {
        mydbg "no interesting files changed, skipping regression\n";
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        #logRun("cd ghostpdl ; git checkout -q $branch ; git reset -q --hard $rev ; git checkout -q master");
        #logRun("cd ghostpdl ; git branch -q -f $branch $rev");
	logRun("cd ghostpdl ; git update-ref -m \"SKipping $rev\" refs/heads/$branch $rev");
        close(GITLOCK);
        `touch queued/$rev ; rm -fr queued/$rev`;
        $job{skip} = 1;
    }

    return %job;
}

sub startMuPDFRegression($$) {
    my %job = startGenericRegression(shift, shift, "mupdf", "mupdf");
    if (%job) {
        my $rev = $job{rev};
        $job{title}="Regression $rev (mupdf)";
        $job{mupdfRegression} = 1;
        $job{trigger} = "mupdf\t$rev";
    }
    return %job;
}

sub startMuJSTestRegression($$) {
    my %job = startGenericRegression(shift, shift, "mujstest", "mupdf");
    if (%job) {
        my $rev = $job{rev};
        $job{title}="Regression $rev (mujstest)";
        $job{mujstestRegression} = 1;
        $job{trigger} = "mujstest\t$rev";
        $maxTouchTime*=2;  # was *3
        $maxTransferTime*=2;  # was *3
    }
    return %job;
}

sub startGSViewBuildTest() {
    my %job = (
        regression => "",
        rev => "",
        type => "gsview",
        dir => "gsview"
    );
    print DBG "\n";
    mydbg "found gsview regression in queue";

    open(GITLOCK, ">gitlock") || gitLockDie();
    flock(GITLOCK,LOCK_EX);
    my $footer=`cd gsview ; git --no-pager log -1 --oneline`;

    $footer.="------------------------------------------------------------------------\n";
    $footer.=`cd gsview ; git --no-pager log -1`;

    $footer.="------------------------------------------------------------------------\n";
    $footer.="gs revision:\n";
    $footer=`cd gsview\\submodules\\ghostpdl ; git --no-pager log -1 --oneline`;
    $footer.="------------------------------------------------------------------------\n";
    $footer.="mupdf revision:\n";
    $footer=`cd gsview\\submodules\\mupdf ; git --no-pager log -1 --oneline`;

    $footer.="\nChanged files:\n";
    my $changes = `cd gsview ; git diff --name-only HEAD`;
    $footer .= $changes;
    close(GITLOCK);

    $job{footer} = $footer;
    $job{changes} = $changes;

    $job{product}="gsview";
    $job{title}="Regression (gsview)";
    $job{gsviewRegression} = 1;
    $job{trigger} = "gsview";
    # gsview only builds on windows for now
    $job{win}=1;
    return %job;
}

sub makeUserTrigger {
    return "user\t$job{userName}\t$job{product}";
}

sub startUserRegression($$)
{
    my %job = (
        regression => shift,
        userRegression => cleanSpaces(shift)
    );
    my $regression = $job{regression};
    my $userRegression = $job{userRegression};
    print DBG "\n";
    mydbg "found user regression in queue: $regression";
    if ($userRegression eq "") {
        my %nojob = { skip => 1 };
        return %nojob;
    }
    my @a=split ' ',$userRegression,2;
    my $userName=$a[0];
    my $product=$a[1];
    my $options="";
    my $bmpcmp;
    my $bmpcmphead;
    my $w32;
    my $win;
    my $luratech;
    my $cal;
    my $ufst;
    if ($product=~m/^bmpcmp/) {
        $bmpcmp=1;
        my @a=split ' ',$product,2;
        if ($product=~m/^bmpcmphead/) {
            $product="bmpcmphead $userName";
            $bmpcmphead=1;
        } else {
            $product="bmpcmp $userName";
        }
        $options=chump($a[1]);
    }
    if ($product=~m/^bmpcmp32/) {
        $bmpcmp=1;
        $w32=1;
        my @a=split ' ',$product,2;
        if ($product=~m/^bmpcmphead/) {
            $product="bmpcmphead $userName";
            $bmpcmphead=1;
        } else {
            $product="bmpcmp $userName";
        }
        $options=chump($a[1]);
    }
    if ($product=~m/luratech/) {
        $luratech=1;
        $product =~ s/luratech//;
    }
    if ($product=~m/cal/) {
        $cal=1;
        $product =~ s/cal//;
    }
    if ($product=~m/ufst/) {
        $ufst=1;
        $product =~ s/ufst//;
    }
    if ($product=~m/win32/) {
        $win=1;
        $product =~ s/win32//;
    }
    if ($product=~m/\b32\b/) {
        $w32=1;
        $product =~ s/\b32\b//;
    }
    if ($product=~m/avx2/) {
        $job{avx2}=1;
        $product =~ s/avx2//;
    }
    if ($product=~m/nonredundant/) {
        $job{nonredundant}=1;
        $product =~ s/nonredundant//;
    }
    if ($product=~m/nopdfwrite/) {
        $job{nopdfwrite}=1;
        $product =~ s/nopdfwrite//;
    }
    if ($product=~m/pdfwrite/) {
        $job{pdfwrite}=1;
        $product =~ s/pdfwrite//;
    }
    if ($product=~m/novector/) {
        $job{novector}=1;
        $product =~ s/novector//;
    }
    if ($product=~m/vector/) {
        $job{vector}=1;
        $product =~ s/vector//;
    }
    if ($product=~m/relaxTimeout/) {
        $job{relaxTimeout}=1;
        $product =~ s/relaxTimeout//;
    }
    if ($product=~m/office/) {
        $job{office}=1;
        $product =~ s/office//;
    }
    if ($product=~m/^mupdf/) {
        $job{userMupdfRegression}=1;
    }
    if ($product=~m/^mujstest/) {
        $maxTouchTime*=2;  # was *3
        $maxTransferTime*=2;  # was *3
        $job{userMujstestRegression}=1;
    }
    $product = cleanSpaces($product);
    if (!$product) {
        $product = "gs pcl ls xps gpdl";
    }
    mydbgv("userName=$userName product=$product options=$options\n");
    my $t=`/bin/date +\"%D %H:%M:%S\"`;
    chomp $t;
    $job{footer}="\n\nUser regression: user $userName  options $product $options start $t\n";
    $job{title}="User Regression $userName (".cleanSpaces("$product $options").")";

    if ($job{userMupdfRegression} || $job{userMujstestRegression}) {
        my $cmd="diff -I '\$Id:' -w -c -r ./mupdf ./users/$userName/mupdf | grep -v \"Only in\" > $userName.diff";
        `$cmd`;
    } else {
        my $cmd="diff -I '\$Id:' -w -c -r ./ghostpdl ./users/$userName/ghostpdl | grep -v \"Only in\" > $userName.diff";
        `$cmd`;
    }

    $job{product}=$product;
    $job{bmpcmp}=$bmpcmp;
    $job{bmpcmphead}=$bmpcmphead;
    $job{options}=$options;
    $job{w32}=$w32;
    $job{win}=$win;
    $job{userName}=$userName;
    $job{luratech}=$luratech;
    $job{cal}=$cal;
    $job{ufst}=$ufst;
    if ($job{bmpcmphead}) {
        $job{trigger} .= "\tbmpcmphead";
    } elsif ($job{bmpcmp}) {
        $job{trigger} .= "\tbmpcmp";
    } elsif ($job{w32}) {
        $job{trigger} .= "\t32";
    } elsif ($job{relaxTimeout}) {
        $job{trigger} .= "\trelaxTimeout";
    }

    $job{makeTrigger} = \&makeUserTrigger;

    return %job;
}

sub makeAutoTrigger() {

    if (-e "auto/$job{autoName}/prepare.sh") {
        `auto/$job{autoName}/prepare.sh`;
    }

    return "auto\t$job{autoName}\t$job{product}\t$job{rev}\t$job{wordSize}\t$job{relaxTimeout}\t$job{makeOption}\t$job{configOptions}\t$job{beforeMake}\t$job{afterMake}\t$job{filterJobs}\t$job{luratech}\t$job{ufst}\t$job{ref}\t$job{env}\t$job{cal}";
}

sub startAutoRegression($$)
{
    my %job = (
        regression => shift,
        autoRegression => cleanSpaces(shift)
    );
    my $regression = $job{regression};
    my $autoRegression = $job{autoRegression};
    print DBG "\n";
    mydbg "found auto regression in queue: $regression";
    if ($autoRegression eq "") {
        my %nojob = { skip => 1 };
        return %nojob;
    }
    if ($autoRegression =~ m/product\s*<(.+?)>/) {
        $job{product} = cleanSpaces($1);
        $autoRegression =~ s/product\s*<$job{product}>//;
    } else {
        my %nojob = { skip => 1 };
        return %nojob;
    }
    if ($autoRegression =~ m/name\s*<(.+?)>/) {
        $job{autoName} = cleanSpaces($1);
        $job{autoName} =~ s/\s/_/g;
        $job{autoName} =~ s/\</_/g;
        $job{autoName} =~ s/\>/_/g;
        $job{autoName} =~ s/\`/_/g;
        $job{autoName} =~ s/\`/_/g;
    } else {
        my %nojob = { skip => 1 };
        return %nojob;
    }
    mydbg "autoName=$job{autoName}\n";
    if ($autoRegression =~ m/ref\s*<(\S+?)>/) {
        $job{ref} = cleanSpaces($1);
    }
    mydbg "ref=$job{ref}\n";
    if ($autoRegression =~ m/trunkCompare/) {
	$job{trunkCompare} = 1;
    }

    if ($job{product} eq "bmpcmp") {
        $job{product} = "bmpcmp auto=$job{autoName}";
        if ($job{ref}) {
            $job{product} .= " ref";
        }
        $job{bmpcmp} = 1;
    }
    mydbg "product=$job{product}\n";
    if ($job{product} =~ m/mupdf/) {
        $job{autoMupdfRegression} = 1;
    }
    if ($job{product} =~ m/mujstest/) {
        $job{autoMujstestRegression} = 1;
    }
    if ($job{product} =~ m/gsview/) {
        $job{autoGSViewRegression} = 1;
        $job{win} = 1;
    }
    if ($job{product} =~ m/mupdfmini/) {
        $job{autoMuPDFMiniRegression} = 1;
        $job{android} = 1;
    }

    $job{rev}="origin/master";
    if ($autoRegression =~ m/rev\s*<(\S+?)>/) {
        $job{rev} = cleanSpaces($1);
    } elsif (defined $job{ref}) {
        $job{rev} = $job{ref};
        $job{nomail} = 1;
        $job{reference} = 1;
    }
    mydbg "rev=$job{rev}\n";

    $job{options}="";
    if ($autoRegression =~ m/options\s*<(.*?)>/) {
        my $opts = $1;

        if ($opts =~ m/luratech/) {
            $job{luratech} = 'luratech';
            $opts =~ s/luratech//;
        }
        if ($opts =~ m/cal/) {
            $job{cal} = 'cal';
            $opts =~ s/cal//;
        }
        if ($opts =~ m/ufst/) {
            $job{ufst} = 'ufst';
            $opts =~ s/ufst//;
        }
        if ($opts =~ m/nopdfwrite/) {
            $job{nopdfwrite}=1;
            $opts =~ s/nopdfwrite//;
        }
        if ($opts =~ m/pdfwrite/) {
            $job{pdfwrite}=1;
            $opts =~ s/pdfwrite//;
        }
        if ($opts =~ m/novector/) {
            $job{novector}=1;
            $opts =~ s/novector//;
        }
        if ($opts =~ m/vector/) {
            $job{vector}=1;
            $opts =~ s/vector//;
        }
        $job{options} = $opts;
    }
    mydbg "options=$job{options}\n";

    $job{makeOption}="";
    if ($autoRegression =~ m/make\s*<(.*?)>/) {
        $job{makeOption} = $1;
    }
    mydbg "makeOption=$job{makeOption}\n";

    $job{env}="";
    if ($autoRegression =~ m/env\s*<(.*?)>/) {
        $job{env} = $1;
    }
    mydbg "env=$job{env}\n";

    $job{configOptions}="";
    if ($autoRegression =~ m/config\s*<(.*?)>/) {
        $job{configOptions} = $1;
    }
    mydbg "configOptions=$job{configOptions}\n";

    $job{beforeMake}="";
    if ($autoRegression =~ m/beforeMake\s*<(.*?)>/) {
        $job{beforeMake} = $1;
    }
    mydbg "beforeMake=$job{beforeMake}\n";

    $job{afterMake}="";
    if ($autoRegression =~ m/afterMake\s*<(.*?)>/) {
        $job{afterMake} = $1;
    }
    mydbg "afterMake=$job{afterMake}\n";

    $job{filterJobs}="";
    if ($autoRegression =~ m/filter\s*<(.*?)>/) {
        $job{filterJobs} = $1;
    }
    mydbg "filterJobs=$job{filterJobs}\n";

    $job{wordSize} = "64";
    if ($pri == 3) {
        $job{wordSize} = "32";
    }
    if ($job{options} =~ m/w32/) {
        $job{wordSize} = "32";
        $job{options} =~ s/w32//;
        $job{w32} = 1;
    }
    if ($job{options} =~ m/office/) {
        $job{options} =~ s/office//;
        $job{office} = 1;
    }
    if ($job{options} =~ m/coverage/) {
        $job{options} =~ s/coverage//;
        $job{coverage} = 1;
    }
    $job{relaxTimeout} = "";
    if ($job{options} =~ m/relaxTimeout/) {
        $job{options} =~ s/relaxTimeout//;
        $job{relaxTimeout} .= "relaxTimeout";
    }

    my $t=`/bin/date +\"%D %H:%M:%S\"`;
    chomp $t;
    $job{footer}="\n\nAuto regression: name $job{autoName} product $job{product} rev $job{rev} options $job{options} start $t\n";
    $job{title}="Auto Regression name $job{autoName} product $job{product} rev $job{rev} options $job{options}";
    $job{makeTrigger} = \&makeAutoTrigger;

    return %job;
}

sub buildJobList {
    my $product = $job{product};
    my $options = $job{options};
    my $baseline="";
    if ($job{coverage}) {
        $options .= " coverage";
       `touch bogus.coverage.tgz; rm -rf *.coverage.tgz`;
    }

    logRun("./build.pl $product $baseline $options >$jobs");
    if ($? != 0) {
        # horrible hack, fix later
        removeTopEntryFromQueue();
        mydbg "build.pl $product failed\n";
        exitNeatly();
    }
    mydbg "build.pl completed\n";
    #`sort jobs >jobs.tmp ; mv jobs.tmp jobs`;
    #if ($job{coverage}) {
    #    `head -200 jobs >jobs.tmp ; mv jobs.tmp jobs`;
    #}
 
    # Exclude the jobs known to timeout.
    `grep -v -f timeouts.lst $jobs >$jobs.tmp ; mv $jobs.tmp $jobs`;

    # Filter the jobs list as appropriate
    if ($job{pdfwrite}) {
        mydbg "filtering for pdfwrite/ps2write/xpswrite jobs\n";
        `grep -e gs-build -e pdfwrite -e ps2write -e xpswrite $jobs > $jobs.tmp`;
        `mv $jobs $jobs.bak ; mv $jobs.tmp $jobs`;
    }
    if ($job{nopdfwrite}) {
        mydbg "filtering pdfwrite/ps2write/xpswrite out jobs\n";
        `grep -v -e pdfwrite -e ps2write -e xpswrite $jobs > $jobs.tmp`;
        `mv $jobs $jobs.bak ; mv $jobs.tmp $jobs`;
    }
    if ($job{vector}) {
        mydbg "filtering for pdfwrite/ps2write/xpswrite/pxlcolor/pxlmono jobs\n";
        `grep -e gs-build -e pdfwrite -e ps2write -e xpswrite -e pxlcolor -e pxlmono $jobs > $jobs.tmp`;
        `mv $jobs $jobs.bak ; mv $jobs.tmp $jobs`;
    }
    if ($job{novector}) {
        mydbg "filtering pdfwrite/ps2write/xpswrite/pxlcolor/pxlmono out jobs\n";
        `grep -v -e pdfwrite -e ps2write -e xpswrite -e pxlcolor -e pxlmono $jobs > $jobs.tmp`;
        `mv $jobs $jobs.bak ; mv $jobs.tmp $jobs`;
    }

    if ($job{nonredundant}) {
        mydbg "filtering redundant jobs\n";
        `./eliminateRedundant.pl redundant.lst $jobs > $jobs.tmp`;
        `mv $jobs $jobs.bak ; mv $jobs.tmp $jobs`;
    }

    # Some disabled code from marcos that lets us override the
    # jobs list with one of our own.
    if (defined($job{userName}) && $job{userName} eq "marcos-disabled") {
        if (open(Z,"<$jobs.marcos")) {
            my $job=<Z>;
            close(Z);
            open(Z,">$jobs");
            for (my $i=0;  $i<5000;  $i++) {
                my $j=$job;
                $j =~ s/\.0\./.$i./g;
                print Z $j;
            }
            close(Z);
        }
    }
}

sub findProductForBmpcmp {
    my $product = $job{product};
    my $gs="";
    my $pcl="";
    my $xps="";
    my $ls="";
    my $gpdf="";
    my $gpdl="";
    my $mupdf="";
    my $mujstest="";
    my $gsBin="__bin__/bin/gs";
    my $pclBin="__bin__/bin/pcl6";
    my $xpsBin="__bin__/bin/gxps";
    my $lsBin="__bin__/bin/pspcl6";
    my $gpdlBin="__bin__/bin/gpdl";
    my $gpdfBin="__bin__/bin/gpdf";
    my $mupdfBin="__bin__/bin/pdfdraw";
    my $mujstestBin="__bin__/bin/mujstest";
    my $jobFile = "$usersDir/$job{userName}/jobs";
    if (defined($job{autoName})) {
        $jobFile = "auto/$job{autoName}/jobs";
    }
    if (open(F2,"<$jobFile")) {
        while (<F2>) {
            chomp;
            if (m/$gsBin/) {
                $gs="gs " ;
            }
            if (m/$pclBin/) {
                $pcl="pcl ";
            }
            if (m/$xpsBin/) {
                $xps="xps ";
            }
            if (m/$lsBin/) {
                $ls="ls ";
            }
            if (m/$gpdfBin/) {
                $gpdf="gpdf ";
            }
            if (m/$gpdlBin/) {
                $gpdl="gpdl ";
            }
            if (m/$mupdfBin/) {
                $mupdf="mupdf ";
            }
            if (m/$mujstestBin/) {
                $mujstest="mujstest ";
            }
        }
        close(F2);
        $product=cleanSpaces("bmpcmp ".$gs.$pcl.$xps.$ls.$gpdf.$gpdl.$mupdf.$mujstest);
    }
    mydbg "done checking jobs, product=$product\n";
    if ($product eq "") {
        checkPID();
        removeTopEntryFromQueue();
        checkPID();
        exitNeatly();
    }

    # And clear the bmpcmp output directory while we are here.
    `touch bmpcmp$prisuf.tmp ; rm -fr bmpcmp$prisuf.tmp ; mv bmpcmp$prisuf bmpcmp$prisuf.tmp ; mkdir bmpcmp$prisuf ; rm -fr bmpcmp$prisuf.tmp &`;
    $slowMachineMaximumJobs=0.25;
    $job{product} = $product;
}

# Return undef if suitable, or a reason why it's not.
sub nodeUnsuitableForJob($) {
    my $node = shift;
    my $caps = $machine{$node}{caps};
    
    if ($job{configOptions}) {
        if ($caps =~ m/win32/) {
            return "win32 incompatible with configOptions";
        }
    }
    if ($job{avx2}) {
        if ($caps =~ m/avx2/) {
        } else {
            return "not avx2 capable";
        }
    }
    if ($job{coverage}) {
        if ($caps =~ m/coverage/) {
        } else {
            return "not coverage capable";
        }
    }
    if (defined $job{autoGSViewRegression}) {
        if ($caps =~ m/devenv2017/) {
        } else {
            return "gsview only builds with VS2017";
        }
    }
    if (defined $job{autoMuPDFMiniRegression}) {
        if ($caps =~ m/android/) {
        } else {
            return "not android capable";
        }
    }
    if ($job{product}=~m/mupdf/ || $job{product}=~m/mujstest/) {
        if (exists $mupdfExclude{$node}) {
            return "incompatible with mupdf/mujstest";
        }
    }
    if ($job{w32}) {
       if ($caps =~ m/w32/ && !exists $w32Exclude{$node}) {
       } else {
           return "incompatible with w32";
       }
    }
    if ($job{office}) {
        if (exists $officeMachines{$node}) {
        } else {
            return "not in the office";
        }
    }
    if ($job{win}) {
        if ($caps =~ m/win32/) {
        } else {
            $machine{$node}{buildonly} = 1;
        }
    } else {
        if ($caps =~ m/win32/) {
            if ($job{bmpcmp}) {
                return "incompatible with non win32";
            } else {
                $machine{$node}{buildonly}=1;
            }
        }
    }
    return undef;
}

sub getJobBatch($$)
{
    my $node = shift;
    my $jobsPerRequest = shift;
    my $gccJob;
    my @jobBatch;
    for (my $i=0;
         $i<$jobsPerRequest && scalar(@jobs);
         $i++) {
        my $a=shift @jobs;
        # If the job to be sent is the one that
        # generates the build.log file (the one
        # from which warnings are scraped), then
        # check that this is a suitable machine
        # for that job. If not, we'll store it
        # and put it back later.
        # RJW: Perhaps this should be generalised
        # to "if this node can't cope with this
        # job, put it into a defer list, and push
        # the defer list back at the end of
        # processing".
        if ($a =~ m/build.log/ &&
            (($pri != 3 && !($machine{$node}{caps} =~ m/gcc=5.4.0/)) ||
             ($pri == 3 && !($machine{$node}{caps} =~ m/gcc=6.3.0/))) &&
            !$job{win}) {
            mydbg "machine $node requested jobs but was not sent build job\n";
            $gccJob=$a;
            $i--;
        } else {
            if ($a =~ m/build.log/) {
                mydbg "machine $node sent build job\n";
            }
            push @jobBatch,$a;
        }
    }
    # put gccjob back into the queue if it didn't get
    # sent and there are some jobs remaining in the
    # queue (if it's the only job remaining it's likely
    # there aren't any machines available to process
    # the job, so just skip it)
    if ($gccJob && scalar(@jobs)>0) {
        unshift @jobs,$gccJob;
    }

    return @jobBatch;
}

sub getMeSomeJobs($) {
    my $node = shift;

    # bmpcmps are really slow (and are limited to 1000
    # jobs) so only send a few at a time. mujstests are
    # slow too. Otherwise send lots of jobs at a time,
    # but slow down as we get towards the end.
    my $jobsPerRequest;
    if ($job{bmpcmp}) {
        $jobsPerRequest= 10;
    } elsif ($job{mujstestRegression}) {
        $jobsPerRequest= 25;
    } elsif (scalar(@jobs)<1000) {
        $jobsPerRequest= 25;
    } elsif (scalar(@jobs)<2000) {
        $jobsPerRequest= 50;
    } elsif (scalar(@jobs)<4000) {
        $jobsPerRequest=100;
    } else {
        $jobsPerRequest=250;
    }
    if ($job{coverage} && $jobsPerRequest > 25) {
        $jobsPerRequest = 25;
    }

    # First off, try to hand out jobs from the normal
    # jobs list.
    my @jobBatch = getJobBatch($node,$jobsPerRequest);
    if (scalar(@jobBatch) != 0) {
        return @jobBatch;
    }

    # So, no jobs left in our normal pool. Let's check for delinquent
    # nodes, and maybe reschedule those.
    my $now = time();
    foreach my $m (keys %machine) {
        if (! defined $machine{$m}{jobs}) {
            next;
        }
        my $n = scalar(@{$machine{$m}{jobs}});
        if ($m ne $node &&
            $machine{$m}{lastConnectionTime} + $maxTransferTime < time() &&
            $n > 0) {
            mydbg("Rescheduling all $n jobs from $m as delinquent");
            # Node $m is delinquent, and has jobs we can reschedule.
            push @jobs,@{$machine{$m}{jobs}};
            undef $machine{$m}{jobs};
            # If $m reconnects in future, then we must not accept
            # these same jobs from it. The best way to ensure that
            # is to kill the node off and make it start from scratch.
            $machine{$m}{delinquent} = 1;
            updateNodeStatus($m, "Down for more than $maxTransferTime seconds, so delinquent");
            my @jobBatch = getJobBatch($node,$jobsPerRequest);
            if (scalar(@jobBatch) != 0) {
                return @jobBatch;
            }
        }
    }

    return undef;
}

sub allJobsCompletedAndUploaded()
{
    my $numJobsLeft = @jobs;
    if ($numJobsLeft != 0) {
        mydbg("$numJobsLeft remain to be scheduled");
        return 0;
    }

    foreach my $m (keys %machine) {
        if (!defined $machine{$m}{jobs}) {
            next;
        }
        my $n = @{$machine{$m}{jobs}};
        if (defined $machine{$m}{jobsPending} &&
            defined $machine{$m}{jobsRunning}) {
            $n = $machine{$m}{jobsPending} + $machine{$m}{jobsRunning};
        }
        if ($n > 0) {
            mydbg("$m still has $n left");
            return 0;
        }
        if ($machine{$m}{needsupload}) {
            mydbg("$m still needs to upload");
            return 0;
        }
        if ($machine{$m}{uploading}) {
            mydbg("$m is still uploading");
            return 0;
        }
    }

    return 1;
}

my $paused = 0;
my $pausedChanged = 0;
my $pausedTime;
sub updateTimestampIfPaused()
{
    # A job of level i (1 <= i <= 2) will be blocked
    # by a job of level j (j < i).
    # ARM jobs (level 3) are not blocked by any others.
    my $we_are_blocked = 0;
    if ($pri < 3) {
        for (my $i = 0; $i < $pri; $i++)
        {
            my $jn = "job.start";
            if ($i > 0) {
                $jn = "job.$i.start";
            }
            if (-e "$jn") {
                $we_are_blocked = 1;
            }
	}
    }

    my $now = time();
    if ($paused) {
        my $d = $now - $pausedTime;
        my $ss = stat($runningSemaphore);
        utime($ss->atime+$d, $ss->mtime+$d, $runningSemaphore);
        # Also, update all the nodes times.
        foreach my $m (keys %machine) {
            if (defined $machine{$m}{lastConnectionTime}) {
                $machine{$m}{lastConnectionTime} += $d;
            }
        }
        # And the startTime (so the elapsed time is correct)
        $job{startTime} += $d;
    }
    $pausedTime = $now;
    $pausedChanged = ($paused != $we_are_blocked);
    $paused = $we_are_blocked;
}

sub getAck($)
{
    my $client = shift;

    shutdown($client, 1); # We won't be sending any more
    my $line = <$client>;
    chomp $line;
    if ($line ne "AYEAYE") {
        mydbg("No ack from the node - connection dropped? (got $line)");
        return 0;
    }
    return 1;
}

sub putJobsBack($)
{
    my $node = shift;

    if (defined $machine{$node}{jobs}) {
        push @jobs,@{$machine{$node}{jobs}};
    }
    if (defined $machine{$node}{jobsUploaded}) {
        push @jobs,@{$machine{$node}{jobsUploaded}};
    }
}

sub runTheJob {
    my $totalJobs=-1;

    if ($job{relaxTimeout}) {
        $maxTransferTime *= 5;
    }

    # In the brave new world, timeouts hold no fear for us.
    # We therefore never retry.

    # OK. Start time!
    #$job{id} = `date -Iseconds`;
    $job{id} = `date -u +"%Y/%m/%d %H:%M:%S"`;
    $job{startTime}=time;

    # Create the job.start file. The nodes watch for changes in this.
    checkPID();
    mydbg("Trigger: $job{trigger}\n");
    chomp $job{id};
    {
        my $rev = $job{rev};
        my $product = $job{product};
        my $userName = $job{userName};
        # The contents of the job.start files (i.e. trigger) is used
        # on the clients; the first line is the jobid. The next is
        # used to influence the checkout (and the logging). The next
        # is used to ensure we are consistent in the test repos we use.
        mydbgv("writing job$prisuf.start\n");
        open(F,">job$prisuf.start");
        print F "$job{id}\n";
        print F "$job{trigger}\n";
        # the order of these must match that of run.pl
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        my $a=`cd /home/git/tests.git ; git log | head -1 | awk '{ print \$2 }'`;
        my $b=`cd /home/git-private/tests_private.git ; git log | head -1 | awk '{ print \$2 }'`;
        close(GITLOCK);
        my $t=`md5sum ./readlog.pl`;
        chomp $t;
        $t=~m/([0-9a-f]{32})/;
        my $c=$1;
        chomp $a;
        chomp $b;
        my $d=0;
        if (-e "valgrind.job") {
            $d=1;
        }
        my $e=0;
        if ($job{coverage}) {
            $e=1;
        }
        print F "$a $b $c $d $e\n";
        close(F);
    }

    checkPID();
    # Start the job processing - set the status.
    updateStatus("$job{title} started at $job{id}");
    if ($job{userRegression}) {
        `touch $usersDir/$job{userName}/starttime`;
    } elsif ($job{autoRegression}) {
        `touch auto/$job{autoName}/starttime`;
    }

    # Read the jobs file into a jobs array.
    @jobs=();
    open(F,"<$jobs");
    while(<F>) {
        if ($_ =~ m/^#/) { next; }
        push @jobs,$_;
    }
    close(F);

    $totalJobs=scalar(@jobs);
    mydbg "We have $totalJobs jobs to process\n";

    if ($totalJobs == 0)
    {
        $job{elapsedTime}=time-$job{startTime};
        $job{failed} = 1;
        return;
    }

    # Now we set ourselves up as a server.
    use IO::Socket;
    use Net::hostent;
    my $PORT = 9100+$pri;

    my $server = IO::Socket::INET->new(
          Proto     => 'tcp',
          LocalPort => $PORT,
          Listen    => SOMAXCONN,
          Reuse     => 1
          );

    if (!$server) {
        markAllNodesInactive();
        mydbg("Can't setup server - fatal error");
        unlink $runningSemaphore;

        die "can't setup server";
    }

    my $client;
    my $doneCount=0;

    do {
        # Check to see if anyone created an abort.job file.
        if (checkForAbortCommand()) {
            print "abort$prisuf.job!";
            goto abortJob;
        }
        # check for being usurped
        checkPID();

        # Update the timestamp on the semaphore, if we're being
        # blocked by another priority job
        updateTimestampIfPaused();
        
        # Run in an eval context so we can use alarm to avoid the
        # server blocking on incoming connections.
        eval
        {
            my $action = "waiting for connection";
            local $SIG{ALRM} = sub { die "$action\n" };
            alarm 30;

            # Wait for a connection (if this takes more than 30 seconds, the
            # alarm will go off, and we'll drop out of the eval.
            $client = $server->accept();
	    $action="reading from connection";

            # Don't know how long we waited for a connection; give us another
            # 30 seconds to deal with this request.
            alarm 30;

            # Ignore any signals due to transmission errors, and flush
            # without being told to.
            $SIG{PIPE} = 'IGNORE';
            $client->autoflush(1);

            # Read the headers/status from the client.
            my %hdr;
            my $clientAbort = 0;
            my $help = 0;
            autoflush $client 1;
            while (<$client>) {
                chomp;
                #print "$_\n";
                if ($_ eq "ABORT") {
                    $clientAbort = 1;
                    last;
                } elsif ($_ eq "OK") {
                    last;
                } elsif ($_ eq "HELP") {
                    $help = 1;
                    last;
                }

                if ($_ =~ m/(\S+)\s(.+)/) {
                    $hdr{$1}=$2;
                } else {
                    $hdr{$_}=1;
                }
            }

            # Debugging facility for socket problems
            if ($help) {
                mydbg("HELP wanted!");
                print $client "HELP command received\n";
                goto closeConnection;
            }

            my $abortReason;
            # Check to see if the node thinks it's doing the same
            # job as we are. If not, abort it.
            if (!defined $hdr{id}) {
                # No id, no entry!
                $abortReason = "No id given!";
                goto abortConnection;
            } else {
                if ($hdr{id} ne $job{id}) {
                    # What planet are you on?
                    $abortReason = "wrong jobid - $hdr{id}";
                    goto abortConnection;
                }
            }

            # We expect the node name to be sent with every request. If not, abort it.
            my $node = $hdr{node};
            if (!defined $node) {
                # No name, no entry.
                $abortReason = "no nodename given";
                goto abortConnection;
            }

            # We expect the nodes id to be sent with every request. If not, abort it.
            my $myid = $hdr{myid};
            if (!defined $myid) {
                # No myid, no entry.
                $abortReason = "no myid given";
                goto abortConnection;
            }

            # Check the node name isn't something like "bogus ; rm -rf /" :)
            if ($node =~ m/^[A-Za-z0-9]+$/) {
            } else {
                $abortReason = "Unacceptable node name";
                goto abortConnection;
            }

            # If the node is delinquent, then we need to kill it off
            # and have it start from scratch.
            if ($machine{$node}{delinquent}) {
                undef $machine{$node}{delinquent};
                $abortReason = "Delinquent node";
                goto abortConnection;
            }

            # If the node has connected before and we rejected it then, (or
            # it has killed itself off before) let's not use it again.
            if ($machine{$node}{culled}) {
                putJobsBack($node);
                $abortReason = "Still unsuitable for job";
                goto abortConnection;
            }

            # If a client is telling us it's dying, better put its jobs
            # back into the queue.
            if ($clientAbort) {
                putJobsBack($node);
                undef $machine{$node};
                $machine{$node}{culled} = 1;
                $abortReason = "Client aborted itself";
                goto abortConnection;
            }

            if (-e "$node.down") {
                putJobsBack($node);
                undef $machine{$node};
                undef $node; # Avoid storing the status, which is just confusing.
                $abortReason = "Node disabled";
                goto abortConnection;
            }
            
            # If a node has connected before, and myid has changed, then
            # we have to redo all its jobs.
            if (defined $machine{$node}{myid} && $machine{$node}{myid} != $hdr{myid}) {
                mydbg("Node $node: changed its id! $machine{$node}{myid} vs $hdr{myid}");
                putJobsBack($node);
                undef $machine{$node};
            }
            
            # If this is the first time a node has connected, check for whether the
            # caps make it usable.
            if (!defined $machine{$node}{buildstarted}) {
                # First time this node has connected.
                # There had better be a caps string.
                if (!defined $hdr{caps}) {
                    $abortReason = "No caps string given on first connection\n";
                    goto abortConnection;
                }
                $machine{$node}{caps}=$hdr{caps};
                # Write the caps to the file for easy debugging. Maybe remove this later.
                putToFile("$node$prisuf.caps", $hdr{caps});

                my $unsuitable = nodeUnsuitableForJob($node);
                if (defined $unsuitable) {
                    $machine{$node}{culled}=1;
                    $abortReason = "Unsuitable for job - $unsuitable";
                    goto abortConnection;
                }

                # OK, so this is a new node, and its suitable for this job.
                mydbg("Node $node: new node => Build\n");
                print $client "BUILD\n";
                if (getAck($client)) {
                    $machine{$node}{building}=1;
                    $machine{$node}{buildstarted}=1;
                    $machine{$node}{myid}=$hdr{myid};
                }
                goto closeConnection;
            }

            # So, what do we want to do with the node?

            # If we've been given a failure code, act on it.
            if (defined $hdr{failure}) {
                if ($machine{$node}{building} == 1) {
                    # Build failure. Client should have aborted.
                    if ($hdr{failure} < 0) {
                        # Fatal error. Tell it to upload, and remember to
                        # abort the whole shebang when the upload completes.
                        mydbg("Fatal build failure reported ($hdr{failure})");
                        print $client "UPLOAD\n";
                        if (getAck($client)) {
                        }
                        $machine{$node}{fatalabort} = 1;
                        $machine{$node}{uploading} = 1;
                        goto closeConnection;
                    }
                    undef $machine{$node}{building}; # You're not building any more.
                    $machine{$node}{failedbuild} = 1;
                    $machine{$node}{needsupload} = 1; # But you do have build logs to upload
                    $abortReason = "Soft build failure reported ($hdr{failure})";
                    goto abortConnection;
                } elsif ($machine{$node}{uploading} == 1) {
                    # Failure during upload.
                    mydbg("Connect: $node uploading failed - retrying");
                    # What should we do here? Tell it to try again, or reschedule jobs on a different node?
                    # FIXME: For now we'll just tell it to try again.
                    print $client "UPLOAD";
                    if (getAck($client)) {
                    }
                    goto closeConnection;
                } else {
                    mydbg("Node $node said it was busy, but I don't know why!");
                    $abortReason = "What are you doing?";
                    goto abortConnection;
                }
            }

            # Check for "busy" status.
            # FIXME: Possibly we should time tasks out?
            if ($hdr{busy}) {
                # The node is busy doing whatever we told it to do before.
                # It was just checking in so we know it hadn't died.
                mydbg("Connect: $node continuing as busy");
                print $client "CONTINUE\n";
                goto closeConnection;
            }

            # So the node is not busy any more.
            if ($machine{$node}{building}) {
                # Build completed (successfully)
                undef $machine{$node}{building};
                mydbg("Connect: $node build complete");
            }
            if ($machine{$node}{uploading}) {
                # Upload completed (successfully)
                if (defined $machine{$node}{jobs}) {
                    push @{$machine{$node}{jobsUploaded}},@{$machine{$node}{jobs}};
                }
                undef $machine{$node}{jobs};
                undef $machine{$node}{uploading};
                if ($machine{$node}{fatalabort}) {
                    $job{buildFail} = 1;
                    $job{failed} = 1;
                    $abortReason = "upload of fatal build error complete";
                    goto abortConnection;
                } else {
                    mydbg("Connect: $node upload complete");
                }
            }

            # If it's a buildonly node, then don't feed it any jobs.
            if ($machine{$node}{buildonly}) {
                if ($machine{$node}{needsupload}) {
                    mydbg("Connect: $node build-only, so upload logs");
                    print $client "UPLOAD\n";
                    if (getAck($client)) {
                        undef $machine{$node}{needsupload};
                    }
                } else {
                    mydbg("Connect: $node build-only, idling");
                    print $client "CONTINUE\n";
                }
                goto closeConnection;
            }
            
            # If it has no jobs pending, let's give it some
            if (!defined $hdr{jobs}) {
                $hdr{jobs}="0 0";
            }
            (my $nodeJobsPending, my $nodeJobsRunning) = split " ",$hdr{jobs};
            $machine{$node}{jobsPending} = $nodeJobsPending;
            $machine{$node}{jobsRunning} = $nodeJobsRunning;
            if ($nodeJobsPending == 0 && !defined $machine{$node}{failedbuild}) {
                # Once we've got the jobs, we can't afford an alarm to
                # go off, cos they'll go missing.
                alarm 0;
                my @jobBatch = getMeSomeJobs($node);
                my $n = scalar(@jobBatch);
                if ($n == 1 && !defined $jobBatch[0]) {
                    # No jobs to hand out at the moment.
                    if ($nodeJobsRunning == 0 && $machine{$node}{needsupload}) {
                        mydbg("Connect: $node - upload logs");
                        print $client "UPLOAD\n";
                        if (getAck($client)) {
                            undef $machine{$node}{needsupload};
                            $machine{$node}{uploading} = 1;
                            # Hackily update the status line.
                            if ($hdr{status} =~ m/(\d+) jobs pending(.*)/) {
                                $hdr{status} = "$1 jobs pending$2 (uploading)";
                            }
                        }
                        goto closeConnection;
                    }
                } else {
                    my $fail = 0;
		    my $n = 0;
                    foreach my $jobline (@jobBatch) {
                        if (defined $jobline) {
			    $n++;
			}
                    }
                    print $client "JOBS $n\n";
                    foreach my $jobline (@jobBatch) {
                        if (defined $jobline) {
                            if (!print($client "$jobline")) {
                                mydbg("$node fail $jobline");
                                $fail = 1;
                                last;
                            }
                            #mydbg("$node $jobline");
                        }
                    }
                    if (!$fail && !getAck($client)) {
                        $fail = 1;
                    }
                    if ($fail) {
                        mydbg("Connect: $node - feed failed");
                        push @jobs,@jobBatch;
                    } else {
                        # Remember the jobs we passed it.
                        push @{$machine{$node}{jobs}},@jobBatch;
                        # It'll need to tell us the results of these.
                        $machine{$node}{needsupload} = 1;
                        my $t = scalar(@{$machine{$node}{jobs}});
                        # Hackily update the status line.
                        if ($hdr{status} =~ m/(\d+) jobs pending(.*)/) {
                            my $newtotal = $n + $1;
                            $hdr{status} = "$newtotal jobs pending$2";
                        }
                        mydbg("Connect: $node - fed $n jobs ($t total)");
                    }
                    goto closeConnection;
                }
            }

            # Otherwise tell it to keep calm and carry on.
            if ($nodeJobsPending > 0 || $nodeJobsRunning > 0) {
                mydbg("Connect: $node - idle ($nodeJobsPending pending, $nodeJobsRunning running)");
            } else {
                mydbg("Connect: $node - idle");
            }
            print $client "CONTINUE\n";
            
          closeConnection:
	    $action="closing connection";
            # Remember the last time that node connected.
            $machine{$node}{lastConnectionTime}=time();
            `touch $node$prisuf.active`;
          abortConnection:
            if (defined $abortReason) {
		$action="aborting connection";
                if (!defined $node) {
                    if (defined $hdr{node}) {
                        $node = "$hdr{node}";
                    } else {
                        $node = "unknown";
                    }
                }
                mydbg("Aborting $node $abortReason");
                print $client "ABORT\n";
                if ($node ne "unknown") {
                    if ($machine{$node}{jobsUploaded}) {
                        # Once a job has given us some jobs, we can never
                        # accept connections from it again, because any jobs
                        # it sends us will overwrite the jobs it's already given
                        # us, and my brain is not big enough to know how to
                        # cope with that.
                        $machine{$node}{culled} = 1;
                    }
                }
            }
            close($client);

            alarm 0;

            if ($node ne "unknown") {
                # Check for a status line.
                if ($hdr{status} && $hdr{status} ne "") {
                    print "Node $node: Status $hdr{status}\n";
                    updateNodeStatus($node, $hdr{status});
                }

                `touch $node.up`;
            }
        }; # End of the eval
        if ($@ ne "") {
            mydbg("Timed out while $@");
        }
        alarm 0;

        if (!$paused || $pausedChanged) {
            # Update the display of how many jobs we've sent.
            my $percentage=int(($totalJobs-scalar(@jobs))/$totalJobs*100+0.5);
            my $ps = "";
            if ($paused) {
                $ps = " - PAUSED";
            }
            #$s=strftime "%H:%M:%S", localtime;
            $job{elapsedTime}=time-$job{startTime};
            updateStatus "$job{title} started at $job{id} - $job{elapsedTime}s elapsed - ".($totalJobs-scalar(@jobs))."/$totalJobs sent - $percentage% $ps";
        }

    } while (!allJobsCompletedAndUploaded() && !$job{abort} && !$job{buildFail});
  abortJob:

    markAllNodesInactive();
    
    # If we didn't fail, update our records of what percentage
    # jobs ran on each node.
    if (!$job{failed}) {
        foreach my $m (keys %machine) {
            my $t=0;
            if (defined $machine{$m}{jobsUploaded}) {
                $t=scalar @{$machine{$m}{jobsUploaded}};
            }
            mydbg "$m completed $t jobs\n";
            open (F9,">$m.contrib");
            printf F9 "%5.2f\n",($t/$totalJobs)*100;
            close(F9);
        }
    }

    # Dump out what jobs were sent to what nodes
    #open(my $f, ">jobs$prisuf.sent") || die "can't open jobs$prisuf.sent";
    #foreach my $m (keys %machine) {
    #    foreach my $j (@{$machine{$m}{jobs}}) {
    #        print $f "$m\tNOTUPLOADED\t$j";
    #    }
    #    foreach my $j (@{$machine{$m}{jobsUploaded}}) {
    #        print $f "$m\tUPLOADED\t$j";
    #    }
    #}
    #close($f);

    $job{elapsedTime}=time-$job{startTime};
}

sub unpackBuildLogs()
{
    checkPID();
    my $buildFail=0;  # did at least one machine report a build failure?
    my $timeoutFail=0;  # did at least one machine report a timeout failure?
    my $failMessage="";
    my $logs="";
    my $tabs="";
    my $outs="";
    # sleep(10);

    if (0) {
        open(F4,">gcc.txt");
    } else {
        `touch gcc.txt`;
    }
    foreach my $m (keys %machine) {
        # Check we got all the expected logs
        if (!-e "$m$prisuf.tab.gz" || !-e "$m$prisuf.log.gz" || !-e "$m$prisuf.out.gz" || !-e "$m$prisuf.make.gz") {
            my $msg = "Warning: missing the following:";
            if (!-e "$m$prisuf.tab.gz") {
                $msg .= " $m$prisuf.tab.gz";
            }
            if (!-e "$m$prisuf.log.gz") {
                $msg .= " $m$prisuf.log.gz";
            }
            if (!-e "$m$prisuf.out.gz") {
                $msg .= " $m$prisuf.out.gz";
            }
            if (!-e "$m$prisuf.make.gz") {
                $msg .= " $m$prisuf.make.gz";
            }
            #if ($job{failed}) {
            mydbg($msg);
            next;
            #} else {
            #    mydbg "ERROR: $m.tab or $m.log or $m.out or $m.make missing\n";
            #    my $a=`ls -ls *tab* *log* *out*`;
            #    mydbg "ls:\n$a";
            #    exitNeatly();
            #}
        }

        mydbg "reading log for $m\n";
        # Unpack the logs.
        `touch $m$prisuf.tab $m$prisuf.log $m$prisuf.out $m$prisuf.make`;
        `rm -f $m$prisuf.tab $m$prisuf.log $m$prisuf.out $m$prisuf.make`;
        `gunzip $m$prisuf.tab.gz $m$prisuf.log.gz $m$prisuf.out.gz $m$prisuf.make.gz`;
        if (!$job{failed} && $?!=0) {
            mydbg "$m$prisuf.tab.gz and/or $m$prisuf.log.gz and/or $m$prisuf.out.gz and/or $m$prisuf.make.gz missing, terminating\n";
            unlink $runningSemaphore;  # force checkPID() to fail
            checkPID();
            exit; # unecessary, checkPID() won't return
        }

        if($job{failed} || $job{bmpcmp}) {
            my $a;
            if ($job{bmpcmp}) {
                $a=`./readlog.pl bmpcmp$prisuf $m$prisuf.log $m$prisuf.tab $m $m$prisuf.out`;
            } else {
                $a=`./readlog.pl $m$prisuf.log $m$prisuf.tab $m $m$prisuf.out`;
            }
            if ($a ne "") {
                chomp $a;
                mydbgv "$m: $a\n";
                $failMessage.="$m reports: $a\n";
                if ($a =~ m/nodeFail/) {
                    $timeoutFail=1;
                    $machine{$m}{timeoutFail}=1;
                } else {
                    $buildFail=1;
                    $machine{$m}{buildFail}=1;
                }
            }
        }

        $logs.=" $m$prisuf.log $m$prisuf.out $m$prisuf.make";
        $tabs.=" $m$prisuf.tab";
        $outs.=" $m$prisuf.out";
    }
    mydbg "finished reading logs";

    # Unpack the build log, if there is one.
    if (!$job{bmpcmp} && !$job{coverage}) {
        if (-e "build$prisuf.log.gz") {
            mydbg "build$prisuf.log.gz exists";
            `gunzip -f build$prisuf.log.gz`;
	    if (!-e "build$prisuf.log") {
		mydbg("build$prisuf.log failed to unpack");
	    }
        }
        if (defined $job{normalRegression}) {
            mydbg("$job{normalRegression}");
        }
        if (defined $job{mupdfRegression}) {
            mydbg("$job{mupdfRegression}");
        }
        if (defined $job{mujstestRegression}) {
            mydbg("$job{mujstestRegression}");
        }
        if (defined $job{userRegression}) {
            mydbg("$job{userRegression}");
        }
        if (defined $job{userMupdfRegression}) {
            mydbg("$job{userMupdfRegression}");
        }
        if (defined $job{userMujstestRegression}) {
            mydbg("$job{userMujstestRegression}");
        }
        #mydbg "$job{normalRegression} $job{mupdfRegression} $job{mujstestRegression} $job{userRegression} $job{userMupdfRegression} $job{userMujstestRegression}";
    }
    if (0) {
        close(F4);
    }

    $job{buildFail}=$buildFail;
    $job{timeoutFail}=$timeoutFail;
    $job{failMessage}=$failMessage;
    $job{logs}=$logs;
    $job{tabs}=$tabs;
    $job{outs}=$outs;
}

sub createJobReport
{
    if (!$job{bmpcmp}) {
        my @t=split '\n',$job{footer};
        open(F,">email.txt");
        foreach my $t (@t) {
            print F "$t\n";
        }
        print F "\n\n";
        close(F);
    }
}

sub findPreviousTab() {

    # Auto, User, and bmpcmp jobs don't rely on the 'previous.tab'
    if ($job{autoRegression} || $job{userRegression} || $job{bmpcmp}) {
        return;
    }

    # Try not to rely on the version of 'current.tab' being correct.
    # grab the newest from the archive.
    my $prefix="";
    if ($job{mupdfRegression}) {
        $prefix="mupdf-";
    }
    if ($job{mujstestRegression}) {
        $prefix="mujstest-";
    }

    my $tab=`ls -t ${prefix}archive | head -1`;
    chomp $tab;
    if (-e $tab) {
        `cp -pr $tab ${prefix}current.tab`;
    }
}

sub populateJobReport
{
    findPreviousTab();

    if (!$job{buildFail} && !$job{timeoutFail}) {
        my $rev = $job{rev};
        my $machineCount=scalar (keys %machine);
        if ($job{normalRegression}) {
            # unlink "log";

            my $prefix="";

            # Take the current tab, strip out any tests that we ran, and
            # store it in t.tab. Thus t.tab are the jobs we DIDN'T run.
            my @a=split ' ',$job{product};
            my $filter="cat ${prefix}current.tab";
            foreach my $a (@a) {
                $filter.=" | grep -v \"\t$a\t\"";
                $filter.=" | grep -v \"\t$a pdfwrite\t\"";
                $filter.=" | grep -v \"\t$a ps2write\t\"";
                $filter.=" | grep -v \"\t$a xpswrite\t\"";
            }
            $filter.=">t.tab";
            logRun("$filter");

            # Move the current tab -> previous tab
            `mv ${prefix}previous.tab ${prefix}previous2.tab`;
            `mv ${prefix}current.tab ${prefix}previous.tab`;

            # Make a new current tab from the result tabs from the job
            # we just ran, and add back in the tests that we didn't run.
            `cat $job{tabs} t.tab | sort >${prefix}current.tab`;

            # `rm $job{tabs}`;
            # `cat $job{logs} >log`;
            # `./readlog.pl log current.tab`;

            if ($gccWarnings && $job{product} =~ /gs/) {
                checkPID();
                my $tmp = `./compareWarningsGCC.pl build$prisuf.old build$prisuf.log | uniq -u`;
                if ($tmp ne "") {
                    appendToFile("New warnings:\n\n$tmp\n", "email.txt");
                } else {
                    appendToFile("No new warnings.\n\n", "email.txt");
                }
                `cp -p build$prisuf.log build$prisuf.old`;

                if (0) {
                    `cd ghostpdl ; ./gs/toolbin/checkdeps.pl | sort > ../gs_checkdeps.log ; cd ..`;
                    checkPID();
                    my $tmp = `./compareCheckdeps.sh gs_checkdeps.old gs_checkdeps.log | uniq -u`;
                    if ($tmp ne "") {
                        appendToFile("New makefile issues:\n\n$tmp\n\n", "email.txt");
                    } else {
                        appendToFile("No new makefile issues.\n\n", "email.txt");
                    }
                    `cp -p gs_checkdeps.log gs_checkdeps.old`;
                }
            }

            checkPID();
            logRun("./compare.pl ${prefix}current.tab ${prefix}previous.tab $jobs $job{elapsedTime} $machineCount false \"$job{product}\" false >>email.txt");
            logRun("./totalTime.pl ${prefix}current.tab ${prefix}previous.tab \"$job{product}\" >>email.txt");
            # mydbg "now running ./memAnalyze.pl previous.tab current.tab\n";
            # `./memAnalyze.pl previous.tab current.tab >>email.txt`;
            # `mail marcos.woehrmann\@artifex.com -s \"$job{title}\" <email.txt`;
            `findMakeIssues.pl $rev >>email.txt`;

            checkPID();
            `touch ${prefix}archive/$rev`;
            `rm -fr ${prefix}archive/$rev`;
            `mv queued/$rev ${prefix}archive/.`;
            `cp -p $job{logs} ${prefix}archive/$rev/.`;
            `cp -p gcc.txt ${prefix}archive/$rev/.`;
            `cp -p email.txt ${prefix}archive/$rev/.`;
            `cp -p $jobs archive/$rev/jobs`;
            `cp -p ${prefix}current.tab ${prefix}archive/$rev/current.tab`;
            `cp -p ${prefix}previous.tab ${prefix}archive/$rev/previous.tab`;
            if ($gccWarnings && $job{product} =~ /gs/) {
                `cp -p build$prisuf.log ${prefix}archive/$rev/build$prisuf.log`;
                # `mv gs_checkdeps.log ${prefix}archive/$rev/.`;
            }
            `gzip -f ${prefix}archive/$rev/*log`;
            `gzip -f ${prefix}archive/$rev/*make`;
            `gzip -f ${prefix}archive/$rev/*tab`;
            `gzip -f ${prefix}archive/$rev/jobs`;
            `cp -p ${prefix}current.tab ${prefix}archive/$rev.tab`;
            if ($job{gpdfRegression}) {
                `touch ${prefix}archive/$rev/gpdf`;
            }
        } elsif ($job{mupdfRegression} || $job{mujstestRegression}) {
            my $prefix="mupdf-";
            if ($job{mujstestRegression}) {
                $prefix="mujstest-";
            }
            `mv ${prefix}previous.tab ${prefix}previous2.tab`;
            `mv ${prefix}current.tab ${prefix}previous.tab`;
            `cat $job{tabs} | sort >${prefix}current.tab`;
            `rm $job{tabs}`;

            if (-e "gccwarnings") {
                checkPID();
                my $tmp = `./compareWarningsGCC.pl ${prefix}build$prisuf.old build$prisuf.log | uniq -u`;
                if ($tmp ne "") {
                    appendToFile("New warnings:\n\n$tmp\n","email.txt");
                } else {
                    appendToFile("No new warnings:\n","email.txt");
                }
                `cp build$prisuf.log ${prefix}build$prisuf.old`;
            }

            checkPID();
            logRun("./compare.pl ${prefix}current.tab ${prefix}previous.tab $jobs $job{elapsedTime} $machineCount false \"$job{product}\" false >>email.txt");
            logRun("./totalTime.pl ${prefix}current.tab ${prefix}previous.tab \"$job{product}\" >>email.txt");

            checkPID();
            mkdirEmpty("${prefix}archive/$rev");
            `cp -p $job{logs} ${prefix}archive/$rev/.`;
            `cp -p $jobs ${prefix}archive/$rev/jobs`;
            `cp -p ${prefix}md5sum.cache ${prefix}archive/$rev/.`;
            `cp -p ${prefix}current.tab ${prefix}archive/$rev/.`;
            `cp -p ${prefix}previous.tab ${prefix}archive/$rev/.`;
            `gzip -f ${prefix}archive/$rev/*`;
            `cp -p email.txt ${prefix}archive/$rev/.`;
            `cp -p ${prefix}current.tab ${prefix}archive/$rev.tab`;
            if (-e "compare.dbg") {
                `mv compare.dbg ${prefix}archive/$rev/.`;
            }
            if (-e "gccwarnings") {
                 `mv build$prisuf.log ${prefix}archive/$rev/${prefix}build$prisuf.log`;
            }
        } elsif ($job{bmpcmp}) {
            my $outputDir;
            if ($job{autoName}) {
                my $dir = $job{autoName};
                $dir =~ s/\//__/;
                $outputDir = "../public_html/$dir";
            } elsif ($job{userName} ne "") {
                $outputDir = "../public_html/$job{userName}";
            } else {
                mylog("What sort of bmpcmp job is this?!");
                return;
            }
            `mkdir $outputDir/bmpcmplogs$prisuf/`;
            `touch $outputDir/bmpcmplogs$prisuf/temp.log $outputDir/bmpcmplogs$prisuf/temp.out $outputDir/bmpcmplogs$prisuf/temp.make`;
            `rm -f $outputDir/bmpcmplogs$prisuf/*.log    $outputDir/bmpcmplogs$prisuf/*.out    $outputDir/bmpcmplogs$prisuf/*.make   `;
            `cp -f $jobs $outputDir/jobs`;
            `mv $job{logs} $outputDir/bmpcmplogs$prisuf/.`;

            unlink("bmpcmp$prisuf.tab");
            `echo >>bmpcmp$prisuf.tab`;
            `echo "bmpcmp errors:" >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `cat $job{tabs} | grep -v "differences" | grep -v "okay" | sort >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `echo "misc errors:" >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `cat $job{outs} | sort >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `echo "Tests showing differences:" >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `cat $job{tabs} | grep    "okay" | cut -f 1 -d " " | sort >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `echo "Tests showing no differences:" >>bmpcmp$prisuf.tab`;
            `echo >>bmpcmp$prisuf.tab`;
            `cat $job{tabs} | grep    "no differences" | cut -f 1 -d " " | sort >>bmpcmp$prisuf.tab`;

            if (0) {
                open(F9,">bmpcmp$prisuf/fuzzy.txt");
                my @logs=split ' ',$job{logs};
                foreach my $i (@logs) {
                    if (open(F8,"<$i")) {
                        my $name;
                        while (<F8>) {
                            chomp;
                            if(m/fuzzy (.+)/) {
                                $name=$1;
                            }
                            if (m/: (.+ max diff)/) {
                                print F9 "$name: $1\n";
                            }
                        }
                        close(F8);
                    }
                }
                close(F9);
            }
            open(F9, ">>bmpcmp$prisuf.tab");
            print F9 "\nJob/Node:\n";
            foreach my $m (keys %machine) {
                foreach my $j (@{$machine{$m}{jobs}}) {
                    $j =~ m/^(\S+)/;
                    print F9 "$1 $m\n";
                }
            }
            close(F9);
        } elsif ($job{coverage}) {
            # Unpack and collate coverage results
            mydbgv("Collating coverage results:");
            `touch bogus.gcov.out; rm -rf *.gcov.out`;
            `touch gcov.out; rm -rf gcov.out;mkdir gcov.out`;
            my @results = <*.coverage.tgz>;
            foreach my $r (sort @results) {
                $r =~ m/(.+)\.coverage\.tgz/;
                my $m = $1;
                mydbgv("Unpacking $r");
                `tar -zxvf $r`;
                `ls $m.gcov.out`;
                opendir(DIR, "$m.gcov.out") || die "can't opendir $m.gcov.out: $!";
                foreach my $t (readdir(DIR)) {
                   `cat $m.gcov.out/$t >> gcov.out/$t`;
                }
                closedir(DIR);
                `rm -rf $m.gcov.out`;
            }
            my $type = "ghostpdl";
            if ($job{product} =~ m/mupdf/ || $job{product} =~ m/mujstest/) {
                $type = "mupdf";
            }
            mydbgv("Kicking off background coverage collation: $job{autoName} $type $job{rev}\n");
            `echo "collateCoverageResults.pl $job{autoName} $type $job{rev} 2>&1 > auto/$job{autoName}/collate.log" > auto/$job{autoName}/collate.log"`;
	    my $runfree = fork();
	    if ($runfree == 0) {
                exec("collateCoverageResults.pl $job{autoName} $type $job{rev} 2>&1 >> auto/$job{autoName}/collate.log &");
		exit(0); # In case exec fails.
	    }
        } elsif ($job{autoRegression}) {
            # Auto jobs always test the same set of files. No 'missing' ones
            # to add back in.
            # Auto jobs always generate temp.tab. "reference" auto jobs
            # move temp.tab to current.tab at the end.
            `cat $job{tabs} | sort >temp.tab`;
            `rm $job{tabs}`;

            checkPID();

            `mkdir -p auto/$job{autoName}`;

            # Figure out which tab is which
            #  $currentTab = current Trunk results
            #  $oldTab     = previous results for this test
            my $tabPrefix = "";
            my $source = "ghostpdl";
            if ($job{autoMupdfRegression}) {
                $tabPrefix = "mupdf-";
                $source = "mupdf";
            } elsif ($job{autoMujstestRegression}) {
                $tabPrefix = "mujstest-";
                $source = "mupdf";
            } elsif ($job{autoGSViewRegression}) {
                $tabPrefix = "gsview-";
                $source = "gsview";
            }
            my $currentTab = "${tabPrefix}current.tab";
            my $archive = "auto/$job{autoName}";
            my $archivePrefix = "$archive/${tabPrefix}";
            my $oldTab = "${archivePrefix}temp.tab";

            # Output the git changes
            my $lastrev = $job{rev};
            if (open(Z,"<$archive/last.git")) {
                $lastrev=chump(<Z>);
                close(Z);
                open(GITLOCK, ">gitlock") || gitLockDie();
                flock(GITLOCK,LOCK_EX);
                my $gitchanges = `cd $source ; git log --oneline $lastrev..$job{rev}`;
                close(GITLOCK);
                if ($gitchanges ne "") {
                    appendToFile("Changes since last run:\n", "email.txt");
                    appendToFile($gitchanges, "email.txt");
                } else {
                    appendToFile("No git changes since last run.\n", "email.txt");
                }
            }
            `cp -f $jobs auto/$job{autoName}/jobs`;

            if ($gccWarnings &&
                ($job{product} =~ /gs/ ||
                 $job{autoMupdfRegression} ||
                 $job{autoGSViewRegression} ||
                 $job{autoMujstestRegression})) {
                my $b="";
                if (-e "build$prisuf.log") {
                    my $oldBuildLog = "$archive/$tabPrefix-build$prisuf.old";
                    if (-e $oldBuildLog) {
                        $b=`./compareWarningsGCC.pl $oldBuildLog build$prisuf.log | uniq -u`;
                    }
                    if ($b ne "") {
                        appendToFile("New warnings:\n\n$b", "email.txt");
                    } else {
                        appendToFile("No new warnings.\n", "email.txt");
                    }
                } else {
                     appendToFile("Missing build$prisuf.log!\n", "email.txt");
                }
            }

            # Do we something compare against?
            my $doCompare = 1;
            if (defined $job{ref} && defined $job{reference}) {
                # Don't compare to anything. Just a reference generation run.
                appendToFile("Reference generating run.\n", "email.txt");
                $doCompare = 0;
            } elsif (open(Z,"<$oldTab")) {
                close(Z);
            } else {
                appendToFile("No old results to compare to\n", "email.txt");
                $doCompare = 0;
            }

            if ($doCompare) {
                appendToFile("\n\nDifferences from previous test:\n\n", "email.txt");
                logRun("./compare.pl temp.tab $oldTab $jobs 1 1 true \"$job{product}\" false >>email.txt");
                logRun("./totalTime.pl temp.tab $oldTab \"$job{product}\" >>email.txt");
            }
            if (!$job{autoMupdfRegression} && !$job{autoMujstestRegression}) {
                logRun("./memAnalyze.pl temp.tab $archive/temp.tab >>email.txt");
                `findMakeIssues.pl $job{autoName} >>email.txt`;
            }

            if (!defined $job{ref} || $job{trunkCompare}) {
                appendToFile("\n\nDifferences from trunk:\n\n", "email.txt");
                if (!$job{autoMupdfRegression} && !$job{autoMujstestRegression}) {
                    logRun("./memAnalyze.pl current.tab temp.tab >>email.txt");
                }
                logRun("./compare.pl temp.tab $currentTab $jobs $job{elapsedTime} $machineCount true \"$job{product}\" true >>email.txt");
                logRun("./totalTime.pl temp.tab $currentTab \"$job{product}\" >>email.txt");
            }

            if (defined $job{ref}) {
                if (defined $job{reference}) {
                    mydbg("reference generation run\n");
                    # Reference generation run.
                    `cp -p temp.tab $archive/${tabPrefix}temp.tab`;
                    $archive .= "/ref";
                    mkdirEmpty("$archive");
                } else {
                    mydbg("reference comparison run\n");
                    `cp -p temp.tab $archive/new-${tabPrefix}temp.tab`;
                }
            } else {
                mydbg("trunk comparison run\n");
                `mv $oldTab $archive/${tabPrefix}previousTemp.tab`;
                `cp -p temp.tab $oldTab`;
                `cp -p ${tabPrefix}current.tab $archive/${tabPrefix}current.tab`;
            }
            `touch $archive/temp.log $archive/temp.out $archive/temp.make`;
            `rm -f $archive/*.log    $archive/*.out    $archive/*.make   `;
            `mv $job{logs} $archive/.`;
            `mv gcc.txt $archive/.`;
            `mv $jobs $archive/jobs`;
            `mv build$prisuf.log $archive/${tabPrefix}build$prisuf.log`;
            `cp -p email.txt $archive/.`;
            `cp -p email.txt results/.`;
        } else {  # $job{userRegression}
            if ($job{userMupdfRegression} || $job{userMujstestRegression}) {
                mydbg("Collating: $job{tabs}");
                `ls -al $job{tabs}`;
                `cat $job{tabs} | sort >temp.tab`;
                `rm $job{tabs}`;
            } else {
                # my @a=split ' ',$job{product};
                # my $filter="cat current.tab";
                # foreach (@a) {
                #     $filter.=" | grep -v \"\t$_\t\"";
                #     $filter.=" | grep -v \"\t$_ pdfwrite\t\"";
                #     $filter.=" | grep -v \"\t$_ ps2write\t\"";
                #     $filter.=" | grep -v \"\t$_ xpswrite\t\"";
                # }
                # $filter.=">t.tab";
                # mydbgv "$filter\n";
                # `$filter`;
                # `cat $job{tabs} t.tab | sort >temp.tab`;
                # `rm $job{tabs}`;
                `cat $job{tabs} >t1.tab`;
                mydbgv "./grepMissing.pl t1.tab current.tab >t2.tab";
                `./grepMissing.pl t1.tab current.tab >t2.tab`;
                `cat t1.tab t2.tab | sort >temp.tab`;
            }
            `cp -f $jobs $usersDir/$job{userName}/jobs`;

            checkPID();

            if ($gccWarnings &&
                ($job{product} =~ /gs/ ||
                 $job{userMupdfRegression} ||
                 $job{userMujstestRegression})) {
                my $b;
                if ($job{userMupdfRegression}) {
                    $b=`./compareWarningsGCC.pl mupdf-build$prisuf.old build$prisuf.log | uniq -u`;
                } elsif ($job{userMujstestRegression}) {
                    $b=`./compareWarningsGCC.pl mujstest-build$prisuf.old build$prisuf.log | uniq -u`;
                } else {
                    $b=`./compareWarningsGCC.pl build$prisuf.old build$prisuf.log | uniq -u`;
                }
                if ($b ne "") {
                    appendToFile("New warnings:\n\n$b", "email.txt");
                } else {
                    appendToFile("No new warnings.", "email.txt");
                }
            }

            if (0) {
                if ($gccWarnings && $job{product} =~ /gs/) {
                    `cd $usersDir/$job{userName}/ghostpdl ; ./gs/toolbin/checkdeps.pl | sort > ../../../gs_checkdeps.log ; cd ../../..`;
                    checkPID();
                    my $tmp = `./compareCheckdeps.sh gs_checkdeps.old gs_checkdeps.log | uniq -u`;
                    if ($tmp ne "") {
                        appendToFile("New makefile issues:\n\n$tmp\n\n", "email.txt");
                    } else {
                        appendToFile("No new makefile issues.\n\n", "email.txt");
                    }
                }
            }

            appendToFile("\n\nDifferences from trunk:\n\n", "email.txt");
            my $tabPrefix = "";
            if ($job{userMupdfRegression}) {
                $tabPrefix = "mupdf-";
            } elsif ($job{userMujstestRegression}) {
                $tabPrefix = "mujstest-";
            } else {
                # mydbg "now running ./memAnalyze.pl current.tab temp.tab\n";
                # `./memAnalyze.pl current.tab temp.tab >>email.txt`;
            }
            my $currentTab = "${tabPrefix}current.tab";
            my $archivePrefix = "$usersDir/$job{userName}/${tabPrefix}";
            my $oldTab = "${archivePrefix}temp.tab";
            logRun("./compare.pl temp.tab $currentTab $jobs $job{elapsedTime} $machineCount true \"$job{product}\" >>email.txt");
            logRun("./totalTime.pl temp.tab $currentTab \"$job{product}\" >>email.txt");

            appendToFile("\n\nDifferences from previous clusterpush:\n\n", "email.txt");

            logRun("./compare.pl temp.tab $oldTab $jobs 1 1 true \"$job{product}\" >>email.txt");
            logRun("./totalTime.pl temp.tab $oldTab \"$job{product}\" >>email.txt");

            # Hack here to allow gpdf to be compared to gs.
            if ($job{gpdfRegression}) {
                appendToFile("\n\nDifferences from trunk gs:\n\n", "email.txt");

                logRun("./crosscompare.pl temp.tab current.tab $jobs 1 1 true gpdf gs >>email.txt");
            }

            if (!$job{userMupdfRegression} && !$job{userMujstestRegression}) {
                # mydbg "now running ./memAnalyze.pl temp.tab $usersDir/$job{userName}/temp.tab\n";
                # `./memAnalyze.pl temp.tab $usersDir/$job{userName}/temp.tab >>email.txt`;
                `findMakeIssues.pl $job{userName} >>email.txt`;
            }
            `touch $usersDir/$job{userName}/temp.log $usersDir/$job{userName}/temp.out $usersDir/$job{userName}/temp.make`;
            `rm -f $usersDir/$job{userName}/*.log    $usersDir/$job{userName}/*.out    $usersDir/$job{userName}/*.make   `;
            `mv $job{logs} $usersDir/$job{userName}/.`;
            `mv gcc.txt $usersDir/$job{userName}/.`;
            `mv build$prisuf.log ${archivePrefix}build$prisuf.log`;
            if (!$job{userMupdfRegression} && !$job{userMujstestRegression} && $gccWarnings && !$job{bmpcmp}) {
                if (0) {
                    `mv gs_checkdeps.log $usersDir/$job{userName}/.`;
                }
            }
            `cp -p email.txt $usersDir/$job{userName}/.`;
            `cp -p email.txt results/.`;
            `mv    $oldTab ${archivePrefix}previousTemp.tab`;
            `cp -p temp.tab $oldTab`;
            `cp -p ${tabPrefix}current.tab ${archivePrefix}current.tab`;
        }
    } else {
        # Failed
        open (F,">>email.txt") or mydbg("Failed to open email.txt");
        print F "An error occurred that prevented the local cluster run from finishing:\n\n$job{failMessage}\n";
        foreach my $m (keys %machine) {
            if ($machine{$m}{buildFail}) {
                print F "\n\n$m log:\n\n";
                `tail -100 $m.log >temp.log`;
                open(F2,"<temp.log");
                while(<F2>) {
                    print F $_;
                }
                close(F2);
                print F "\n\n$m stdout:\n\n";
                `tail -100 $m.out >temp.out`;
                open(F2,"<temp.out");
                while(<F2>) {
                    print F $_;
                }
                close(F2);
            }
            if ($machine{$m}{timeoutFail}) {
                print F "\n\n$m log:\n\n";
                my $msg="";
                open(F2,"<$m.log");
                while(<F2>) {
                    if (m/^===/) {
                        $msg="" ;
                    }
                    $msg.=$_;
                    if (m/killed: timeout/) {
                        print F "$msg\n";
                    }
                }
                close(F2);
                my $print=0;
                open(F2,"<$m.log");
                while(<F2>) {
                    chomp;
                    $msg=$_;
                    if ($msg =~ m/nodeFail: log file/) {
                        $print=1;
                    }
                    if ($print) {
                        print F "$msg\n";
                    }
                }
                close(F2);
            }
        }
        if ($job{normalRegression} || $job{mupdfRegression} || $job{mujstestRegression}) {
            my $rev = $job{rev};
            my $tempRev="archive/$rev";
            if ($job{mupdfRegression}) {
                $tempRev="mupdf-archive/$rev";
            }
            if ($job{mujstestRegression}) {
                $tempRev="mujstest-archive/$rev";
            }
            `touch $tempRev`;
            `rm -fr $tempRev`;
            # `mkdir $tempRev`;
            `mv queued/$rev $tempRev`;
            `mv $job{logs} $tempRev/.`;
            `mv gcc.txt $tempRev/.`;
            `gzip -f $tempRev/*log`;
            `cp -p email.txt $tempRev/.`;
            if ($job{gpdfRegression}) {
                `touch $tempRev/gpdf`;
            }
        } elsif ($job{autoRegression}) {
            `touch auto/$job{autoName}/temp.log auto/$job{autoName}/temp.out auto/$job{autoName}/temp.make`;
            `rm -f auto/$job{autoName}/*.log    auto/$job{autoName}/*.out    auto/$job{autoName}/*.make   `;
            `mv $job{logs} auto/$job{autoName}/.`;
            `mv gcc.txt auto/$job{autoName}/.`;
            `cp -p email.txt auto/$job{autoName}/.`;
        } else {
            `touch $usersDir/$job{userName}/temp.log $usersDir/$job{userName}/temp.out $usersDir/$job{userName}/temp.make`;
            `rm -f $usersDir/$job{userName}/*.log    $usersDir/$job{userName}/*.out    $usersDir/$job{userName}/*.make   `;
            `mv $job{logs} $usersDir/$job{userName}/.`;
            `mv gcc.txt $usersDir/$job{userName}/.`;
            `cp -p email.txt $usersDir/$job{userName}/.`;
        }
        close(F);
    }
}

sub finishAndFileJobReport
{
    # Restrict the size of the report
    `/usr/bin/head -c10MB email.txt >tmp.tmp ; /bin/mv tmp.tmp email.txt`;

    my $emailTo = "gs-regression\@artifex.com";
    my $subject = $job{title};
    my $magic = "";#" (xefitra)";
    if ($job{userRegression}) {
        $emailTo = $emails{$job{userName}};
        $magic="";
        if ($job{bmpcmp}) {
            $subject = "bmpcmp finished";
            if ($job{bmpcmphead}) {
                $subject = "bmpcmphead finished";
            }
            `mv bmpcmpResults.txt bmpcmpResults.txt.old`;
            `echo >>bmpcmpResults.txt`;
            `echo http://www.ghostscript.com/~regression/$job{userName} >>bmpcmpResults.txt`;
            `echo >>bmpcmpResults.txt`;
            `cat bmpcmp.tab >>bmpcmpResults.txt`;
            `cp -f bmpcmpResults.txt email.txt`;
        } else {
            my $tmp = `head -2500 $job{userName}.diff`;
            if ($tmp ne "") {
                appendToFile("Source differences from trunk (first 2500 lines):\n$tmp\n", "email.txt");
            } else {
                appendToFile("No source differences from trunk.:\n", "email.txt");
            }
        }
    }

    #if ($job{autoRegression}) {
    #    $emailTo="robin.watts\@artifex.com";
    #}

    if (! defined $job{reference} && !defined $job{coverage}) {
        mydbg "Running: mail -a \"From: cluster\@artifex.com\" $emailTo -s \"$subject$magic\" <email.txt\n";
        $subject =~ s/"/\\"/g;
        my $a = `mail -a \"From: cluster\@artifex.com\" $emailTo -s \"$subject$magic\" <email.txt`;
        mydbg "$a";
    }
}

sub completeJob {
    if ($job{gpdfRegression}) {
        my $rev = $job{rev};

        mydbg "test complete, performing final git update\n";
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        #logRun("cd ghostpdl ; git checkout pdfi ; git reset --hard $rev ; git checkout master");
        #logRun("cd ghostpdl ; git branch -q -f pdfi $rev");
        logRun("cd ghostpdl ; git update-ref -m \"Cluster run $rev complete\" refs/heads/pdfi $rev");
        close(GITLOCK);
        logRun("./cp.all.sh");
        logRun("./cachearchive.pl > md5sum.cache");
        logRun("./segfaults.pl");
        mydbg("finished cachearchive.pl");
    } elsif ($job{normalRegression}) {
        my $rev = $job{rev};

        mydbg "test complete, performing final git update\n";
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        #logRun("cd ghostpdl ; git branch -q -f master $rev");
        #logRun("cd ghostpdl ; git checkout master && git reset --hard $rev");
        logRun("cd ghostpdl ; git update-ref -m \"Cluster run $rev complete\" refs/heads/master $rev && git reset --hard");
        close(GITLOCK);

        logRun("./cp.all.sh");
        logRun("./cachearchive.pl >md5sum.cache");
        logRun("./segfaults.pl");
        mydbg("finished cachearchive.pl");
    } elsif ($job{mupdfRegression}) {
        my $rev = $job{rev};
        logRun("./cachearchive.pl mupdf >mupdf-md5sum.cache");
        mydbg("finished cachearchive.pl mupdf");
        mydbg "mupdf test complete, performing final git update\n";
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        logRun("cd mupdf ; git reset -q --hard ; git checkout -q master 2>/dev/null ; git reset -q --hard $rev ; git submodule update --init 2>/dev/null");
        close(GITLOCK);
    } elsif ($job{mujstestRegression}) {
        my $rev = $job{rev};
        logRun("./cachearchive.pl mujstest >mujstest-md5sum.cache");
        mydbg("finished cachearchive.pl mujstest");
        mydbg "mujstest test complete, performing final git update\n";
        mydbg "cd mupdf ; git checkout -q master ; git reset -q --hard $rev\n";
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        logRun("cd mupdf ; git reset -q --hard ; git checkout -q master 2>/dev/null ; git reset -q --hard $rev ; git submodule update --init 2>/dev/null");
        close(GITLOCK);
    } elsif ($job{autoRegression}) {
        `touch auto/$job{autoName}/stoptime`;
        open(GITLOCK, ">gitlock") || gitLockDie();
        flock(GITLOCK,LOCK_EX);
        if (!defined $job{ref}) {
            if ($job{autoMupdfRegression} || $job{autoMujstestRegression}) {
                `(cd mupdf ; git rev-parse $job{rev}) > auto/$job{autoName}/last.git`;
            } else {
                `(cd ghostpdl ; git rev-parse $job{rev}) > auto/$job{autoName}/last.git`;
            }
        }
        close(GITLOCK);
    } elsif ($job{userRegression}) {
        `touch $usersDir/$job{userName}/stoptime`;
    } else {
        mydbg("internal error");
    }
}

sub createBmpcmpOutput
{
    updateStatus("$job{completeText} - generating results");

    my $outputDir;
    if ($job{autoName}) {
        my $dir = $job{autoName};
        $dir =~ s/\//__/;
        $outputDir = "../public_html/$dir";
    } elsif ($job{userName} ne "") {
        $outputDir = "../public_html/$job{userName}";
    } else {
        return;
    }
    mydbg("Starting to create output in $outputDir");
    `touch $outputDir`;
    `rm -fr $outputDir`;
    `mkdir $outputDir`;
    `chmod 777 $outputDir`;
    `cd $outputDir; ln -s compare.html index.html`;
    logRun("./pngs2html.pl bmpcmp$prisuf $outputDir");
    mydbg("bmpcmp run complete");
    # `cp bmpcmp/fuzzy.txt $outputDir/.`;
}

# Actual work starts here
# main

openDebugOutput();

# Whenever the clustermaster starts up, detect any pending jobs
# and move then onto the queue. Lower ones get run first.
if ($pri == 1) # Should be 1
{
    enqueueGhostPDLJobs();
    enqueueMuPDFJobs();
}
#enqueueMuPDFFormsJobs();
if($pri == 0)
{
    enqueueUserJobs();
}

# Check to see if there are other clustermasters running.
# If there are, this won't return. If there aren't, then
# we become the current one.
checkForOtherClusterMasters();

# Periodically we call checkPID. This checks to see if the
# PID in the semaphore file is ours. If not, something has
# gone wrong, and we need to shut down.
$job{abort}=0;
checkPID();

# We no longer communicate using files left around as droppings
# (We might keep the files around for the html dashboard, but
# the files are now informative, rather than required).
#`touch temp.start ; rm *.start`;
#`touch temp.abort ; rm *.abort`;

%emails = getEmails();
my $regression = peekAtTopOfQueue();

# If there is no job to do, then just exit.
if (!$regression) {
    exitNeatly();
}

# Now look at the value of $regression and figure out what sort
# of job we want to run.
if ($regression =~ m/^git (.+)$/) {
    %job = startGhostPDLRegression($regression, $1, "master");
} elsif ($regression =~ m/^gpdf (.+)$/) {
    %job = startGhostPDLRegression($regression, $1, "gpdf");
} elsif ($regression=~/auto (.+)/) {
    %job = startAutoRegression($regression, $1);
} elsif ($regression=~/user (.+)/) {
    %job = startUserRegression($regression, $1);
} elsif ($regression=~/mupdf (.+)/) {
    %job = startMuPDFRegression($regression, $1);
} elsif ($regression=~/mujstest (.+)/) {
    %job = startMuJSTestRegression($regression, $1);
} elsif ($regression=~/gsview/) {
    %job = startGSViewBuildTest();
} else {
    print DBG "\n";
    mydbg "found unknown entry in $queue, removing";
}

if (! defined $job{skip}) {

    $job{product} = cleanSpaces($job{product});

    # Build the actual list of required jobs, in the $jobs file.
    buildJobList();

    checkPID();

    # If we are running a bmpcmp, then we need to figure out what
    # products to build from the tests that need to be run.
    if ($job{bmpcmp}) {
        mydbg("Looking for product: Was '$job{product}'");
        findProductForBmpcmp();
        mydbg("Finished looking for product: Is now '$job{product}'");
    }
    # Some job types (bmpcmp ones) can't make the trigger until after
    # the bmpcmp products have been found.
    if (defined $job{makeTrigger})
    {
        $job{trigger} = $job{makeTrigger}->();
    }
    checkPID();

    # Run the actual job (possibly trying several times)
    runTheJob();

    if (!$job{abort}) {
        my $s = `date -u +"%Y/%m/%d %H:%M:%S"`;
        chomp $s;
        $job{completeText} = "$job{title} started at $job{id} - $job{elapsedTime}s elapsed - finished at $s";
        updateStatus("$job{completeText} - collating");

        open(COLLATELOCK,">collatelock") or die "Couldn't open collatelock";
        flock(COLLATELOCK,LOCK_EX);
        # Now we prepare the report for the job we just ran.
        unpackBuildLogs();

        createJobReport();

        populateJobReport();

        finishAndFileJobReport();

        completeJob();
        close(COLLATELOCK);

        if ($job{bmpcmp}) {
            createBmpcmpOutput();
        }

        updateStatus("$job{completeText}");
    }
}

checkPID();
if (!$job{abort}) {
    removeTopEntryFromQueue();
}

checkPID();
exitNeatly();
