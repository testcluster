#!/usr/bin/perl

# Credit for the ideas in this script go to Marcos. I've collated
# several of his scripts here into a single perl file.

# The gcov data is all in gcov.out.
# The script is invoked as: collateCoverageResults.pl <autoname> <product> <rev>
# where product is "ghostpdl" or "mupdf".

use strict;
use warnings;
use Digest::MD5 qw(md5_hex);
use File::Basename;

sub html_header($$)
{
    my $fh = shift();
    my $inputfile = shift();
	
    print $fh <<HTML;
<html lang="en">
  <head>
  <title>$inputfile</title>
  <meta charset="utf-8">
  <style type="text/css">
  a[data-tooltip]
        {
        cursor:pointer !important;
        }
  *[data-tooltip]
        {
        position:relative;
        cursor:help;
        text-decoration:none;
        /* Remove the styles for IE7 and below - could be passed using conditional comments */
        *text-decoration:inherit;
        *border-bottom-width:inherit;
        *border-bottom-style:inherit;
        *cursor:inherit;
        *position:inherit;
        }
  *[data-tooltip]:after,
  *[data-tooltip]:before
        {
        content:"";
        /* Don't show tooltip by default */
        opacity:0;
        /* Set a high z-index */
        z-index:999;
        }
  /* Tooltip body - shown on hover or focus */
  *[data-tooltip]:hover:after,
  *[data-tooltip]:focus:after
        {
        opacity:0.94;
        display:block;
        content:attr(data-tooltip);
        position:absolute;
        min-width:100px;
        padding:5px;
        line-height:14px;
        min-height:30px;
        color:#000000;
        font-size:11px;
        font-weight:normal;
        font-family:Verdana, Geneva, sans-serif;
        background:#FFFFcc;
        -moz-box-shadow:4px 4px 12px #888;
        -webkit-box-shadow:4px 4px 12px #888;/
        box-shadow:4px 4px 12px #888;
        }
  </style>
  </head>
  <body>
<pre>
HTML
}

my @args = @ARGV;
my $autoname = $args[0];
my $product  = $args[1];
my $rev      = $args[2];
if (!defined $product || $product eq "") {
    $product="ghostpdl";
}
if (!defined $rev || $rev eq "") {
    $rev="master";
}
#print "$product\n";

my $date = `date`;
print "Starting: $date\n";

my $autodir = "auto/$autoname";
my $htmldir = "auto/$autoname/html";
`touch $htmldir; rm -rf $htmldir`;
`mkdir $htmldir`;

sub random ($$) {
    my $md5=shift;
    my $dir=shift;
    my @list;
    my $count=3;
    open(F,"<$dir/md5/$md5.html") || die "file $dir/md5/$md5.html not found";
    while(<F>) {
        chomp;
        push @list,$_ if (!m/^</);;
    }
    close(F);
    my $t='';
    while($count>0 && scalar(@list)) {
        my $n=int(rand(scalar(@list)));
        $t.="$list[$n]\n";
        splice(@list,$n,1);
        $count--;
    }
    $t.="and ".(scalar(@list))." more.\n";
    return($t);
}

# Copy us a source dir (actually a git repo) and update the revision
my $srcdir = "auto/$autoname/$product";
`touch $srcdir; rm -rf $srcdir`;
`cp -pr $product $srcdir`;
`cd $srcdir; git reset --hard $rev`;

# Eliminate duplicates in the files. There shouldn't be any, but Marcos felt the need to
# do this.
my @files=<gcov.out/*.out>;
print "Eliminating duplicate results\n";
foreach my $f (@files) {
    open(F,"<$f") || die "Couldn't open $f";
    my %seen;
    open(G,">$f.nodup") || die "Couldn't open $f.nodup";
    while(<F>) {
        my @a=split ' ',$_,2;
        if (exists $seen{$a[0]}) {
	    print "Ignoring duplicate $a[0] in %f\n";
	} else {
            print G $_;
            $seen{$a[0]}=1;
        }
    }
    close(F);
    close(G);
    `mv $f.nodup $f`;
}

# Now for every output file, read it in, and prepare the html.
@files=<gcov.out/*.out>;
# We have 5 extra lines of data at the start for each test record
# that we need to skip.
my $extraLines = 5;
foreach my $f (@files) {
    my @tests;
    my @lines;
    my @digests;
    open(F,"<$f") || die "Couldn't open $f";
    print "Processing $f\n";
    while(<F>) {
	chomp;
        my @a=split ' ';
        push @tests,$a[0]; # testname
        my $s=reverse $a[1]; # 1 char per line of data
        # Skip the prefix of data
        for (my $i=0; $i<$extraLines; $i++) {
            chop $s;
        }
	# Create a transposed version of the data
        my $n=length($s);
        for (my $i=0;  $i<$n;  $i++) {
            $lines[$i].=chop $s;
        }
    }

    $f =~ m/gcov.out\/(.*)\.out/;
    my $inputfile = $1;
    $inputfile =~ s/__/\//g;
    my $dir = dirname("$htmldir/$inputfile.html");
    if (!-e "$dir/md5") {
        `mkdir -p $dir/md5`;
    }

    # Now, for every line in the (.c) source file,
    # we need to create a tooltip file that lists
    # the files it uses.
    # Because the ordering of the tests is unique
    # to each source file, we include the source
    # filename in the digest. Marcos wasn't doing
    # this, which, I fear, leaves us open to possible
    # collisions.
    for (my $i=0;  $i<scalar(@lines);  $i++) {
        my $digest = md5_hex($f.$lines[$i]);
        if (-e "$dir/md5/$digest.html") {
            #print "md5/$digest.html - exists\n";
        } else {
	    #print "Creating $f.$lines[$i] -> $digest\n";
            my @results = ();
            my @b=split "",$lines[$i];
            for (my $i=0;  $i<scalar(@b);  $i++) {
                if ($b[$i] eq '1') {
                    push @results,$tests[$i]
                }
            }
            open(F2,">$dir/md5/$digest.html") || die "can't write to file $dir/md5/$digest.html";
            print F2 "<html>\n<body>\n<pre>\n";
            foreach (sort @results) {
                print F2 "$_\n";
            }
            print F2 "</pre>\n</body>\n</html>\n";
            close(F2);
        }
        $digests[$i]=$digest;
    }

    # Now we can actually build the HTML output.
    if (open(G,"<$srcdir/$inputfile")) {
        # Create the path down to where we want to output the html.
        open(my $fh,">$htmldir/$inputfile.html") || die "can't open $htmldir/$inputfile.html";
	html_header($fh, $inputfile);
	my $linenum = 1;
	my $prevMd5 = "";
        while (<G>) {
            chomp;
	    my $line = $_;
            my $md5 = $digests[$linenum-1];
            $line=sprintf("%4d: %s",$linenum,$line);
	    my $hack = $lines[$linenum-1];
            # Trailing lines in the data file may not be defined, so cope with this.
	    if (!defined $hack) {
		$hack="";
	    }
	    my $match = ($hack =~ tr/1/!/);
	    if ($match == 0) {
	        $match = ($hack =~ tr/0/+/);
		if ($match == 0) {
		    # Comment only line (grey).
                    print $fh "<a style=\"background-color:#FFFFFF;color:#AFAFAF;text-decoration:none\">$line</a>\n";
		} else {
		    # Code line that nothing called (red).
                    print $fh "<a style=\"background-color:#FFFFFF;color:#FF0000;text-decoration:none\">$line</a>\n";
		}
	    } else {
                my $t=random($md5,$dir);
		if ($prevMd5 eq $md5) {
                    print $fh "<a href='md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFFF;color:#000000;text-decoration:none\">$line</a>\n";
		} else {
                    print $fh "<a href='md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFAF;color:#000000;text-decoration:none\">$line</a>\n";
                    $prevMd5=$md5;
		}
	    }
	    $linenum++;
        }
        print $fh "</pre></body></html>";
        close($fh);
        close(G);
    } else {
	print "Failed to open $srcdir/$inputfile - ignoring\n";
    }
}

$date = `date`;
print "Finished: $date\n";

#sub myMkdir($) {
#  my $d=shift;
#  $d=~m/(.+)\//;
#  $d=$1;
##print "$d\n";
#  `mkdir -p $d`;
#}
#
#sub checkEmpty($) {
#  my $md5=shift;
#  my $count=0;
#  open(F,"<md5/$md5.html") || die "file md5/$md5.html not found";
#  while(<F>) {
#    chomp;
#    $count++ if (!m/^</);;
#  }
#  close(F);
##print "$count ";
#  return(1) if ($count==0);
#  return(0);
#}
#
#
#sub random ($) {
#  my $md5=shift;
#  my @list;
#  my $count=3;
#  open(F,"<md5/$md5.html") || die "file md5/$md5.html not found";
#  while(<F>) {
#    chomp;
#    push @list,$_ if (!m/^</);;
#  }
#  close(F);
#  my $t='';
#  while($count>0 && scalar(@list)) {
#    my $n=int(rand(scalar(@list)));
#    $t.="$list[$n]\n";
#    splice(@list,$n,1);
#    $count--;
#  }
#  $t.="and ".(scalar(@list))." more.\n";
#  return($t);
#}
#
#my @files=<*trans>;
##print Dumper(\@files);  exit;
#
#foreach my $file (@files) {
#
#  my $f2=$file;
#  $f2=~s/.trans$//;
#  my $a=`gunzip $f2 ; head -n 1 $f2 ; gzip $f2`;
#  my @a=split ' ',$a;
#  my @b=split '',$a[1];
#  
#
#  # harfbuzz is hard to profile, so we skip those files we know aren't going to be valid
#  next if($file=~m/\-machine/);
#  next if($file=~m/^NONE/);
#  
#  my $f=$file;
#  $f=~s/source\///;
#  $f=~s/\.tab$//;
#  $f=~s/\.trans$//;
#  $f=~s/\.out$//;
#  $f=~s/__/\//g;
#  if ($product eq "mupdf") {
#    if (!open(F2,"<mupdf/$f")) {
#      $f=~s/debugobj/jpeg/;
#      if (!open(F2,"<mupdf/$f")) {
#          $f=~s/libopenjpeg/src\/lib\/openjp2/;
#print "$f\n";
#          if (!open(F2,"<mupdf/$f")) {
#          $f=~s/openjp2/openmj2/;
#print "$f\n";
#          if (!open(F2,"<mupdf/$f")) {
#            print STDERR "source file matching $file not found\n";
##exit;
#            next;
#          }
#        }
#      }
#    }
#  } 
#
##cups__gdevcups.c.out.trans cups/gdevcups.c
##base/gsmd5.c
##debugobj__gsmd5.c.out.trans base/gsmd5.c
##base/jcapimin.c
##base/jcapimin.c
##source file matching debugobj__jcapimin.c.out.trans not found
#
#  if ($product eq "pcl" || $product eq "xps") {
#    if (!open(F2,"<ghostpdl/$f")) {
#      $f=~s/debugobj/base/;
#print "1: $f\n";
#      if (!open(F2,"<ghostpdl/$f")) {
#        $f=~s/base/jpeg/;
#print "2: $f\n";
#        if (!open(F2,"<ghostpdl/$f")) {
#          print STDERR "source file matching $file not found\n";
#exit;
#          next;
#        }
#      }
#    }
#  } 
#  if ($product eq "gs") {
#    if (!open(F2,"<ghostpdl/$f")) {
#      $f=~s/debugobj/base/;
#print "$f\n";
#      if (!open(F2,"<ghostpdl/$f")) {
#        $f=~s/base/jpeg/;
#print "$f\n";
#        if (!open(F2,"<ghostpdl/$f")) {
#          print STDERR "source file matching $file not found\n";
#exit;
#          next;
#        }
#      }
#    }
#  } 
#
#
#print "$file $f\n";
#    $f.=".html";
#    myMkdir("html/$dir/$f");
#    open(F3,">html/$dir/$f") || die "can't write to html/$dir/$f";
#    print F3 <<HTML;
#<html lang="en">
#<head>
#  <meta charset="utf-8">
#  <style type="text/css">
#  a[data-tooltip]
#        {
#        cursor:pointer !important;
#        }
#  *[data-tooltip]
#        {
#        position:relative;
#        cursor:help;
#        text-decoration:none;
#        /* Remove the styles for IE7 and below - could be passed using conditional comments */
#        *text-decoration:inherit;
#        *border-bottom-width:inherit;
#        *border-bottom-style:inherit;
#        *cursor:inherit;
#        *position:inherit;
#        }
#  *[data-tooltip]:after,
#  *[data-tooltip]:before
#        {
#        content:"";
#        /* Don't show tooltip by default */
#        opacity:0;
#        /* Set a high z-index */
#        z-index:999;
#        }
#  /* Tooltip body - shown on hover or focus */
#  *[data-tooltip]:hover:after,
#  *[data-tooltip]:focus:after
#        {
#        opacity:0.94;
#        display:block;
#        content:attr(data-tooltip);
#        position:absolute;
#        min-width:100px;
#        padding:5px;
#        line-height:14px;
#        min-height:30px;
#        color:#000000;
#        font-size:11px;
#        font-weight:normal;
#        font-family:Verdana, Geneva, sans-serif;
#        background:#FFFFcc;
#        -moz-box-shadow:4px 4px 12px #888;
#        -webkit-box-shadow:4px 4px 12px #888;/
#        box-shadow:4px 4px 12px #888;
#        }
#  </style>
#  </head>
#  <body>
#<pre>
#HTML
#
#  my $previousMd5=0;
#  my $count=1;
#  open(F1,"<$file") || die "file $file not found";
#  while(<F1>) {
#    chomp;
#    my $md5=$_;
#    my $line=<F2>;
#    if (!$line) {
#      print STDERR "file $f too short compared to $file\n";
#exit;
#      $line="\n";
#    }
#    chomp $line;
#    $line=sprintf("%4d: %s",$count,$line);
#    if (checkEmpty($md5)) {
#      if ($b[$count-1+5] eq '-') {
##print "1 ";
#        print F3 "<a style=\"background-color:#FFFFFF;color:#AFAFAF;text-decoration:none\">$line</a>\n";
#      } else {
##print "2 ";
#        print F3 "<a style=\"background-color:#FFFFFF;color:#FF0000;text-decoration:none\">$line</a>\n";
#      }
#    } else {
#      my $t=random($md5);
#      if ($previousMd5 eq $md5) {
##       print F3 "<a href='/coverage/md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFFF;color:#A0A0FF;text-decoration:none\">$line</a>\n";
##print "3 ";
#        print F3 "<a href='/coverage/$dir/md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFFF;color:#000000;text-decoration:none\">$line</a>\n";
#      } else {
##       print F3 "<a href='/coverage/md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFFF;color:#0000FF;text-decoration:none\">$line</a>\n";
##print "4 ";
#        print F3 "<a href='/coverage/$dir/md5/$md5.html' data-tooltip=\n\"$t\" style=\"background-color:#FFFFAF;color:#000000;text-decoration:none\">$line</a>\n";
#        $previousMd5=$md5;
#      }
#    }
##print "$md5 $b[$count-1+5] $line\n";
#    $count++;
#  }
#  print F3 <<HTML;
#</pre>
#</body>
#</html>
#HTML
#  close F1;
#  close F2;
#  close F3;
#}
