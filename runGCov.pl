#!/usr/bin/perl

# This routine is invoked to run a coverage test, and to extract
# the data into a usable form. The tricky thing is that we want
# to have multiple coverage tests running at once, and the gcov
# tool assumes that only 1 can run at once. This requires us to
# copy the obj directory contents.
#
# A typical invocation of this script would be:
#
# runGCov.pl testname bin/gs -sDEVICE=ppmraw -o /dev/null ....
#
# with an environment var RUNGCOV_OBJDIR set to the obj directory.
# and RUNGCOV_OUTDIR set to the out directory.
#
# The script then does the following:
#
# 1) Make a tempdir
# 2) Copy objdir/*.gcno -> tempdir
# 3) set GCOV_PREFIX_STRIP and GCOV_PREFIX so that when we run
#    the gcda files are created in the tempdir
# 4) Run the job
# 5) Process the results using gcov to get gcov and summary files.
# 6) Process the gcov and summary files into our global output
#    directory.

use strict;
use warnings;
use Cwd;
use Fcntl ':flock';

# Make a tempdir (deleting any stray old one)
my $tempdir = getcwd."/gcovtemp.$$";
`touch $tempdir`;
`rm -rf $tempdir`;
`mkdir $tempdir`;
`mkdir $tempdir/done`;

# Prepare directories
my $outdir = $ENV{'RUNGCOV_OUTDIR'};
print "RUNGCOV_OUTDIR=$outdir\n";
if (!defined $outdir || $outdir eq "") {
    my $dir= getcwd;
    $outdir="$dir/gcov.out";
    print "RUNGCOV_OUTDIR not set; assuming $outdir\n";
}
my $objdir = $ENV{'RUNGCOV_OBJDIR'};
print "RUNGCOV_OBJDIR=$objdir\n";
if (!defined $objdir || $objdir eq "") {
    my $dir= getcwd;
    $objdir="$dir/obj";
    print "RUNGCOV_OBJDIR not set; assuming $objdir\n";
}
my $objdir_prefix;
my $objdir_suffix;
if ($objdir =~ m|(.*)/(.*)|) {
    $objdir_prefix = "$1/";
    $objdir_suffix = $2;
} else {
    $objdir_prefix = "./";
    $objdir_suffix = $objdir;
}

# Ensure we have an output dir
`mkdir $outdir`;
`mkdir $tempdir/$objdir_suffix`;

# Copy the gcno files
#print "cd $objdir_prefix ; find $objdir_suffix -name \\*.gcno -exec echo cp --parents {} $tempdir \\;\n";
`cd $objdir_prefix ; find $objdir_suffix -name \\*.gcno -exec cp --parents {} $tempdir \\;`;

# Set GCOV_PREFIX_STRIP and GCOV_PREFIX
my $depth = ($objdir =~ tr/\///);
if ($depth > 1) {
    $depth--;
}
$ENV{'GCOV_PREFIX_STRIP'} = $depth;
$ENV{'GCOV_PREFIX'} = "$tempdir";
#print "Need to convert $objdir -> $tempdir\n";
#print "GCOV_PREFIX_STRIP=$ENV{'GCOV_PREFIX_STRIP'}\n";
#print "GCOV_PREFIX=$ENV{'GCOV_PREFIX'}\n";

# Run the job
my @args = @ARGV;
my $testname = $args[0];
shift @args;
system @args;
my $ret = $?;

# Process results to get summaries and gcov files
my $b=`cd $tempdir ; find . -name \\*.gcno -print`;
my @b=split "\n",$b;
foreach my $b (@b) {
    #print "Examining $b\n";
    $b =~ m|(.+)/|;
    my $dir=$1;
    `cd $tempdir ; gcov --function-summaries -o $dir $b > $b.summary 2>/dev/null`;
    my @files=<$tempdir/*.gcov>;
    #print "@files\n";
    foreach my $f (@files) {
	my $c=`head -1 $f`;
	chomp $c;
	$c=~s/.+://;
	$c=~s/^\.\.//;
	$c=~s/^\.//;
	$c=~s/^\///;
	$c=~s/\/\//\//;
	$c=~s/\//__/g;
	#   print "$f $c\n";
	`mv $f $tempdir/done/$c.gcov`;
    }
    # print "\n";
}

# Process .gcov files and append their data to the global .out files
my @files=<$tempdir/done/*.gcov>;
foreach my $file (@files) {
    my $file2 = $file;
    open (F,"<$file") || die "file $file not found";
    $file2 =~ s/\.gcov/.out/;
    $file2 =~ s/$tempdir\/done/$outdir/;
    open (F2,">>$file2") || die "can't write to $file2";
    flock(F2, LOCK_EX) or die "Could not lock $file2";
    #print "Processing $file -> $file2\n";
    print F2 "$testname ";
    while(<F>) {
        chomp;
        my @a=split ":",$_,3;
        if (scalar(@a)==3) {
            my $c=$a[0];
            my $l=$a[1]+0;
            if ($c =~ m/ +\-/) {
                print F2 "-";
            } elsif ($c =~ m/ +\d+/) {
                print F2 "1";
            } else {
                print F2 "0";
            }
        } else {
            print F2 "\nunexpected line: $_\n";
        }
    }
    print F2 "\n";
    close(F2);
    close(F);
}

# Process .summary files into executed tables
my @summaryFiles=`find $tempdir -name \\*.summary -print`;
my %executed;
foreach my $file (@summaryFiles) {
    chomp $file;

    my %exe;
    my $func="";
    open(F,"<$file") || die "file $file not found";
    while(<F>) {
        chomp;
	if (m/Function '(.+)'/) {
	    $func=$1;
	}
	if (m/Lines executed:(.+). of/) {
	    my $count=$1;
	    $exe{$func}=($count > 0 ? 1 : 0);
	}
	if (m/File '(.+)'/) {
	    my $f = $1;
	    foreach my $key (keys %exe) {
		$executed{$f}{$key}=$exe{$key};
		delete $exe{$key};
	    }
	}
    }
    close(F);
}

open(F,">$outdir/$testname.summary");
foreach my $i (sort keys %executed) {
  foreach my $j (sort keys %{$executed{$i}}) {
    print F "$i\t$j\t$executed{$i}{$j}\n";
  }
}
close(F);


# 6) Delete tempdir
`rm -rf $tempdir`;

exit($ret);
