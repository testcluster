#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;
use POSIX ":sys_wait_h";

use File::stat;

use File::Basename;
use File::Spec;
use File::Temp qw/ tempfile tempdir /;
use Fcntl ':flock';

use threads;
use threads::shared;

# Detect what priority level we are by what name we are being run as.
my $pri = 0;
my $prisuf= "";
if ($0 =~ m/run\.(\d).pl/) {
    $pri = $1;
    $prisuf = ".$1";
}

#my $thisDir=dirname(File::Spec->rel2abs(__FILE__));

$ENV{LC_ALL} = "C";

# Negative failcodes are fatal for the entire run, not
# just this node.
my %failcodes = (
    "md5sum" => 1,
    "systemWithRetry" => 2,
    "compile" => -1
    );

# When in doubt mark variables as shared, so they can be
# used in both the main and task threads.
my $host                  :shared = "cluster.ghostscript.com";

my $compileFail           :shared = "";
my %logged                :shared;
my $user                  :shared;
my $autoName              :shared;
my $revs                  :shared = 0;
my $bmpcmp                :shared = 0;
my $mupdf                 :shared = 0;
my $mujstest              :shared = 0;
my $gsview                :shared = 0;
my $mupdfmini             :shared = 0;
my $luratech              :shared = 0;
my $ufst                  :shared = 0;
my $ref                   :shared;
my $cal                   :shared = 0;

my $products              :shared = "";

my $windows               :shared = 0;
my $exe                   :shared = "";
my $obj                   :shared = "obj";
my $clusterhome           :shared = $ENV{"CLUSTERHOME"};
my $machine               :shared;

my $debug                 :shared = 0;
my $verbose               :shared = 1;

my $wordSize              :shared = "64";
my $arch                  = `uname -m`;
my $arm                   :shared = 0;
if ($arch =~ m/arm/) {
    $arm = 1;
    $wordSize = 32;
}
my $timeOut               :shared = 320;

my $logFileSizeLimit      :shared = 1000000;    # log file size limit (in blocks)

my $makeOption            :shared = "";
my $configOption          :shared = "";
my $beforeMake            :shared;
my $afterMake             :shared;
my $filterJobs            :shared;

my @testRepositoryRev     :shared;
my $readlogMd5sum         :shared = 0;
my $valgrind              :shared = 0;
my $coverage              :shared = 0;
my $status                :shared;

my $maxSimultaneousJobs   :shared = 16;

my $maxRaster             :shared = 25;

my $logFilesUploaded      :shared = 0;

my $build                 :shared = 0;
my $building              :shared = 0;
my $uploading             :shared = 0;
my $nothingtoupload       :shared = 0;

my $baseDirectory         :shared = `pwd`;
chomp $baseDirectory;
my $testBaseDirectory     :shared = $ENV{"CLUSTERTESTDIR"};
if ( !$testBaseDirectory || $testBaseDirectory eq "" ) {
    $testBaseDirectory = $baseDirectory;
}

my $temp                  :shared = $baseDirectory . "/temp$prisuf";
my $temp2                 :shared = $baseDirectory . "/temp$prisuf.tmp";
my $bmpcmpOutput          :shared = $baseDirectory . "/temp$prisuf/bmpcmp";

my $newDirectoryStructure :shared = 1;

my $gitDirectory          :shared = $baseDirectory . "/ghostpdl";
my $gpdlSource            :shared = $baseDirectory . "/ghostpdl";
my $mupdfSource           :shared = $baseDirectory . "/mupdf";
my $mupdfminiSource       :shared = $baseDirectory . "/mupdfmini";
# Priority 2 jobs (auto jobs (overnights/weeklies/releases) use the
# ghostpdl directory for historical reasons. Priority 0 jobs
# (users), use rsynced source dirs, so these settings aren't used.
if ($pri != 2) {
    $gitDirectory .= "$prisuf";
    $gpdlSource .= "$prisuf";
    $mupdfSource .= "$prisuf";
    $mupdfminiSource .= "$prisuf";
}
my $gsSource              :shared = $gpdlSource . "/gs";
if ($newDirectoryStructure) {
    $gsSource    = $gpdlSource;
}
my $gsBin                 :shared = $baseDirectory . "/gs$prisuf";
my $refBin                :shared = $baseDirectory . "/ref";
my $headBin               :shared = $baseDirectory . "/head";
if ($pri == 2) {
    $headBin .= ".2";
}
my $gsviewSource          :shared = $baseDirectory . "/gsview/gsview";

my $usersDirRemote        :shared = "/home/regression/cluster/users";
my $gsviewDirRemote       :shared = "/home/regression/cluster/gsview";
my $ufstConfig            :shared;
my $compilers             :shared;
my $gpdlConfig            :shared;
my $gsConfig              :shared;
my $make                  :shared;
my $environment           :shared = "";
my $cluster_make          :shared;
my $cluster_devenv        :shared;
my $cluster_devenv2017    :shared;
my %products              :shared;

my $wrun                  :shared = "/mnt/c/windows/system32/cmd.exe /c ";

# The following variables are definitely only used in the main thread.
my @jobsList;
my $runningSemaphore = "./run$prisuf.pid";
my %pids;
my %timeOuts;
my %parentProcessHash;
my %processNameHash;
my %task;
my $maxTimeout           = 15;  # starting value, is adjusted by value below based on jobs completed
my $maxTimeoutPercentage = 1.0;
my $jobSpawnCount        = undef;

# Subroutines

# writeBuild(file, msg);
# write lines from msg into file (skipping any lines that start with "echo").
sub writeBuild($$) {
    my $file = shift;
    my $s    = shift;
    $s =~ s/ ; /\n/g;
    my @a = split "\n", $s;
    open( F5, ">>$file" );
    print F5 "\n";
    foreach my $a (@a) {
        $a =~ s/^ +//;
        if ( !( $a =~ m/^echo/ ) ) {
            print F5 "$a\n";
        }
    }
    print F5 "\n";
    close(F5);
}

# Simple log function; outputs given string, datestamped, to LOG.
sub mylog($) {
    my $d = `date`;
    chomp $d;
    my $s = shift;
    chomp $s;
    print LOG "$d: $s\n";
    if ($verbose) {
        print "$s\n";
    }
}

sub docmd ($) {
    my $cmd = shift;
    my $res;
    print F4 "$cmd\n";
    mylog($cmd);
    $res = `$cmd`;
    if ($res ne "") {
        print F4 "$res";
        mylog($res);
    }
}

sub dieClean($) {
    my $log = shift;
    mylog "$log\n";
    print "$log\n";
    unlink $runningSemaphore;
    `rm -f job$prisuf.start`;
    close(LOG);
    exit;
}

# the concept with checkPID is that if the semaphore file is missing or doesn't
# contain our PID something bad has happened and we just just exit
sub checkPID {
    if ( open( F, "<$runningSemaphore" ) ) {
        my $t = <F>;
        close(F);
        chomp $t;
        if ( $t == $$ ) {
            return (1);
        }
        dieClean("terminating: $runningSemaphore pid mismatch $t $$");
    }
    dieClean("terminating: $runningSemaphore missing");
}


sub updateStatus($) {
    my $message = shift;
    chomp $message;
    $status = $message;
    mylog($message);
}

sub systemWithRetry($) {
    my $cmd = shift;
    my $a;

    mylog "$cmd";
    if ($verbose) {
        print "$cmd\n";
    }
    my $count = 0;
    do {
        my $b = `$cmd`;
        $a = $?;
        $a = $a / 256;
        chomp $b;
        if ( $a != 0 ) {
            mylog "error with: $cmd; a=$a b=$b count=$count\n";
            updateStatus("error with: $cmd; a=$a b=$b count=$count")
        }
        $count++;
        if ( $a != 0 ) {
            sleep 10;
        }
    } while ( $a != 0 && $count < 5 );
    if ( $a != 0 ) {
        updateStatus("Fatal error, aborting cluster run");
        if (threads->tid() == 0) {
            # Main thread. Fatal error, stop everything.
            dieClean("systemWithRetry failed - fatal error");
        } else {
            # Task thread - stop the task.
            threads->exit($failcodes{'systemWithRetry'});
        }
    }
}

# addToLog(logfile, file)
# Copy all of "file.log" into the logfile.
sub addToLog($$) {

    my $logfile = shift;
    my $outname = shift;
    if ( $logfile eq "build" )
    {
        return ;
    }
    my $md5sumIncluded = 0;

    # mylog("adding to log: ===$logfile=== ".tell(F4));
    print F4 "===$logfile===\n";
    my $count = 0;
    if ( open( F, "<$outname.log" ) ) {
        while (<F>) {
            print F4 $_;
            if ( ( $count++ ) >= 10000 ) {
                print F4 "*** log truncated ***\n";
                last;
            }
        }
        close(F);
        unlink("$outname.log");
    } else {
        mylog "file $outname.log not found";
    }
    if ( open( F, "<$outname.md5" ) ) {
        while (<F>) {
            chomp;
            $md5sumIncluded = 1
              if ( m/^([0-9a-f]{32})$/
                || m/^([0-9a-f]{32})  -/
                || m/^([0-9a-f]{32})  \.\/temp/
                || m/^MD5 .+ = ([0-9a-f]{32})$/ );
            print F4 "$_\n";
        }
        close(F);
        unlink("$outname.md5");
        if ($md5sumIncluded) {
            $logged{$logfile} = 1;
        }
    }
    if ($bmpcmp) {
        $logged{$logfile} = 1;
    }
    if ( exists $timeOuts{$logfile} ) {
        print F4 "killed: timeout\n";
    }
    print F4 "\n";
}

sub collectStrayLogFiles() {
    mylog('collecting log files');
    my %logfiles;
    opendir( DIR, $temp ) || die "can't opendir $temp: $!";
    foreach ( readdir(DIR) ) {
        if (m/(.+)\.log$/) {
            $logfiles{$1} = 1;
        }
    }
    closedir DIR;
    my $lastTime = time;
    foreach my $logfile ( keys %logfiles ) {
        if ( $lastTime - time > 60 ) {
            $lastTime = time;
        }
        my $short = $logfile;
        if ($short =~ m/(.+)\.\.(gs|pcl|xps|svg|gpdl|gpdf)(|_pdf|_xps|_ps)$/) {
            $short = $1;
        }
        if ( !exists $logged{$short} ) {
            mylog("missed: $logfile");
            addToLog($short, "$temp/$logfile");
            print F4 "(Stray logfile)\n";
            mylog "clearing logFilesUploaded (2)";
            $logFilesUploaded = 0;
            $logged{$short} = 1;
        }
    }
}

sub uploadLogFiles() {
    if ($logFilesUploaded) {
        return 0;
    }
    unlink "$machine$prisuf.tab.gz";
    unlink "$machine$prisuf.tab";
    `touch $machine$prisuf.tab`;
    unlink "$machine$prisuf.log.gz";
    unlink "$machine$prisuf.out.gz";
    unlink "$machine$prisuf.make.gz";
    unlink "$machine$prisuf.make";
    `touch $machine$prisuf.make`;
    close(F4);

    # the following line causes problems on the xeon cluster node with the file tests_private__pdf__sumatra__543_-_does_not_open.pdf
    # which prints out some unusual characters in a warning
    `grep -a -v '^\$' $machine$prisuf.log | uniq > $machine$prisuf.tmp ; mv $machine$prisuf.log $machine$prisuf.log.bak ; mv $machine$prisuf.tmp $machine$prisuf.log`;

    #   my $count=`grep -a ^=== $machine.log | wc -l`;
    #   chomp $count;
    `touch $machine$prisuf.tab`;
    if (  !$bmpcmp
        && $compileFail eq ""
    #    && $md5sumFail  eq ""
    #    && $timeoutFail eq ""
    )
    {
        mylog "./readlog.pl $machine$prisuf.log $machine$prisuf.tab $machine $machine$prisuf.out";
        `./readlog.pl $machine$prisuf.log $machine$prisuf.tab $machine $machine$prisuf.out`;
    }
    open( F4, ">>$machine$prisuf.log" );
    if ($compileFail ne "") {
        my $gccVersion = `gcc --version`;
        print F4 $gccVersion;
        #if ( $md5sumFail ne "" ) {
        #    print F4 "md5sumFail: $md5sumFail";
        print F4 "compileFail: $compileFail\n\n";
        my $dir = "";
        $dir = "ghostpdl"             if ($revs);
        $dir = "mupdf"                if ( $mupdf || $mujstest );
        $dir = "mupdfmini"            if ( $mupdfmini );
	if ($pri != 2) {
	    $dir .= "$prisuf";
	}
        $dir = "users/$user/ghostpdl" if ($user);
        $dir = "users/$user/mupdf"    if ( $user && ( $mupdf || $mujstest ) );
        $dir = "users/$user/mupdfmini" if ( $user && $mupdfmini );
        foreach my $i (
            'makegs.out',  'makepcl.out',
            'makexps.out', 'makegpdf.out',
            'makegpdl.out',
            'makemupdf.out', 'makemujstest.out',
	    'makemupdfmini.out'
        )
        {
            my $count = 0;
            my $start = -1;
            if ( open( F3, "<$dir/$i" ) ) {
                while (<F3>) {
                    $start = $count if (m/^gcc/);
                    $count++;
                }
                close(F3);
                my $t1 = $count;
                $count -= 50;
                $count = $start - 10 if ( $start != -1 );
                $count = $t1 - 20    if ( $t1 - $count < 20 );
                $count = $t1 - 250   if ( $t1 - $count > 250 );
                $t1 -= $count;
                print F4 "\n$i (last $t1 lines):\n\n";

                if ( open( F3, "<$dir/$i" ) ) {
                    while (<F3>) {
                        if ( $count-- < 0 ) {
                            print F4 $_;
                        }
                    }
                    close(F3);
                }
            }
        }
    }
    if ($newDirectoryStructure) {
        `cat $gsSource/*.out $mupdfSource/*.out $mupdfminiSource/*.out >$machine$prisuf.make`;
    } else {
        `cat $gsSource/*.out $gpdlSource/*.out $mupdfSource/*.out $mupdfminiSource/*.out >$machine$prisuf.make`;
    }

    if ($coverage) {
        mylog "tar -zcvf $machine.coverage.tgz $machine.gcov.out >> $machine$prisuf.log 2>&1";
        `tar -zcvf $machine.coverage.tgz $machine.gcov.out >> $machine$prisuf.log 2>&1`;
        mylog "about to upload $machine.coverage.tgz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $machine.coverage.tgz regression\@cluster.ghostscript.com:/home/regression/cluster/$machine.coverage.tgz 2>&1");
        mylog "done with uploading $machine.coverage.tgz";
    }

    `gzip -c $machine$prisuf.tab  >$machine$prisuf.tab.gz`;
    `gzip -c $machine$prisuf.log  >$machine$prisuf.log.gz`;
    `gzip -c $machine$prisuf.out  >$machine$prisuf.out.gz`;
    `gzip -c $machine$prisuf.make >$machine$prisuf.make.gz`;

    if (-e "$temp/build.log") {
        `gzip -c $temp/build.log > $temp/build.log.gz`;
        mylog "about to upload $temp/build.log.gz as build$prisuf.log.gz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $temp/build.log.gz regression\@cluster.ghostscript.com:/home/regression/cluster/build$prisuf.log.gz");
        mylog "done with uploading build$prisuf.log.gz";
    }
    if ( 1 || !$bmpcmp ) {
        mylog "about to upload $machine$prisuf.tab.gz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $machine$prisuf.tab.gz regression\@cluster.ghostscript.com:/home/regression/cluster/$machine$prisuf.tab.gz");
        mylog "done with uploading $machine$prisuf.tab.gz";

        mylog "about to upload $machine$prisuf.out.gz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $machine$prisuf.out.gz regression\@cluster.ghostscript.com:/home/regression/cluster/$machine$prisuf.out.gz 2>&1");
        mylog "done with uploading $machine$prisuf.out.gz";

        mylog "about to upload $machine$prisuf.make.gz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $machine$prisuf.make.gz regression\@cluster.ghostscript.com:/home/regression/cluster/$machine$prisuf.make.gz 2>&1");
        mylog "done with uploading $machine$prisuf.make.gz";

        mylog "about to upload $machine$prisuf.log.gz";
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key $machine$prisuf.log.gz regression\@cluster.ghostscript.com:/home/regression/cluster/$machine$prisuf.log.gz");
        mylog "done with uploading $machine$prisuf.log.gz";
    }
    $logFilesUploaded = 1;
    return 0;
}

sub updateUserSource()
{
    mkdir "users";
    mkdir "users/$user";
    mkdir "users/$user/ghostpdl";
    mkdir "users/$user/ghostpdl/gs";
    mkdir "users/$user/mupdf";
    my $os = `uname`;
    chomp $os;
    $gpdlSource  = $baseDirectory . "/users/$user/ghostpdl";
    if ($newDirectoryStructure) {
        $gsSource = $gpdlSource;
    } else {
        $gsSource = $gpdlSource . "/gs";
    }
    $mupdfSource = $baseDirectory . "/users/$user/mupdf";
    $mupdfminiSource = $baseDirectory . "/users/$user/mupdfmini";
    unlink("users/$user/rsync.log");

    updateStatus("Fetching source from users/$user");
    my $sourceName="ghostpdl";
    if ( $mupdf || $mujstest ) {
        $sourceName="mupdf";
    }
    if ( $mupdfmini ) {
        $sourceName="mupdfmini";
    }
    my $cmd = "cd users/$user ; ".
              "rsync -cvvlogDtprxe.iLsz -e \"ssh -l regression -i \$HOME/.ssh/cluster_key\" --timeout=60 --progress --delete regression\@$host:$usersDirRemote/$user/$sourceName . >rsync.log 2>&1";
    systemWithRetry($cmd);
}

sub updateGitSource($)
{
    my $revs = shift;
    if ($mupdfmini) {
        updateStatus('Updating mupdfmini');
        if ( !-e "$mupdfminiSource/.git" ) {
            mylog("running git clone");
            `git clone git://git.ghostscript.com/mupdf-android-viewer-mini.git $mupdfminiSource`;
            `cd $mupdfminiSource ; git submodule update --init --recursive`;
        }

        # get update
        my $cmd = "cd $mupdfminiSource ; git fetch -q";
        systemWithRetry($cmd);
        $cmd = "cd $mupdfminiSource ; ".
               "git reset --hard -q master ; ".
               "git reset --hard -q origin/master ; ".
               "git reset --hard -q $revs ; ".
               "git submodule update --init --recursive";
        systemWithRetry($cmd);

    } elsif ( $mupdf || $mujstest ) {
        updateStatus('Updating mupdf');
        if ( !-e "$mupdfSource/.git" ) {
            mylog("running git clone");
            `git clone git://git.ghostscript.com/mupdf.git $mupdfSource`;
            `cd $mupdfSource ; git submodule update --init`;
        }

        # get update
        my $cmd = "cd $mupdfSource ; git fetch -q";
        systemWithRetry($cmd);
        $cmd = "cd $mupdfSource ; ".
               "git reset --hard -q master ; ".
               "git reset --hard -q origin/master ; ".
               "git reset --hard -q $revs ; ".
               "git submodule update --init";
        systemWithRetry($cmd);

    } else {

        updateStatus('Updating Ghostscript');

        if ( !-e "$gsSource/.git" ) {
            mylog("running git clone");
            `git clone git://git.ghostscript.com/ghostpdl.git $gsSource`;
        }

        my $cmd = "cd $gsSource ; ".
                  "touch base/gscdef.c base/gscdefs.h; ".
                  "rm -f base/gscdef.c base/gscdefs.h; ".
                  "git checkout base/gscdef.c base/gscdefs.h; ".
                  "cd $gitDirectory ; ".
                  "git fetch -q ; ".
                  "git fetch --tags -q";
        systemWithRetry($cmd);
        $cmd = "cd $gitDirectory ; ".
               "git reset --hard -q master ; ".
               "git reset --hard -q origin/master ; ".
               "git reset --hard -q $revs ; ".
               "git clean -x -f -d -q";
        systemWithRetry($cmd);
    }
}

sub updateGSViewSource()
{
    mkdir("gsview");
    unlink("gsview/rsync.log");

    updateStatus("Fetching source from gsview");
    # Rsync into a symlink kills the symlink, so rsync into
    # a directory within the symlink
    my $cmd = "cd gsview ; ".
              "rsync -cvvlogDtprxe.iLsz -e \"ssh -l regression -i \$HOME/.ssh/cluster_key\" --timeout=60 --progress --delete regression\@$host:$gsviewDirRemote . >rsync.log 2>&1";
    systemWithRetry($cmd);
}

sub updateSource($)
{
    my $revs = shift;

    if ($gsview) {
        updateGSViewSource();
    } elsif ($user) {
        updateUserSource();
    } else {
        updateGitSource($revs);
    }
}

sub patchSourceForRev()
{
    # modify product name so that files that print that information don't
    # change when we rev. the revision
    # open(F1,"<$gsSource/base/gscdef.c") || die "file $gsSource/base/gscdef.c not found";
    if ( open( F1, "<$gsSource/base/gscdef.c" ) ) {
        open( F2, ">$gsSource/base/gscdef.tmp" )
          || die "$gsSource/base/gscdef.tmp cannot be opened for writing";
        while (<F1>) {
            print F2 $_;
            if (m/define GS_PRODUCT/) {
                my $dummy = <F1>;
                print F2 "\t\"GPL Ghostscript\"\n";
            }
        }
        close(F1);
        close(F2);
        docmd("mv $gsSource/base/gscdef.tmp $gsSource/base/gscdef.c");
    }
    if ( open( F1, "<$gsSource/base/gscdefs.h" ) ) {
        open( F2, ">$gsSource/base/gscdef.tmp" )
          || die "$gsSource/base/gscdef.tmp cannot be opened for writing";
        while (<F1>) {
            print F2 $_;
            if (m/define GS_PRODUCT/) {
                my $dummy = <F1>;
                print F2 "\t\"GPL Ghostscript\"\n";
            }
        }
        close(F1);
        close(F2);
        docmd("mv $gsSource/base/gscdef.tmp $gsSource/base/gscdefs.h");
    }
}

sub removeOutFiles()
{
    `touch $gsSource/temp.out ; rm -f $gsSource/*.out`;
    `touch $gpdlSource/temp.out ; rm -f $gpdlSource/*.out`;
    `touch $mupdfSource/temp.out ; rm -f $mupdfSource/*.out`;
    `touch $mupdfminiSource/temp.out ; rm -f $mupdfminiSource/*.out`;
    `touch $gsSource/makegs.out`;
    `touch $gpdlSource/makepcl.out`;
    `touch $gpdlSource/makexps.out`;
    `touch $gpdlSource/makesvg.out`;
    `touch $gpdlSource/makels.out`;
    `touch $gpdlSource/makegpdf.out`;
    `touch $gpdlSource/makegpdl.out`;
    `touch $mupdfSource/makemupdf.out`;
    `touch $mupdfSource/makemujstest.out`;
    `touch $gsviewSource/../makegsview.out`;
    `touch $mupdfminiSource/../makemupdfmini.out`;
}

sub emptyLogFile($)
{
    my $filename = shift;
    mylog("Clearing $filename");
    `touch $filename; rm -f $filename; touch $filename`;
}

sub buildMuPDF($)
{
    my $ref = shift;

    updateStatus("Building mupdf$ref");
    docmd("touch $mupdfSource/build $mupdfSource/generated; rm -fr $mupdfSource/build $mupdfSource/generated");

    emptyLogFile("$mupdfSource/makemupdf.out");

    my $covOpts="";
    if ($coverage) {
        $covOpts = "--coverage";
    }

    my $cmd;
    if ($windows) {
        $cmd = "cd $mupdfSource; ".
               "$cluster_devenv platform\\\\win32\\\\mupdf.sln /rebuild \"Release\"  >> makemupdf.out 2>&1";
    } else {
        $cmd = "cd $mupdfSource ; ".
               "nice make CLUSTER=1 build=release clean ; ".
               "echo '---mupdf_pmake_start---' >>makemupdf.out ; ".
               "nice make CLUSTER=1 HAVE_GLUT=no HAVE_X11=no NOX11=1 build=release XCFLAGS=\"-DCLUSTER $covOpts\" XLIBS=\"$covOpts\" -j 12 all >>makemupdf.out 2>&1 ; ".
               "echo '---mupdf_pmake_end---' >>makemupdf.out ; ".
               "echo >>makemupdf.out ; ".
               "echo '---mupdf_make_start---' >>makemupdf.out ; ".
               "nice make CLUSTER=1 HAVE_GLUT=no HAVE_X11=no NOX11=1 build=release XCFLAGS=\"-DCLUSTER $covOpts\" XLIBS=\"$covOpts\" all >>makemupdf.out 2>&1 ; ".
               "echo '---mupdf_make_end---' >>makemupdf.out";
    }
    writeBuild( "$mupdfSource/makemupdf.out", $cmd );
    docmd($cmd);

    if ($windows) {
    } else {
        mylog("Checking for javac...");
        `which javac > /dev/null 2>&1`;
        if ($? == 0) {
	    my $version = `javac -version`;
	    if ($version >= 1.8) {
                mylog("Found javac 1.8 or above - doing java build\n");
                $cmd =
                   "cd $mupdfSource/platform/java; ".
                   "nice make CLUSTER=1 clean >> ../../makemupdf.out 2>&1;".
                   "echo '---mupdf_java_pmake_start---' >> ../../makemupdf.out 2>&1;".
                   "nice make CLUSTER=1 -j12 >> ../../makemupdf.out 2>&1;".
                   "echo '---mupdf_java_pmake_end---' >> ../../makemupdf.out 2>&1;".
                   "echo '---mupdf_java_make_start---' >> ../../makemupdf.out 2>&1;".
                   "nice make CLUSTER=1 >> ../../makemupdf.out ;".
                   "echo '---mupdf_java_make_end---' >> ../../makemupdf.out 2>&1;".
                writeBuild( "$mupdfSource/makemupdf.out", $cmd );
                docmd($cmd);
                if (! -e "$mupdfSource/platform/java/libmupdf.jar") {
                    $compileFail .= "mupdf.jar ";
                }
                if (! -e "$mupdfSource/platform/java/mupdf_native.o") {
                    $compileFail .= "mupdf.jni ";
                }
	    }
        }
    }

    mkdir "$gsBin";
    mkdir "$gsBin/bin";
    docmd("touch $gsBin/bin/pdfdraw$exe ; rm $gsBin/bin/pdfdraw$exe");

    if ( -e "$mupdfSource/build/release/mudraw$exe" ) {
        docmd("cp -p $mupdfSource/build/release/mudraw$exe $gsBin/bin/pdfdraw$exe");
    } elsif ( -e "$mupdfSource/build/debug/mudraw$exe" ) {
        docmd("cp -p $mupdfSource/build/debug/mudraw$exe $gsBin/bin/pdfdraw$exe");
    } elsif ( -e "$mupdfSource/build/release/mupdfdraw$exe" ) {
        docmd("cp -p $mupdfSource/build/release/mupdfdraw$exe $gsBin/bin/pdfdraw$exe");
    } elsif ( -e "$mupdfSource/build/debug/mupdfdraw$exe" ) {
        docmd("cp -p $mupdfSource/build/debug/mupdfdraw$exe $gsBin/bin/pdfdraw$exe");
    } elsif ( -e "$mupdfSource/build/release/pdfdraw$exe" ) {
        docmd("cp -p $mupdfSource/build/release/pdfdraw$exe $gsBin/bin/.");
    } elsif ( -e "$mupdfSource/build/debug/pdfdraw$exe" ) {
        docmd("cp -p $mupdfSource/build/debug/pdfdraw$exe $gsBin/bin/.");
    } elsif ( -e "$mupdfSource/build/release/mutool$exe" ) {
        docmd("cp -p $mupdfSource/build/release/mutool$exe $gsBin/bin/pdfdraw$exe");
    } elsif ( -e "$mupdfSource/build/debug/mutool$exe" ) {
        docmd("cp -p $mupdfSource/build/debug/mutool$exe $gsBin/bin/pdfdraw$exe");
    } else {
        docmd("cp -p $mupdfSource/platform/win32/Release/mutool$exe $gsBin/bin/pdfdraw$exe");
    }

    if ( -e "$gsBin/bin/pdfdraw$exe" ) {
        if (defined $autoName) {
# FIXME
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/pdfdraw$exe" ) {
                `rm -f $headBin~1/bin/pdfdraw$exe ; cp -p $headBin/bin/pdfdraw$exe $headBin~1/bin/`;
            }
            if ( -e "$gsBin/bin/pdfdraw$exe" ) {
                `rm -f $headBin/bin/pdfdraw$exe ; cp -p $gsBin/bin/pdfdraw$exe $headBin/bin/`;
            }
        }
    } else {
        $compileFail .= "mupdf ";
    }
}

sub buildMuJSTest($)
{
    my $ref = shift;

    updateStatus("Building mujstest$ref");
    my $cpp_exe = system("c++");
    if ( ( $cpp_exe & 255 ) == 0 ) {
        $cpp_exe = "c++";
    } else {
        $cpp_exe = "g++";
    }
    my $cmd = "cd $mupdfSource ; ".
              "nice make CLUSTER=1 build=release clean ; ".
              "echo '---mujstest_pmake_start---' >>makemujstest.out ; ".
              "nice make CLUSTER=1 HAVE_X11=no NOX11=1 build=release CXX=\"$cpp_exe\" XCFLAGS=\"-DCLUSTER\" -j 12 >>makemujstest.out 2>&1 ; ".
              "echo '---mujstest_pmake_end---' >>makemujstest.out ; ".
              "echo >>makemujstest.out ; ".
              "echo '---mujstest_make_start---' >>makemujstest.out ; ".
              "nice make CLUSTER=1 HAVE_X11=no NOX11=1 build=release CXX=\"$cpp_exe\" XCFLAGS=\"-DCLUSTER\" >>makemujstest.out 2>&1 ; ".
              "echo '---mujstest_make_end---' >>makemujstest.out";
    writeBuild( "$mupdfSource/makemujstest.out", $cmd );
    docmd($cmd);
    mkdir "$gsBin";
    mkdir "$gsBin/bin";

    $cmd = "cp -p $mupdfSource/build/release/mujstest$exe $gsBin/bin/mujstest$exe";
    docmd($cmd);
    if ( -e "$gsBin/bin/mujstest$exe" ) {
        if (defined $autoName) {
# FIXME
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/mujstest$exe" ) {
                `rm -f $headBin~1/bin/mujstest$exe ; cp -p $headBin/bin/mujstest$exe $headBin~1/bin/`;
            }
            if ( -e "$gsBin/bin/mujstest$exe" ) {
                `rm -f $headBin/bin/mujstest$exe ; cp -p $gsBin/bin/mujstest$exe $headBin/bin/`;
            }
        }
    } else {
        $compileFail .= "mujstest ";
    }
}

sub buildMuPDFMini($)
{
    my $ref = shift;

    updateStatus("Building mupdfmini$ref");
    docmd("touch $mupdfminiSource/jni/build $mupdfminiSource/jni/generated; rm -fr $mupdfminiSource/jni/build $mupdfminiSource/jni/generated");

    emptyLogFile("$mupdfminiSource/makemupdfmini.out");

    my $covOpts="";
    if ($coverage) {
        $covOpts = "--coverage";
    }

    my $cmd;
    if ($windows) {
	# Not supported.
        #$cmd = "cd $mupdfminiSource; ".
        #       "$cluster_devenv platform\\\\win32\\\\mupdf.sln /rebuild \"Release\"  >> makemupdfmini.out 2>&1";
    } else {
        $cmd = "cd $mupdfminiSource ; ".
	       "./gradlew clean ; ".
               #"cd $mupdfminiSource/jni/libmupdf ; ".
               #"nice make CLUSTER=1 build=release clean ; ".
               #"echo '---mupdfmini_pmake_start---' >> $mupdfminiSource/makemupdfmini.out ; ".
               #"nice make CLUSTER=1 HAVE_GLUT=no HAVE_X11=no NOX11=1 build=release XCFLAGS=\"-DCLUSTER $covOpts\" XLIBS=\"$covOpts\" -j 12 generate >> $mupdfminiSource/makemupdfmini.out 2>&1 ; ".
	       #"cd $mupdfminiSource ; ".
	       #"./gradlew assembleRelease >> $mupdfminiSOurce/makemupdfmini.out 2>&1 ; ".
               #"echo '---mupdf_pmake_end---' >>$mupdfminiSource/makemupdfmini.out ; ".
               #"echo >>$mupdfminiSource/makemupdfmini.out ; ".
               "echo '---mupdf_make_start---' >>$mupdfminiSource/makemupdfmini.out ; ".
               "cd $mupdfminiSource/jni/libmupdf ; ".
               "nice make CLUSTER=1 HAVE_GLUT=no HAVE_X11=no NOX11=1 build=release XCFLAGS=\"-DCLUSTER $covOpts\" XLIBS=\"$covOpts\" generate >> $mupdfminiSource/makemupdfmini.out 2>&1 ; ".
	       "cd $mupdfminiSource ; ".
	       "./gradlew assembleRelease >> $mupdfminiSource/makemupdfmini.out 2>&1 ; ".
               "echo '---mupdf_make_end---' >> $mupdfminiSource/makemupdfmini.out";
    }
    writeBuild( "$mupdfminiSource/makemupdfmini.out", $cmd );
    docmd($cmd);

    # We don't stash any build products.
    #mkdir "$gsBin";
    #mkdir "$gsBin/bin";
    #docmd("touch $gsBin/bin/pdfdraw$exe ; rm $gsBin/bin/pdfdraw$exe");

    #docmd("cp -p $mupdfSource/build/release/mudraw$exe $gsBin/bin/pdfdraw$exe");

    if (!-e "$mupdfminiSource/app/build/outputs/apk/release/app-arm64-v8a-release-unsigned.apk" ||
        !-e "$mupdfminiSource/app/build/outputs/apk/release/app-armeabi-v7a-release-unsigned.apk" ||
        !-e "$mupdfminiSource/app/build/outputs/apk/release/app-universal-release-unsigned.apk" ||
        !-e "$mupdfminiSource/app/build/outputs/apk/release/app-x86_64-release-unsigned.apk" ||
        !-e "$mupdfminiSource/app/build/outputs/apk/release/app-x86-release-unsigned.apk") {
        $compileFail .= "mupdfmini ";
    }
}

sub configureGhostPDL($)
{
    my $cmd;
    my $ref = shift;

    updateStatus("Configuring GhostPDL$ref");
    if ($newDirectoryStructure)
    {
        if ($windows) {
            $cmd = "cd $gsSource ; ".
                   "touch bin obj ; ".
                   "rm -fr bin obj";
        } else {
            $cmd = "cd $gsSource ; ".
                   "nice $make CLUSTER=1 distclean >>/dev/null 2>&1 ; ".
                   "touch bin obj ; ".
                   "rm -fr bin obj ; ".
                   "echo \$"."PATH >>autogen.out 2>&1 ;".
                   "echo ./autogen.sh $gsConfig >>autogen.out 2>&1 ;".
                   "nice ./autogen.sh $gsConfig >>autogen.out 2>&1";
        }
        writeBuild( "$gsSource/autogen.out", $cmd );
        docmd($cmd);
    } else {
        if ($windows) {
            if ($makeOption eq "") {
                $cmd = "cd $gsSource; ".
                       "$cluster_devenv -f windows\\\\GhostPDL.sln /project All /clean Release >> confgs.out 2>&1";
            } else {
                $cmd = "cd $gsSource; ".
                       "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= clean $makeOption >> confgs.out 2>&1";
            }
        } else {
            $cmd = "cd $gsSource ; ".
                   "nice $make CLUSTER=1 distclean >>/dev/null 2>&1 ; ".
                   "nice ./autogen.sh $gsConfig >> confgs.out 2>&1";
        }
        writeBuild( "$gsSource/confgs.out", $cmd );
        docmd($cmd);
    }
}

sub copyBinariesToRef()
{
    mkdir "ref";
    mkdir "ref/bin";
    if ( $products{'gs'} ) {
        if ( -e "$gsBin/bin/gs$exe" ) {
            `rm -f $refBin/bin/gs$exe ; cp -p $gsBin/bin/gs$exe $refBin/bin/.`;
        }
        if ( -e "$gsBin/bin/gsdll64.dll" ) {
            `rm -f $refBin/bin/gsdll64.dll ; cp -p $gsBin/bin/gsdll64.dll $refBin/bin/.`;
        }
    }

    if ( $products{'pcl'} ) {
        if ( -e "$gsBin/bin/pcl6$exe" ) {
            `rm -f $refBin/bin/pcl6$exe ; cp -p $gsBin/bin/pcl6$exe $refBin/bin/pcl6$exe`;
        }
    }

    if ( $products{'xps'} ) {
        if ( -e "$gsBin/bin/gxps$exe" ) {
            `rm -f $refBin/bin/gxps$exe ; cp -p $gsBin/bin/gxps$exe $refBin/bin/gxps$exe`;
        }
    }

    if ( $products{'svg'} ) {
        if ( -e "$gsBin/bin/gsvg$exe" ) {
            `rm -f $refBin/bin/gsvg$exe ; cp -p $gsBin/bin/gsvg$exe $refBin/bin/.`;
        }
    }

    if ( $products{'gpdf'} ) {
        if ( -e "$gsBin/bin/gpdf$exe" ) {
            `rm -f $refBin/bin/gpdf$exe ; cp -p $gsBin/bin/gpdf$exe $refBin/bin/.`;
        }
    }

    if ( $products{'gpdl'} ) {
        if ( -e "$gsBin/bin/gpdl$exe" ) {
            `rm -f $refBin/bin/gpdl$exe ; cp -p $gsBin/bin/gpdl$exe $refBin/bin/.`;
        }
    }

    if ( $products{'mupdf'} ) {
        if ( -e "$gsBin/bin/pdfdraw$exe" ) {
            `rm -f $refBin/bin/pdfdraw$exe ; cp -p $gsBin/bin/pdfdraw$exe $refBin/bin/.`;
        }
    }
}

sub buildGhostscript($$)
{
    my $ref = shift;
    my $nongs_run = shift;
    my $cmd;

    mylog("Building GS");
    updateStatus("Building Ghostscript$ref");

    emptyLogFile("$gpdlSource/makegs.out");

    if ( open( Q1, "<$gsSource/Makefile" ) ) {
        open( Q2, ">$gsSource/Makefile.tmp" );
        while (<Q1>) {
            if (m/^CUPSLIBS/) {
                s/ tiff / /;
                s/ jpeg / /;
                s/ png / /;
                s/ krb5 / /;
                s/ k5crypto / /;
                s/ com_err / /;
                s/ gcrypt / /;
            }
            if (m/^CUPSLIBDIRS/) {
                s|/x86_64-linux-gnu||;
                s|/usr/lib/x86_64-linux-gnu||;
                s|/lib/x86_64-linux-gnu||;
                s|/usr/lib||;
                s| ||g;
            }
            print Q2 "$_";
        }
        close(Q1);
        close(Q2);
        `mv $gsSource/Makefile     $gsSource/Makefile.old`;
        `mv $gsSource/Makefile.tmp $gsSource/Makefile    `;
    }
    if ($beforeMake) {
        docmd("cd $gsSource ; ../../$beforeMake");
    }
    if ($windows) {
        if ($makeOption eq "") {
            $cmd = "cd $gsSource; ".
                   "$cluster_devenv windows\\\\GhostPDL.sln /project Ghostscript /build Release >> makegs.out 2>&1";
        } else {
            $cmd = "cd $gsSource; ".
                   "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= $makeOption >> makegs.out 2>&1";
        }
    } else {
        if ( !$newDirectoryStructure ) {
            $cmd = "cd $gsSource ; ".
                   "echo '---gs_pmake_start---' >>makegs.out ; ".
                   "nice $make CLUSTER=1 -j 12 $makeOption >>makegs.out 2>&1 ; ".
                   "echo '---gs_pmake_end---' >>makegs.out ; ".
                   "echo >>makegs.out ; ".
                   "echo '---gs_make_start---' >>makegs.out ; ".
                   "nice make CLUSTER=1 $makeOption >>makegs.out 2>&1 ; ".
                   "echo '---gs_make_end---' >>makegs.out";
        } else {
            $cmd = "cd $gsSource ; ".
                   "echo '---gs_pmake_start---' >>makegs.out ; ".
                   "nice $make CLUSTER=1 gs -j 12 $makeOption >>makegs.out 2>&1 ; ".
                   "echo '---gs_pmake_end---' >>makegs.out ; ".
                   "echo >>makegs.out ; ".
                   "echo '---gs_make_start---' >>makegs.out ; ".
                   "nice $make CLUSTER=1 gs $makeOption >>makegs.out 2>&1 ; ".
                   "echo '---gs_make_end---' >>makegs.out";
        }
    }
    writeBuild( "$gsSource/makegs.out", $cmd );
    docmd($cmd);
    if ($afterMake) {
        docmd("cd $gsSource ; ../../$afterMake");
    }
    updateStatus("Installing Ghostscript$ref");

    # install ghostscript
    mkdir("$gsBin");
    mkdir("$gsBin/bin");
    $cmd = "touch $gsBin/bin/gs$exe ; ".
           "rm -f $gsBin/bin/gs$exe ; ";
    if ($windows) {
        if ( $makeOption eq "debug" ) {
            $cmd .= "cp $gsSource/debugbin/gswin32c.exe $gsBin/bin/gs.exe ; ".
                    "cp $gsSource/debugbin/gsdll32.dll $gsBin/bin/ ";
        } else {
            $cmd .= "cp $gsSource/bin/gswin32c.exe $gsBin/bin/gs.exe ; ".
                    "cp $gsSource/bin/gsdll32.dll $gsBin/bin/ ";
        }
    } else {
        if ( $makeOption eq "debug" ) {
            $cmd .= "cp $gsSource/debugbin/gs $gsBin/bin/ ";
        } else {
            if ( !$newDirectoryStructure ) {
                $cmd .= "cd $gsSource ; ".
                        "nice $make CLUSTER=1 install >> makegs.out 2>&1";
            } else {
                $cmd .= "cd $gsSource ; ".
                        "nice $make CLUSTER=1 install-gs >> makegs.out 2>&1";
            }
        }
    }
    writeBuild( "$gsSource/makegs.out", $cmd );
    docmd($cmd);

    if ( -e "$gsBin/bin/gs$exe" ) {

        if (defined $autoName) {
        } elsif ( $nongs_run ) {
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/gs$exe" ) {
                `rm -f $headBin~1/bin/gs$exe ; cp -p $headBin/bin/gs$exe $headBin~1/bin/`;
            }
            if ( -e "$headBin/bin/gsdll64.dll" ) {
                `rm -f $headBin~1/bin/gsdll64.dll ; cp -p $headBin/bin/gsdll64.dll $headBin~1/bin/`;
            }
            if ( -e "$gsBin/bin/gs$exe" ) {
                `rm -f $headBin/bin/gs$exe ; cp -p $gsBin/bin/gs$exe $headBin/bin/`;
            }
            if ( -e "$gsBin/bin/gsdll64.dll" ) {
                `rm -f $headBin/bin/gsdll64.dll ; cp -p $gsBin/bin/gsdll64.exe $headBin/bin/`;
            }
        }
    } else {
        mylog("compileFail: binary $gsBin/bin/gs$exe does not exist");
        writeBuild("$gsSource/makegs.out", "Failed to find $gsBin/bin/gs$exe");
        $compileFail .= "gs$exe test ";
    }
}

sub buildPCL($$)
{
    my $cmd;
    my $ref = shift;
    my $nonpcl_run = shift;
    my $objDir = "main/$obj";
    my $binDir = "main/$obj";
    my $binary = "pcl6$exe";
    updateStatus("Building GhostPCL$ref");

    emptyLogFile("$gpdlSource/makepcl.out");

    if ( !$newDirectoryStructure ) {
        `mkdir -p $gpdlSource/$objDir > /dev/null 2>&1`;
        `  touch $gpdlSource/$objDir > /dev/null 2>&1 `;
        `rm -fr $gpdlSource/$objDir > $gpdlSource/makepcl.out 2>&1`;
    } else {
        $objDir = "obj";
        $binDir = "bin";
        if ($windows) {
            $binary = "gpcl6win32$exe";
        } else {
            $binary = "gpcl6$exe";
        }
    }
    if ($windows) {
        if ($configOption eq "") {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir; ".
                   "rm -fr $binDir ; ".
                   "mkdir -p $binDir ; ".
                   "$cluster_devenv windows\\\\GhostPDL.sln /project ghostpcl /build Release >> makepcl.out 2>&1";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir; ".
                   "rm -fr $binDir ; ".
                   "mkdir -p $binDir ; ".
                   "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= gpcl6 $configOption >> makepcl.out 2>&1";
        }
    } else {

#       $cmd="cd $gpdlSource ; nice $make pcl-clean ; nice $make pcl >makepcl.out 2>&1 -j 12; echo >>makepcl.out ; nice $make pcl >>makepcl.out 2>&1";
        if ( !$newDirectoryStructure ) {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "nice ./autogen.sh $gpdlConfig >>makepcl.out 2>&1 ; ".
                   "echo '---pcl_pmake_start---' >>makepcl.out ; ".
                   "nice $make CLUSTER=1 pcl -j 12 >>makepcl.out 2>&1 ; ".
                   "echo '---pcl_pmake_end---' >>makepcl.out ; ".
                   "echo >>makepcl.out ; echo '---pcl_make_start---' >>makepcl.out ; nice $make CLUSTER=1 pcl >>makepcl.out 2>&1 ; echo '---pcl_make_end---' >>makepcl.out";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "echo '---pcl_pmake_start---' >>makepcl.out ; ".
                   "nice $make CLUSTER=1 gpcl6 -j 12 >>makepcl.out 2>&1 ; ".
                   "echo '---pcl_pmake_end---' >>makepcl.out ; ".
                   "echo >>makepcl.out ; echo '---pcl_make_start---' >>makepcl.out ; nice $make CLUSTER=1 gpcl6 >>makepcl.out 2>&1 ; echo '---pcl_make_end---' >>makepcl.out";
        }
    }
    writeBuild( "$gpdlSource/makepcl.out", $cmd );
    docmd($cmd);
    if ( -e "$gpdlSource/$binDir/$binary" ) {
        mkdir "$gsBin";
        mkdir "$gsBin/bin";
        docmd("rm -f $gsBin/$binDir/$binary ; cp -p $gpdlSource/$binDir/$binary $gsBin/bin/pcl6$exe");
        if (defined $autoName) {
        } elsif ( $nonpcl_run ) {
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/pcl6$exe" ) {
                `rm -f $headBin~1/bin/pcl6$exe ; cp -p $headBin/bin/pcl6$exe $headBin~1/bin/.`;
            }
            `rm -f $headBin/bin/pcl6$exe ; cp -p $gsBin/bin/pcl6$exe $headBin/bin/.`;
        }
    } else {
        mylog("compileFail: binary $gpdlSource/$binDir/$binary does not exist");
        $compileFail .= "pcl ";
    }
}

sub buildGhostXPS($$)
{
    my $cmd;
    my $ref = shift;
    my $nonxps_run = shift;
    my $objDir = "xps/$obj";
    my $binDir = "xps/$obj";
    my $binary = "gxps$exe";
    updateStatus("Building GhostXPS$ref");

    emptyLogFile("$gpdlSource/makexps.out");

    if ( !$newDirectoryStructure ) {
        `mkdir -p $gpdlSource/$objDir > /dev/null 2>&1`;
        `  touch $gpdlSource/$objDir > /dev/null 2>&1 `;
        `rm -fr $gpdlSource/$objDir > $gpdlSource/makexps.out 2>&1`;
    } else {
        $objDir = "obj";
        $binDir = "bin";
        if ($windows) {
            $binary = "gxpswin32$exe";
        } else {
            $binary = "gxps$exe";
        }
    }
    if ($windows) {
        if ($configOption eq "") {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "mkdir -p $binDir ; ".
                   "$cluster_devenv windows\\\\GhostPDL.sln /project ghostxps /build Release >> makexps.out 2>&1";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= gxps $configOption >> makexps.out 2>&1";
        }
    } else {

        # $cmd="cd $gpdlSource ; nice $make xps-clean ; nice $make xps >makepcl.out 2>&1 -j 12; echo >>makepcl.out ; nice $make xps >>makepcl.out 2>&1";
        if ( !$newDirectoryStructure ) {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "nice ./autogen.sh $gpdlConfig >>makexps.out 2>&1 ; ".
                   "echo '---xps_pmake_start---' >>makexps.out ; ".
                   "nice $make CLUSTER=1 xps -j 12 >>makexps.out 2>&1 ; ".
                   "echo '---xps_pmake_end---' >>makexps.out ; ".
                   "echo >>makexps.out ; echo '---xps_make_start---' >>makexps.out ; nice $make CLUSTER=1 xps >>makexps.out 2>&1 ; echo >>makexps.out ; echo '---xps_make_end---' >>makexps.out";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "echo '---xps_pmake_start---' >>makexps.out ; ".
                   "nice $make CLUSTER=1 gxps -j 12 >>makexps.out 2>&1 ; ".
                   "echo '---xps_pmake_end---' >>makexps.out ; ".
                   "echo >>makexps.out ; echo '---xps_make_start---' >>makexps.out ; nice $make CLUSTER=1 gxps >>makexps.out 2>&1 ; echo '---xps_make_end---' >>makexps.out";
        }
    }
    writeBuild( "$gpdlSource/makexps.out", $cmd );
    docmd($cmd);
    if ( -e "$gpdlSource/$binDir/$binary" ) {
        mkdir "$gsBin";
        mkdir "$gsBin/bin";
        docmd("rm -f $gsBin/$binDir/gxps$exe ; cp -p $gpdlSource/$binDir/$binary $gsBin/bin/gxps$exe");
        if (defined $autoName) {
        } elsif ( $nonxps_run) {
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/gxps$exe" ) {
                `rm -f $headBin~1/bin/gxps$exe ; cp -p $headBin/bin/gxps$exe $headBin~1/bin/gxps$exe`;
            }
            `rm -f $headBin/bin/gxps$exe ; cp -p $gsBin/bin/gxps$exe $headBin/bin/gxps$exe`;
        }
    } else {
        mylog("compileFail: binary $gpdlSource/$binDir/gxps$exe does not exist");
        $compileFail .= "gxps ";
    }
}

sub buildGhostSVG($)
{
    my $cmd;
    my $ref = shift;

    updateStatus("Building GhostSVG$ref");
    mkdir "$gpdlSource/svg/";
    `touch $gpdlSource/svg/$obj > /dev/null 2>&1 `;
    `rm -fr $gpdlSource/svg/$obj > $gpdlSource/makesvg.out 2>&1`;
    if ( -e "$gpdlSource/svg/$obj" ) {
        $compileFail .= "gsvg ";
    } else {
        if ($windows) {
            $cmd = "cd $gpdlSource ; ".
                   "rm -fr svg/$obj ; ".
                   "rm -f makesvg.out ; ".
                   "cd svg; ".
                   "$cluster_make -f svg_msvc.mak CLUSTER=1 WIN32= DEVSTUDIO= $configOption >>../makesvg.out 2>&1";
        } else {
            if ( $products{'pcl'} ) {
                # we do not need to autogen.sh again
                $cmd = "cd $gpdlSource ; ".
                       "nice $make CLUSTER=1 svg-clean ; ".
                       "echo '---pmake_start---' >>makesvg.out ; ".
                       "nice $make CLUSTER=1 svg >>makesvg.out 2>&1 -j 12 ; ".
                       "echo '---pmake_end---' >>makesvg.out ; ".
                       "echo >>makesvg.out ; ".
                       "nice $make CLUSTER=1 svg >>makesvg.out 2>&1";
            } else {
                $cmd = "cd $gpdlSource ; ".
                   "nice $make CLUSTER=1 svg-clean ; ".
                   "nice ./autogen.sh $gpdlConfig >>makesvg.out 2>&1 ; ".
                   "echo '---pmake_start---' >>makesvg.out ; ".
                   "nice $make CLUSTER=1 svg >>makesvg.out 2>&1 -j 12 ; ".
                   "echo '---pmake_end---' >>makesvg.out ; ".
                   "echo >>makesvg.out ; ".
                   "nice $make CLUSTER=1 svg >>makesvg.out 2>&1";
            }
        }
        writeBuild( "$gpdlSource/makesvg.out", $cmd );
        docmd($cmd);
        if ( -e "$gpdlSource/svg/$obj/gsvg$exe" ) {
            mkdir "$gsBin";
            mkdir "$gsBin/bin";
            docmd("rm -f $gsBin/bin/gsvg$exe ; cp -p $gpdlSource/svg/$obj/gsvg$exe $gsBin/bin/");
            if (defined $autoName) {
                if ($ref ne "") {
                }
            } elsif ( $revs ) {
                if ( -e "$headBin/bin/gsvg$exe" ) {
                    `rm -f $headBin~1/bin/gsvg$exe ; cp -p $headBin/bin/gsvg$exe $headBin~1/bin/`;
                }
                `rm -f $headBin/bin/gsvg$exe ; cp -p $gsBin/bin/gsvg$exe $headBin/bin/`;
            }
        } else {
            $compileFail .= "gsvg ";
        }
    }
}

sub buildGhostPDF($)
{
    my $cmd;
    my $ref = shift;
    my $objDir = "gpdf/$obj";
    my $binDir = "gpdf/$obj";
    my $binary = "gpdf$exe";
    updateStatus("Building GhostPDF$ref");

    emptyLogFile("$gpdlSource/makegpdf.out");

    if ( !$newDirectoryStructure ) {
        `mkdir -p $gpdlSource/$objDir > /dev/null 2>&1`;
        `  touch $gpdlSource/$objDir > /dev/null 2>&1 `;
        `rm -fr $gpdlSource/$objDir > $gpdlSource/makegpdf.out 2>&1`;
    } else {
        $objDir = "obj";
        $binDir = "bin";
        if ($windows) {
            $binary = "gpdfwin32$exe";
        } else {
            $binary = "gpdf$exe";
        }
    }
    if ($windows) {
        if ($configOption eq "") {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "mkdir -p $binDir ; ".
                   "$cluster_devenv windows\\\\GhostPDL.sln /project ghostpdf /build Release >> makegpdf.out 2>&1";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= ghostpdf $configOption >> makegpdf.out 2>&1";
        }
    } else {

        # $cmd="cd $gpdlSource ; nice $make gpdfclean ; nice $make gpdf >makegpdf.out 2>&1 -j 12; echo >>makegpdf.out ; nice $make gpdf >>makegpdf.out 2>&1";
        if ( !$newDirectoryStructure ) {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "nice ./autogen.sh $gpdlConfig >>makegpdf.out 2>&1 ; ".
                   "echo '---gpdf_pmake_start---' >>makegpdf.out ; ".
                   "nice $make CLUSTER=1 gpdf -j 12 >>makegpdf.out 2>&1 ; ".
                   "echo '---gpdf_pmake_end---' >>makegpdf.out ; ".
                   "echo >>makegpdf.out ; ".
                   "echo '---gpdf_make_start---' >>makegpdf.out ; ".
                   "nice $make CLUSTER=1 gpdf >>makegpdf.out 2>&1 ; ".
                   "echo >>makegpdf.out ; ".
                   "echo '---gpdf_make_end---' >>makegpdf.out";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "echo '---gpdf_pmake_start---' >>makegpdf.out ; ".
                   "nice $make CLUSTER=1 gpdf -j 12 >>makegpdf.out 2>&1 ; ".
                   "echo '---gpdf_pmake_end---' >>makegpdf.out ; ".
                   "echo >>makegpdf.out ; ".
                   "echo '---gpdf_make_start---' >>makegpdf.out ; ".
                   "nice $make CLUSTER=1 gpdf >>makegpdf.out 2>&1 ; ".
                   "echo '---gpdf_make_end---' >>makegpdf.out";
        }
    }
    writeBuild( "$gpdlSource/makegpdf.out", $cmd );
    docmd($cmd);
    if ( -e "$gpdlSource/$binDir/$binary" ) {
        mkdir "$gsBin";
        mkdir "$gsBin/bin";
        docmd("rm -f $gsBin/$binDir/gpdf$exe ; cp -p $gpdlSource/$binDir/$binary $gsBin/bin/gpdf$exe");
        if (defined $autoName) {
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/gpdf$exe" ) {
                `rm -f $headBin~1/bin/gpdf$exe ; cp -p $headBin/bin/gpdf$exe $headBin~1/bin/gpdf$exe`;
            }
            `rm -f $headBin/bin/gpdf$exe ; cp -p $gsBin/bin/gpdf$exe $headBin/bin/gpdf$exe`;
        }
    } else {
        mylog("compileFail: binary $gpdlSource/$binDir/gpdf$exe does not exist");
        $compileFail .= "gpdf ";
    }
}

sub buildGhostPDL($)
{
    my $cmd;
    my $ref = shift;
    my $objDir = "gpdl/$obj";
    my $binDir = "gpdl/$obj";
    my $binary = "gpdl$exe";
    updateStatus("Building GhostPDL$ref");

    emptyLogFile("$gpdlSource/makegpdl.out");

    if ( !$newDirectoryStructure ) {
        `mkdir -p $gpdlSource/$objDir > /dev/null 2>&1`;
        `  touch $gpdlSource/$objDir > /dev/null 2>&1 `;
        `rm -fr $gpdlSource/$objDir > $gpdlSource/makegpdl.out 2>&1`;
    } else {
        $objDir = "obj";
        $binDir = "bin";
        if ($windows) {
            $binary = "gpdlwin32$exe";
        } else {
            $binary = "gpdl$exe";
        }
    }
    if ($windows) {
        if ($configOption eq "") {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "mkdir -p $binDir ; ".
                   "$cluster_devenv windows\\\\GhostPDL.sln /project ghostpdl /build Release >> makegpdl.out 2>&1";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "$cluster_make -f psi/msvc32.mak CLUSTER=1 WIN32= DEVSTUDIO= ghostpdl $configOption >> makegpdl.out 2>&1";
        }
    } else {

        # $cmd="cd $gpdlSource ; nice $make gpdlclean ; nice $make gpdl >makegpdl.out 2>&1 -j 12; echo >>makegpdl.out ; nice $make gpdl >>makegpdl.out 2>&1";
        if ( !$newDirectoryStructure ) {
            $cmd = "cd $gpdlSource ; ".
                   "touch $binDir ; ".
                   "rm -fr $binDir ; ".
                   "nice ./autogen.sh $gpdlConfig >>makegpdl.out 2>&1 ; ".
                   "echo '---gpdl_pmake_start---' >>makegpdl.out ; ".
                   "nice $make CLUSTER=1 gpdl -j 12 >>makegpdl.out 2>&1 ; ".
                   "echo '---gpdl_pmake_end---' >>makegpdl.out ; ".
                   "echo >>makegpdl.out ; ".
                   "echo '---gpdl_make_start---' >>makegpdl.out ; ".
                   "nice $make CLUSTER=1 gpdl >>makegpdl.out 2>&1 ; ".
                   "echo >>makegpdl.out ; ".
                   "echo '---gpdl_make_end---' >>makegpdl.out";
        } else {
            $cmd = "cd $gpdlSource ; ".
                   "echo '---gpdl_pmake_start---' >>makegpdl.out ; ".
                   "nice $make CLUSTER=1 gpdl -j 12 >>makegpdl.out 2>&1 ; ".
                   "echo '---gpdl_pmake_end---' >>makegpdl.out ; ".
                   "echo >>makegpdl.out ; ".
                   "echo '---gpdl_make_start---' >>makegpdl.out ; ".
                   "nice $make CLUSTER=1 gpdl >>makegpdl.out 2>&1 ; ".
                   "echo '---gpdl_make_end---' >>makegpdl.out";
        }
    }
    writeBuild( "$gpdlSource/makegpdl.out", $cmd );
    docmd($cmd);
    if ( -e "$gpdlSource/$binDir/$binary" ) {
        mkdir "$gsBin";
        mkdir "$gsBin/bin";
        docmd("rm -f $gsBin/$binDir/gpdl$exe ; cp -p $gpdlSource/$binDir/$binary $gsBin/bin/gpdl$exe");
        if (defined $autoName) {
        } elsif ( $revs ) {
            if ( -e "$headBin/bin/gpdl$exe" ) {
                `rm -f $headBin~1/bin/gpdl$exe ; cp -p $headBin/bin/gpdl$exe $headBin~1/bin/gpdl$exe`;
            }
            `rm -f $headBin/bin/gpdl$exe ; cp -p $gsBin/bin/gpdl$exe $headBin/bin/gpdl$exe`;
        }
    } else {
        mylog("compileFail: binary $gpdlSource/$binDir/gpdl$exe does not exist");
        $compileFail .= "gpdl ";
    }
}

sub buildGSView($)
{
    my $ref = shift;

    updateStatus("Building gsview$ref");

    emptyLogFile("$gsviewSource/../makegsview.out");

    my $cmd = "cd $gsviewSource/.. ; ".
              "$cluster_devenv2017 gsview gswinapps.sln /project gsview /rebuild \"Release\"  >> makegsview.out 2>&1";
    writeBuild( "$gsviewSource/../makegsview.out", $cmd );
    docmd($cmd);

    if ( -e "$gsviewSource/gsview/bin/Release/gsview.exe" ) {
    } else {
        $compileFail .= "gsview ";
    }
}

sub buildSource($)
{
    my $ref = shift;

    # cluster_make.bat needs to live in the windows world, so for
    # now, we'll keep it in the top of the users dir.
    $cluster_make="$wrun ..\\\\users\\\\cluster_make.bat";
    $cluster_devenv="$wrun ..\\\\users\\\\cluster_devenv.bat";
    $cluster_devenv2017="$wrun ..\\\\users\\\\cluster_devenv2017.bat";
    if ($user) {
        $cluster_make="$wrun ..\\\\..\\\\cluster_make.bat";
        $cluster_devenv="$wrun ..\\\\..\\\\cluster_devenv.bat";
        $cluster_devenv2017="$wrun ..\\\\..\\\\cluster_devenv2017.bat";
    }

    if ( -e "$gpdlSource/Makefile.in" ) {
        $newDirectoryStructure = 1;
    }

    if ($newDirectoryStructure) {
        $gsSource = $gpdlSource;
    }

    if ($bmpcmp) {
        # with the update to 1.5.x, libpng has a dynamically created
        # configuration header. We use the predefined one by copying
        # it to the cwd and adding cwd to the header search path. Then
        # remove it when we're done
        `cp $baseDirectory/ghostpdl/libpng/scripts/pnglibconf.h.prebuilt ./pnglibconf.h`;
        my $t9 = `cc -I. -I$baseDirectory/ghostpdl/libpng -I$baseDirectory/ghostpdl/zlib -o bmpcmp -DHAVE_LIBPNG $baseDirectory/ghostpdl/toolbin/bmpcmp.c $baseDirectory/ghostpdl/libpng/png.c $baseDirectory/ghostpdl/libpng/pngerror.c $baseDirectory/ghostpdl/libpng/pngget.c $baseDirectory/ghostpdl/libpng/pngmem.c $baseDirectory/ghostpdl/libpng/pngpread.c $baseDirectory/ghostpdl/libpng/pngread.c $baseDirectory/ghostpdl/libpng/pngrio.c $baseDirectory/ghostpdl/libpng/pngrtran.c $baseDirectory/ghostpdl/libpng/pngrutil.c $baseDirectory/ghostpdl/libpng/pngset.c $baseDirectory/ghostpdl/libpng/pngtrans.c $baseDirectory/ghostpdl/libpng/pngwio.c $baseDirectory/ghostpdl/libpng/pngwrite.c $baseDirectory/ghostpdl/libpng/pngwtran.c $baseDirectory/ghostpdl/libpng/pngwutil.c $baseDirectory/ghostpdl/zlib/adler32.c $baseDirectory/ghostpdl/zlib/crc32.c $baseDirectory/ghostpdl/zlib/infback.c $baseDirectory/ghostpdl/zlib/inflate.c $baseDirectory/ghostpdl/zlib/uncompr.c $baseDirectory/ghostpdl/zlib/compress.c $baseDirectory/ghostpdl/zlib/deflate.c $baseDirectory/ghostpdl/zlib/inffast.c $baseDirectory/ghostpdl/zlib/inftrees.c $baseDirectory/ghostpdl/zlib/trees.c $baseDirectory/ghostpdl/zlib/zutil.c -lm 2>&1`;
        mylog("Reference binary from git commit:");
        if ($mupdf || $mujstest) {
            mylog(`cd $baseDirectory/mupdf ; git log -1`);
        } else {
            mylog(`cd $baseDirectory/ghostpdl ; git log -1`);
        }
        mylog "cc bmpcmp: $t9";
        `rm -f ./pnglibconf.h`;
    }

    mkdir("$gsBin");
    mkdir("$gsBin/bin");

    patchSourceForRev();
    removeOutFiles();

    # RJW - What is this next one about? I think this might be broken code
    # left over from dontBuild.
    #if ( -e "$headBin/bin/gs" ) {
    #   `cp -p $headBin/bin/* $gsBin/bin/`;
    #}

    if ( $mupdf ) {
        buildMuPDF($ref);
    }
    if ( $mujstest ) {
        buildMuJSTest($ref);
    }
    if ( $mupdfmini ) {
        buildMuPDFMini($ref);
    }

    # Copy in Luratech or Enable UFST if required.
    $ufstConfig="";
    if (!$mupdf && !$mujstest) {
        `cd $gsSource ; rm -rf luratech`;
        if ($luratech) {
            `cd $gsSource ; cp -pr ../luratech luratech`;
        }
        `cd $gsSource ; rm -rf cal`;
        if ($cal) {
            `cd $gsSource ; cp -pr ../cal cal`;
        }
        `cd $gsSource ; rm -rf ufst`;
        if ($ufst) {
            $ufstConfig = "--with-ufst=../ufst";
        }
    }

    my $covOpts="";
    if ($coverage) {
        $covOpts = "--coverage";
    }
    my $archFlags = " -m$wordSize";
    if ($arm) {
	$archFlags = "";
    }
    $compilers="\"CC=gcc $archFlags $covOpts\" \"CCLD=gcc $archFlags $covOpts\"";
    $gpdlConfig="$ufstConfig $compilers $configOption --prefix=$gsBin --enable-cluster";
    $gsConfig="$gpdlConfig --disable-fontconfig --without-system-libtiff --without-libpaper --disable-dbus --without-libidn";
    $make="make $compilers";

    if ( $newDirectoryStructure && !$mupdf && !$mujstest && !$gsview && !$mupdfmini)
    {
        configureGhostPDL($ref);
    }

    # If we use gpdf/pcl, then we need to build gs too to cope with
    # ps2written jobs.
    if ( $products{'gs'} || $products{'gpdf'} || $products{'pcl'} ) {
	my $nongs = 1;
	if ($products{'gs'}) {
	    $nongs = 0;
	}
        buildGhostscript($ref, $nongs);
    }

    if ( $products{'pcl'} || $products{'gpdf'} ) {
	my $nonpcl = 1;
	if ($products{'pcl'}) {
	    $nonpcl = 0;
	}
        buildPCL($ref, $nonpcl);
    }

    if ( $products{'xps'} || $products{'gpdf'} ) {
	my $nonxps = 1;
	if ($products{'xps'}) {
	    $nonxps = 0;
	}
        buildGhostXPS($ref, $nonxps);
    }

    if ( $products{'svg'} ) {
        buildGhostSVG($ref);
    }

    if ( $products{'gpdf'} ) {
        buildGhostPDF($ref);
    }

    if ( $products{'gpdl'} ) {
        buildGhostPDL($ref);
    }

    if ( $products{'gsview'} ) {
        buildGSView($ref);
    }
}

sub env($) {
    my $var = shift;
    my $val = $ENV{$var};
    if (!defined $val) {
        return "";
    }
    return $val;
}

sub getCaps() {
    my $cap;
    my $os=env('OS');
    my $ostype=env('OSTYPE');
    my $hosttype=`uname -m`;
    chomp $hosttype;

    # No safe way to detect windows bash that I can find. Rely on an
    # env var for now.
    my $winbash=env('WINDOWSBASH');

    if ($os eq "Windows_NT" or $winbash eq "1") {
        $cap = "win32";
        if ($hosttype eq "x86_64") {
            $cap .= ",win64";
        }
    } elsif ($ostype =~ m/darwin/) {
        $cap="macos";
    } else {
        $cap="linux";

        if (open(F,">test32.c")) {
            print F "int main(int argc, const char *argv[]){return 32;}\n";
            close(F);
            `gcc test32.c -o test32 -m32 > /dev/null 2>&1`;
            `./test32 > /dev/null 2>&1`;
            if (($?>>8) == 32) {
                $cap .= ",w32";
            }
        }
        system("which valgrind");
        if (($?>>8) == 0) {
            $cap .= ",valgrind"
        }
    }

    my $gcc=`gcc --version 2> /dev/null | head -1`;
    if ($gcc =~ m/^gcc.*\s(\d+\.\d+\.\d+)/) {
        $cap.=",gcc=$1";
    }

    system("which gcov");
    if (($?>>8) == 0) {
        $cap.=",coverage";
    }

    # This is a bit crap.
    if (-e "/opt/android") {
        $cap.=",android";
        $ENV{ANDROID_HOME}="/opt/android";
    }

    mylog("Caps: $cap");
    return $cap;
}

sub startJobLog()
{
    my $processors = 8;
    my $cpuType    = "unknown";

    my $machineType = `uname -s`;
    chomp $machineType;

    open( F4, ">$machine$prisuf.log" );
    if ( !$user ) {
        $user = "";
    }
    print F4 "machine=$machine pri=$pri products='$products' rev=$revs user=$user mupdf=$mupdf mujstest=$mujstest bmpcmp=$bmpcmp luratech=$luratech cal=$cal ufst=$ufst gsview=$gsview\n\n";
    my $gccVersion = `gcc --version`;
    print F4 $gccVersion;
    print F4 "bmpcmp=$bmpcmp\n";
    print F4 "uname=$machineType\n";

    if ( !$windows ) {
        if ( -e "/sbin/ifconfig" ) {
            my $inet = `/sbin/ifconfig | grep "inet "`;
            print F4 $inet;
        }
        my $t = `which convert`;
        chomp $t;
        print F4 "convert=$t\n";
        $t = `which valgrind`;
        chomp $t;
        print F4 "valgrind=$t\n";
        $t = `which flock`;
        chomp $t;
        print F4 "flock=$t\n";

        if ( -e "/proc/cpuinfo" ) {
            $t = `grep processor /proc/cpuinfo | tail -1 | cut -f 2 -d ':'`;
            chomp $t;
            $processors = $t + 1;
            $t          = `grep "model name" /proc/cpuinfo | head -1`;
            chomp $t;
            $t =~ s/model name      : //;
            $cpuType = $t;
        }
        if ( -e "/usr/sbin/sysctl" ) {
            $t = `/usr/sbin/sysctl hw.ncpu | cut -f 2 -d ':'`;
            chomp $t;
            $processors = $t + 0;
            $t          = `/usr/sbin/sysctl -n machdep.cpu.brand_string`;
            chomp $t;
            $cpuType = $t;
        }
        print F4 "processors=$processors\n";
        print F4 "cpuType=$cpuType\n";
        $processors = 16 if ( $processors <= 1 );

        if ( -e "tests_private/.git" && -e "tests/.git" ) {
            print F4 "git test repositories\n";
        } else {
            print F4 "svn test repositories\n";
        }

        print F4 "\n";
    }

    # We've noticed intermittent timeouts, probably due to contention (disk or memory)
    # that may be due to running too many processes at once. Limit to the number of processors
    # NB. This may actually help throughput by avoiding contention on the faster nodes
    $maxSimultaneousJobs = $processors;

    mylog "processors=$processors maxCount=$maxSimultaneousJobs cpuType=$cpuType\n";
}

sub updateTestRepos() {
    if ( -e "tests_private/.git" ) {
    } else {
        updateStatus("Cloning tests_private");
        `rm -fr tests_private`;
        systemWithRetry("git clone git.ghostscript.com:/home/git-private/tests_private.git >>clone.log 2>&1");
    }
    if ( -e "tests/.git" ) {
    } else {
        updateStatus("Cloning tests");
        `rm -fr tests`;
        systemWithRetry("git clone git.ghostscript.com:/home/git/tests.git >>clone.log 2>&1");
    }

    # the order of these must match the .start order set in clustermaster.pl
    my %testSource = (
        $testBaseDirectory . "/tests/"         => '1',
        $testBaseDirectory . "/tests_private/" => '1'
    );

    my $count = 0;

    # The -q in the ssh command below is required for cygwin clients
    foreach my $testSource ( sort keys %testSource ) {
        unlink('$testSource/git.log');
        my $currentRev = `cd $testSource ; git log | head -1 | awk '{ print \$2 }'`;
        chomp $currentRev;
        mylog "$count $testRepositoryRev[$count] $currentRev";
        if ( !exists $testRepositoryRev[$count]
            || $testRepositoryRev[$count] ne $currentRev )
        {
            updateStatus('Updating test files');
            if ( -e "$testSource/.git" ) {
                systemWithRetry("cd $testSource ; git fetch > git.log 2>&1");
            }
	    `cd $testSource && git reset --hard $testRepositoryRev[$count]`;
            if ( open( F8, "<$testSource/git.log" ) ) {
                while (<F8>) {
                    chomp;
                    mylog "git: $_";
                }
                close(F8);
            }
            #unlink('$testSource/git.log');
        }
        $count++;
    }
}

sub updateLuratechRepo() {
    if ( -e "luratech/.git" ) {
    } else {
        `rm -fr luratech`;
        unlink("clone.log");
        updateStatus('Cloning luratech');
        systemWithRetry("git clone regression\@git.ghostscript.com:/home/git-private/luratech.git >>clone.log 2>&1");
    }

    updateStatus('Updating luratech');
    systemWithRetry("cd luratech ; git pull >>../git.log 2>&1");
    if ( open( F8, "<git.log" ) ) {
        while (<F8>) {
            chomp;
            mylog "git: $_";
        }
        close(F8);
    }
}

sub updateCalRepo() {
    if ( -e "cal/.git" ) {
    } else {
        `rm -fr cal`;
        unlink("clone.log");
        updateStatus('Cloning CAL');
        systemWithRetry("git clone regression\@git.ghostscript.com:/home/git-private/cal.git >>clone.log 2>&1");
    }

    updateStatus('Updating CAL');
    systemWithRetry("cd cal ; git pull >>../git.log 2>&1");
    if ( open( F8, "<git.log" ) ) {
        while (<F8>) {
            chomp;
            mylog "git: $_";
        }
        close(F8);
    }
}

sub updateUFSTRepo() {
    if ( -e "ufst/.git" ) {
    } else {
        `rm -fr ufst`;
        unlink("clone.log");
        updateStatus('Cloning ufst');
        systemWithRetry("git clone regression\@git.ghostscript.com:/home/git-private/ufst.git >>clone.log 2>&1");
    }

    updateStatus('Updating ufst');
    systemWithRetry("cd ufst ; git pull >>../git.log 2>&1");
    if ( open( F8, "<git.log" ) ) {
        while (<F8>) {
            chomp;
            mylog "git: $_";
        }
        close(F8);
    }
}

sub getParentProcessHash() {
    if (%parentProcessHash) {
        return;
    }

    undef %processNameHash;
    undef %parentProcessHash;
    my $a = `ps -ef`;
    my @a = split '\n', $a;
    foreach (@a) {
        chomp;
        if (   m/\S+ +(\d+) +(\d+) .+ \d+:\d\d.\d\d (.+)$/
            && !m/<defunct>/
            && !m/\(sh\)/ )
        {
            $parentProcessHash{$1} = $2; # $2 is the parent of $1
            $processNameHash{$1}   = $3;
        }
    }
}

sub timeoutProcessAndChildren($);
sub timeoutProcessAndChildren($)
{
    my $pid = shift;

    getParentProcessHash();
    kill 1, $pid;
    mylog("Killing (timeout) $pid, $processNameHash{$pid}");
    kill 9, $pid;
    foreach my $child (keys %parentProcessHash) {
        if ($parentProcessHash{$child} == $pid) {
            timeoutProcessAndChildren($child);
        }
    }
}

sub killProcessAndChildren($);
sub killProcessAndChildren($)
{
    my $pid = shift;

    kill 1, $pid;
    mylog("Killing $pid, $processNameHash{$pid}");
    kill 9, $pid;
    foreach my $child (keys %parentProcessHash) {
        if ($parentProcessHash{$child} == $pid) {
            killProcessAndChildren($child);
        }
    }
}

sub killAllJobs() {
    mylog "in killAllJobs()\n";

    undef %parentProcessHash;
    getParentProcessHash();
    foreach my $pid ( keys %pids ) {
        killProcessAndChildren($pid);
    }
}

sub pauseProcessAndChildren($);
sub pauseProcessAndChildren($)
{
    my $pid = shift;

    kill 'STOP', $pid;
    mylog("Pausing $pid, $processNameHash{$pid}");
    foreach my $child (keys %parentProcessHash) {
        if ($parentProcessHash{$child} == $pid) {
            pauseProcessAndChildren($child);
        }
    }
}

my $pauseTime;
my $pauseTime2;
sub pauseAllJobs() {
    $pauseTime = time();
    $pauseTime2 = $pauseTime;
    undef %parentProcessHash;
    getParentProcessHash();
    foreach my $pid ( keys %pids ) {
        pauseProcessAndChildren($pid);
    }
}

sub unpauseProcessAndChildren($);
sub unpauseProcessAndChildren($)
{
    my $pid = shift;

    kill 'CONT', $pid;
    mylog("Unpausing $pid, $processNameHash{$pid}");
    foreach my $child (keys %parentProcessHash) {
        if ($parentProcessHash{$child} == $pid) {
            unpauseProcessAndChildren($child);
        }
    }
}

sub unpauseAllJobs() {
    undef %parentProcessHash;
    getParentProcessHash();
    my $delay = time() - $pauseTime;
    foreach my $pid ( keys %pids ) {
        unpauseProcessAndChildren($pid);
        $pids{$pid}{'timeout'} += $delay;
        $pids{$pid}{'time'} += $delay;
    }
}

sub updateSemaphoreTimestamp() {
    my $nt = time();
    my $d = $nt - $pauseTime2;
    $pauseTime2 = $nt;
    my $ss = stat($runningSemaphore);
    utime($ss->atime+$d, $ss->mtime+$d, $runningSemaphore);
}

sub spawnNewJob() {
    my $n       = rand( scalar @jobsList );
    my @a       = split '\t', $jobsList[$n];
    my $mytimeout = $timeOut;
    splice( @jobsList, $n, 1 );
    my $filename = $a[0];
    my $outname  = $a[1];
    my $cmd      = $a[2];
    my $midname;
    if (!defined($cmd)) {
        $cmd="";
    }
    $filename =~ s/__temp__/$temp/g;
    $outname =~ s/__temp__/$temp/g;
    $cmd =~ s/__temp__/$temp/g;
    $cmd =~ s/__bin__/$gsBin/g;
    $cmd =~ s/__head__/$headBin/g;
    $cmd =~ s|/home/regression/cluster/bmpcmp|/home/regression/cluster/bmpcmp$prisuf|g;
    if ( $cmd =~ m/__gsSource__/ || $cmd =~ m/__mupdfSource/ || $cmd =~ m/__gsviewSource/) {
        if ( $cmd =~ m/__gsSource__/ ) {
            $mytimeout *= 2;
        }
        $build = 1;
        if ($cmd =~ m/__gsviewSource/) {
            $cmd = "cp $gsviewSource/../makegsview.out $temp/build$prisuf.log";
        }
        $cmd =~ s/__gsSource__/$gsSource/g;
        $cmd =~ s/__mupdfSource__/$mupdfSource/g;

        mylog("build command: $cmd");
    }
    if ($cmd =~ m/psdcmyk16/) {
        $mytimeout *= 7/3;
    } elsif ($cmd =~ m/psdcmyk/ || $cmd =~ m/pamcmyk32/) {
        $mytimeout *= 4/3;
    }
    if ($cmd =~ m/runGCov.pl/) {
        $mytimeout *= 2;
    }

    if ($cmd =~ m/pdfwrite/ || $cmd =~ m/ps2write/ || $cmd =~ m/xpswrite/) {
        if ($cmd =~ m/-sOutputFile=(\.\/temp\/\S+)/) {
            $midname=$1;
        }
    }
    
    if ( $cmd =~ m| ./bmpcmp | ) {
        $maxSimultaneousJobs = 8;
    }

    #printf "\n$t1 $t2 $t3  %5d %5d  %3d",$jobSpawnCount,$totalJobs, $percentage if ($debug);

    my $tmpname="";
    my $tmpname2="";
    my $wpre="";
    my $wdir="";
    my $wexe="";
    my $wcmd="";
    my $wlog="";
    my $wpipe="";
    if ($windows) {
        if ($cmd =~ m/'\|\s*?(md5sum\s*?\>\>(.+?))'/) {
            $wpipe=$1;
            (my $tmph, $tmpname2) = tempfile(DIR => "temp");
            close($tmph);
            $cmd =~ s/'\|\s*?md5sum\s*?\>\>.+?'/\/c\/regression\/$tmpname2/g;
        }
        #$cmd =~ s/\| md5sum >>/\|cluster_md5sum.bat /g;
        if ($cmd =~ m/time(.*?\".*?\".*?)\.\/(gs|ref|head|head\~1)\/bin\/(\S+)(.*?)\>\>(\s*\S+\.log)/) {
            $wpre=$1;
            $wdir=$2;
            $wexe=$3;
            $wcmd=$4;
            $wlog=$5;
        }
        if ($wcmd ne "") {
            (my $tmph, $tmpname) = tempfile(DIR => "temp");
            $cmd =~ s/time(.*?\".*?\".*?)\.\/(gs|ref|head|head\~1)\/bin\/(\S+)(.*?)\>\>(\s*\S+\.log)/time$1$wrun \/mnt\/c\/Program\\ Files\/Git\/bin\/bash.exe \/c\/regression\/$tmpname \>\>$wlog/g;
            print $tmph "#!/usr/bin/bash\ncd /c/regression\n";
            # It would be much nicer to use a pipe, but something
            # appears to go wrong with the fifo on windows, because
            # it doesn't close when gs closes it, so the cat | md5sum
            # doesn't exit, and everything hangs, leading to timeouts
            # and unclosed handles. Hopefully this might get fixed
            # in future releases of Bash on Windows.
            if ($wpipe ne "") {
                # Nasty race condition.
                #print $tmph "rm -f $tmpname2\n";
                #print $tmph "mkfifo $tmpname2\n";
                #print $tmph "cat $tmpname2 | $wpipe &\n";
                #print $tmph "\$pid = \$!\n";
            }
            print $tmph ".\/$wdir\/bin/$wexe$exe$wcmd\n";
            if ($wpipe ne "") {
                # Provide a kick to close the pipe.
                #print $tmph "echo -n \"\" >> $tmpname2\n";
                # Wait for $pid to exit. Sadly wait seems to cause
                # outbash to have conniptions, so we busy wait.
                #print $tmph "while true; do\nps -p \$pid || break;\nsleep(1);\ndone;\n";
                #print $tmph "wait \$pid && echo success || echo failure\nrm -f $tmpname2\n";
                print $tmph "cat $tmpname2 | $wpipe\n";
                print $tmph "rm $tmpname2\n";
                $tmpname2="";
            }
            close($tmph);
        }
    }
    if ($configOption =~ m/\-\-disable\-compile\-inits/) {
	if ($pri == 1) {
            $cmd =~ s/%rom%/ghostpdl\.1\//g;
	} else {
            $cmd =~ s/%rom%/ghostpdl\//g;
	}
    }

    $jobSpawnCount++;
    my $pid = fork();
    if ( not defined $pid ) {
        mylog "fork() failed: $cmd";
        dieClean("fork() failed");
    } elsif ( $pid == 0 ) {
        # I am the child. I will run the job.
        `touch $outname.log ; rm -f $outname.log`;
        exec("( $cmd ) > $outname.log 2>&1") or dieClean("exec $cmd failed!");
    } else {
        mylog("NEWJOB $pid: $cmd");
        # I am the parent. Add the details of the child to our list.
        $pids{$pid}{'time'} = time;
        $pids{$pid}{'timeout'} = time + $mytimeout;
        $pids{$pid}{'filename'} = $filename;
        $pids{$pid}{'tmpname'} = $tmpname;
        $pids{$pid}{'tmpname2'} = $tmpname2;
        $pids{$pid}{'tmpname3'} = $midname;
        $pids{$pid}{'outname'} = $outname;
        $pids{$pid}{'cmd'} = $cmd;
    }
}

sub newTask($$)
{
    my $start = shift;
    my $end = shift;

    mylog("Starting task");
    $task{thread} = threads->create($start) or dieClean("newTask thread creation failed");
    $task{end}=$end;
}

sub killTask()
{
    # If we have no task, nothing to do.
    if (!defined $task{thread}) {
        return;
    }

    $task{thread}->kill('SIGINT');
    $task{thread}->join();
}

sub checkTaskComplete()
{
    # If we have no task, nothing to do.
    if (!defined $task{thread}) {
        return undef;
    }

    # If it's still running, return undef
    if ($task{thread}->is_running()) {
        return undef;
    }

    # Otherwise get the results
    my $result = $task{thread}->join();

    # Process the result if we have to.
    mylog("Ending task: $result");
    if ($task{end}) {
        $result = $task{end}->($result);
    }

    # No more task any more
    undef %task;

    mylog("Task complete $result");

    # Return the result
    return $result;
}

sub startBuild()
{
    $building = 1;
    # Ensure we have git repos for the required tests/modules and
    # that they are up to date.
    open (my $repoLock, ">>", "repolock") or dieClean("Failed to lock for repo update");
    flock($repoLock, LOCK_EX);
    updateTestRepos();
    if ($luratech) {
        updateLuratechRepo();
    }
    if ($cal) {
        updateCalRepo();
    }
    if ($ufst) {
        updateUFSTRepo();
    }
    close $repoLock;

    # Update readlog.pl if required
    {
       my $t = `md5sum ./readlog.pl`;
       chomp $t;
       $t =~ m/([0-9a-f]{32})/;
       my $c = $1;
       mylog("$c $readlogMd5sum");
       if ( $c ne $readlogMd5sum ) {
           systemWithRetry("scp -q -o StrictHostKeyChecking=no -i ~/.ssh/cluster_key  regression\@cluster.ghostscript.com:/home/regression/cluster/readlog.pl . >/dev/null 2>/dev/null");
           `chmod +x readlog.pl`;
       }
    }

    # Ensure our coverage scripts are up to date
    if ( $coverage ) {
        #unlink "coverage.job";
        #if ($coverageJob) {
        #    docmd("scp -q -o StrictHostKeyChecking=no -i ~/.ssh/cluster_key  regression\@cluster.ghostscript.com:/home/regression/cluster/coverage.job . >/dev/null 2>/dev/null");
        #}
        systemWithRetry("scp -q -o StrictHostKeyChecking=no -i ~/.ssh/cluster_key  regression\@cluster.ghostscript.com:/home/regression/cluster/runGCov.pl . >/dev/null 2>/dev/null");
        `chmod +x runGCov.pl`;
        `touch gcovtemp.bogus; rm -rf gcovtemp.*`;
        `touch $machine.gcov.out; rm -rf $machine.gcov.out; mkdir $machine.gcov.out`;
    }

    # Make output directories, clearing the ones from old runs.
    if ($windows) {
        docmd("rm -rf $temp/*");
    } else {
        docmd("touch $temp ; touch $temp2 ; rm -fr $temp2 ; mv $temp $temp2 ; mkdir $temp ; rm -fr $temp2 &");
    }
    docmd("touch baselineraster ; mv baselineraster baselineraster.tmp ; mkdir baselineraster ; mkdir $bmpcmpOutput ; rm -fr baselineraster.tmp &");

    if (defined $ref && $ref ne $revs && $ref ne "") {
        updateSource($ref);
        buildSource(" (ref)");
        copyBinariesToRef();
    }
    updateSource($revs);

    buildSource("");
    if (defined $ref && $ref eq $revs) {
        copyBinariesToRef();
    }

    # Ensure we have a "time" command
    if ( -e '../bin/time' ) {
        `cp ../bin/time $gsBin/bin/`;    # which is different on Mac OS X, so get the one I built
    } else {
        `cp /usr/bin/time $gsBin/bin/`;    # get the generic time command
    }

    # Ensure we have a flock
    if ( -e '/usr/bin/flock' ) {
        `cp /usr/bin/flock $gsBin/bin/.`;
    } else {
        `scp -o StrictHostKeyChecking=no -i ~/.ssh/cluster_key -q regression\@cluster.ghostscript.com:/home/regression/cluster/flock . >/dev/null 2>/dev/null`;
        `chmod +x flock`;
        `mv flock $gsBin/bin/.`;
    }

    `which md5sum`;
    if ($? != 0) {
        return $failcodes{md5sum};
    }

    if ( $compileFail ne "" ) {
        updateStatus("Compile fail ($compileFail)");
        mylog("setting $machine.fail on cluster\n");
        return $failcodes{compile};
    } else {
        updateStatus("Build successful");
    }

    return 0;
}

sub endBuild($)
{
    my $retval = shift;

    # Set the encironment stuff here, so it's in the parent, not the child.
    if (defined $environment && $environment ne "") {
        my @env = split ' ', $environment;
        foreach my $e (@env) {
            $e =~ m/(.*?)=(.*)/;
            if ($1 ne "" and $2 ne "") {
                mylog("Environment: $1:=$2\n");
                $ENV{$1}=$2;
            }
        }
    }

    if ($coverage) {
        $ENV{'RUNGCOV_OUTDIR'}="$machine.gcov.out";
        mylog("Coverage set to $ENV{'RUNGCOV_OUTDIR'}");
        if ($mupdf || $mujstest) {
            $ENV{'RUNGCOV_OBJDIR'}="$mupdfSource/build/release";
        } else {
            $ENV{'RUNGCOV_OBJDIR'}="$gsSource/obj";
        }
    }
    $building = 0;

    return $retval;
}

sub startUpload()
{
    $uploading = 1;
    collectStrayLogFiles();
    return uploadLogFiles();
}

sub endUpload($)
{
    my $retval = shift;

    $uploading = 0;
    $nothingtoupload = 1;

    return shift;
}

sub sendAck($)
{
    my $handle = shift;
    if (!print $handle "AYEAYE\n") {
        print LOG "Acknowledgement failed to send!";
    }
    shutdown($handle, 2); # Stopped using socket entirely
}

# Entry here.
# main

$machine = shift || die "usage: run.pl machine_name";

# Create (and rotate) debug log
`touch $machine$prisuf.dbg`;
my $filesize = -s "$machine$prisuf.dbg";
if ( $filesize > 10000000 ) {
    `mv $machine$prisuf.dbg $machine$prisuf.dbg.old`;
}
open( LOG, ">>$machine$prisuf.dbg" );
LOG->autoflush;
my $d = `date`;
chomp $d;
print LOG "\n----- New run.pl ----- $d -----\n";

print LOG "Disc space before cleaning:\n";
print LOG `df -k .`;

mylog "testBaseDirectory: $testBaseDirectory\n";

if ( $machine eq "fermis"
  || $machine eq "plancks"
  || $machine eq "i7"
  || $machine eq "macpro"
  || $machine eq "bohrs"
  || $machine eq "beards"
  || $machine eq "microns"
  || $machine eq "angstroms"
  || $machine eq "chains"
  || $machine eq "furlongs"
  || $machine eq "leagues"
  || $machine eq "links" ) {
    $maxSimultaneousJobs *= 1.5;
}

if ( !$clusterhome || $clusterhome eq "" ) {
    if ( -d "/home/regression/cluster" ) {
        $clusterhome = "/home/regression/cluster";
    } else {
        $clusterhome = "/home/marcos/cluster";
    }
}
my $windowsbash = $ENV{'WINDOWSBASH'};
if (!defined $windowsbash) {
    $windowsbash=0;
}
if ( "$^O" eq "cygwin" or "$^O" eq "msys" or $windowsbash) {
    $exe     = ".exe";
    $obj     = "obj64";
    $windows = 1;
}

# Check that there is only 1 of us running.
if ( -e $runningSemaphore ) {
    my $fileTime = stat($runningSemaphore)->mtime;
    my $t        = time;
    if ( $t - $fileTime > 7200 ) {
        mylog "semaphore file too old, removing\n";
        open( F, ">status" );
        print F "Regression terminated due to timeout";
        close(F);
        mylog("unlink1");
        unlink $runningSemaphore;
    }
    close(LOG);
    exit;
}

# Claim the semaphore as our own.
open( F, ">$runningSemaphore" );
print F "$$\n";
close(F);

`touch $machine$prisuf.log`;
`touch $machine$prisuf.out`;

my $caps = getCaps();

# Tidyup any stray droppings left from before.
if (1) {
    `touch $temp; rm -rf $temp`;
    `rm __mupdfSource__ >/dev/null 2>&1`;
    `rm __gsSource__ >/dev/null 2>&1`;
    `rm __gsviewSource__ >/dev/null 2>&1`;
    `rm *.md5 >/dev/null 2>&1`;
    `rm *Raw*raw >/dev/null 2>&1`;
    `rm *Raw*raw >/dev/null 2>&1`;
    `rm *Raw*raw >/dev/null 2>&1`;
    `rm *Device*raw >/dev/null 2>&1`;
    `rm *Image*raw >/dev/null 2>&1`;
    `rm *Composed*raw >/dev/null 2>&1`;
    `rm *Raw*pam >/dev/null 2>&1`;
    `rm *Device*pam >/dev/null 2>&1`;
    `rm *Image*pam >/dev/null 2>&1`;
    `rm *Composed*pam >/dev/null 2>&1`;
    `rm *Raw*pgm >/dev/null 2>&1`;
    `rm *Device*pgm >/dev/null 2>&1`;
    `rm *Image*pgm >/dev/null 2>&1`;
    `rm *Composed*pgm >/dev/null 2>&1`;
    `rm *_*x*raw >/dev/null 2>&1`;
    `rm dump*png >/dev/null 2>&1`;
    `rm *.icc >/dev/null 2>&1`;
    `rm *.icm >/dev/null 2>&1`;
    if ($pri == 1) {
        `rm -rf gcov.out >/dev/null 2>&1`;
        `rm -rf *.gcov.out >/dev/null 2>&1`;
        `rm -rf gcovtemp.* >/dev/null 2>&1`;
        `rm covera >/dev/null 2>&1`;
        `rm coverag >/dev/null 2>&1`;
        `rm *.coverage.tgz >/dev/null 2>&1`;
    }
    for ( my $i = 0 ; $i <= 9 ; $i++ ) {
        `rm $i*PATTERN*raw >/dev/null 2>&1`;
    }
    for ( my $i = 1 ; $i <= 9 ; $i++ ) {
        `rm 0$i*.pam >/dev/null 2>&1`;
        `rm 0$i*.raw >/dev/null 2>&1`;
    }
    for ( my $i = 10 ; $i <= 99 ; $i++ ) {
        `rm $i*.pam >/dev/null 2>&1`;
        `rm $i*.raw >/dev/null 2>&1`;
    }
}

print LOG "Disc space after cleaning:\n";
print LOG `df -k .`;

# the commands below only need to be done once
mkdir("$headBin");
mkdir("$headBin/bin");
mkdir("$headBin~1");
mkdir("$headBin~1/bin");
mkdir("$temp");

# Redirect temporary files into a local tmpdir so we can safely
# clean it out.
my $tmpdir = "/tmp/tmpdir$prisuf";
`touch $tmpdir ; mv $tmpdir $tmpdir.del`;
mkdir("$tmpdir");
`rm -rf $tmpdir.del &`;
$ENV{'TMPDIR'}="$tmpdir";

# So we're actually going to try and run stuff.
mylog "starting run.pl:  pid=$$\n";
updateStatus('Starting cluster job');

# Kill any old jobs
#my $a = `ps auxww | grep ClusterJob | grep -v grep`;
#$a .= `ps auxww | grep pdfdraw | grep "\\-stm" | grep -v grep`;
#my @a = split '\n', $a;
#foreach (@a) {
#    chomp;
#    my @b = split ' ', $_, 10;
#    mylog "leftover job $b[1]  startTime $b[8]: $b[9]\n";
#    kill 9, $b[1];
#}

# Read the contents of the start file
open( F, "<job$prisuf.start" ) || dieClean("Failed to read job$prisuf.start");
my $jobId = <F>;
chomp $jobId;
my $t = <F>;
chomp $t;
my $t2 = <F>;
chomp $t2;
close(F);

# Put the contents into the log for debugging purposes
mylog("job$prisuf.start:\n$jobId\n$t\n$t2\n");

# Parse the job start details.
if (!$t2) {
    dieClean("Unexpected job$prisuf.start contents (2)");
} else {
    my @a = split ' ', $t2;
    if ( scalar(@a) != 5 ) {
        dieClean("Unexpected job$prisuf.start contents");
    }
    $testRepositoryRev[0] = $a[0];
    $testRepositoryRev[1] = $a[1];
    $readlogMd5sum        = $a[2];
    $valgrind             = $a[3];
    $coverage             = $a[4];
}
if (!$t) {
    dieClean("Unexpected job$prisuf.start contents (3)");
} else {
    my $gs;
    my $pcl;
    my $xps;
    my $gpdf;
    my $gpdl;
    my @a = split '\t', $t;
    if ( $a[0] eq "git" ) {
        $revs     = $a[1];
        $products = $a[2];
    } elsif ( $a[0] eq "mupdf" ) {
        $mupdf                = 1;
        $products             = "mupdf";
        $revs                 = $a[1];
        $maxSimultaneousJobs  = 8;
        $maxTimeoutPercentage = 5.0;
    } elsif ( $a[0] eq "mupdfmini" ) {
        $mupdfmini            = 1;
        $products             = "mupdfmini";
        $revs                 = $a[1];
        $maxSimultaneousJobs  = 8;
        $maxTimeoutPercentage = 5.0;
    } elsif ( $a[0] eq "mujstest" ) {
        $mujstest             = 1;
        $products             = "mujstest";
        $revs                 = $a[1];
        $maxSimultaneousJobs  = 8;
        $maxTimeoutPercentage = 5.0;
    } elsif ( $a[0] eq "gsview" ) {
        $gsview               = 1;
        $products             = "gsview";
        $revs                 = $a[1];
        $maxSimultaneousJobs  = 8;
        $maxTimeoutPercentage = 5.0;
    } elsif ( $a[0] eq "user" ) {
        $user     = $a[1];
        $products = $a[2];
        if ($products =~ m/bmpcmp/) {
            $bmpcmp = 1;
        }
        if ( exists $a[3]) {
            my $tmp = $a[3];
            if ($tmp =~ m/bmpcmp/) {
                $bmpcmp   = 1;
                $tmp =~ s/bmpcmp//;
            }
            if ( $tmp =~ 'luratech' ) {
                $luratech = 1;
                $tmp =~ s/luratech//;
            }
            if ( $tmp =~ 'cal' ) {
                $cal = 1;
                $tmp =~ s/cal//;
            }
            if ( $tmp =~ 'ufst' ) {
                $ufst = 1;
                $tmp =~ s/ufst//;
            }
            if ( $tmp =~ '32' ) {
                $wordSize = 32;
                $tmp =~ s/32//;
            }
            if ( $tmp =~ 'relaxTimeout' ) {
                $timeOut  = 1800;
                $tmp =~ s/relaxTimeout//;
            }
        }
        if ( $a[2] =~ m/gs/ ) {
            $gs      = 1;
        }
        if ( $a[2] =~ m/pcl/ ) {
            $pcl     = 1;
        }
        if ( $a[2] =~ m/xps/ ) {
            $xps     = 1;
        }
        if ( $a[2] =~ m/gpdf/ ) {
            $gpdf    = 1;
        }
        if ( $a[2] =~ m/gpdl/ ) {
            $gpdl    = 1;
        }
        if ( $a[2] =~ m/mupdfmini/ ) {
            $mupdfmini = 1;
        } elsif ( $a[2] =~ m/mupdf/ ) {
            $mupdf    = 1;
        }
        if ( $a[2] =~ m/mujstest/ ) {
            $mujstest = 1;
        }
        if ( $mupdf || $mujstest ) {
            $maxSimultaneousJobs = 12;
        }
        if ($bmpcmp) {
            $products = "";
            if ($gs) {
                $products .=" gs";
            }
            if ($pcl) {
                $products .=" pcl";
            }
            if ($xps) {
                $products .=" xps";
            }
            if ($gpdf) {
                $products .=" gpdf";
            }
            if ($gpdl) {
                $products .=" gpdl";
            }
            if ($mupdf) {
                $products .= " mupdf";
            }
            if ($mujstest) {
                $products .= " mujstest";
            }
        }
    } elsif ( $a[0] eq "auto" ) {
        $autoName     = $a[1];
        $products     = $a[2];
        $revs         = $a[3];
        $wordSize     = $a[4];
        if (exists $a[5] && $a[5] ne "") {
            $timeOut      = 1800;
        }
        $makeOption   = $a[6];
        $configOption = $a[7];
        $beforeMake   = $a[8];
        $afterMake    = $a[9];
        $filterJobs   = $a[10];
        $luratech     = $a[11];
        $ufst         = $a[12];
        $ref          = $a[13];
        $environment  = $a[14];
        $cal          = $a[15];
        if ( $a[2] =~ m/bmpcmp/ ) {
            $bmpcmp       = 1;
        }
        if ( $a[2] =~ m/mupdfmini/ ) {
            $mupdfmini    = 1;
        } elsif ( $a[2] =~ m/mupdf/ ) {
            $mupdf        = 1;
        }
        if ( $a[2] =~ m/mujstest/ ) {
            $mujstest     = 1;
        }
        if ( $a[2] =~ m/gsview/ ) {
            $gsview       = 1;
        }
        if ( $mupdf || $mujstest ) {
            $maxSimultaneousJobs     = 12;
        }
    }
}    

# Sanitise variable values to avoid warnings later.
if (not defined $makeOption) {
    $makeOption = "";
}
if (not defined $configOption) {
    $configOption = "";
}
if (not defined $beforeMake) {
    $beforeMake = "";
}
if (not defined $afterMake) {
    $afterMake = "";
}
if (not defined $filterJobs) {
    $filterJobs = "";
}
if (not defined $ref) {
    $ref = "";
}
if (not defined $revs) {
    $revs = "";
}
if (not defined $luratech) {
    $luratech = "";
}
if (not defined $cal) {
    $cal = "";
}
if (not defined $ufst) {
    $ufst = "";
}
if (not defined $environment) {
    $environment = "";
}
if (not defined $wordSize) {
    $wordSize = "64";
}

# Output some debug to describe the kind of job we think we're starting.
if ($autoName) {
    mylog "auto products=$products name=$autoName rev=$revs wordsize=$wordSize timeout=$timeOut makeOption=$makeOption configOption=$configOption beforeMake=$beforeMake afterMake=$afterMake filterJobs=$filterJobs ref=$ref\n";
}
if ($user) {
    mylog "user products=$products user=$user\n";
}
if ($revs && !$autoName) {
    mylog "git products=$products rev=$revs\n";
}
if ( $mupdf && !$user && !$autoName ) {
    mylog "git mupdf rev=$revs\n";
}
if ( $mujstest && !$user && !$autoName ) {
    mylog "git mujstest rev=$revs\n";
}

my $cmd;
my $s;

mylog("products detected as: $products\n");
if ( length($products) == 0 ) {
    $products = "gs pcl xps ls";
}
my @productList = split ' ', $products;
foreach (@productList) {
    $products{$_} = 1;
}

# Start the logging for this job
startJobLog();

# Make ourselves a git wrapper to ensure we use the cluster key
if (open(F, ">$baseDirectory/gitssh.sh")) {
    print F "#!/bin/bash\nssh -i ~/.ssh/cluster_key -o StrictHostKeyChecking=no \"\$\@\"\n";
    close(F);
    `chmod +x gitssh.sh`;
}
$ENV{GIT_SSH}="$baseDirectory/gitssh.sh";

# New main loop
my $abortServer = 0;
my $nextConnectionTime = time();
my $connectionInterval = 30;
my $failure = undef;
my $lastSuccessfulConnectionTime = time();
my $paused = 0;
while ($abortServer == 0) {
    checkPID();

    # Check to see if any job has ended
    undef %parentProcessHash;
    my $aJobCompleted = 0;
    foreach my $pid (keys %pids) {
        # Check to see if they've timed out.
        my $end = 0;
        if (!$paused && time() > $pids{$pid}{'timeout'}) {
            # Job has timed out
            timeoutProcessAndChildren($pid);

            $timeOuts{$pids{$pid}{'filename'}} = 1;
            $end = 1;
        } elsif (waitpid($pid, WNOHANG) < 0) {
            # Job has finished
            $end = 1;
        }
        if ($end) {
            mylog("FINISHED $pid: $pids{$pid}{cmd}");;
            addToLog( $pids{$pid}{'filename'}, $pids{$pid}{'outname'} );
            if ($pids{$pid}{'tmpname'} ne "") {
                unlink($pids{$pid}{'tmpname'});
            }
            if ($pids{$pid}{'tmpname2'} ne "") {
                unlink($pids{$pid}{'tmpname2'});
            }
            if ($pids{$pid}{'tmpname3'} ne "") {
                unlink($pids{$pid}{'tmpname3'});
            }
            if ($timeOuts{$pids{$pid}{'filename'}} == 1) {
                print F4 "(Timeout!)\n";
            }
            delete $pids{$pid};
            $aJobCompleted = 1;
        }
    }

    # Check to see if we've hit too many timeouts.
    if (scalar( keys %timeOuts ) > $maxTimeout) {
        killAllJobs();
        $abortServer = 1;
    }

    # Check to see if we're being blocked by any higher priority jobs
    my $was_paused = $paused;
    # If there is a job.X.start for X < $pri, then we should be paused.
    $paused = 0;
    for (my $i = 0; $i < $pri; $i++)
    {
        my $js_name = "job.start";
        if ($i > 0) {
            $js_name = "job.$i.start";
        }
        if (-e "$js_name") {
            $paused = 1;
        }
    }

    my $countOfRunningJobs;
    if ($paused)
    {
        if (!$was_paused)
        {
            updateStatus("Pausing to allow higher priority job to run");
            # We have just started to be paused. Better stop all our
            # jobs.
            pauseAllJobs();
        }
        else
        {
            # Still paused. Nothing to do.
            updateStatus("Still paused");
        }

        # Avoid a helpful watchdog killing us off.
        updateSemaphoreTimestamp();

        # We need this up to date for status messages
        $countOfRunningJobs = scalar(keys %pids);
    }
    else
    {
        if ($was_paused)
        {
            updateStatus("Higher priority job finished - unpausing");
            unpauseAllJobs();
        }
    
        # Spawn new jobs if possible
        while (($countOfRunningJobs = scalar(keys %pids)) < $maxSimultaneousJobs &&
               scalar(@jobsList) > 0 && !$abortServer) {
            spawnNewJob();
        }

        # If haven't got any more jobs to queue, and we haven't aborted, then we
        # really need some new ones, so short cut the time we wait before our
        # next connection to the server. Note that we only do this if a job
        # just ended (i.e. if we only just finished out last job); this avoids
        # us spamming it with repeated connections when it has no more jobs to
        # hand out.
        if ($aJobCompleted && scalar(@jobsList) == 0 && !$abortServer) {
            $nextConnectionTime = time();
        }
    }

    # Check to see if any background task (building or uploading) has completed.
    $failure = checkTaskComplete();
    if (defined $failure) {
        $nextConnectionTime = time();
    }
        
    # Connect to the server for status/instructions
    if (!$paused && $nextConnectionTime <= time()) {
        # As part of the connection process, we'll send status updates.
        # Let's prepare those status updates now.
        my $nodeAbort            = 0;

        # Update the jobs status
        if (defined $jobSpawnCount) {
            my $t = int( $jobSpawnCount * $maxTimeoutPercentage / 100 + 0.5 );
            if ( $maxTimeout < $t ) {
                $maxTimeout = $t;
                mylog("maxTimeout set to $maxTimeout\n");
            }
            my $count2 = scalar(keys %timeOuts);
            my $n = scalar(@jobsList);
            my $u = "";
            if ($uploading) {
                $u = " (uploading)";
            } elsif ($nothingtoupload) {
                $u = " (uploaded)";
            }
            updateStatus("$n jobs pending, $countOfRunningJobs running, $jobSpawnCount complete ($count2/$maxTimeout timeouts)$u");
        }

        # Check for low disc space
        {
            my $space = `df $baseDirectory | tail -1`;
            chomp $space;
            my $used = 0;
            if ( $space =~ m/(\d+)\%/ ) {
                $used = $1;
            }
            if ( $used >= 95 ) {
                updateStatus('Low on disk space');
            }
            if ( $used >= 98 ) {
                $nodeAbort = 1;
            }
        }

        # Check for the logs getting too large
        {
            `touch temp/dummy.log`;
            my $a = `ls -s temp/*log | sort -n | tail -1`;
            chomp $a;
            `rm temp/dummy.log`;
            my $s = 0;
            if ( $a =~ m/^(\d+)/ ) {
                $s = $1;
            }

            if ( $s > $logFileSizeLimit ) {
                my $a = `ls -s temp/*log | sort -n | tail -10`;
                updateStatus('Log file size fail');
                $nodeAbort = 1;
            }
        }
        
        # Do the actual connection
        eval {
            local $SIG{ALRM} = sub { die 'Timeout'; };
            alarm 10;
            use IO::Socket;
            my $host = "cluster.ghostscript.com";
            my $port = 9100+$pri;

            mylog("Trying for a socket connection");
            my $handle = IO::Socket::INET->new(
                Proto    => "tcp",
                PeerAddr => $host,
                PeerPort => $port,
                Timeout  => 1
            );
            if ($handle) {
                #mylog("connected");
                alarm 30;
                # Ignore any signals due to transmission errors, and flush
                # without being told to.
                $SIG{PIPE} = 'IGNORE';
                $handle->autoflush(1);

                # We have a server connection
                # Send the state of play to the server
                my $send = "node $machine\nid $jobId\nmyid $$\n";
                if (defined $caps) {
                    $send .= "caps $caps\n";
                }
                if (%task) {
                    $send .= "busy\n";
                }
                if (defined $failure && $failure != 0) {
                    $send .= "failure $failure\n";
                }
                if ($status) {
                    $send .= "status $status\n";
                }
                if (defined $jobSpawnCount) {
                    my $jobsPending = scalar @jobsList;
                    $send .= "jobs $jobsPending $countOfRunningJobs\n";
                }
                if ($nodeAbort) {
                    $send .= "ABORT\n";
                    $abortServer = 1;
                } else {
                    $send .= "OK\n";
                }
                if (print $handle "$send") {
                    #mylog("Sent to socket");
                } else {
                    mylog("Socket write failed");
                    die("Socket write failed"); # Leave the eval
                }
                alarm 30;
                # Now get the servers response
                my $block = "";
                my $n;
                do {
                    my $line;
                    $n = sysread( $handle, $line, 4096 );
                    #mylog("read $n");
                    $block .= $line;
                } until (!defined($n) || $n == 0);
                alarm 30;
                my @response = split '\n', $block;
                #mylog("$response[0] [$block]");
                if ($response[0] eq "ABORT") {
                    mylog("Server response: ABORT");
                    $abortServer = 1;
                    killTask();
                    killAllJobs();
                } elsif ($response[0] eq "BUILD") {
                    mylog("Server response: BUILD");
                    sendAck($handle);
                    newTask(\&startBuild, \&endBuild);
                } elsif ($response[0] eq "CONTINUE") {
                    mylog("Server response: CONTINUE");
                    # Nothing to do
                } elsif ($response[0] =~ m/^JOBS (\d+)/) {
                    my $m = $1;
                    shift @response;
                    my $n = scalar(@response);
                    if ($n != $m) {
                        mylog("Server response: JOBS $m - but $n sent");
                    } else {
                        sendAck($handle);
                        mylog("Server response: JOBS ($n)");
                        push @jobsList, @response;
                        if (!defined $jobSpawnCount) {
                            $jobSpawnCount = 0;
                        }
                        #foreach my $j (@response) {
                        #    mylog(">>> $j");
                        #}
                        $nothingtoupload = 0;
                    }
                } elsif ($response[0] eq "UPLOAD") {
                    mylog("Server response: UPLOAD");
                    sendAck($handle);
                    newTask(\&startUpload, \&endUpload);
                } else {
                    mylog("Unknown server response: $response[0]");
                    $abortServer = 1;
                }
                close($handle);
                if (!$abortServer) {
                    $lastSuccessfulConnectionTime = time();
                }
            }
            alarm 0;
        };
        alarm 0;
        if ($@ ne "") {
            mylog("Eval error: $@");
        }
        if ($lastSuccessfulConnectionTime + 60 <= time()) {
            # Server might be down.
            mylog("Connection to server refused.");
            unlink("job$prisuf.start");
            my $resp = `scp -q -o StrictHostKeyChecking=no -o ConnectTimeout=15 -i ~/.ssh/cluster_key regression\@cluster.ghostscript.com:/home/regression/cluster/job$prisuf.start . 2>&1`;
            if ($resp =~ m/No such file/) {
                # Job finished!
                mylog("No job$prisuf.start - job finished and no one told us!");
                $abortServer=1;
            }
        }
        $nextConnectionTime = time() + $connectionInterval;
    } elsif ($countOfRunningJobs == 0 || $paused) {
        # No need to spin.
        sleep 5;
    }
}

if ($compileFail ne "") {
    print F4 "Compile fail: $compileFail\n";
}

# clean out the tmpdir
`rm -rf $tmpdir`;

mylog "exiting run.pl:  pid=$$\n";
dieClean("All done!");
