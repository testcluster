#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my $debug=0;

if ($debug) {
    open (F9,">compare.dbg");
}

sub dprint($) {
    my $string = shift;
    if ($debug) {
        print $string;
    }
}

my $verbose=0;
my $reportAllErrors=0;
my $previousValues=50;

my @errorDescription=(
    "none",
    "Error_reading_input_file",
    "Error_reading_Ghostscript_produced_PDF/PS/XPS_file",
    "Timeout_reading_input_file",
    "Timeout_reading_Ghostscript_produced_PDF/PS/XPS_File",
    "Input_file_missing",
    "Ghostscript_generated_PDF/PS/XPS_file_missing",
    "Seg_Fault_during_pdfwrite/ps2write/xpswrite",
    "Seg_Fault",
    "Seg_Fault_reading_Ghostscript_produced_PDF/PS/XPS_File",
    "Internal_error"
);

my %productBits=(
    gs  =>  1,
    pcl =>  2,
    xps =>  4,
    svg =>  8,
    ls  => 16,
    mupdf => 32,
    mujstest => 64,
    gpdf => 128,
    gpdl => 256
);

my $current=shift;
my $previous=shift;
my $jobs=shift;
my $elapsedTime=shift;
my $machineCount=shift || die "usage: compare.pl current.tab previous.tab jobs elapsedTime machineCount [skipMissing [products]] [skipErrors]";
my $skipMissing=shift;
my $products=shift;
my $skipErrors=shift;

if (!defined $skipErrors) {
    $skipErrors = "false";
}
if ($skipErrors eq "true") {
    $reportAllErrors = 0;
}

# Restrict the results tables to the jobs that actually ran.
# i.e. we're stripping out any results that were carried
# forward.
`./filterTab.pl $current $jobs >$current.tmp`;
$current=$current.".tmp";
`./filterTab.pl $previous $jobs >$previous.tmp`;
$previous=$previous.".tmp";

if (!$products) {
    $products="gs pcl xps svg ls";
}

# Populate $productBits with the actual products.
# (i.e. strip out "lowres" or "highres" etc)
my $productBits = 0;
{
    my @a=split ' ',$products;
    foreach my $i (@a) {
        #if ($i eq "lowres") { next; }
        #if ($i eq "highres") { next; }
        #if (!exists $productBits{$i}) { die "product '$i' does not exist"; }
        if (!exists $productBits{$i}) { next; };
        $productBits+=$productBits{$i};
    }
}

#print "$productBits\n";  exit;

# Fill %skip with the list of tests to skip
my %skip;
if (open(F,"<skip.lst")) {
    while(<F>) {
        chomp;
        s|__|/|g;
        my @a=split '\s';
        $skip{$_}=$a[0];
    }
    close(F);
}

my %current;
my %currentTest;
my %currentError;
my %currentProduct;
my %currentMachine;
my %currentTime1;
my %currentTime2;
my %currentSize;
my %currentInterMD5;
my %previous;
my %previousTest;
my %previousError;
my %previousProduct;
my %previousProductBits;
my %previousMachine;
my %previousTime1;
my %previousTime2;
my %previousSize;
my %previousInterMD5;
my %archive;
my %archiveProduct;
my %archiveMachine;

my %archiveCache;

my @filesRemoved;
my @filesAdded;
my @allErrors;
my @allPreviousErrors;
my @brokePrevious;
my @repairedPrevious;
my @timeOutStarted;
my @timeOutStopped;
my @differencePrevious;
my @differencePreviousGS;
my @differencePreviousPCL;
my @differencePreviousXPS;
my @differencePreviousGPDF;
my @differencePreviousGPDL;
my @differencePreviousPdfwrite;
my @differencePreviousPs2write;
my @differencePreviousXpswrite;
my @archiveMatch;
my %archiveCount=( 'gs'=>0, 'pcl'=>0, 'xps'=>0, 'gpdf'=>0, 'gpdl'=>0,
                   'gs pdfwrite'=>0, 'gs ps2write'=>0, 'gs xpswrite'=>0, 'pcl pdfwrite'=>0, 'xps pdfwrite'=>0,
                   'gpdf pdfwrite'=>0, 'gpdf ps2write'=>0, 'gpdf xpswrite'=>0,
                   'gpdl pdfwrite'=>0, 'gpdl ps2write'=>0, 'gpdl xpswrite'=>0,
                   'mupdf'=>0, 'mujstest'=>0 );

my @baselineUpdateNeeded;

my %seen;

my $t2;

# Tab format:
#  0   testfile.device.res.band
#  1   error
#  2   time1
#  3   time2
#  4   mem1
#  5   mem2
#  6   md5sum
#  7   rev
#  8   product
#  9   machine
#  10  size of intermediate
#  11  md5sum of intermediate

# Fill %current{,Error,Product,Machine,Time1,Time2,Size} etc with the current results
print STDERR "reading $current\n" if ($verbose);
open(F,"<$current") || die "file $current not found";
while(<F>) {
    chomp;
    s|__|/|g;
    my @a=split '\t';
    my $product=$a[8]; # Product

    # Strip trailing spaces off the product
    if($product =~ m/^(.+) /) {
        $product=$1;
    }

    # Look up the product as a mask
    my $bits=$productBits{$product};

    # Unknown product? Just skip this whole line.
    if (!$bits) { next; }

    # If this isn't a product we're testing, skip this whole line.
    if (!($bits & $productBits)) { next; }

    # If this test is in the ones we have been told to skip, skip it if there is no error.
    if (exists $skip{$a[0]} && $a[1]==0) { next; }  # skip only if no error

    # if ($a[1] ne "0") { $a[6]=$a[1]; }

    my $key="$a[0]:$product";
    $current{$key}=$a[6]; # md5sum
    $currentTest{$key}=$a[0];
    $currentError{$key}=0;
    if ($a[1]!=0) {
        $currentError{$key}='unknown';
        if (exists $errorDescription[$a[1]]) {
            $currentError{$key}=$errorDescription[$a[1]];
        }
        $current{$key}=0;
    }
    $currentProduct{$key}=$a[8]; # Product
    $currentMachine{$key}=$a[9]; # Machine
    $currentTime1{$key}=$a[2];   # Time1
    $currentTime2{$key}=$a[3];   # Time2
    $currentSize{$key}=0;        # Size
    if (scalar(@a)>10) {
        $currentSize{$key}=$a[10]; # Intermediate size
    }
    if (scalar(@a)>11) {
        $currentInterMD5{$key}=$a[11]; # Intermediate md5
    } else {
        $currentInterMD5{$key}="0";
    }
}

#print Dumper(\%currentSize);  exit;

# Fill %previous{,Error,Product,Machine,Time1,Time2,Size} etc with the previous results.
print STDERR "reading $previous\n" if ($verbose);
open(F,"<$previous") || die "file $previous not found";
while(<F>) {
    chomp;
    s|__|/|g;
    my @a=split '\t';
    my $product=$a[8]; # Product

    # Strip trailing spaces off the product
    if($product =~ m/^(.+) /) {
        $product=$1;
    }

    # Look up the product as a mask
    my $bits=$productBits{$product};

    # Unknown product? Just skip the whole line.
    if (!$bits) { next; }

    # If this isn't a product we're testing, skip this whole line.
    if (!($bits & $productBits)) { next; }

    # If this test is in the ones we have been told to skip, skip it if there is no error.
    if (exists $skip{$a[0]} && $a[1]==0) { next; }  # skip only if no error

    # $a[6]=$a[1] if ($a[1] ne "0");

    my $key="$a[0]:$product";
    $previous{$key}=$a[6]; #md5sum
    $previousTest{$key}=$a[0];
    $previousError{$key}=0;
    if ($a[1]!=0) {
        $previousError{$key}='unknown';
	if (exists $errorDescription[$a[1]]) {
            $previousError{$key}=$errorDescription[$a[1]];
	}
        $previous{$key}=0;
    }
    $previousProduct{$key}=$a[8]; # Product
    $previousMachine{$key}=$a[9]; # Machine
    $previousTime1{$key}=$a[2];   # Time1
    $previousTime2{$key}=$a[3];   # Time2
    $previousSize{$key}=0;        # Size
    if (scalar(@a)>10) {
        $previousSize{$key}=$a[10]; # Intermediate size
    }
    if (scalar(@a)>11) {
        $previousInterMD5{$key}=$a[11]; # Intermediate md5
    } else {
        $previousInterMD5{$key}="0";
    }
}
close(F);

# Read the MD5sum cache.
my $cache="md5sum.cache";
$cache="mupdf-md5sum.cache" if ($products eq "mupdf");
$cache="mujstest-md5sum.cache" if ($products eq "mujstest");
if ($elapsedTime!=0) {
    if (open(F,"<$cache")) {
        print STDERR "reading $cache\n" if ($verbose);
        while(<F>) {
            chomp;
            if (m/(.+) \| (.+)/) {
                $archiveCache{$1}=$2;
            } elsif (m/^(\d+)$/) {
                $previousValues=$1;
            }
        }
        close(F);
        #print Dumper(\%archiveCache);
    }
}

#print "previous\n".Dumper(\%previous);
#print "current \n".Dumper(\%current);
#exit;

my $first=1;

# List the asserts and seg faults
if (!($previous=~m/users/)) {
    foreach my $t (sort keys %current) {
        if ($currentError{$t} =~ m/Seg_Fault/) {
            if ($first) {
                if ($products eq "mupdf" || $products eq "mujstest") {
                    print "\n*****************************************************************************\nAsserts or seg faults with current rev:\n\n";
                } else {
                    print "\n*****************************************************************************\nSeg faults with current rev:\n\n";
                }
            }
            $first=0;
            if ($products eq "mupdf" || $products eq "mujstest") {
                print "$currentTest{$t} $currentProduct{$t} $currentMachine{$t} Assert_or_Seg_Fault\n";
            } else {
                print "$currentTest{$t} $currentProduct{$t} $currentMachine{$t} $currentError{$t}\n";
            }
        }
    }
    if (!$first) {
        print "\n*****************************************************************************\n\n\n";
    }
}

#print Dumper(\%previous);

# Now process the data sets.
foreach my $t (sort keys %previous) {
    if (exists $current{$t}) {
        # Result $t is in both previous and current. Compare it.
        my $match=0;
        $seen{$currentProduct{$t}}++;
        dprint("\n$t\n");
        dprint("currentError $currentError{$t}  previousError $previousError{$t}\n");
        dprint("current $current{$t}  previous $previous{$t}\n");
        if ($previousError{$t}) {
            push @allPreviousErrors,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $currentError{$t}";
        }
        if ($currentError{$t}) {
            push @allErrors,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $currentError{$t}";
            if ($previousError{$t}) {
                # Was an error before, still an error now
                # FIXME: Report changes in error codes
            } else {
                # File has started causing errors
                dprint("currentError && !previousError:\n");
                dprint("$t $currentError{$t} $previousError{$t} $current $previous\n");
                if (exists $archiveCache{$t.' '.$current{$t}}) {
                    dprint("exists archiveCache{current}\n");
                    my @a=split "\t", $archiveCache{$t.' '.$current{$t}};
                    my $message="";
                    $message=$currentError{$t} if ($currentError{$t});
                    push @archiveMatch,"$previousTest{$t} $previousProduct{$t} $a[1] $a[2] $currentMachine{$t} $a[0] $a[3] $message";
                    $archiveCount{$currentProduct{$t}}++;
                    $match=1;
                } else {
                    dprint("!exists archiveCache{current}\n");
                    if ($currentError{$t} =~ m/Timeout/) {
                        dprint("previousError == Timeout\n");
                        push @timeOutStarted,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $currentError{$t}";
                    } else {
                        dprint("previousError != Timeout\n");
                        push @brokePrevious,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $currentError{$t}";
                    }
                }
            }
        } else {
            if ($previousError{$t}) {
                # File has stopped giving errors
                dprint("!currentError && previousError:\n");
                dprint("$previousTest{$t} $previousProduct{$t} $currentError{$t} $previousError{$t} $current $previous\n");
                if (exists $archiveCache{$t.' '.$current{$t}}) {
                    dprint("exists archiveCache{current}\n");
                    my @a=split "\t", $archiveCache{$t.' '.$current{$t}};
                    my $message="";
                    $message=$currentError{$t} if ($currentError{$t});
                    push @archiveMatch,"$previousTest{$t} $previousProduct{$t} $a[1] $a[2] $currentMachine{$t} $a[0] $a[3] $message";
                    $archiveCount{$currentProduct{$t}}++;
                    $match=1;
                } else {
                    dprint("!exists archiveCache{current}\n");
                    if ($previousError{$t} =~ m/Timeout/) {
                        dprint("previousError == Timeout\n");
                        push @timeOutStopped,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $previousError{$t}";
                    } else {
                        dprint("previousError != Timeout\n");
                        push @repairedPrevious,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $previousError{$t}";
                    }
                }
            } else {
                # Wasn't an error before, isn't now
                if ($current{$t} eq $previous{$t}) {
                    # print "$t match $previous and $current\n";
                } else {
                    # foreach my $p (sort {$b cmp $a} keys %archive) {
                    #     if (!$match && exists $archive{$p}{$t} && $archive{$p}{$t} eq $current{$t}) {
                    #         $match=1;
                    #         push @archiveMatch,"$t $archiveProduct{$p}{$t} $archiveMachine{$p}{$t} $currentMachine{$t} $p $archiveCount{$p}";
                    #     }
                    # }
                    if (exists $archiveCache{$t.' '.$current{$t}}) {
                        dprint("\n(currentError eq previous) && exists archiveCache{current}:\n");
                        dprint("$t $currentError{$t} $previousError{$t} $current $previous\n");
                        my @a=split "\t", $archiveCache{$t.' '.$current{$t}};
                        my $message="";
                        if ($currentError{$t}) {
                            $message=$currentError{$t};
                        }
                        # die "happened" if ($currentError{$t});
                        push @archiveMatch,"$previousTest{$t} $previousProduct{$t} $a[1] $a[2] $currentMachine{$t} $a[0] $a[3] $message";
                        $archiveCount{$currentProduct{$t}}++;
                        $match=1;
                    }
                    if ($current{$t} eq 0 || $previous{$t} eq 0) {
                        $match=1;
                    }
                    if (!$match) {
                        #print "$currentProduct{$t} $current{$t} $previous{$t} $t\n";
			my $stage;
			if ($currentInterMD5{$t} ne $previousInterMD5{$t}) {
			    $stage = "Early";
			} else {
			    $stage = "Late";
                        }
                        if ($currentProduct{$t} =~ m/pdfwrite/) {
                            push @differencePreviousPdfwrite,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $stage";
                        } elsif ($currentProduct{$t} =~ m/ps2write/) {
                            push @differencePreviousPs2write,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $stage";
                        } elsif ($currentProduct{$t} =~ m/xpswrite/) {
                            push @differencePreviousXpswrite,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t} $stage";
                        } else {
                            push @differencePrevious   ,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}";
                            if ($currentProduct{$t} eq "gs") {
                                push @differencePreviousGS ,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}";
                            }
                            if ($currentProduct{$t} eq "pcl") {
                                push @differencePreviousPCL,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}";
                            }
                            if ($currentProduct{$t} eq "xps") {
                                push @differencePreviousXPS,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}" ;
                            }
                            if ($currentProduct{$t} eq "gpdf") {
                                push @differencePreviousGPDF ,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}";
                            }
                            if ($currentProduct{$t} eq "gpdl") {
                                push @differencePreviousGPDL ,"$previousTest{$t} $previousProduct{$t} $previousMachine{$t} $currentMachine{$t}";
                            }
                        }
                    }
                }
            }
        }
        if ($currentMachine{$t} eq $previousMachine{$t}) {
            my $timeDelta1=0;
            my $timeDelta2=0;
            if ($previousTime1{$t}>0) {
                $timeDelta1=($currentTime1{$t}-$previousTime1{$t})/$previousTime1{$t};
            }
            if ($previousTime2{$t}>0) {
                $timeDelta2=($currentTime2{$t}-$previousTime2{$t})/$previousTime2{$t};
            }
            #printf  "%10f %10f %10f %s\n",$timeDelta1,$currentTime1{$t},$previousTime1{$t},$t;
        }
    } else {
        # Result $t has been removed
        if (!exists $skip{$t}) {
            push @filesRemoved,"$previousTest{$t} $previousProduct{$t}";
        }
    }
}

#print Dumper(\%seen);

#print Dumper(\@differencePrevious);
#print Dumper(\@differencePreviousGS);
#print Dumper(\@differencePreviousPCL);
#print Dumper(\@differencePreviousXPS);
#print Dumper(\@differencePreviousPdfwrite);
#print Dumper(\@differencePreviousPs2write);
#print Dumper(\@differencePreviousXpswrite);
#exit;


my $pdfwriteTestCount=0;
my $ps2writeTestCount=0;
my $xpswriteTestCount=0;
my $notPdfwriteTestCount=0;
my $gsTestCount=0;
my $pclTestCount=0;
my $xpsTestCount=0;
my $gpdfTestCount=0;
my $gpdlTestCount=0;

foreach my $t (sort keys %current) {
    if (!exists $previous{$t} && !exists $skip{$t}) {
        # Result $t is a new one.
        push @filesAdded,"$currentTest{$t} $currentProduct{$t}";
        if ($currentError{$t}) {
            push @allErrors,"$currentTest{$t} $currentMachine{$t} $currentError{$t}";
            if ($currentError{$t} =~ m/Timeout/) {
                push @timeOutStarted,"$currentTest{$t} $currentProduct{$t} $currentMachine{$t} $currentError{$t}";
            } else {
                push @brokePrevious,"$currentTest{$t} $currentProduct{$t} $currentMachine{$t} $currentError{$t}";
            }
        }
    }
    my $p=$currentProduct{$t};
    $p =~ s/ pdfwrite//;
    $p =~ s/ ps2write//;
    $p =~ s/ xpswrite//;
    if ($products =~ m/$p/) {
        if ($currentProduct{$t} =~ m/pdfwrite/) {
            $pdfwriteTestCount++;
        } elsif ($currentProduct{$t} =~ m/ps2write/) {
            $ps2writeTestCount++;
        } elsif ($currentProduct{$t} =~ m/xpswrite/) {
            $xpswriteTestCount++;
        } else {
            $notPdfwriteTestCount++;
            $gsTestCount++ if ($currentProduct{$t} eq "gs");;
            $pclTestCount++ if ($currentProduct{$t} eq "pcl");;
            $xpsTestCount++ if ($currentProduct{$t} eq "xps");;
            $gpdfTestCount++ if ($currentProduct{$t} eq "gpdf");;
            $gpdlTestCount++ if ($currentProduct{$t} eq "gpdl");;
        }
    }
}

if ($elapsedTime==0 || $elapsedTime==1) {
} else {
    print "ran ".($pdfwriteTestCount+$ps2writeTestCount+$xpswriteTestCount+$notPdfwriteTestCount)." tests in $elapsedTime seconds on $machineCount nodes\n\n";
}

print "Headlines:\n";
if (exists $seen{'mupdf'} || exists $seen{'mujstest'}) {
    my $p;
    my $count=$archiveCount{'mupdf'}+$archiveCount{'mujstest'};
    $p='mupdf' if (exists $seen{'mupdf'});
    $p='mujstest' if (exists $seen{'mujstest'});
    print "".scalar(@differencePrevious)."\t(+$count swallowed)\t$notPdfwriteTestCount\t$p tests\n";
} else {
    print "".scalar(@differencePreviousGS)."\t(+".$archiveCount{'gs'}." swallowed)\t$gsTestCount\tGhostscript non-pdfwrite/ps2write/xpswrite tests\n";
    print "".scalar(@differencePreviousPCL)."\t(+".$archiveCount{'pcl'}." swallowed)\t$pclTestCount\tGhostPCL non-pdfwrite/ps2write/xpswrite tests\n";
    print "".scalar(@differencePreviousXPS)."\t(+".$archiveCount{'xps'}." swallowed)\t$pclTestCount\tGhostXPS non-pdfwrite/ps2write/xpswrite tests\n";
    print "".scalar(@differencePreviousGPDF)."\t(+".$archiveCount{'gpdf'}." swallowed)\t$gpdfTestCount\tGhostPDF non-pdfwrite/ps2write/xpswrite tests\n";
    print "".scalar(@differencePreviousGPDL)."\t(+".$archiveCount{'gpdl'}." swallowed)\t$gpdlTestCount\tGhostPDL non-pdfwrite/ps2write/xpswrite tests\n";
    my $count=$archiveCount{'gs pdfwrite'}+$archiveCount{'pcl pdfwrite'}+$archiveCount{'xps pdfwrite'};
    print "".scalar(@differencePreviousPdfwrite)."\t(+$count swallowed)\t$pdfwriteTestCount\tpdfwrite tests\n";
    print "".scalar(@differencePreviousPs2write)."\t(+".$archiveCount{'gs ps2write'}." swallowed)\t$ps2writeTestCount\tps2write tests\n";
    print "".scalar(@differencePreviousXpswrite)."\t(+".$archiveCount{'gs xpswrite'}." swallowed)\t$xpswriteTestCount\txpswrite tests\n";
}
print "\n";
print "[Files where the md5 differed from expected, but matched one seen in the previous $previousValues runs are \"swallowed\" as they are assumed to be indeterminisms. These are listed separately below.]\n\n";

my $numCurrentErrors = scalar(@allErrors);
my $numPreviousErrors = scalar(@allPreviousErrors);
print "$numCurrentErrors errors (".scalar(@brokePrevious)." new, ".scalar(@timeOutStarted)." new timeouts, ".scalar(@repairedPrevious)." fixed, ".scalar(@timeOutStopped)." fixed timeouts). Was $numPreviousErrors.\n\n";

#print Dumper(\%archiveCount);
my $count=$archiveCount{'gs'}+$archiveCount{'pcl'}+$archiveCount{'xps'};

if (@brokePrevious) {
    print "The following ".scalar(@brokePrevious)." regression file(s) have started producing errors:\n";
    while(my $t=shift @brokePrevious) {
        print "  $t\n";
    }
    print "\n";
}

if (exists $seen{'mupdf'} || exists $seen{'mujstest'}) {
    my $p;
    $count=$archiveCount{'mupdf'}+$archiveCount{'mujstest'};
    $p='mupdf' if (exists $seen{'mupdf'});
    $p='mujstest' if (exists $seen{'mujstest'});
    if (@differencePrevious) {
        print "Differences in ".scalar(@differencePrevious)." (+$count swallowed) of $notPdfwriteTestCount $p tests:\n";
        while(my $t=shift @differencePrevious) {
            print "  $t\n";
            push @baselineUpdateNeeded,$t;
        }
        print "\n";
    } else {
        print "No differences (+$count swallowed) in $notPdfwriteTestCount $p tests\n\n";
    }
} else {
    $count=$archiveCount{'gs'};
    print "Differences in ".scalar(@differencePreviousGS)." (+$count swallowed) of $gsTestCount Ghostscript non-pdfwrite/ps2write/xpswrite tests:\n";
    while(my $t=shift @differencePreviousGS) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'pcl'};
    print "Differences in ".scalar(@differencePreviousPCL)." (+$count swallowed) of $pclTestCount GhostPCL non-pdfwrite/ps2write/xpswrite tests:\n";
    while(my $t=shift @differencePreviousPCL) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'xps'};
    print "Differences in ".scalar(@differencePreviousXPS)." (+$count swallowed) of $xpsTestCount GhostXPS non-pdfwrite/ps2write/xpswrite tests:\n";
    while(my $t=shift @differencePreviousXPS) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'gpdf'};
    print "Differences in ".scalar(@differencePreviousGPDF)." (+$count swallowed) of $gpdfTestCount GhostPDF non-pdfwrite/ps2write/xpswrite tests:\n";
    while(my $t=shift @differencePreviousGPDF) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'gpdl'};
    print "Differences in ".scalar(@differencePreviousGPDL)." (+$count swallowed) of $gpdlTestCount GhostPDL non-pdfwrite/ps2write/xpswrite tests:\n";
    while(my $t=shift @differencePreviousGPDL) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'gs pdfwrite'}+$archiveCount{'pcl pdfwrite'}+$archiveCount{'xps pdfwrite'};
    print "Differences in ".scalar(@differencePreviousPdfwrite)." (+$count swallowed) of $pdfwriteTestCount pdfwrite tests:\n";
    while(my $t=shift @differencePreviousPdfwrite) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'gs ps2write'};
    print "Differences in ".scalar(@differencePreviousPs2write)." (+$count swallowed) of $ps2writeTestCount ps2write tests:\n";
    while(my $t=shift @differencePreviousPs2write) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";

    $count=$archiveCount{'gs xpswrite'};
    print "Differences in ".scalar(@differencePreviousXpswrite)." (+$count swallowed) of $xpswriteTestCount xpswrite tests:\n";
    while(my $t=shift @differencePreviousXpswrite) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";
}

if (@repairedPrevious) {
    print "The following ".scalar(@repairedPrevious)." regression file(s) have stopped producing errors:\n";
    while(my $t=shift @repairedPrevious) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";
}

if (@timeOutStarted) {
    print "The following ".scalar(@timeOutStarted)." regression file(s) have started timing out:\n";
    while(my $t=shift @timeOutStarted) {
        print "  $t\n";
    }
    print "\n";
}

if (@timeOutStopped) {
    print "The following ".scalar(@timeOutStopped)." regression file(s) have stopped timing out:\n";
    while(my $t=shift @timeOutStopped) {
        print "  $t\n";
        push @baselineUpdateNeeded,$t;
    }
    print "\n";
}

$first=1;
foreach my $t (sort keys %currentSize) {
#print "$currentSize{$t} $previousSize{$t}\n";
    if (exists $previousSize{$t} && $currentSize{$t}>0 && $previousSize{$t}>0 && ($currentSize{$t}-$previousSize{$t})/$previousSize{$t}>=0.05) {
        if ($first) {
            print "\nThe following files had a 5% or greater increase in intermediate pdf/ps/xps file size:\n";
            $first=0;
        }
        print "  $t $previousSize{$t} $currentSize{$t}\n";
    }
}
print "\n" if (!$first);



if (!$skipMissing || $skipMissing eq "false" || $skipMissing eq "0") {

    if (@filesRemoved) {
        print "The following ".scalar(@filesRemoved)." regression file(s) have been removed:\n";
        while(my $t=shift @filesRemoved) {
            print "  $t\n";
        }
        print "\n";
    }

    if (@filesAdded) {
        print "The following ".scalar(@filesAdded)." regression file(s) have been added:\n";
        while(my $t=shift @filesAdded) {
            print "  $t\n";
            push @baselineUpdateNeeded,$t;
        }
        print "\n";
    }

    $first=1;
    foreach my $t (sort keys %current) {
        if ($t =~ m/(.+\.)1$/) {
            $t2=$1.'0';
            if (exists $current{$t2}) {
                if ($current{$t} ne 0 && $current{$t2} ne 0 && $current{$t} ne $current{$t2} && (!exists $previous{$t} || !exists $previous{$t2} || $previous{$t} eq $previous{$t2})) {
                    if ($first) {
                        print "\nThe following files are showing a new mismatch between banded and page mode:\n";
                        $first=0;
                    }
                    print "  $currentTest{$t} $currentProduct{$t}\n";
                }
            }
        }
    }
    print "\n" if (!$first);

    # open(F,">>baselineupdateneeded.lst");
    # while(my $t=shift @baselineUpdateNeeded) {
    #     my @a=split ' ',$t;
    #     $a[0] =~ s/\//__/g;
    #     print F "$a[0]\n";
    # }
    # close(F);
}


if (@archiveMatch) {
    print "-------------------------------------------------------------------------------------------------------\n\n";
    print "The following ".scalar(@archiveMatch)." regression file(s) had differences but matched at least once in the previous $previousValues runs:\n";
    while(my $t=shift @archiveMatch) {
        print "  $t\n";
    }
    print "\n";
}


if ($reportAllErrors) {
    print "-------------------------------------------------------------------------------------------------------\n\n";
    if (@allErrors) {
        print "The following ".scalar(@allErrors)." regression file(s) are producing errors:\n";
        while(my $t=shift @allErrors) {
            print "  $t\n";
        }
        print "\n";
    }
}

unlink($current);
unlink($previous);

if ($debug) {
    close(F9);
}
