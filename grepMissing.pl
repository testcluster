#!/usr/bin/perl

use strict;
use warnings;

my $tempFile=shift;
my $currentFile=shift || die "usage: grepMissing.pl <tempFile> <currentFile>";

my %files;

open(F,"<$tempFile") || die "file $tempFile does not exist";
while (<F>) {
  chomp;
  my @a=split '\t';
  $files{$a[0]}=1;
}
close(F);

open(F,"<$currentFile") || die "file $currentFile does not exist";
while (<F>) {
  chomp;
  my @a=split '\t';
  print "$_\n" if (!exists $files{$a[0]});
}
close(F);



