#!/usr/bin/perl

use strict;
use warnings;

# send a heartbeat signal back to the clustermaster, if the
# clustermaster returns the string 'true' then there is a
# job waiting

my $machine=shift;
my $file=shift;
my $message=shift;

# Typical cases:
#   file=undefined, message undefined
#   file="abort", message undefined
#   file="cap", message "w32,w64"
#   file="status", message "32 jobs etc"

$machine || die "usage: heartbeat.pl <machine> (file) (message)";

my $retry=10;

use IO::Socket;
my ($host, $port, $handle, $line);

$host="cluster.ghostscript.com";
$port=9001;
do {
  $handle = IO::Socket::INET->new(
    Proto     => "tcp",
    PeerAddr  => $host,
    PeerPort  => $port,
    Timeout   => 1);
} until ($handle || $retry--<=0);
if (!$handle) {
  exit 0;
# die "can't open connection";
}

if (!$file) {
    $file = "heartbeat";
}
if (!$message) {
    $message = "null";
}
print $handle "$machine.$file $message\n";

my $t="";
$t=<$handle>;
close($handle);

if ($t) {
    chomp $t ;
    if ($t eq 'yes') {
	exit 1;
    }
}

exit 0;
