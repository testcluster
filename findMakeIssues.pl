#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my $run=shift;

$run="" if (!$run);

my @f=<*.make>;

#print Dumper(\@f);

my $header=0;
foreach my $f (@f) {
  `grep -q "Clock skew detected" $f`;
  next if ($?==0);
  foreach my $p ('gs','pcl','xps') {
    my $start=$p."_make_start";
    my $end  =$p."_make_end";
    `grep -q $start $f`;
    if ($?==0) {
      `grep -A1 $start $f | grep -q $end`;
      my $s=$?;
#   print "$f $p $s\n";
      if ($s) {
        print "\nThere were parallel make issues with:\n" if ($header==0);
        $header=1;
        $f=~/(.*)\.make/;
        print "  $p on $1\n";
        print "    <http://ghostscript.com/regression/cgi-bin/clustermonitor.cgi?log=make&machine=$1&report=$run>\n";
      }
    }
  }
}
print "\n" if ($header);
